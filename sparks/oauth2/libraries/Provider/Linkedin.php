<?php

//@author name Shafiq
/**
 * Linkedin OAuth2 Provider
 *
 * @package    CodeIgniter/OAuth2
 * @category   Provider
 */
class OAuth2_Provider_Linkedin extends OAuth2_Provider {

    public $scopes = array('r_basicprofile r_emailaddress r_contactinfo r_fullprofile');
    public $responseType = 'json';
    public $fields = array('id', 'email-address', 'siteStandardProfileRequest', 'first-name', 'last-name', 'headline', 'location', 'industry', 'picture-url', 'public-profile-url');

    public function url_authorize() {
        return 'https://www.linkedin.com/uas/oauth2/authorization';
    }

    public function url_access_token() {
        return 'https://www.linkedin.com/uas/oauth2/accessToken';
    }

    public function get_user_info(OAuth2_Token_Access $token) {
        //$url = 'https://api.linkedin.com/v1/people/~?format=json&oauth2_access_token=' . $token;
        $url = 'https://api.linkedin.com/v1/people/~:(' . implode(",", $this->fields) . ')?format=json&oauth2_access_token=' . $token;
        $user = json_decode(file_get_contents($url));
        // Create a response from the request
        return array(
            'uid' => $user->id,
            'first_name' => $user->firstName,
            'last_name' => $user->lastName,
            'email' => isset($user->emailAddress) ? $user->emailAddress : null,
            'location' => isset($user->location->name) ? $user->location->name : null,
            'location_country_code' => isset($user->location->country->code) ? $user->location->country->code : null,
            'positions' => isset($user->headline) ? $user->headline : null,
            'industry' => isset($user->industry) ? $user->industry : null,
            'profile_url' => isset($user->publicProfileUrl) ? $user->publicProfileUrl : '',
            'urls' => isset($user->siteStandardProfileRequest->url) ? $user->siteStandardProfileRequest->url : '',
        );
    }

}
