<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "frontend/home";
$route['404_override'] = '';
/* * **************admin side adminuser Routing ********************** */
$route['auth/login'] = "frontend/auth";
$route['admin'] = 'admin/auth/login';
$route['dashboard'] = 'admin/dashboard';
$route['admin/list'] = 'admin/admin/index';
$route['admin/add'] = 'admin/admin/add';
$route['admin/edit/(:num)'] = 'admin/admin/edit/$1';
$route['admin/active/(:num)'] = 'admin/admin/activate/$1';
$route['admin/deactive/(:num)'] = 'admin/admin/deactivate/$1';
/**
 * Admin Group routing
 * */
$route['admin/groups'] = 'admin/admingroups';
$route['admin/groups/add'] = 'admin/admingroups/add';
$route['admin/groups/edit/(:num)'] = 'admin/admingroups/edit/$1';
$route['admin/groups/active/(:num)'] = 'admin/admingroups/activate/$1';
$route['admin/groups/deactive/(:num)'] = 'admin/admingroups/deactivate/$1';
$route['admin/groups/delete/(:num)'] = 'admin/admingroups/delete/$1';

/**
 * Admin Category routing
 * */
$route['admin/categories'] = 'admin/category';
$route['admin/categories/add'] = 'admin/category/add';
$route['admin/categories/edit/(:num)'] = 'admin/category/edit/$1';
$route['admin/categories/active/(:num)'] = 'admin/category/activate/$1';
$route['admin/categories/deactive/(:num)'] = 'admin/category/deactivate/$1';
$route['admin/categories/delete/(:num)'] = 'admin/category/delete/$1';

/**
 * Admin Attribute routing
 * */
$route['admin/attributes'] = 'admin/attribute';
$route['admin/attributes/add'] = 'admin/attribute/add';
$route['admin/attributes/edit/(:num)'] = 'admin/attribute/edit/$1';
$route['admin/attributes/active/(:num)'] = 'admin/attribute/activate/$1';
$route['admin/attributes/deactive/(:num)'] = 'admin/attribute/deactivate/$1';
$route['admin/attributes/delete/(:num)'] = 'admin/attribute/delete/$1';

/**
 * Admin Varitaion routing
 * */
$route['admin/variations'] = 'admin/variation';
$route['admin/variations/add'] = 'admin/variation/add';
$route['admin/variations/edit/(:num)'] = 'admin/variation/edit/$1';
$route['admin/variations/active/(:num)'] = 'admin/variation/activate/$1';
$route['admin/variations/deactive/(:num)'] = 'admin/variation/deactivate/$1';
$route['admin/variations/delete/(:num)'] = 'admin/variation/delete/$1';

/*
 * Admin Prducts routing
 */
$route['admin/product'] = 'admin/products';
$route['admin/product/add'] = 'admin/products/add';
$route['admin/product/edit/(:num)'] = 'admin/products/edit/$1';
$route['admin/product/active/(:num)'] = 'admin/products/activate/$1';
$route['admin/product/deactive/(:num)'] = 'admin/products/deactivate/$1';
$route['admin/product/delete/(:num)'] = 'admin/products/delete/$1';

$route['admin/product/variation'] = 'admin/ajax/variation/';
$route['admin/product/image'] = 'admin/ajax/uploadimage/';
$route['admin/product/design'] = 'admin/ajax/uploaddesign/';
$route['admin/product/details/(:num)'] = 'admin/ajax/productdetails/$1';










/*
 * Front end routing
 */

$route['home/advsearch'] = "frontend/home/advsearch";
$route['home/visualizer/(:num)'] = "frontend/home/visualizer/$1";
$route['catalogue/createpdf'] = "frontend/home/downloadpdf";
$route['catalogue/emailpdf'] = "frontend/home/emailpdf";
$route['product/ajaxdetails/(:num)'] = "frontend/products/index/$1";
$route['product/ajaxcatalog/(:num)'] = "frontend/products/addtocatalog/$1";
$route['product/ajaxcatalogdelete/(:num)'] = "frontend/products/removefromcatalog/$1";
$route['product/ajaxcatalogsave'] = "frontend/products/catalogsave";
$route['product/ajaxcatalognew'] = "frontend/products/catalognew";
$route['product/ajaxview'] = "frontend/products/ajaxview";
$route['products/getdetails'] = "frontend/products/imagedetails";
$route['product/storeval'] = "frontend/products/storeval";

/* End of file routes.php */
/* Location: ./application/config/routes.php */    
