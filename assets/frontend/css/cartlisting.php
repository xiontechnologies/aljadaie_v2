<?php
/*
 * @auther Shafiq
 */
$userdata = $this->session->userdata(SITE_NAME . '_user_data');
?>
<section>
    <div id="content-section"> 
        <?php //$this->load->view('frontend/layout/leftwidgets', $this->data); ?>
        <!-- Filter module -->

        <!-- End Filter module --> 
        <div class="order-table">
            <?php if ($this->cart->total_items()) { ?>
                <?php echo form_open('cart/update'); ?>
                <div class="confirm order cart-btn fl">
                    <input type ="submit" name="delete" value="Delete" class="org-btn default-btn">
                </div>

                <div class="confirm order cart-btn fr">
                    <div class="confirm order">
                        <span><?php echo form_submit('update', 'Update your Cart', 'class="default-btn"'); ?></span>
                        <a href="<?php echo base_url(); ?>" class="default-btn">Continue shopping </a>
                        <a href="<?php echo base_url('checkout'); ?>" class="org-btn default-btn">Check out</a>
                    </div>
                </div>
                <div class="clr">&nbsp;</div>
                <table cellpadding="6" cellspacing="1" style="width:100%" border="0" class="table-module">
                    <tr>
                        <th><?php echo form_checkbox("", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                        <th>Product Name</th>
                        <th>SKU</th>
                        <th>Colorcode</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>  
                        <th style="text-align:right">Sub-Total</th>
                        <th>Action</th>
                    </tr>
                    <?php $i = 1; ?>
                    <?php foreach ($this->cart->contents() as $items) { ?>
                        <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
                        <tr>
                            <td><?php echo (form_checkbox("option[]", $items['rowid'], '', 'class="case"')); ?></td>
                            <td><?php echo $items['name']; ?></td>
                            <td><?php echo $items['options']['sku']; ?></td>
                            <td><?php echo $items['options']['colorcode']; ?></td>
                            <td><img src="<?php echo base_url(); ?>/<?php echo PRODUCTIMAGE_PATH; ?>/thumb/<?php echo $items['options']['file_name']; ?>" height="50" width="50"></td>
                            <td style="text-align: center"><?php echo form_input(array('name' => $i . '[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'class' => 'qty', 'size' => '5')); ?></td>
                            <td><?php echo $this->cart->format_number($items['price']); ?></td>
                            <td style="text-align:right">$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
                            <td>
                                <a href="<?php echo base_url('cart/delete') . '/' . $items['id']; ?>" alt="Delete" title="Delete" class="btn-mini">
                                    <span class="delete-btn">Delete</span></a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php } ?>
                </table>
                <div class="total-price fr">
                    <ul class="maketabs">
                        <li>Total</li>
                        <li>$<?php
                            if (isset($totalamount) && !empty($totalamount))
                                echo $totalamount;
                            else
                                echo $this->cart->format_number($this->cart->total());
                            ?>
                        </li>
                    </ul>
                </div>


                <?php
//                $coupon = array(
//                    'name' => 'coupon',
//                    'id' => 'coupon',
//                );
//                echo form_label('Coupon: ', 'coupon');
//                echo form_input($coupon);
                ?>

                <!--                <a href="javascript:void(0);" class='apply-coupon'>Apply Coupon</a>-->

                <div class="clr">&nbsp;</div>
                <div class="fr cart-btn">
                    <div class="confirm order">
                        <span><?php echo form_submit('update', 'Update your Cart', 'class="default-btn"'); ?></span>
                        <a href="<?php echo base_url(); ?>" class="default-btn">Continue shopping </a>
                        <a href="<?php echo base_url('checkout'); ?>" class="org-btn default-btn">Check out</a>
                    </div>
                </div>
                <div class="clr">&nbsp;</div>
            </div>
            <?php echo form_close(); ?>
        <?php }else { ?>
        <div class="cart-empty"><div>Cart is empty</div>
            <a href="<?php echo base_url(); ?>" class="default-btn org-btn">Continue shopping </a>
        </div>
        <?php } ?>
    </div>

    <!-- Right widgets -->
    <?php //$this->load->view('frontend/layout/rightwidgets', $this->data); ?>
    <!-- end widgets --> 
    <!-- Popup -->
    <div id="dialog" class="popup-module"></div>
    <!-- end Popup --> 
</div>
</section>
<input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
<input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
<input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>