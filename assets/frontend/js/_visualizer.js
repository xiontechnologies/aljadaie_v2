$(function() {
    $('.close-button').hide();
	
	/* commented by Urvisha 18th oct 
    $('#slides').slidesjs({
        width: 940,
        height: 528,
        navigation: {
            effect: "fade"
        },
        effect: {
            fade: {
                speed: 400
            }
        }
    });*/
    var id;
    $(".currency-symbol").text('SAR');
    /*
     * tabindexing to auto
     */
    $("#search input").keyup(function() {
        if ($(this).attr('maxlength') == $(this).val().length) {
            $(this).next('input').focus();
        }
    });

    tooltipcall();
    $("#accordion").accordion();
    $("#format").buttonset();
    $("#search-widgt ul").buttonset();
    $("#tabs").tabs();
    share();
    /*
     * For fabric sliding
     */
    $(document).on("click", "#select-fabric, .select-fabric #close-pop", function() {
        if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
            $(".select-fabric").toggle('blind', {
                direction: 'left',
                mode: 'show'
            }, 500);
        } else {
            $(".select-fabric").toggle('blind', {
                direction: 'right',
                mode: 'show'
            }, 500);
        }
        $(".select-style").hide();
    });
    $(document).on("submit", "#search", function(event) {
        event.preventDefault();
        //        var value = $("#searchfield").val();
        var postData = $(this).serializeArray();
        //        alert(formdata)
        //        $("#filtersearch").val(value);
        $(".visualizer").removeClass('display-none');
        $(".style-icon").parents('li').attr('id', 'fabric-style');
        $(".slider-module").remove();
        $(".steps-module").remove();
        $(".advance-search-module").hide();
        //        $("#filter-module").remove();
        $("#preloader").remove();
        $(".search-filter").after('<img id="preloader" src="assets/images/loading.gif" alt="" title="" class="img" />');
        $(".right-widget .select-fabric").toggle();
        $('.change-view').html('');
        if ($("#canvas").attr('src') == '#') {
            reset(($('.select-style .listing .img').first().attr('data')));
            load_scene_coords();
            share();
        }
        tabsoption();
        if (products('topsearch', '', 0, postData)) {
            $("#preloader").remove();
        }
    });
    $(document).on("change", "#search-widgt input[type=checkbox]", function() {
        //$("#search-widgt input[type=checkbox]").on('change',function(){
        $("#preloader").remove();
        $(".search-filter").after('<img id="preloader" src="assets/images/loading.gif" alt="" title="" class="img" />');
        var data = new Array();
        $.each($("#search-widgt input[type=checkbox]:checked"), function(index, value) {
            data.push(value.value);
        });
        $('.change-view').html('');
        if ($("#canvas").attr('src') == '#') {
            reset(($('.select-style .listing .img').first().attr('data')));
            load_scene_coords();
            share();
        }
        tabsoption();
        if (products('filter', data, 0, $("#filtersearch").val())) {
            $("#preloader").remove();
        }
    });
    $(document).on("submit", "#advsearch", function(event) {
        event.preventDefault();
        var data = new Array();
        $.each($("#advsearch input[type=checkbox]:checked"), function(index, value) {
            $('#rightwigdth' + value.value).attr('checked', true);
            $('#rightwigdth' + value.value).button("refresh")
            data.push(value.value);
        });
        $(".visualizer").removeClass('display-none');
        $(".style-icon").parents('li').attr('id', 'fabric-style');
        $(".slider-module").remove();
        $(".steps-module").remove();
        $(".advance-search-module").hide();
        //        $("#filter-module").remove();
        $("#preloader").remove();
        $(".search-filter").after('<img id="preloader" src="assets/images/loading.gif" alt="" title="" class="img" />');
        $(".right-widget .select-fabric").toggle();
        $('.change-view').html('');
        tabsoption();
        if (products('advancesearch', data, 0, '')) {
            $("#preloader").remove();
        }
        return false;
    });

    $('.change-view').scroll(function() {
	
	
        totalheight = parseInt($(this)[0].scrollHeight);
        scrollposition = parseInt($(this).scrollTop()) + parseInt($(this).innerHeight());
        if (scrollposition == totalheight) {
            /*First check its filter result or not
             *if yes then show result for filter only
             *else
             *calling ajax for more data
             */
            var pagenumber = $("#page").html();
            var totalrecords = $("#total").html();
            if (parseInt(pagenumber) < parseInt(totalrecords)) {
                var data = new Array();
                $.each($("#search-widgt input[type=checkbox]:checked"), function(index, value) {
                    data.push(value.value);
                });
                $(this).after('<div class="listing-preloader"> <img src="assets/images/loading.gif" alt="" title="" class="img" /></div>')
                if (products('scroll', data, $(".pagintation").val(), $("#filtersearch").val())) {
                    $(".listing-preloader").remove();
                }
            }
        }
    });
    function textfilter() {
        $("#preloader").remove();
        var data = new Array();
        $.each($("#search-widgt input[type=checkbox]:checked"), function(index, value) {
            data.push(value.value);
        });
        $(".search-filter").after('<img id="preloader" src="assets/images/loading.gif" alt="" title="" class="img" />');
        $('.change-view').html('');
        if (products('filter', data, 0, $("#filtersearch").val())) {
            $("#preloader").remove();
        }
    }
    /*
     * For advance filter reset 
     */
    $(document).ready('click', '#resetfilter', function() {
        //        $("#filtersearch").val('');
        $.each($(".filter-text input[type=checkbox]"), function(index, value) {
            $('#rightwigdth' + value.value).attr('checked', false);
            $('#rightwigdth' + value.value).button("refresh");
        });
    });
    $(document).on("click", '.refresh-btn', function() {
        $("#filtersearch").val('');
        $.each($("#search-widgt input[type=checkbox]"), function(index, value) {
            $('#rightwigdth' + value.value).attr('checked', false);
            $('#rightwigdth' + value.value).button("refresh");
        });
        $("#preloader").remove();
        $(".search-filter").after('<img id="preloader" src="assets/images/loading.gif" alt="" title="" class="img" />');
        $('.change-view').html('');
        if (products('filter', '', 0, '')) {
            $("#preloader").remove();
        }
    });
    $(document).on("keyup", "#filtersearch", function() {
        clearTimeout($.data(this, 'timer'));
        $('.change-view').html('');
        /*
         *If enter is press
         */
        //         if (e.keyCode == 13)
        //textfilter(true);
        //else
        //$(this).data('timer', setTimeout(textfilter, 500));
        var wait = setTimeout(textfilter, 500);
        $(this).data('timer', wait);
    });
    /*
     *Used for advance search 
     *filter
     *and scroller lazy script
     */
    function products(type, data, pagination, val) {
        $("#preloader").remove();
        $(".change-view").after('<img id="preloader" src="' + site_url + 'assets/images/loading.gif" alt="" title="" class="img">');
        $.ajax({
            'url': site_url + "product/ajaxview/",
            'dataType': 'json',
            'data': {
                'variation': data,
                'type': type,
                'pagintation': pagination,
                'val': val
            },
            success: function(data) {
                share();
                $(".pagintation").val(data.offset);
                $("#page").html(data.page);
                $("#total").html(data.total);
                if (type == 'filter') {
                    $('.change-view').html('');
                }
                $('.change-view').append(data.view);
                $("#preloader").remove();
                $('.storyboard').attr('id', 'save-visulizer');
                tooltipcall();
            }
        });
        return true;
    }
    $(document).on("click", "#fabric-style, .select-style .listing .img, .select-style #close-pop", function() {
        //$( "#fabric-style, .select-style #close-pop" ).on('click',function() {
        $(".select-style").toggle('blind', {
            direction: 'left',
            mode: 'show'
        }, 500);
        $(".select-fabric").hide();
    });
    /*For dailog box (popup)
     */
    $(document).on("click", ".info-icon, .color-icon", function() {
        id = $(this).attr('data');
        $("#dialog").dialog("open");
    });
    $("#dialog").dialog({
        autoOpen: false,
        height: 'auto',
        width: 930,
        resizable: false,
        hide: "slide",
        draggable: false,
        position: ['top', 20],
        modal: true,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "blind",
            duration: 1000
        },
        dialogClass: 'dialog',
        close: function(event, ui) {
            $('#dailog').html('');
            $(".zoomContainer").remove();
        },
        open: function(event, ui) {
            $(".zoomContainer").remove();
            if (id) {
                $("#dialog").html("<img class='preloader' src='" + site_url + "/assets/images/loading.gif'/>");
                $.ajax({
                    'url': site_url + "product/ajaxdetails/" + id,
                    success: function(data) {
                        $(".preloader").remove();
                        $("#dialog").html(data);
                    }
                });
            }
        }
    });
    /***********/
    $(document).on('click', ".catalogue-article .delete-icon", function() {
        id = $(this).attr('data');
        ele = $(this);
        $("#grid-fabric ul li .catlg-icon").filter(function() {
            if ($(this).attr('data') == id) {
                $(this).append('<a title="Add to Catalogue" href="javascript:void(0)">&nbsp;</a>');
                $(this).removeClass('selected');
            }
        });
        $.ajax({
            'url': site_url + 'product/ajaxcatalogdelete/' + id,
            success: function(data) {
                count = ele.parents('ul').find('li').size();
                count = parseInt(count) - 1;
                $(".catlg-property span.blue-color").text(count);
                if (count) {
                    ele.parents('li').remove();
                } else {
                    $('.catnotavail').remove();
                    ele.parents('ul').remove();
                    $(".next.browse.right,.prev.browse.left,.cataloge-options").hide();
                    var html = '<p class=catnotavail> No Product available in Catalogue </p>';
                    $('#tabs-2 .scroll-container').html(html);
                }
            }
        });
    });

    //     $(document).on('click', ".tab-storyboard .delete-icon", function() {
    //        id = $(this).attr('data');
    //        ele = $(this);
    //        $("#storyboard ul li .catlg-icon").filter(function() {
    //            if ($(this).attr('data') == id) {
    //                $(this).append('<a title="Add to Catalogue" href="javascript:void(0)">&nbsp;</a>');
    //                $(this).removeClass('selected');
    //            }
    //        });
    //        $.ajax({
    //            'url': site_url + 'product/ajaxcatalogdelete/' + id,
    //            success: function(data) {
    //                count = ele.parents('ul').find('li').size();
    //                count = parseInt(count) - 1;
    //                $(".catlg-property span.blue-color").text(count);
    //                if (count) {
    //                    ele.parents('li').remove();
    //                } else {
    //                    $('.catnotavail').remove();
    //                    ele.parents('ul').remove();
    //                    $(".next.browse.right,.prev.browse.left,.cataloge-options").hide();
    //                    var html = '<p class=catnotavail> No Product available in Catalogue </p>';
    //                    $('#tabs-2 .scroll-container').html(html);
    //                }
    //            }
    //        });
    //    });
    /********/
    $(document).on("click", ".next.browse.right", function() {
        $('.next.browse.right').attr('disabled', 'true');
        var btn = $(this);
        parentid = $(this).parents('.panel').attr('id');
        //$(".next.browse.right").on('click',function () {
        var size = $("#" + parentid + ' .ftr-slider .listing-table.lsit-view').find('li').size();
        var liwidth = $("#" + parentid + ' .ftr-slider .listing-table.lsit-view').find('li').width();
        var containerwidth = $("#" + parentid + " .scroll-container").width();
        var position = $("#" + parentid + ' .ftr-slider .listing-table.lsit-view').position();
        var liOuterwidth = $("#" + parentid + ' .ftr-slider .listing-table.lsit-view').find('li').outerWidth(true);
        if ((Math.abs(position.left)) < (((size - 1) * liwidth))) {
            $("#" + parentid + " .ftr-slider .listing-table.lsit-view").animate({
                left: "-=" + liOuterwidth + "px"
            }, 100, function() {
                btn.removeAttr('disabled');
            });
        }
    });
    $(document).on("click", ".prev.browse.left", function() {
        $(this).attr('disabled', 'true');
        parentid = $(this).parents('.panel').attr('id');
        var btn = $(this);
        //$(".prev.browse.left").on('click',function () {
        var size = $("#" + parentid + ' .ftr-slider .listing-table.lsit-view').find('li').size();
        var liwidth = $("#" + parentid + ' .ftr-slider .listing-table.lsit-view').find('li').width();
        var containerwidth = $("#" + parentid + " .scroll-container").width();
        var position = $("#" + parentid + ' .ftr-slider .listing-table.lsit-view').position();
        var liOuterwidth = $("#" + parentid + ' .ftr-slider .listing-table.lsit-view').find('li').outerWidth(true);
        if ((Math.floor(Math.abs(position.left))) > 0) {
            $("#" + parentid + " .ftr-slider .listing-table.lsit-view").animate({
                left: "+=" + liOuterwidth + "px"
            }, 100, function() {
                btn.removeAttr('disabled');
            });
        }
    });
    /***********/
    $(document).on("click", '.create-catalogue', function() {
        var ele = $(this);
        $.ajax({
            'url': site_url + "product/ajaxcatalognew",
            success: function(data) {
                $('.catnotavail').remove();
                ele.parents().find('.cataloge-options').nextAll().find('#tabs-2.scroll-container ul').remove()
                $("#tabs-2 .next.browse.right,#tabs-2 .prev.browse.left,#tabs-2 .cataloge-options").hide();
                var html = '<p class=catnotavail> No Product available in Catalogue </p>';
                $('#tabs-2 .scroll-container').html(html);
                $("#tabs-2 .cataloge-options").html('');
                var input = '<h3>CATALOGUE TITLE</h3><input id="catelogue-title" class="catelogue-title" type="text" placeholder="Catalogue Title" value="" name="cateloguetitle">' +
                        '<a class="catelogue-option-btn save-catalogue" href="javascript:void(0)">SAVE CATALOGUE</a>' +
                        '<a href="javascript:void(0)" class="catelogue-option-btn import-catalogue">IMPORT CATALOGUE</a>';
                $("#tabs-2 .cataloge-options").html(input);
                $("#tabs-2 .catlg-property span.blue-color").text(0);
                var cataloguespan = $("#tabs-2 .change-view").find('span.catlg-icon.brd-none.selected');
                cataloguespan.html('<a href="javascript:void(0)" class="orangetip_right_center">&nbsp;</a>');
                cataloguespan.removeClass();
                cataloguespan.addClass('catlg-icon orangetip_right_center');
            }
        });
    });
    $(document).on("click", ".save-catalogue", function() {
        //$(".save-catalogue").on('click',function(){
        if ($("#catelogue-title").val() == '') {
            dailogmessage("Please choose name for your catalogue");
        } else {
            var value = $("#catelogue-title").val();
            $.ajax({
                'url': site_url + "product/ajaxcatalogsave",
                'data': {
                    val: value
                },
                success: function(data) {
                    if (data == 1) {
                        dailogmessage("Catalogue saved in my account");
                        $('.catnotavail').remove();
                        //                        ele.parents('ul').remove(); 
                        //$(".next.browse.right,.prev.browse.left,.cataloge-options").hide();
                        var html = '<p class=catnotavail> Catalogue ' + value + ' save to my account </p>';
                        $("#catelogue-title").remove();
                        $(".catelogue-option-btn.save-catalogue").before('<b><label id="catelogue-title-save">' + value + '</label></b><a class="catelogue-option-btn create-catalogue" href="javascript:void(0)">CREATE NEW CATALOGUE</a>');
                        $(".catelogue-option-btn.save-catalogue").remove();
                        //$('.scroll-container').html(html);
                    } else {
                        //                        dailogmessage("sorry we can not save your catalogue at this time");
                        dailogmessage("Please Login First");
                    }
                }
            });
        }
    });
    /*************/
    /*
     * For barcode and catalogue
     */
    $(document).on("change", "#barcode-new", function(e) {
        e.preventDefault();
        var barcode = $("#barcode-new").val();
        barcode = barcode.replace('-', '');
        if (barcode != '' || $("#barcode-new").val().length != 0) {
            $.ajax({
                'url': site_url + "product/searchBySku/",
                'dataType': 'json',
                'data': {
                    'barcode': barcode
                },
                success: function(data) {
                    aftersuccess(data, data.id);
                    $("#barcode-new").val('');
                }
            });
        }
    });
    $(document).on("click", ".catlg-icon", function() {
        element = $(this);
        if ($(this).hasClass('selected')) {
           //commeneted by urvisha dailogmessage("Already added to catalogue");
        } else {
            id = $(this).attr('data');
            $.ajax({
                'url': site_url + "product/ajaxcatalog/" + id,
                'dataType': 'json',
                success: function(data) {
                    aftersuccess(data, element);
                }
            });
        }
    });
    function aftersuccess(data, element) {
        if (data == '') {
            dailogmessage("Already added");
        } else if (data.result) {
            dailogmessage("Product not found");
        } else if ($("#tabs-2 .ftr-slider").find('.catnotavail').length) {
            $('.catnotavail').remove();
            $("#tabs-2 .next.browse.right,#tabs-2 .prev.browse.left,#tabs-2 .cataloge-options").show();
            $('#tabs-2 .scroll-container').append(' <ul class="maketabs listing-table lsit-view"></ul>');
            $("#tabs-2 .scroll-container ul").append(data.view);
            catIconChange(element);
            $("#tabs-2 .catlg-property span.blue-color").text(($('#tabs-2 .ftr-slider').find('ul li').size()));
            tooltipcall();
        } else {
            $("#tabs-2 .ftr-slider ul li:first").before(data.view);
            catIconChange(element);
            $("#tabs-2 .catlg-property span.blue-color").text(($('#tabs-2 .ftr-slider').find('ul li').size()));
            tooltipcall();
        }
    }
    function catIconChange(element) {
        if ($.isNumeric(element) && typeof (element) == "number") {
            chatalogueIconProduct(element);
        } else if ($.isEmptyObject(element.parent('.info'))) {
            element.children('a').remove();
            element.addClass('selected');
        } else if (element.parent().is(".info")) {
            element.children('a').remove();
            element.addClass('selected');
        } else if (element.parent().is('li')) {
            popupAddChange(element);
        } else if (element.parent('li')) {
            popupAddChange(element);
        }
    }
    function popupAddChange(element) {
	    
        // commenetd by urvisha var html = '<span class="deactive">Added to catalogue</span>';
		var html = '';
        element.parent().html(html);
        chatalogueIconProduct(element.attr('data'));
        element.remove();
    }
    function chatalogueIconProduct(id) {
        $("#grid-fabric ul li .catlg-icon").filter(function() {
            if ($(this).attr('data') == id) {
                $(this).children('a').remove();
                $(this).addClass('selected');
            }
        });
    }
//    $(document).on("click", ".info-icon, .color-icon", function() {
//        id = $(this).attr('data');
//        $("#dialog").dialog("open");
//    });
    $('.select-fabr').resizable();
    /*
     * For grid view and list view
     */
    $(document).on("click", "#fabric-tabs .tab-btn.fabric-tabs li", function() {
        $("#fabric-tabs li.active").removeClass('active');
        $(this).addClass('active');
        $(".change-view").toggleClass('lsit-view');
    });
    $("#tabs").tabs({
        collapsible: true,
        active: false,
        disabled: [0]
    });
    $("#fabric-tabs-style").tabs({
        activate: function(event, ui) {
            $('#fabric-tabs-style').find('li.active').removeClass('active');
            $('.ui-state-active').addClass('active');
        }
    }
    );
    /*
     * Draggable fix width
     */
    $(document).on("click", ".select-fabric .box-drag", function() {
        $(".select-fabric").toggleClass("fabric-fullview");
    });
    $(document).on("click", ".select-style .box-drag", function() {
        $(".select-style").toggleClass("style-fullview");
    });
});
/*
 * download pdf and send pdf
 */
$(document).on("click", '.downloadpdf', function() {
    /*
     * Check catalogue products
     */
    if ($('.ftr-slider .listing-table.lsit-view').find('li').size()) {
        /*
         * ask for title and save
         */
        if ($("#catelogue-title-save").html()) {

            var url = site_url + "catalogue/createpdf";
            //                $(this).target = "_blank";
            window.open(url);
        } else {
            dailogmessage("Please first save catalogue");
        }
    } else {
        dailogmessage("Product not available in catalogue");
    }
});
$(document).on("click", '.email_pdf', function(e) {
    var id = $(this).attr('data');
    var url = site_url + "frontend/home/email/" + id;
    var email = $("#email"),
            subject = $("#subject"),
            allFields = $([]).add(email).add(subject);
    $("#dialogmodel").load(url, function() {
        $("#dialogmodel").dialog({
            height: 'auto',
            width: 500,
            resizable: false,
            modal: true,
            hide: "slide",
            draggable: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "blind",
                duration: 1000
            },
            dialogClass: 'dialog',
            buttons: {
                "Submit": function() {
                    var bValid = true;
                    allFields.removeClass("ui-state-error");
                    bValid = bValid && checkSubject($("#subject"), 'Please Enter Subject') && checkRegexp($("#email"), /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "Please enter valid email");
                    bValid = bValid && sendEmail($("#email"), $("#subject"), $("#message"));
                }
            }
        });
    });
});
$(document).on("click", '.emailpdf', function(e) {
    if ($('.ftr-slider .listing-table.lsit-view').find('li').size()) {
        /*
         * ask for title and save
         */
        if ($("#catelogue-title-save").html()) {
            var url = site_url + "frontend/home/email";
            var email = $("#email"),
                    subject = $("#subject"),
                    allFields = $([]).add(email).add(subject);
            $("#dialogmodel").load(url, function() {
                $("#dialogmodel").dialog({
                    height: 'auto',
                    width: 500,
                    resizable: false,
                    modal: true,
                    hide: "slide",
                    draggable: false,
                    show: {
                        effect: "blind",
                        duration: 1000
                    },
                    hide: {
                        effect: "blind",
                        duration: 1000
                    },
                    dialogClass: 'dialog',
                    buttons: {
                        "Submit": function() {
                            var bValid = true;
                            allFields.removeClass("ui-state-error");
                            bValid = bValid && checkSubject($("#subject"), 'Please Enter Subject') && checkRegexp($("#email"), /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "Please enter valid email");
                            bValid = bValid && sendEmail($("#email"), $("#subject"), $("#message"));
                        }
                    }
                });
            });
        } else {
            dailogmessage("Please first save catalogue");
        }
    } else {
        dailogmessage("Product not available in catalogue");
    }

});
function checkSubject(o, n) {
    if (o.val() == '') {
        o.addClass("ui-state-error");
        $("#validate-error").text(n).addClass("ui-state-highlight");
        setTimeout(function() {
            $("#validate-error").removeClass("ui-state-highlight", 1500);
        }, 500);
        return false;
    } else {
        return true;
    }
}
function checkRegexp(o, regexp, n) {
    if (!(regexp.test(o.val()))) {
        o.addClass("ui-state-error");
        $("#validate-error").text(n).addClass("ui-state-highlight");
        setTimeout(function() {
            $("#validate-error").removeClass("ui-state-highlight", 1500);
        }, 500);
        return false;
    } else {
        return true;
    }
}
function sendEmail(o, n, m) {
    var demail = o.val();
    var sub = n.val();
    var msg = m.val();
    $('.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only').after("sending email please wait...")
    $('.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only').hide();
    $.ajax({
        url: site_url + 'catalogue/emailpdf',
        cache: false,
        dataType: 'json',
        data: {
            demail: demail,
            sub: sub,
            msg: msg
        },
        type: 'GET',
        success: function(data) {
            if (data == '') {
                setTimeout(function() {
                    $("#validate-error").removeClass("ui-state-highlight", 1500);
                }, 500);
                return false;
            } else {
                close_popup();
            }
        }
    });
}
function dailogmessage(message) {
    $("#dialogmodel").html(message);
    $("#dialogmodel").dialog({
        height: 'auto',
        width: 400,
        resizable: false,
        hide: "slide",
        draggable: false,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "blind",
            duration: 1000
        },
        dialogClass: 'dialog',
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        }
    });
}
function close_popup() {
    $('#dialogmodel').dialog("close");
}
/*
 *Tooltip Function
 *
 */

function tooltipcall() {
    $('.bluetip').tooltipster({
        position: 'right',
        theme: '.tip_blue'
    });
    $('.greentip').tooltipster({
        position: 'right',
        theme: '.tip_green'
    });
    $('.orangetip').tooltipster({
        position: 'right',
        theme: '.tip_orange'
    });
    $('.tooltip').tooltipster({
        position: 'right'
    });
    $('.orangetip_left').tooltipster({
        position: 'top-left',
        theme: '.tip_orange'
    });
    $('.orangetip_right').tooltipster({
        position: 'top-center',
        theme: '.tip_orange'
    });
    $('.orangetip_right_center').tooltipster({
        position: 'top-right',
        theme: '.tip_orange'
    });
}

/*
 *All visualizer code here
 *
 */
var style = "grandsofa1";
var current_object = 0;
var object_count = 0;
var server = "http://vivek2.picarisplatform.com/picaris";
var qtypath = "&q=100";
var querystring = {};
var querytn = {};
var imgpath = "";
var fabric = "";
var surface = "";
var alt = "";
var imgwidth = "250";
var imgheight = "250";
var designpath = 'uploads/productimage/';
var fabricId = '';
var dpi = 72;
var storyboardIds = '';
function editStoryborad(style, canvasUrl, ids) {
    $('.storyboard').attr('id', 'save-visulizer');
    reset(style);
    load_scene_coords();
    storyboardIds = ids;
    //tn is array index
    //productthumbimagepath from loop
    var productthumbimagepath = "";
    var txt = "";
    for (var x in ids)
    {
        fabric = ids[x].name;
        fabricId = ids[x].id;
        thumbclick(x);
    }
}
function thumbsStoryBoard() {
    for (var x in storyboardIds)
    {
        tn = x;
        $("#tabs-1").children("div").eq(tn).find('.fabric-choose').remove();
        $("#tabs-1").children("div").eq(tn).append('<div class="fabric-choose" style="background-image:url(' + storyboardIds[x].name + ')"></div>');
    }
    storyboardIds = '';
}
$(document).on("click", ".storyboard", function(event) {
    event.preventDefault();
});
$(document).on("click", ".delete-icon-storyboard", function() {
    element = $(this);
    $.ajax({
        url: site_url + "storyboard/delete?id=" + $(this).attr('data'),
        dataType: "json",
        success: function(data) {
            if (data) {
                count = element.parents('ul').find('li').size();
                count = parseInt(count) - 1;
                $(".catlg-property span.blue-color").html(count);
                if (count) {
                    element.parents("li:first").remove();
                } else {
                    element.parents('ul').remove();
                    $(".storyboard-prev,.storyborad-property,.storyboard-next,.storyboard-options").hide();
                    $(".scroll-container.storyboard-div").html("");
                    $(".scroll-container.storyboard-div").html('<p class="storyborad-notavail"> No Story available </p>');
                }
                //                element.parents("li:first").remove();
            }
        }
    });
});
$(document).on("click", ".select-style .listing .img", function() {
    $("#canvaspreloader").show();
    reset($(this).attr('data'));
    load_scene_coords();
    share();
});
$(document).on("click", ".select-fabric .listing .img,.ftr-slider .img", function() {

    if ($("#canvas").attr('src') == '#') {
        reset(($('.select-style .listing .img').first().attr('data')));
        load_scene_coords();
        share();
    }
    alt = $(this).attr('alt');
    fabricId = $(this).attr('data');
    fabric = site_url + designpath + alt;
    backgroudImag = site_url + designpath + 'thumb/' + alt;
    //    $(".visualizer-module").css('background-image', 'url(' + backgroudImag + ')');
    //    $(".visualizer-module").css('padding-top', '0');
    //    $(".visualizer-module").css('background-repeat', 'repeat');
    $("body").css('background-image', 'url(' + backgroudImag + ')');
    $("body").css('padding-top', '0');
    $("body").css('background-repeat', 'repeat');
    $.ajax({
        url: site_url + "products/getdetails?imge=" + fabric,
        dataType: "json",
        success: function(data) {
            imgwidth = data[0];
            imgheight = data[1];
        }
    });
    if ($(".hidden").val() == '1') {
        $(".slider-module").remove();
        $(".steps-module").remove();
        //        $("#filter-module").remove();
        $("#fullscreen").removeClass('display-none');
        $("#select-style").parent().removeClass('display-none');
        $("#add-storyboard").parent().removeClass('display-none');
        $("#save-visulizer").parent().removeClass('display-none');
        $(".tab-catalogue").show();
        $(".tab-surfaces").show();
        $(".tab-storyboard").show();
        $(".visualizer").removeClass('display-none');
        $(".style-icon").parents('li').attr('id', 'fabric-style');
        $(".storyboard").attr("id", 'save-visulizer');
        tabsoption();

        $("#tabs").tabs({
            active: 1
        });
        $("#tabs").tabs({
            active: 2
        });
        $("#tabs").tabs({
            active: 0
        });
    }
});
function reset(s) {
//    imgpath = fabric = surface = style = "";
    style = s;
    querystring = {};
    querytn = {};
    fabricId = '';
    var img = new Image();
    img.src = server + '/getimage.ashx?ft=1&fn=' + style + qtypath;
    $("#canvas").attr('src', img.src).load(function() {
        $("#canvaspreloader").hide();
    });
}
function thumbclick(tn) {
    console.log(fabric)
    if (fabric) {
        $("#tabs-1").children("div").eq(tn).find('.fabric-choose').remove();
        $("#tabs-1").children("div").eq(tn).append('<div class="fabric-choose" style="background-image:url(' + site_url + 'uploads/productimage/thumb/' + alt + ')"></div>');
        var temp = 'tn' + tn;
        var tw = 'tw' + tn;
        var th = 'th' + tn;
        querytn[tn] = fabricId;
        querystring[temp] = fabric;
        querystring[tw] = imgwidth * 25.4 / dpi;
        querystring[th] = imgheight * 25.4 / dpi;
        imgpath = server + '/getimage.ashx?ft=1&fn=' + style + '&' + $.param(querystring);
        createscreen();
    } else {
        display_activity('Please select Fabric first');
    }

}
function createscreen() {
    share();
    var img = new Image();
    $("#canvaspreloader").show();
    img.src = imgpath + qtypath + '&h=500';
    $("#canvas").attr('src', img.src).load(function() {
        $("#canvaspreloader").hide();
    });
}
function count_objects(coords) {
    var oa = [];
    for (var i = a_start_index; i <= a_last_index; i++)
        for (var x = 0; x < coords[i].length; x++)
            if (coords[i][x][1] != -1 && ($.inArray(coords[i][x][1], oa) == -1))
                oa.push(coords[i][x][1]);
    return (oa.length);
}
function load_scene_coords() {
    $(".surfaces-article").html('<img src="' + site_url + 'assets/images/loading.gif" id="surfacepreloader"></img>');
    $.getScript(server + "/getcoords.ashx?fn=" + style + "&ot=3&w=600&h=540", function(data, textStatus, jqxhr) {
        if (a != undefined && a.length > 0) {
            var icount = 0;
            while (icount < 2000)
                if (a[icount++] != undefined)
                    break;
            a_start_index = icount - 1;
            a_last_index = a.length - 1;
            object_count = count_objects(a);
            load_object_thumbnails();
        }
    });
}
function load_object_thumbnails() {
    for (i = 0; i < object_count; i++) {
        $(".surfaces-article").append('<div style="width:96px; height: auto; border: 0px solid grey; float: left;"><div onclick=thumbclick(' + i + ') style="width: 80px; height: 80px; border: 1px solid grey; margin: 0 0px 10px 10px; background-color: white; background-image: url(' + server + '/getimage.ashx?ft=4&on=' + i + '&fn=' + style + '&h=80&w=80&bg=white&q=50);" ></div></div>');
        $("#surfacepreloader").remove()
    }
    if (storyboardIds)
        thumbsStoryBoard();
}
function display_activity(text) {
    $("#canvas").before("<div class='canvasmsg'><span class='tags'> " + text + "</span></div>");
    $(".canvasmsg").fadeOut(7000);
//$(".canvasmsg").fadeIn().delay(7000).fadeOut("slow");
}
$(document).on('click', "#canvas", function(e) {
    var x = e.pageX - $("#canvas").offset().left;
    var y = e.pageY - Math.round($("#canvas").offset().top);
    var obj_no = 8;
    if (a == undefined) {
        display_activity("Coordinates for image unavailable.");
        return;
    }
    if (a[y] == undefined) {
        display_activity("Error getting coordinates.");
        return;
    }

    if (y >= a_start_index && y < a_last_index) {
        for (var c = 0; c < a[y].length; c++)
            if ((x >= a[y][c][0]))
                obj_no = a[y][c][1];
        thumbclick(obj_no);
    }
});
//$(document).on('click', '#fullscreen', function() {
//    if (imgpath) {
//        html = '<img src="' + imgpath + qtypath + '&w=' + $(document).innerWidth() + '" alt="" />';
//    } else {
//        html = '<img src="' + $("#canvas").attr('src') + '" alt="" />';
//    }
//
//    $("#dialogmodel").html(html);
//    $("#dialogmodel").css('text-align', 'center');
//    $("#dialogmodel").dialog({
//        height: 'auto',
//        width: 'auto',
//        resizable: false,
//        hide: "slide",
//        draggable: false,
//        create: function(event, ui) {
//            $(this).parents(".ui-dialog").css("padding", 0);
//            $(this).parents(".ui-dialog").find(".ui-dialog-content").css("padding", 0);
//        },
//        show: {
//            effect: "blind",
//            duration: 1000
//        },
//        position: ['top', 10],
//        hide: {
//            effect: "blind",
//            duration: 1000
//        },
//        dialogClass: 'dialog',
//        modal: true
//    });
//});
$(document).on('click', "#save-visulizer", function(event) {
    event.preventDefault();
    var url = $(this).attr('href');
    $.ajax({
        url: url,
        dataType: "json",
        data: {
            'fabrics': querytn,
            'style': style,
            'url': $("#canvas").attr('src')
        },
        success: function(data) {
            if (data.success) {
                if ($('.tab-storyboard').find('.storyborad-notavail').length) {
                    $('.storyborad-notavail').remove();
                    $(".storyboard-next").show();
                    $(".storyboard-prev").show();
                    $(".storyborad-property").show();
                    $(".storyboard-options").show();
                    $('.storyboard-div').append('<ul class="maketabs listing-table lsit-view storyboard-ul"></ul>');
                    $(".storyboard-ul").html('');
                    $(".storyboard-ul").html(data.view);
                    tooltipcall();
                } else {
                    $(".storyboard-ul").html('');
                    $(".storyboard-ul").html(data.view);
                }
                count = $('.storyboard-ul').find('li').size();
                count = parseInt(count) - 1;
                $(".story-property span.blue-color").html(count);
            } else if (data.error) {
                dailogmessage(data.error);
            }
        }
    });
});
/*
 * visulizer js end here
 * 
 */
function toggleChecks(obj) {
    $('.case').prop('checked', obj.checked);
}
function tabsoption() {
    $("#tabs").tabs("option", "active", 0);
    $("#tabs").tabs("option", "disabled", false);
}
function share() {

/* commented by Urvisha 18th oct 
    stWidget.addEntry({
        "service": "sharethis",
        'onhover': false,
        "element": document.getElementById('button_1'),
        "url": site_url,
        "title": site_url,
        "type": "small",
        "text": "ShareThis",
        "image": $('.hidden').val() ? site_url + 'assets/frontend/images/logo.png' : ((imgpath != '') ? (imgpath + qtypath) : $('#canvas').attr('src')),
        "summary": "this is description"
    });*/
}
function checkValid() {
    if ($(".case:checked").length > 0) {
        document.getElementById('frmDelete').submit();
    } else {
        alert("You didn't select any row");
    }
}
$(document).on("click", ".apply-coupon", function() {
    var coupon = document.getElementById('coupon');
    $.ajax({
        url: site_url + "coupon",
        dataType: "json",
        'data': {
            'coupon': coupon.value
        },
        success: function(data) {
            if (data.error) {
                $(".coupan-message").remove();
                $(".coupan-error").remove();
                $(".fprice").remove();
                $(".discount-total").remove();
                $('.apply-coupon').after('<span class="coupan-error">' + data.message + '</span>')
            } else if (data.success) {

                var html = '<ul class = "maketabs discount-total"><li>Discount</li><li>$ -' + data.discount + '</li></ul>';
                $('.total-price ul').after(html);
                var html = '<ul class = "maketabs fprice"><li>Final Price</li><li>$' + data.finalprice + '</li></ul>';
                $('.total-price ul:last').after(html);
                $('.coupon').hide();
            }
            //            $('.maketabs li:last-child').add(data);
        }
    });
});
$(document).on('change', '.check-add', function(event) {
    $('.block-shipping').toggle(function() {
        if ($('.check-add').is(":checked")) {
            $(".order-table").addClass('billing-add');
            $(".order-table .login-box").addClass('fullwid');

            $(".block-shipping :input").removeAttr('required');
        } else {
            $(".order-table").removeClass('billing-add');
            $(".order-table .login-box").removeClass('fullwid');
            $(".block-shipping :input").attr('required', 'required');
        }
    });
});
/*
 * Cart js
 */
$(document).on('click', '.add-to-cart,.add-to-cart-details', function(event) {
    event.preventDefault();
    element = $(this);
    //    var flag = $(this).hasClass('add-to-cart');
    //    if (flag) {
    //        if ($(this).hasClass('selected')) {
    //            dailogmessage("Already Added to cart");
    //            qty = 0;
    //        } else {
    //            qty = 1;
    //        }
    //    }
    if ($(this).hasClass('selected'))
        dailogmessage("Already Added to cart");
    else
        //qty = $('#qty').val();
        qty = element.next('.qty-container').find('input').val();
    var flag = element.hasClass('add-to-cart');
    if (qty == '') {
        dailogmessage("Qty cannot be empty");
    } else if (qty == 0) {
        dailogmessage("Qty cannot be zero");
    } else if (qty != 0 || qty != '') {
        $.ajax({
            url: element.attr('href'),
            dataType: "json",
            data: {
                'qty': qty
            },
            success: function(data) {
                if (data.result == 1 && !flag) {
                    var html = '<a href="' + site_url + 'cartlist" class="deactive">View Cart</a>';
                    element.after(html);
                    element.remove();
                    $('#qty').remove();
                    $('#qty-container').remove();
                    $('.cart-count').text(parseInt($('.cart-count').text()) + parseInt(1));
                    var changeIcon = true;
                } else if (data.result == 1 && flag) {
                    //                    element.children('a').remove();
                    element.addClass('selected');
                    //                    var html = '<a href="' + site_url + 'cartlist" class="deactive">View Cart</a>';
                    //                    element.after(html);
                    //                    element.remove();
                    $('.cart-count').text(parseInt($('.cart-count').text()) + parseInt(1));
                    var changeIcon = true;
                } else {
                    var changeIcon = false;
                    dailogmessage("Sorry we can not add this product");
                }
                if (changeIcon && flag) {
                    element.next('.qty-container').hide();
                } else if (changeIcon && !flag) {
                    popupElement = $(".change-view span.cart-parent-span[data='" + element.attr('data') + "']");
                    popupElement.html('');
                    popupElement.removeClass('brd-none cart-parent-span');
                    popupElement.addClass('cart-icon');
                    popupElement.removeAttr('title');
                    popupElement.addClass('selected');
                }
            }
        });
    } else {

    }
});
$(document).on("click", "#selectestyle", function() {
    id = $(this).attr('data');
    alert(id);
    $.ajax({
        'url': site_url + "home/select_style_images/" + id,
        success: function(data) {
            //                console.log(data);
            if (data != '') {
                $('#style-grid').html(data);
                //                    $('.list-style-view').html(data); 
            }
        }
    });
});
//$(document).on("click", "#selectestyle123", function() {
//    id = $(this).attr('data');
//    
//    $.ajax({
//        'url': site_url + "home/select_style_images_list/" + id,
//        success: function(data) {
//            //                console.log(data);
//            if (data != '') {
//                //                    $('#style-grid-view').html(data); 
//                $('#style-grid').html(data);
//            }
//        }
//    });
//});

$(document).on("click", "#forgotpassword", function() {
    $('.login-form').hide();
    $(".order-table").addClass('billing-add');
    $(".order-table .login-box").addClass('fullwid');
    $('.social').hide();
    $('.forgot-pass').show();
});

$(document).on('click', '.fav-icon', function(event) {
    event.preventDefault();
    element = $(this);
    var product_id = $(this).attr('data');
    $.ajax({
        url: 'favorities',
        dataType: "json",
        data: {
            'product_id': product_id
        },
        success: function(data) {
            if (data.id) {
                var html = '<a href="' + site_url + 'favoriteslist" class="deactive">View Favorite</a>';
                element.after(html);
                element.remove();
            } else {
                dailogmessage(data.message);
            }
        }
    });

});

$(document).on("click", ".save-storyboard", function() {
    if ($("#storyboard-title").val() == '') {
        dailogmessage("Please choose name for your Storyboard");
    } else {
        var value = $("#storyboard-title").val();
        $.ajax({
            'url': site_url + "product/storyboardsave",
            'data': {
                val: value
            },
            success: function(data) {
                if (data == 1) {
                    dailogmessage("Storyboard saved in my account");
                    $('.storyboardnotavail').remove();
                    //                        ele.parents('ul').remove(); 
                    //$(".next.browse.right,.prev.browse.left,.cataloge-options").hide();
                    var html = '<p class=storyboardnotavail> Storyboard ' + value + ' save to my account </p>';
                    $("#storyboard-title").remove();
                    $(".catelogue-option-btn.save-storyboard").before('<b><label id="storyboard-title-save">' + value + '</label></b><a class="catelogue-option-btn create-storyboard" href="javascript:void(0)">CREATE NEW Storyboard</a>');
                    $(".catelogue-option-btn.save-storyboard").remove();
                    //$('.scroll-container').html(html);
                } else {
                    //                        dailogmessage("sorry we can not save your catalogue at this time");
                    dailogmessage("Please Login First");
                }
            }
        });
    }
});

$(document).on("click", '.create-storyboard', function() {
    var ele = $(this);
    $.ajax({
        'url': site_url + "product/ajaxnewstoryboard",
        success: function(data) {
            $('.storyboardnotavail').remove();
            ele.parents().find('#tabs-3 .cataloge-options').nextAll().find('#tabs-3 .scroll-container ul').remove()
            $("#tabs-3 .next.browse.right,#tabs-3 .prev.browse.left,#tabs-3 .cataloge-options").hide();
            var html = '<p class=catnotavail> No Product available in Storyboard </p>';
            $('#tabs-3 .scroll-container').html(html);
            $("#tabs-3 .cataloge-options").html('');
            var input = '<h3>Storyboard TITLE</h3><input id="storyboard-title" class="storyboard-title" type="text" placeholder="Storyboard Title" value="" name="storyboardtitle">' +
                    '<a class="catelogue-option-btn save-storyboard" href="javascript:void(0)">SAVE Storyboard</a>';
            $(".storyboard-options").html(input);
            //            $(".storyborad-property span.blue-color").text(o);
            var cataloguespan = $("#tabs-3 .change-view").find('span.catlg-icon.brd-none.selected');
            cataloguespan.html('<a href="javascript:void(o)" class="orangetip_right_center">&nbsp;</a>');
            cataloguespan.removeClass();
            cataloguespan.addClass('catlg-icon orangetip_right_center');
        }
    });
});

$(document).on('click', '.downloadpdf-storyboard', function() {
    if ($('.ftr-slider .listing-table.lsit-view').find('li').size()) {
        /*
         * ask for title and save
         */
        if ($("#storyboard-title-save").html()) {
            var url = site_url + "storyboard/createpdf";
            //                $(this).target = "_blank";
            window.open(url);
        } else {
            dailogmessage("Please first save storyboard");
        }
    } else {
        dailogmessage("Product not available in storyboard");
    }
});
$(document).on("click", ".advflt-togle", function() {
    $(".advance-search-module").toggle('blind', {
        mode: 'show'
    }, 500);
});
$(document).on("change", '.select-lng', function() {
    window.location.reload();
});
$("document").on("#imgContainer .close-button", 'click', function() {
    $('#imgContainer').hide();
    $('#positionButtonDiv').hide();
    $('.mask').hide();
})
//Zoom script:
$(document).on('click', '#fullscreen', function() {
    $("#imageFullScreen").attr('src', $('#canvas').attr('src')).load(function() {
        $("#canvaspreloader").hide();
        $('#imgContainer').show();
        $('#positionButtonDiv').show();
        $('.mask').show();
        zoom();
    });

})
$(document).on('click', '.mask', function() {
    $('#imgContainer').hide();
    $('#positionButtonDiv').hide();
    $('.mask').hide();
})

//key event when press esc then close popup
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $('#imgContainer').hide();
        $('#positionButtonDiv').hide();
        $('.mask').hide();
    }   // esc
});

function zoom() {
    $('#imageFullScreen').smartZoom("destroy");
//    $('#imageFullScreen').smartZoom('Reset') ;
    $('#imageFullScreen').smartZoom({
        'containerClass': 'zoomableContainer'
    });

    $('#topPositionMap,#leftPositionMap,#rightPositionMap,#bottomPositionMap').bind("click", moveButtonClickHandler);
    $('#zoomInButton,#zoomOutButton').bind("click", zoomButtonClickHandler);

    function zoomButtonClickHandler(e) {
        var scaleToAdd = 0.8;
        console.log("zoombutton" + e.target.id)
        if (e.target.id == 'zoomOutButton')
            scaleToAdd = -scaleToAdd;
        $('#imageFullScreen').smartZoom('zoom', scaleToAdd);
    }

    function moveButtonClickHandler(e) {
        var pixelsToMoveOnX = 0;
        var pixelsToMoveOnY = 0;
        console.log("zoombuttonmove" + e.target.id)
        switch (e.target.id) {
            case "leftPositionMap":
                pixelsToMoveOnX = 50;
                break;
            case "rightPositionMap":
                pixelsToMoveOnX = -50;
                break;
            case "topPositionMap":
                pixelsToMoveOnY = 50;
                break;
            case "bottomPositionMap":
                pixelsToMoveOnY = -50;
                break;
        }
        $('#imageFullScreen').smartZoom('pan', pixelsToMoveOnX, pixelsToMoveOnY);
    }

}