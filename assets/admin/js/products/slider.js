/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $("#userfile").on("change",function(){
        var formData = false;
        //Use FormData to send the files
        var formData = new FormData();
        $.each(this.files,function(i,file){
            formData.append(i,file); 
        });
        formData.append('csrf_test_name', $('input[name=csrf_test_name]').val());
        $(".sliderimage").remove();
        $("#userfile").after("<img class='preloader' src='"+site_url+"/assets/images/loading.gif' />");
        $.ajax({
            'url':site_url+"admin/ajax/slider_image",
            type: 'POST',
            processData: false,
            dataType: 'json',
            //contentType:'multipart/form-data',
            contentType:false,
            data:formData,
            success: function(data) {
                $(".preloader").remove();
                if(data.error){
                    $("#userfile").val('');
                    $("#userfile").after("<p class='error'>"+ data.error+"</p>");
                }
                if(data.message){
                    $.each(data.message,function(i,m){
                        $(".sliderimage").remove();
                        $("#sliderimage").val(m.id);
                        $("#userfile").after("<img class='sliderimage' src=" + m.path + " height='100px' width='100px'/>");
                    });
                }
            }
        }); 
    });
});