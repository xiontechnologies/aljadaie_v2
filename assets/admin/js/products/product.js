/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $("#design").on("change",function(){
        var formData = false;
        //Use FormData to send the files
        var formData = new FormData();
        $.each(this.files,function(i,file){
            formData.append(i,file); 
        });
        formData.append('csrf_test_name', $('input[name=csrf_test_name]').val());
        //append the files to the formData object
        //if you are using multiple attribute you would loop through 
        //but for this example i will skip that
        $(".designimage").remove();
        $("#design").after("<img class='preloader' src='"+site_url+"/assets/images/loading.gif'/>");
        $.ajax({
            'url':site_url+"admin/product/design",
            type: 'POST',
            processData: false,
            dataType: 'json',
            //contentType:'multipart/form-data',
            contentType:false,
            data:formData,
            success: function(data) {
                $(".preloader").remove();
                if(data.error){
                    $("#design").val('');
                    $("#design").after("<p class='error'>"+ data.error+"</p>");
                }
                if(data.message){
                    $.each(data.message,function(i,m){
                        $(".designimage").remove();
                        $("#productdesign").val(m.id);
                        $("#design").after("<img class='designimage' src=" + m.path + " height='100px' width='100px'/>");
                        
                    });
                }
            }
        }); 
    });
    $("#image").on("change",function(){
        var formData = false;
        //Use FormData to send the files
        var formData = new FormData();
        $.each(this.files,function(i,file){
            formData.append(i,file); 
        });
        formData.append('csrf_test_name', $('input[name=csrf_test_name]').val());
        $(".uploadedimage").remove();
        $("#image").after("<img class='preloader' src='"+site_url+"/assets/images/loading.gif' />");
        $.ajax({
            'url':site_url+"admin/product/image",
            type: 'POST',
            processData: false,
            dataType: 'json',
            //contentType:'multipart/form-data',
            contentType:false,
            data:formData,
            success: function(data) {
                $(".preloader").remove();
                if(data.error){
                    $("#image").val('');
                    $("#image").after("<p class='error'>"+ data.error+"</p>");
                }
                if(data.message){
                    $.each(data.message,function(i,m){
                        $(".uploadedimage").remove();
                        $("#productimage").val(m.id);
                        $("#image").after("<img class='uploadedimage' src=" + m.path + " height='100px' width='100px'/>");
                    });
                }
            }
        }); 
    });
    $(".addnew").on('click',function(){
        var element = $(this);
        var data = element.attr('data').split("|");
        var id = data[0];
        var name =data[1];
        html = "You are going to add new variation in <b>"+name+"</b></br>\n\
        <input type='text' value='' class='mini' id='newvariation' name='newvariation' />";
        $("#dialog").html(html);
        $( "#dialog" ).dialog({
            resizable: false,
            width:600,
            modal: true,
            buttons: {
                "Add": function() {
                    $(".error").remove();
                    var value = $("#newvariation").val();
                    if(value){
                        var dailogbox =$(this);
                        /**
                                     * calling ajax for adding new value
                                     */
                        $.ajax({
                            'url':site_url+"admin/product/variation",
                            'type': 'POST',
                            'data':{
                                'val':value,
                                'data':id,
                                'csrf_test_name': $('input[name=csrf_test_name]').val()
                            },
                            'dataType': 'json',
                            success:function(data){
                                if(data.error){
                                    $("#newvariation").after("<p class='error'>"+ data.error+ "</p>");
                                }else if(data.success){
                                    element.before(data.success);
                                    dailogbox.dialog( "close" );
                                }else{
                                    $("#newvariation").after("Sorry .Some thing wrong we are not able to add this variation try from variation module");
                                }
                            }
                        });
                    }else{
                        $("#newvariation").after("<p class='error'>Please enter value for variation</p>");
                    }
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });
});
         
