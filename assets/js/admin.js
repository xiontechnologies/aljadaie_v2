/********* For delete ******************/
function deleteAction($id,$controller){
    if(confirm("Are you sure !")){
        var url = "admin/"+$controller+"/delete/"+$id;           
        var xhr = $.get(site_url+url,
            function(data){
                alert('delete successfully..');
                window.location.reload(true);
            });
    }
}    
/**
 * function activeAction
 * @param $id = id of user
 * @param $controller = name of controller for url
 * @param $action = name of action for url 
 * @descp Used active and deactive the records in database
 * @where view page datatable list
 **/
function activeAction($id,$controller,$action){
    if($action=='activate'){
        var xhr = $.get(site_url+"admin/"+$controller+"/activate/"+$id,
            function(data){
                window.location.reload(true);
            });
    }else if($action=='deactivate'){
        var xhr = $.get(site_url+"admin/"+$controller+"/deactivate/"+$id,
            function(data){
                window.location.reload(true);
            });
    }else{
        alert("something is wrong");
    }
        
    
} 

$(document).on('click','#downloadreport',function(){
    var from_date = $('#fromdate').val();
    var to_date = $('#todate').val();
    var username = $('#user_name').val();
    window.open(site_url+'admin/reports/export?from_date='+from_date+'&to_date='+to_date+'&username='+username);
    
});
/**
 * function toggleCheck 
 * @param object of html
 * @descp Used for check all checkboxes
 * in the datatable listing on view page
 * @where view page datatable list
 **/

function toggleChecks(obj){
    $('.case').prop('checked', obj.checked);
}

function toggleChecksgroup(obj){
    $('.casegroup').prop('checked', obj.checked);
}

/**
 * function checkValid 
 * @param 
 * @retun form submit action else error alert
 * @descp checking any checkbox selected on not
 * @where view page in datatable list
 **/
function checkValid(){
    if($(".case:checked").length >0){
        document.getElementById('frmDelete').submit();
    }else{
        alert("You didn't select any row");
    }
}

/******************* For addrelation box in add medmber and edit member************************/
$(document).ready(function(){
    $('#relationship').change(function(){
        var code = $(this).find('option:selected').val();
        if(code=='2' || code=='3' || code=='4'){
            $("#relwith").css('display','block');
        }else{
            $("#relwith").css('display','none');
        }
    });
    /**
     * @descp checking any th checkbox
     * will selected on not when td checkboxes
     * unchecked
     * @where view page in datatable list
     **/
    $(".case").on('click',function(){
        if($(".case").length == $(".case:checked").length) {
            $("#selectall").prop('checked', true);
            $("#selectall").attr("checked", "checked");
        } else {
            $("#selectall").prop('checked', false);
            $("#selectall").removeAttr("checked");
        }
    });
    $('#check:button').click(function(){
        var checked = !$(this).data('checked');
        $('input:checkbox').prop('checked', checked);
        $(this).val(checked ? 'uncheck all' : 'check all' )
        $(this).data('checked', checked);
    });
          
    /*************************************function for acticve deactive freind in freind list ***************************/
    $('.accept').on('click', function(e){
        e.preventDefault();
        var data = $(this).attr('data').split('|');
        var answer = confirm('Are you sure you want to accept "'+data[1]+'"'+' as a friend')
        if (answer){
            var getAccept =  $.ajax({
                url: site_url+"admin/friendAccept",
                cache: false,
                type: "POST",
                data: {
                    data : data[0]
                },
                success: function(html){
                    location.reload(); 
                }
            });
        }
    });
    $('.decline').on('click', function(e){
        e.preventDefault();
        var data = $(this).attr('data').split('|');
        var answer = confirm('Are you sure you want to deactive "'+data[1]+'"'+' as a friend')
        if (answer){
            var getAccept =  $.ajax({
                url: site_url+"admin/friendDecline",
                cache: false,
                type: "POST",
                data: {
                    data : data[0]
                },
                success: function(html){
                    location.reload(); 
                 
                }
            });
        }
    });
    /******************************* common message fadeout js ***********************/
    
    $('.message ').fadeOut(5000);
    

});
/******************** Function for counting length of string in text area *********************/
function countChar(val) {
    var len = val.value.length;
    if (len >= 100) {
        val.value = val.value.substring(0, 100);
    } else {
        $('#charNum').text(100 - len);
    }
};
function deleteRow(currentNode,className){
    var rowLength = $(className).length;
    if(rowLength>1){
        $(currentNode).parent().parent().remove();
    } 
}
$(document).on('click','.designname',function(){
    var source = $(this).attr('src');
    var name = $(this).attr('title');
    $(".designimage").remove();
    $("#design").after("<img class='preloader' src='"+site_url+"/assets/images/loading.gif'/>");
    $.ajax({
        url : site_url+'admin/products/adddesign',
        type : 'GET',
        dataType : 'json',
        data : {
            source : source,
            name : name
        },
        success : function(data){
            $(".preloader").remove();
            if(data.error){
                $("#design").val('');
                $("#design").after("<p class='error'>"+ data.error+"</p>");
            }
            if(data.message){
                $.each(data.message,function(i,m){
                    $(".designimage").remove();
                    $("#productdesign").val(m.id);
                    $("#design").after("<img class='designimage' src=" + m.path + " height='100px' width='100px'/>");
                    close_popup();  
                });
            } 
        }
    })
});
$(document).on('click','.imagename',function(){
    var source = $(this).attr('src');
    var name = $(this).attr('title');
    $(".uploadedimage").remove();
    $("#image").after("<img class='preloader' src='"+site_url+"/assets/images/loading.gif'/>");
    $.ajax({
        url : site_url+'admin/products/addimage',
        type : 'GET',
        dataType : 'json',
        data : {
            source : source,
            name : name
        },
        success : function(data){
            $(".preloader").remove();
            if(data.error){
                $("#image").val('');
                $("#image").after("<p class='error'>"+ data.error+"</p>");
            }
            if(data.message){
                $.each(data.message,function(i,m){
                    $(".uploadedimage").remove();
                    $("#productimage").val(m.id);
                    $("#image").after("<img class='uploadedimage' src=" + m.path + " height='100px' width='100px'/>");
                    close_popup();  
                });
            } 
        }
    })
});

function close_popup() {
    $('#dialog').dialog("close");
}
