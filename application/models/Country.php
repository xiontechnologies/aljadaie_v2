<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Country extends ActiveRecord\Model {

    static function find_assoc($params = FALSE, $select = TRUE) {
        if ($params)
            $module = self::find('all', $params);
        else
            $module = self::find('all');
        $out = array();
//        if ($select) {
//            $out[''] = "Select";
//        }
        foreach ($module as $grp)
            $out[$grp->countrycode] = $grp->countryname . "(" . $grp->countrycode . ")";
        return $out;
    }

}

?>
