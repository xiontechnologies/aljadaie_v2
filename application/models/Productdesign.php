<?php

/*
 * @auther Shafiq
 */

class Productdesign extends ActiveRecord\Model {

    static $belongs_to = array(
        array('product', 'conditions' => array('status=?', 1)),
        array('design'),
    );

}