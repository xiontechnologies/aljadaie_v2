<?php

/*
 * @auther Shafiq
 */

class Productphoto extends ActiveRecord\Model {

    static $belongs_to = array(
        array('product', 'conditions' => array('status=?', 1)),
        array('photo'),
    );

}