<?php

/*
 * @auther Shafiq
 */

class Section extends ActiveRecord\Model {

    static $has_many = array(
        array('product')
    );

    static function find_assoc($params = FALSE, $select = TRUE) {
        if ($params)
            $module = self::find('all', $params);
        else
            $module = self::find('all');
        $out = array();
        if ($select) {
            $out[''] = "Select";
        }
        foreach ($module as $grp)
            $out[$grp->id] = $grp->name;
        return $out;
    }

}