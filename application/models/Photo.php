<?php

/*
 * @auther Shafiq
 */

class Photo extends ActiveRecord\Model {

    static $has_many = array(
        array('productphoto'),
        array('productphoto'), array('photo', 'through' => 'userprofile'),
        array('userprofile', 'class' => 'Userprofile'),
        array('styleimage', 'class' => 'StyleImage')
    );

}