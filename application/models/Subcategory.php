<?php

/*
 * @auther Shafiq
 */

class Subcategory extends ActiveRecord\Model {

    static $belongs_to = array(
        array('category', 'conditions' => array('status = ? ', 1))
    );
    static $has_many = array(
        array('productvariation'),
        array('products', 'through' => 'productvariation','conditions'=>array('status=?',1))
    );

    static function find_assoc($params = FALSE, $select = TRUE) {
        if ($params)
            $module = self::find('all', $params);
        else
            $module = self::find('all');
        $out = array();
        if ($select) {
            $out['0'] = "Select";
        }
        foreach ($module as $grp)
            $out[$grp->id] = $grp->name;
        return $out;
    }

}