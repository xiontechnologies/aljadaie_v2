<?php

/*
 * @auther Shafiq
 */

class Productvariation extends ActiveRecord\Model {

    static $belongs_to = array(
        array('subcategory', 'conditions' => array('status = ? ', '1')),
        array('product', 'conditions' => array('status = ? ', '1'))
    );
    static $after_create = array('update_subcategory');
    static $after_destroy = array('update_subcategory');
    static $after_save = array('update_subcategory');
    static $after_update = array('update_subcategory');

    function update_subcategory() {
        $result = Productvariation::count(array('conditions' => array('subcategory_id = ? ', $this->subcategory_id), 'group' => 'subcategory_id'));
        $subcategory = Subcategory::find('first', array('conditions' => array('id = ? ', $this->subcategory_id)));
        $subcategory->productcount = $result;
        $subcategory->save();
    }

}
