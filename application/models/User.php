<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class User extends ActiveRecord\Model {

    static $has_one = array(
        array('facebook', 'class_name' => 'Socialnetwork', 'conditions' => array('provider = ?', 'facebook')),
        array('twitter', 'class_name' => 'Socialnetwork', 'conditions' => array('provider = ?', 'twitter')),
        array('google', 'class_name' => 'Socialnetwork', 'conditions' => array('provider = ?', 'google')),
        array('linkedin', 'class_name' => 'Socialnetwork', 'conditions' => array('provider = ?', 'linkedin')),
        array('user_profile', 'class' => 'Userprofile'),
        array('usercoupon', 'class' => 'Usercoupon'),
    );
    static $has_many = array(
        array('socialnetwork'),
        array('userprofile', 'class' => 'Userprofile'),
        array('photo', 'through' => 'userprofile'),
        array('membergroup', 'class' => 'User_Group'),
            //  array('membergroup', 'through' => 'usergroup','class_name'=>'User_Group', 'foreign_key' => 'group_id'),
    );

    static function find_assoc($params = '', $select = TRUE) {
        if ($params)
            $admingrp = self::find('all', $params);
        else
            $admingrp = self::find('all');
        $out = array();
        if ($select) {
            $out['0'] = "Select";
        }
        foreach ($admingrp as $grp)
            $out[$grp->id] = $grp->firstname;

        return $out;
    }

    function get_fullname() {
        if ($this->firstname != '' && $this->lastname != '') {
            $name = $this->firstname . ' ' . $this->lastname;
        } else {
            $name = $this->email;
        }
        return $name;
    }

}
