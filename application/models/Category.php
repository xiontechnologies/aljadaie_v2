<?php

/*
 * @auther Shafiq
 */

class Category extends ActiveRecord\Model {

    static $has_many = array(
        array('subcategory', 'conditions' => array('status = ? ', 1)),
        array('filtersubcategory', 'class' => 'Subcategory', 'conditions' => array('status = ? and showinfilter = ? ', 1, 1))
    );

    static function find_assoc($params = FALSE, $select = TRUE) {
        if ($params)
            $module = self::find('all', $params);
        else
            $module = self::find('all');
        $out = array();
        if ($select) {
            $out['0'] = "Select";
        }
        foreach ($module as $grp)
            $out[$grp->id] = $grp->name;
        return $out;
    }

}