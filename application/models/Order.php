<?php

/*
 * @auther Shafiq
 */

class Order extends ActiveRecord\Model {

    static $has_many = array(
        array('orderdetail', 'class' => 'Orderdetail'),
    );

}

function get_fullname() {
    if ($this->first_name != '' && $this->last_name != '') {
        $name = $this->first_name . ' ' . $this->last_name;
    } else {
        $name = $this->email;
    }
    return $name;
}