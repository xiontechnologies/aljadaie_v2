<?php

/*
 * @auther Shafiq
 */

class Catalogueproduct extends ActiveRecord\Model {

    static $belongs_to = array(
        array('product', 'conditions' => array('status =?', 1)),
        array('catalogue'),
    );

}