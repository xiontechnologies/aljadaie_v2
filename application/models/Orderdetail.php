<?php

/*
 * @auther Shafiq
 */

class Orderdetail extends ActiveRecord\Model {

    static $belongs_to = array(
        array('order'),
        array('product', 'class' => 'Product'),
    );

}
