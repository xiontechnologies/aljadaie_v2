<?php

/*
 * @auther Shafiq
 */

class Product extends ActiveRecord\Model {

    static $belongs_to = array(
        array('section', 'conditions' => array('status = ? ', 1)),
        array('catalogue', 'through' => 'catalogueproducts')
    );
    static $has_one = array(
        array('productphoto', 'class' => 'Productphoto'),
        array('productdesign'),
        array('photo', 'through' => 'productphoto'),
    );
    static $has_many = array(
        array('orderdetail', 'class' => 'Orderdetail'),
        array('productvariation'),
        array('design', 'through' => 'productdesign'),
    );

    function get_variation() {
        $temp = '';
        foreach ($this->productvariation as $k => $v) {
            $temp[] = $v->subcategory_id;
        }
        return $temp;
    }

    public function get_frv($user_id, $product_id) {
        $fav = Favorite::find('first', array('conditions' => array('user_id=? and product_id=?', $user_id, $product_id)));
        if ($fav)
            return TRUE;
    }

    public function get_colors() {
//        $str_split = str_split($this->sku, 1);
//        $str_split[0] . $str_split[1] . $str_split[2] . $str_split[3] . $str_split[4] . $str_split[5] . $str_split[6] . $str_split[7] . $str_split[8] . $str_split[9] . $str_split[10] . $str_split[11];
        $data = substr($this->sku, 0, -3);
        $where = array('concat(item_kind,item_definition,collection,product_design) like "%' . $data . '%" and id != ' . $this->id);
        $color = self::find('all', array('conditions' => $where));
        return $color;
    }

    public function get_collections() {
        $data = substr($this->sku, 12,2 );
        //echo '<pre>'; print_r($data);     die;
        //$where = array('concat(item_kind,item_definition,collection) like "%' . $data . '%" and id != ' . $this->id . ' GROUP BY product_design');
        $where = array('collection like "%' . $this->collection . '%" and id != ' . $this->id . ' and primary_color = "'.$data.'"  GROUP BY product_design');
        $collection = self::find('all', array('conditions' => $where));
        return $collection;
//        $collection = self::find('all', array('conditions' => array('collection = ? and id != ?', $this->collection, $this->id)));
//        return $collection;
    }

    public function get_thumimagepath() {
        if ($this->productphoto)
            return str_replace('/./', '/', base_url(PRODUCTIMAGE_PATH . '/thumb/' . $this->productphoto->photo->file_name));
        return false;
    }

    public function get_imagepath() {
        if ($this->productphoto)
            return str_replace('/./', '/', base_url(PRODUCTIMAGE_PATH . '/' . $this->productphoto->photo->file_name));
        return false;
    }

    public function get_designpath() {
        if ($this->design)
            return str_replace('/./', '/', base_url(PRODUCTDESIGN_PATH . '/' . str_replace('.JPG', '.jpg', strtoupper($this->design[0]->file_name))));
        return false;
    }

}
