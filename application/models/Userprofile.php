<?php

/*
 * @auther Shafiq
 */

class Userprofile extends ActiveRecord\Model {

    static $belongs_to = array(
        array('user'),
        array('photo'),
        );

    public function get_profileimage() {
        $imgData = array(
            'src' => ($this->photo->file_name != '') ? base_url(FRONTEND_USER_PROFILE_IMAGE . '/' . $this->photo->file_name) : base_url('uploads/admin/profileimages/noimage.png'),
            'alt' => $this->user->firstname . " " . $this->user->lastname,
            'width' => '50',
            'height' => '50',
            'title' => $this->user->firstname . " " . $this->user->lastname,
        );
        return $imgData;
    }

}
