<?php

/*
 * @auther Shafiq
 */

class Membergroup extends ActiveRecord\Model {

    static $has_many = array(
        array('membergroup', 'class' => 'User_Group', 'foreign_key' => 'group_id')
    );

    static function find_assoc($params = '', $select = TRUE) {
        if ($params)
            $admingrp = self::find('all', $params);
        else
            $admingrp = self::find('all');
        $out = array();
        if ($select) {
            $out['0'] = "Select";
        }
        foreach ($admingrp as $grp)
            $out[$grp->id] = $grp->group;

        return $out;
    }

}