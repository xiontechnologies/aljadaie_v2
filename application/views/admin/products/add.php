<script>
    $(document).ready(function() {
        var ajaxcall;
        $(".viewdesigns").on('click', function() {
            $("#dialog").dialog({
                height:400,
                close: function(event, ui) {
                    clearInterval(ajaxcall);
                    $('.offset').val('0');
                },
                open: function(event, ui) {
                    $("#dialog").html("<img class='preloader' src='" + site_url + "/assets/images/loading.gif'/>");
                    //                    ajaxcall = setInterval(function() {
                    offset = $('.offset').val();
                    $.ajax({
                        'url': site_url + "admin/products/viewdesigns?of="+offset,
                        'dataType':'json',
                        success: function(data) {
                            $(".preloader").remove();
                            $("#dialog").append(data.view);
                            $(".offset").val(data.offset);
                            if(data.error){
                                clearInterval(ajaxcall); 
                            }
                        }
                    });
                    //                    }, 5000);
                }
            });
            
        });
        $(".viewimages").on('click', function() {
            $("#dialog").dialog({
                height:400,
                close: function(event, ui) {
                    clearInterval(ajaxcall);
                    $('.offset').val('0');
                },
                open: function(event, ui) {
                    $("#dialog").html("<img class='preloader' src='" + site_url + "/assets/images/loading.gif'/>");
                    //                    ajaxcall = setInterval(function() {
                    offset = $('.offset').val();
                    $.ajax({
                        'url': site_url + "admin/products/viewimages?of="+offset,
                        'dataType':'json',
                        success: function(data) {
                            $(".preloader").remove();
                            $("#dialog").append(data.view);
                            $(".offset").val(data.offset);
                            if(data.error){
                                clearInterval(ajaxcall); 
                            }
                        }
                    });
                    //                    }, 5000);
                }
            });
        });
 
       
    });
</script>
<?php
$name = array(
    'name' => 'title',
    'id' => 'title',
    'value' => (isset($product->title) && !empty($product->title)) ? $product->title : '',
    'class' => form_error('title') ? 'mini fl' : 'mini'
);
$description = array(
    'name' => 'description',
    'id' => 'description',
    'value' => (isset($product->description) && !empty($product->description)) ? $product->description : '',
    'class' => form_error('description') ? 'mini fl' : 'mini'
);
$weight = array(
    'name' => 'weight',
    'id' => 'weight',
    'value' => (isset($product->weight) && !empty($product->weight)) ? $product->weight : '',
    'class' => form_error('weight') ? 'mini fl' : 'mini'
);
$shortdescription = array(
    'name' => 'shortdescription',
    'id' => 'shortdescription',
    'value' => (isset($product->shortdescription) && !empty($product->shortdescription)) ? $product->shortdescription : '',
    'class' => form_error('shortdescription') ? 'mini fl' : 'mini'
);
$sku = array(
    'name' => 'sku',
    'id' => 'sku',
    'value' => (isset($product->sku) && !empty($product->sku)) ? $product->sku : '',
    'class' => form_error('sku') ? 'mini fl' : 'mini'
);
$colorcode = array(
    'name' => 'colorcode',
    'id' => 'colorcode',
    'placeholder' => 'Color Code',
    'value' => (isset($product->colorcode) && !empty($product->colorcode)) ? $product->colorcode : '',
    'class' => form_error('colorcode') ? 'mini fl' : 'mini'
);
//$collection = array(
//    'name' => 'collection',
//    'id' => 'collection',
//    'value' => (isset($product->collection) && !empty($product->collection)) ? $product->collection : '',
//    'class' => form_error('collection') ? 'mini fl' : 'mini'
//);
$price = array(
    'name' => 'price',
    'id' => 'price',
    'value' => (isset($product->price) && !empty($product->price)) ? $product->price : '',
    'class' => form_error('price') ? 'mini fl' : 'mini'
);
$discountprice = array(
    'name' => 'discountprice',
    'id' => 'discountprice',
    'value' => (isset($product->discountprice) && !empty($product->discountprice)) ? $product->discountprice : '',
    'class' => form_error('colorcode') ? 'mini fl' : 'mini'
);
$stock = array(
    'name' => 'stock',
    'id' => 'stock',
    'value' => (isset($product->stock) && !empty($product->stock)) ? $product->stock : '',
    'class' => form_error('colorcode') ? 'mini fl' : 'mini'
);
$washcare = array(
    'name' => 'washcare',
    'id' => 'washcare',
    'value' => (isset($product->washcare) && !empty($product->washcare)) ? $product->washcare : '',
    'class' => form_error('washcare') ? 'mini fl' : 'mini'
);
$tags = array(
    'name' => 'tags',
    'id' => 'tags',
    'value' => (isset($product->tags) && !empty($product->tags)) ? $product->tags : '',
    'class' => form_error('tags') ? 'mini fl' : 'mini'
);
$metakeywords = array(
    'name' => 'metakeywords',
    'id' => 'metakeywords',
    'value' => (isset($product->metakeywords) && !empty($product->metakeywords)) ? $product->metakeywords : '',
    'class' => form_error('metakeywords') ? 'mini fl' : 'mini'
);
$image = array(
    'name' => 'image',
    'id' => 'image',
    'class' => form_error('metakeywords') ? 'mini fl' : 'mini'
);
$design = array(
    'name' => 'design',
    'id' => 'design',
    'class' => form_error('metakeywords') ? 'mini fl' : 'mini'
);

$active = (isset($postdata->status) && ($postdata->status == 1) ? TRUE : true);
$deactive = (isset($product->status)) && ($product->status == 0) ? TRUE : "";
$active = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '1',
    'checked' => $active,
    'class' => 'fancy-radio'
);
$deactive = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '0',
    'checked' => $deactive,
    'class' => 'fancy-radio'
);

$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
?>
<?php
if ($action == 'edit') {
    $hidden = array('id' => $product->id);
    echo form_open_multipart("admin/product/edit/$product->id", '', $hidden);
    ?> 
    <h2>Edit Product</h2>
    <?php
} else {
    echo form_open_multipart('admin/product/add');
    ?>
    <h2>Add Product</h2>
<?php } ?>
<div class="block ">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <table class="form">
        <tr>
            <td valign="top"> <?php echo form_label('Title : ', 'title', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($name); ?>
                <?php echo form_error('title', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Short Description : ', 'short description', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_textarea($shortdescription); ?>
                <?php echo form_error('shortdescription', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Description : ', 'description', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($description); ?>
                <?php echo form_error('description', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('SKU : ', 'sku', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($sku); ?>
                <?php echo form_error('sku', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                <?php echo form_input($colorcode); ?>
                <?php echo form_error('colorcode', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr> 
<!--        <tr>
            <td valign="top"> <?php // echo form_label('Collection : ', 'collection', array('class' => 'required')); ?></td>
            <td>
                <?php // echo form_input($collection); ?>
                <?php // echo form_error('collection', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>-->
        <tr>
            <td valign="top"> <?php echo form_label('Category : ', 'Category', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_dropdown('category', $section, (isset($product->category) && !empty($product->category)) ? $product->category : '', ''); ?>
                <?php echo form_error('category', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Price : ', 'Price', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($price); ?>
                <?php echo form_error('price', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Discounted price : ', 'Discountedprice'); ?></td>
            <td>
                <?php echo form_input($discountprice); ?>
                <?php echo form_error('discountprice', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Weight : ', 'weight'); ?></td>
            <td>
                <?php echo form_input($weight); ?>
                <?php echo form_error('weight', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Stock : ', 'Stock', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($stock); ?>
                <?php echo form_error('stock', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Wash care : ', 'washcare', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_textarea($washcare); ?>
                <?php echo form_error('washcare', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Tags : ', 'Tags'); ?></td>
            <td>
                <?php echo form_textarea($tags); ?>
                <?php echo form_error('tags', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Status : ', 'status', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_radio($active); ?>Active
                <?php echo form_radio($deactive); ?>Deactive
                <?php echo form_error('status', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('keywords : ', 'keywords', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($metakeywords); ?>
                <?php echo form_error('metakeywords', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Image : ', 'image', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_upload($image); ?>
                <?php echo form_error('productimage', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                <?php
                if (isset($product->productimage) && !empty($product->productimage)) {
                    $path = explode('|', $product->productimage);
                    ?>
                    <img class='uploadedimage' src="<?php echo base_url(PRODUCTIMAGE_PATH . '/' . $path[1]); ?>" height='100px' width='100px'/>
                <?php } ?>
                <a class="btn btn-black viewimages" style="float:right">Add from media</a>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Design : ', 'design', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_upload($design); ?>
                <?php echo form_error('productdesign', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                <?php
                if (isset($product->productdesign) && !empty($product->productdesign)) {
                    $path = explode('|', $product->productdesign);
                    ?>
                    <img class='designimage' src="<?php echo base_url(PRODUCTDESIGN_PATH . '/' . $path[1]); ?>" height='100px' width='100px'/>
                <?php } ?>
                <a class="btn btn-black viewdesigns" style="float:right">Add from media</a>
            </td>
        </tr>
        <?php foreach ($category as $key => $value) { ?>
            <tr>
                <td valign="top"> <?php echo form_label(ucfirst($value->name) . ' : ', $value->name); ?></td>
                <td>
                    <?php
                    foreach ($value->subcategory as $catk => $catv) {
                        $checked = '';
//                                if (isset($product->variation[$value->name]) && !empty($product->variation[$value->name]) && in_array($catv->id, $product->variation[$value->name])) {
//                                    $checked = TRUE;
//                                }
                        if (isset($product->variation) && !empty($product->variation) && in_array($catv->id, $product->variation)) {
                            $checked = TRUE;
                        }
                        ?>
                        <span>
                            <?php echo form_checkbox('variation[]', $catv->id, $checked) ?>
                            <?php echo ucfirst($catv->name); ?>
                        </span>
                    <?php } ?>
                    <?php
                    $button = array(
                        'name' => 'add new',
                        'data' => $value->id . "|" . ucfirst($value->name),
                        'class' => 'addnew',
                        'value' => 'Add New',
                        'type' => 'Button'
                    );
                    echo form_input($button);
                    echo form_error('variation[]', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>');
                    ?>
                </td>
            </tr>
        <?php } ?>
        <tr id="submit">
            <td><input type="hidden" name="productimage" value="<?php echo isset($product->productimage) && !empty($product->productimage) ? $product->productimage : '' ?>" id="productimage"/>
                <input type="hidden" name="productdesign" value="<?php echo isset($product->productdesign) && !empty($product->productdesign) ? $product->productdesign : '' ?>" id="productdesign" /></td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>

<div id="dialog" class="popup-module" title="Are you sure ?"></div>



