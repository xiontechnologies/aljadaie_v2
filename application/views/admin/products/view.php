<script>
    $(document).ready(function() {
        $(".copyproduct").on('click', function(event) {
            if(confirm("Are you sure you want to copy product")){
                return true;        
            }else{
                event.preventDefault();
            }
        });
        
        $(".viewdetails").on('click', function() {
            var id = $(this).attr('data');
            $("#dialog").dialog({
                open: function(event, ui) {
                    $("#dialog").html("<img class='preloader' src='" + site_url + "/assets/images/loading.gif'/>");
                    $.ajax({
                        'url': site_url + "admin/product/details/" + id,
                        success: function(data) {
                            $(".preloader").remove();
                            $("#dialog").html(data);
                        }
                    });
                }
            });
        });
    });
</script>
<h2>View Products</h2>
<?php
if ($userRights['add']) {
    ?>
    <p class="fl "><br />
        <a href = "<?php echo base_url("admin/product/add") ?>" class="btn btn-blue" style="float:left">Add New Product</a></p>
<?php } ?>
<?php
if ($userRights['delete']) {
    $frmaction = 'admin/' . $controller . '/delete';
    $attributes = 'id="frmDelete"';
    echo form_open($frmaction, $attributes);
    ?>
    <p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="btn btn-blue" style="float:left">Delete Rows</a></p>
<?php } ?>
<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
<div class="block">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <?php if (isset($products) && !empty($products)) { ?>
        <table id="product-table" class="data display datatable" width="100%">
            <thead>
                <tr>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?>  <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   <?php } ?>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Sku</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Stock </th>
                    <th class="sort">Updated Date</th>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?><th class="sort">Action</th><?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $prodk => $prodv) { ?>
                    <tr class="odd gradeX">
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?>
                            <td><?php echo form_checkbox("option[]", $prodv->id, '', 'class="case"'); ?></td>
                        <?php } ?>
                        <td class="sorting_1"><?php echo $prodv->title; ?></td>
                        <td class="sorting_1"><img src="<?php echo $prodv->thumimagepath ?>" height="50" width="50"></td>
                        <td>
                            <?php
                            if ($prodv->status == '1') {
                                $statusAction = 'javascript:void(0);';
                                if ($userRights['edit']) {
                                    $statusAction = base_url("admin/product/deactive/" . $prodv->id);
                                }
                                ?>
                                <a href="<?php echo $statusAction ?>" id="deactive" title="Active" class="btn-mini btn-black btn-active"><span></span>Active</a> 
                                <?php
                            } else {
                                $statusAction = '';
                                $statusAction = 'javascript:void(0);';
                                if ($userRights['edit']) {
                                    $statusAction = base_url("admin/product/active/" . $prodv->id);
                                }
                                ?>
                                <a href="<?php echo $statusAction; ?>" id="active_priori" title="De-active" class="btn-mini btn-black btn-deactive"><span></span>De-active</a>                              
                            <?php } ?>
                        </td>
                        <td class="sorting_1"><?php echo $prodv->sku; ?></td>
                        <td class="sorting_1"><?php echo $prodv->section->name; ?></td>
                        <td class="sorting_1"><?php echo $prodv->price; ?></td>
                        <td class="sorting_1"><?php echo $prodv->stock; ?></td>
                        <td class="center"><?php echo date("d-m-Y", strtotime($prodv->updated)); ?></td>
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?><td class="center">
                            <?php if ($userRights['edit']) { ?>
                                    <a href="<?php echo base_url() ?>admin/products/edit/<?php echo $prodv->id; ?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>
                                <?php } if ($userRights['delete']) { ?>
                                    <a onclick="deleteAction(<?php echo $prodv->id; ?>, '<?php echo $controller; ?>')" href="javascript:void(0);" title="Delete" class="btn-mini btn-black btn-cross">
                                        <span></span>Delete</a>        
                                <?php } ?>
                                <a class="copyproduct" href="<?php echo base_url('admin/products/copyproduct') . '/' . $prodv->id; ?>" title="Copy Product" class="">copy product</a>
                                <a href="javascript:void(0)" data="<?php echo $prodv->id; ?>" class="viewdetails">View Details</a>        
                            </td><?php } ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    } else {
        echo "There is no data to display";
    }
    ?>
</div>

<div id="dialog" title="Products Details" class="popup-module"></div> 