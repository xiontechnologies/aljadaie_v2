<?php
/*
 * @auther Shafiq
 */
?><table class="data display datatable" width="100%">
    <tbody>
        <tr class="odd gradeX">
            <th>Title</th><td class="sorting_1"><?php echo $products->title; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Short Description</th><td class="sorting_1"><?php echo $products->shortdescription; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Description</th><td class="sorting_1"><?php echo $products->description; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Sku</th><td class="sorting_1"><?php echo $products->sku; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Color Code</th><td class="sorting_1"><?php echo $products->colorcode; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Category</th><td class="sorting_1"><?php echo $products->section->name; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Price</th><td class="sorting_1"><?php echo $products->price; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Discount Price</th><td class="sorting_1"><?php echo $products->discountprice; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Stock</th><td class="sorting_1"><?php echo $products->stock; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Wash care</th><td class="sorting_1"><?php echo $products->washcare; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Tags</th><td class="sorting_1"><?php echo $products->tags; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Keywords </th><td class="sorting_1"><?php echo $products->metakeywords; ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Image </th><td class="sorting_1">
                <img style="margin-top:10px" src="<?php echo base_url(PRODUCTIMAGE_PATH . '/' . $products->photo->file_name); ?>" height='100px' width='100px'/>
            </td>
        </tr>
        <tr class="odd gradeX">
            <th>Design </th><td class="sorting_1">
                <img style="margin-top:10px" src="<?php echo base_url(PRODUCTDESIGN_PATH . '/' . $products->design[0]->file_name); ?>" height='100px' width='100px'/>
            </td>
        </tr>
        <?php
        foreach ($products->productvariation as $k => $v) {
            $temp[$v->subcategory->category->name][] = $v->subcategory->name;
        }
        foreach ($category as $k => $v) {
            ?>
            <tr class="odd gradeX">
                <th><?php echo $v->name; ?> </th><td class="sorting_1">
                    <?php if (isset($temp[$v->name]) && !empty($temp[$v->name])) { ?>
                        <table>

                            <?php
                            foreach ($temp[$v->name] as $varik => $variv) {
                                echo '<tr><td>' . $variv . '</td></tr>';
                            }
                            ?>

                        </table>
                        <?php
                    } else {
                        ?>
                        N-A
                    <?php } ?>

                </td>
            </tr>    
        <?php } ?>
        <tr class="odd gradeX">
            <th>Create Date</th><td class="center"><?php echo date("d-m-Y", strtotime($products->created)); ?></td>
        </tr>
        <tr class="odd gradeX">
            <th>Updated Date</th><td class="center"><?php echo date("d-m-Y", strtotime($products->updated)); ?></td>
        </tr>





        </tr>
    </tbody>
</table>