<h2>View Admin Group</h2>
<?php
if ($userRights['add']) {
    ?>
    <p class="fl "><br /><a href = "<?php echo base_url() ?>admin/groups/add" class="btn btn-blue" style="float:left">Add Admin Group</a></p>
<?php } ?>
<?php
if ($userRights['delete']) {
    $frmaction = 'admin/' . $controller . '/delete';
    $attributes = 'id="frmDelete"';
    echo form_open($frmaction, $attributes);
    ?>
    <p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="btn btn-blue" style="float:left">Delete Rows</a></p>
<?php } ?>
<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
<div class="block">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <?php if (isset($allAdminuser) && !empty($allAdminuser)) { ?>
        <table id="product-table" class="data display datatable" width="100%">
            <thead>
                <tr>
                    <?php if ($userRights['edit']) { ?>  <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   <?php } ?>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Created Date</th>
                    <th class="sort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allAdminuser as $user) { ?>
                    <tr class="odd gradeX">
                        <?php if ($userRights['edit']) { ?>
                            <td><?php echo ($user->id != '1') ? form_checkbox("option[]", $user->id, '', 'class="case"') : ''; ?></td>
                        <?php } ?>
                        <td class="sorting_1"><?php echo $user->name; ?></td>
                        <td>
                            <?php if ($user->id == '1') { ?>
                                <?php if ($user->status == '1') { ?>
                                    <a href="javascrip:void(0)" class="btn-mini btn-black btn-active"><span></span>Active</a> 
                                <?php } else { ?>
                                    <a href="javascrip:void(0)" class="btn-mini btn-black btn-deactive"><span></span>De-active</a>                              
                                    <?php
                                }
                            } else if ($user->status == '1') {
                                $statusAction = 'javascript:void(0);';
                                if ($userRights['edit']) {
                                    $statusAction = base_url("admin/groups/deactive/" . $user->id);
                                }
                                ?>
                                <a href="<?php echo $statusAction ?>" id="deactive" title="Active" class="btn-mini btn-black btn-active"><span></span>Active</a> 
                                <?php
                            } else {
                                $statusAction = '';
                                $statusAction = 'javascript:void(0);';
                                if ($userRights['edit']) {
                                    $statusAction = base_url("admin/groups/active/" . $user->id);
                                }
                                ?>
                                <a href="<?php echo $statusAction; ?>" id="active_priori" title="De-active" class="btn-mini btn-black btn-deactive"><span></span>De-active</a>                              
                            <?php } ?>
                        </td>
                        <td class="center"><?php echo date("d-m-Y", strtotime($user->created)); ?></td>
                        <td class="center">
                            <?php
                            if ($user->id != '1') {
                                if ($userRights['edit']) {
                                    ?>
                                    <a href="<?php echo base_url() ?>admin/groups/edit/<?php echo $user->id; ?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>
                                <?php } if ($userRights['delete']) { ?>
                                    <a href="javascript:void(0);"  title="Delete" class="btn-mini btn-black btn-cross" onclick="deleteAction('<?php echo $user->id; ?>', '<?php echo $this->data['controller']; ?>')"><span></span>Delete</a>        
                                    <?php
                                }
                            } else {
                                ?>
                                <div>No Action 
                                </div>
                                <div>&nbsp;</div>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    } else {
        echo "There is no data to display";
    }
    ?>
</div>
</div>
</div>
<div class="clear"></div>