
        <p>Billing Address</p>
        <div class="Billing Address">
            <?php foreach ($allproducts as $order) { ?>
                <span><?php echo $order->address; ?></span>
                <span><?php echo $order->city; ?></span>
                <span><?php echo $order->state; ?></span>
                <span><?php echo $order->country; ?></span>
                <p>Shipping Address</p>
                <span><?php echo $order->shipping_address; ?></span>
                <?php echo form_open("admin/orders/changestatus/$order->id", 'id="addadminfrm"'); ?>
                <p>Order Status</p>
                <span>
                    <?php
                    $submit = array('name' => 'submit',
                        'id' => 'submit',
                        'value' => 'Submit',
                        'class' => 'btn btn-blue',
                    );
                    $status = $order->order_status;
                    $options = array(
                        'pending' => 'pending',
                        'process' => 'process',
                        'delivered' => 'delivered',                        
                    );

                    $shirts_on_sale = array('small', 'large');

                    echo form_dropdown('status', $options, $status);
                    ?>   

                </span>
                <?php echo form_submit($submit); ?>
            <?php } ?>
        </div>       

        <div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
        <div class="block">

            <?php if (isset($allproducts) && count($allproducts) > 0) {
                ?>
                <table id="product-table" class="data display datatable" width="100%">
                    <thead>
                        <tr>

                            <th>Product Name</th>
                            <th>Order Date</th>
                            <th>sku</th>
                            <th>colorcode</th>
                            <th>collection</th> 
                            <th>price</th>                           
                            <th>Quantity</th>  
                            <th>Total</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $amount = 0;
                        foreach ($allproducts as $order) {//                          
                            foreach ($order->orderdetail as $orderdetail) {
                                ?>
                                <tr class="odd gradeX">
                                    <td class="sorting_1"><?php echo $orderdetail->product->title; ?></td>
                                    <td class="center"><?php echo date("d-m-Y", strtotime($order->created_date)); ?></td>
                                    <td class="sorting_1"><?php echo $orderdetail->product->sku; ?></td>
                                    <td class="center"><?php echo $orderdetail->product->colorcode; ?></td>
                                    <td class="center"><?php echo $orderdetail->product->collection; ?></td>
                                    <td class="center"><?php echo $orderdetail->product->price; ?></td>
                                    <td class="center"><?php echo $orderdetail->quantity; ?></td>
                                    <td class="center"><?php
                    $total = ($orderdetail->quantity) * ($orderdetail->product->price);
                    $amount = $amount + $total;
                    echo $total;
                                ?></td>


                                <?php }
                                ?>
                            </tr>

                        </tbody>
                    </table>
                <?php }
                ?> 
                <div>
                    <p>Total Amount</p>   
                    <span><?php echo $amount; ?></span>
                </div>
                <?php
            } else {
                echo "There is no data to display";
            }
            ?>
        </div>
