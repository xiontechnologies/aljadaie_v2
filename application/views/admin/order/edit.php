<?php
$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue',
);
$reset = array(
    'name' => 'reset',
    'id' => 'reset',
    'value' => 'Reset',
    'class' => 'btn btn-blue'
);
//'pending','process','delivered
$pending = isset($order) && isset($order->order_status) && ($order->order_status == 'pending') ? TRUE : FALSE;
$process = isset($order) && isset($order->order_status) && ($order->order_status == 'process') ? TRUE : FALSE;
$delivered = isset($order) && isset($order->order_status) && ($order->order_status == 'delivered') ? TRUE : FALSE;
$pending = array(
    'name' => 'order_Status',
    'id' => 'order_Status',
    'value' => 'pending',
    'class' => 'fancy-radio',
    'checked' => $pending,
);
$process = array(
    'name' => 'order_Status',
    'id' => 'order_Status',
    'value' => 'process',
    'class' => 'fancy-radio',
    'checked' => $process,
);
$delivered = array(
    'name' => 'order_Status',
    'id' => 'order_Status',
    'value' => 'delivered',
    'class' => 'fancy-radio',
    'checked' => $delivered,
);
$firstname = array(
    'name' => 'firstname',
    'id' => 'firstname',
    'class' => form_error('firstname') ? 'mini fl' : 'mini',
    'value' => isset($order->first_name) ? $order->first_name : '',
    'readonly' => TRUE
);

$lastname = array(
    'name' => 'lastname',
    'id' => 'lastname',
    'class' => form_error('lastname') ? 'mini fl' : 'mini',
    'value' => isset($order->last_name) ? $order->last_name : '',
    'readonly' => TRUE
);

$total_amount = array(
    'name' => 'total_amount',
    'id' => 'total_amount',
    'class' => form_error('total_amount') ? 'mini fl' : 'mini',
    'value' => isset($order->total_amt) ? $order->total_amt : '',
    'readonly' => TRUE
);
?>

<?php
$hidden = array(
    'name' => 'id',
    'value' => $order->id,
);
//        echo '<pre>';print_r($editdata);exit;
echo form_open_multipart("admin/orders/edit/$order->id", 'id="addadminfrm"', $hidden);
?>
        <h2>Edit User</h2>

        <div class="block ">
<?php echo $add_error; ?>
<?php echo $add_warning; ?>
            <?php echo $add_info; ?>
            <?php echo $add_success; ?>
            <table class="form">



                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('First Name : ', 'firstname', array('class' => 'required')); ?></td>
                    <td class="col2">  
<?php echo form_input($firstname); ?>
<?php echo form_error('firstname', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Last Name : ', 'lastname', array('class' => 'required')); ?></td>
                    <td class="col2">  
<?php echo form_input($lastname); ?>
<?php echo form_error('lastname', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>             
                <tr><td valign="top"> <?php echo form_label('Order Status : ', 'order_Status', array('class' => 'required')); ?></td>
                    <td>
                        <p class="fl"><?php echo form_radio($pending); ?>&nbsp;&nbsp;<?php echo form_label('Pending', 'Pending'); ?>&nbsp;&nbsp;</p>
                        <p class="fl"><?php echo form_radio($process); ?>&nbsp;&nbsp;<?php echo form_label('Process', 'Process'); ?>&nbsp;&nbsp;</p>
                        <p class="fl"><?php echo form_radio($delivered); ?>&nbsp;&nbsp;<?php echo form_label('Delivered', 'Delivered'); ?>&nbsp;&nbsp;</p>
<?php echo form_error('order_Status', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr> 


                <tr><td valign="top"> <?php echo form_label('Total Amount : ', 'total_amount', array('class' => 'required')); ?></td>
                    <td>
<?php echo form_input($total_amount); ?>
<?php echo form_error('total_amount', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>



                <tr id="submit">
                    <td>&nbsp;</td>
                    <td valign="top">
<?php echo form_submit($submit); ?>
<?php echo form_reset($reset); ?>
                    </td>
                    <td></td>
                </tr>
            </table>
<?php echo form_close(); ?>
        </div>

