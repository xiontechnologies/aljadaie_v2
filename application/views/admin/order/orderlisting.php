<div class="order-table">
    <?php
    if (isset($allorders) && count($allorders) > 0) {
        ?>
        <table cellpadding="6" cellspacing="1" style="width:100%" border="0" class="table-module">
            <thead>
                <tr>                            
                    <th>Username</th>
                    <th>Order Date</th>
                    <th>Order Status</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <?php
            foreach ($allorders as $k => $items) {
                ?>
                <tbody>
                    <tr>                               
                        <td><?php echo $items->first_name; ?></td>
                        <td><?php echo date("Y-m-d", strtotime($items->created_date)); ?></td>
                        <td><?php echo $items->order_status; ?></td>
                        <td><?php echo $items->total_amt; ?></td>
                        <td><a href="<?php echo base_url() ?>productlisting/<?php echo $items->id; ?>" title="products" class="">
                                <span></span>View Products</a> </td>
                    </tr>
                </tbody>
            <?php } ?>
        </table>
        <?php
    } else {
        echo "No Product Found";
    }
    ?>
</div><!--Personal Info content-->