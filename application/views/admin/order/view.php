<script>
    $(document).ready(function() {
        $(".viewdetails").on('click', function() {
            var id = $(this).attr('data');
            $("#dialog").dialog({
                open: function(event, ui) {
                    $("#dialog").html("<img class='preloader' src='" + site_url + "/assets/images/loading.gif'/>");
                    $.ajax({
                        'url': site_url + "admin/orders/viewproduct/" + id,
                        success: function(data) {
                            $(".preloader").remove();
                            $("#dialog").html(data);
                        }
                    });
                }
            });
        });
    });
</script>
<h2>View Orders</h2>      
<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
<div class="block">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <?php if (isset($allorders) && count($allorders) > 0) {
        ?>
        <table id="product-table" class="data display datatable" width="100%">
            <thead>
                <tr>
                    <th>User Name</th>
                    <th>Order Date</th>
                    <th>Order Status</th>
                    <th class="sort">Total Amount</th>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?><th class="sort">Action</th><?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allorders as $order) { ?>
                    <tr class="odd gradeX">
                        <td class="sorting_1"><?php echo $order->first_name; ?></td>
                        <td class="center"><?php echo date("d-m-Y", strtotime($order->created_date)); ?></td>
                        <td class="sorting_1"><?php echo $order->order_status; ?></td>
                        <td class="center"><?php echo $order->total_amt; ?></td>
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?><td class="center">
                                <a href="<?php echo base_url() ?>admin/orders/productlisting/<?php echo $order->id; ?>" title="edit status" class="btn-edit-status">
                                    <span></span>Edit status</a> 
                                <?php if ($userRights['edit']) { ?>
                                    <a href="<?php echo base_url() ?>admin/orders/edit/<?php echo $order->id; ?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>
                                <?php }
                                ?>
                                <a href="javascript:void(0)" data="<?php echo $order->id ?>" title="view product" class="btn-mini viewdetails"><span></span>View Product</a>
                            <?php }
                            ?>
                            <!--<div>&nbsp;</div>-->
                        </td><?php } ?>
                </tr>

            </tbody>
        </table>
        <?php
    } else {
        echo "There is no data to display";
    }
    ?>
</div>
<div id="dialog" title="Products Details" class="popup-module"></div> 