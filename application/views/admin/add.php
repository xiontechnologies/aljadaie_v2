<?php
$name = array(
    'name' => 'name',
    'id' => 'name',
    'value' => $admingroup->name,
    'class' => 'mini fl validate[required]'
);
$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
$activeRadio = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '1',
    'class' => 'fancy-radio',
    'checked' => $admingroup->status ? TRUE : '',
);
$deactiveRadio = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '0',
    'class' => 'fancy-radio',
    'checked' => !$admingroup->status ? TRUE : '',
);
?>
<?php
if ($action == 'edit') {
    $hidden = array('id' => $admingroup->id);
    echo form_open_multipart("admin/groups/edit/$admingroup->id", 'id="addadminfrm"', $hidden);
    ?> 
    <h2>Edit Group</h2>
    <?php
} else {
    echo form_open_multipart('admin/groups/add', 'id="addadminfrm"');
    ?>
    <h2>Add Group</h2>
<?php } ?>
<div class="block ">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <table class="form">
        <tr><td valign="top"> <?php echo form_label('Name : ', 'Name', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($name); ?>
                <?php echo form_error('name', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Status : ', 'Status', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($activeRadio); ?>&nbsp;&nbsp;<?php echo form_label('Active', 'Active'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($deactiveRadio); ?>&nbsp;&nbsp;<?php echo form_label('Deactivate', 'Deactivate'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('status', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>
