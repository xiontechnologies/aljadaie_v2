<h2>My Account</h2>
<?php
$password = array(
    'name' => 'password',
    'id' => 'password',
    'class' => form_error('password') ? 'mini fl' : 'mini'
);
$Cpassword = array(
    'name' => 'Cpassword',
    'id' => 'Cpassword',
    'class' => form_error('Cpassword') ? 'mini fl' : 'mini'
);
$contact_num = array(
    'name' => 'contact_number',
    'id' => 'contact_number',
    'value' => $adminusers->contact_number,
    'class' => form_error('contact_number') ? 'mini fl' : 'mini'
);
$userName = array(
    'name' => 'username',
    'id' => 'username',
    'value' => $adminusers->username,
    'class' => form_error('username') ? 'mini fl' : 'mini'
);
$emailAd = array(
    'name' => 'email',
    'id' => 'email',
    'value' => $adminusers->email,
    'class' => form_error('email') ? 'mini fl' : 'mini',
);
$image = array(
    'name' => 'photo',
    'id' => 'photo',
    'class' => 'mini fl',
);
$activeRadio = array(
    'name' => 'activated',
    'id' => 'status',
    'value' => '1',
    'checked' => $adminusers->activated ? TRUE : '',
    'class' => 'fancy-radio'
);
$deactiveRadio = array(
    'name' => 'activated',
    'id' => 'status',
    'value' => '0',
    'checked' => !$adminusers->activated ? TRUE : '',
    'class' => 'fancy-radio'
);
$banRadioYes = array(
    'name' => 'ban',
    'id' => 'ban',
    'value' => '1',
    'class' => 'fancy-radio'
);
$banRadioNo = array(
    'name' => 'ban',
    'id' => 'ban',
    'value' => '0',
    'class' => 'fancy-radio'
);
$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
//$reset = array(
//    'name' => 'reset',
//    'id' => 'reset',
//    'value' => 'Reset',
//    'class' => 'btn btn-blue'
//);
?>

<?php echo form_open_multipart("admin/myaccount", 'id="addadminfrm"'); ?>
<div class="block ">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <table class="form">
        <tr><td valign="top"><?php echo form_label('Email : ', 'email', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($emailAd); ?>
                <?php echo form_error('email', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Password : ', 'password', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_password($password); ?> 
                <?php echo form_error('password', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Confirm Password : ', 'Confirm Password', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_password($Cpassword); ?>
                <?php echo form_error('Cpassword', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>

        <tr><td valign="top"> <?php echo form_label('Name : ', 'Name', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($userName); ?>
                <?php echo form_error('username', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Contact number : ', 'Contact number', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($contact_num); ?>
                <?php echo form_error('contact_number', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Status : ', 'Status', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($activeRadio); ?>&nbsp;&nbsp;<?php echo form_label('Active', 'Active'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($deactiveRadio); ?>&nbsp;&nbsp;<?php echo form_label('Deactivate', 'Deactivate'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('activated', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Profile image : ', 'image', array('class' => 'lefpad10')); ?></td>
            <td><?php echo form_upload($image); ?>
                <?php if (isset($uploadError) && !empty($uploadError)) { ?>
                    <div id="error"><div class="error-left"></div><div class="error-inner">
                            <?php echo $uploadError; ?>
                        </div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <?php
        if ($adminusers->image_name) {
            ?>
            <tr>
                <td valign="top">&nbsp;</td>
                <td><img width="200" height="180" src="<?php echo base_url($adminusers->image_path . '/' . $adminusers->image_name); ?>" title="" /></td>
            </tr>
        <?php } ?>


        <tr>
            <td>&nbsp;</td>
            <td colspan="1">
                <table class="rights-tables">
                    <tbody>
                        <tr>
                            <th><?php echo form_label('Module', 'headers', array('class' => 'lefpad10')); ?> </th>
                            <th><?php echo form_label('Landing Page', 'Landing Page', array('class' => 'lefpad10')); ?> </th>
                        </tr>
                        <?php
                        foreach ($adminusers->adminright as $k => $v) {
                             $checked = FALSE;
                            if ($adminusers->landingpage && $v->module && ($v->module->id == $adminusers->landingpage->module_id)) {
                                $checked = True;
                            }
                            ?>
                            <?php if ($v->module) { ?>
                                <tr>
                                    <td><?php echo form_label($v->module->name, $v->module->name, 'class="lefpad10"'); ?></td>
                                    <td><?php echo form_radio("landing", $v->module->id, $checked, 'class="case"'); ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </td>
        </tr>

        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
                <?php // echo form_reset($reset);   ?>
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>

