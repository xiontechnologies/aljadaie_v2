<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo SITE_NAME; ?> | <?php echo $add_title; ?></title>
        <?php
        echo $add_css;
        echo $add_js;
        ?>
    </head>
    <body>
        <div class="container_12">
            <div class="grid_12 header-repeat">
                <div id="branding">
                    <div class="floatleft">
                        <img src="<?php echo base_url(); ?>assets/admin/images/logo.png" alt="Logo"  /></div>
                    <div class="floatright">
                        <div class="floatleft">
                            <?php
                            if ($this->session->userdata[SITE_NAME . '_admin_user_data']['image'] && $this->session->userdata[SITE_NAME . '_admin_user_data']['image_name']) {
                                $path = base_url($this->session->userdata[SITE_NAME . '_admin_user_data']['image'] . '/thumb/' . $this->session->userdata[SITE_NAME . '_admin_user_data']['image_name']);
                            } else {
                                $path = base_url('assets/admin/img/img-profile.jpg');
                            }
                            ?>
                            <img src="<?php echo $path; ?>" alt="Profile Pic" /></div>
                        <div class="floatleft marginleft10">
                            <ul class="inline-ul floatleft">
                                <li>Hello <?php echo $this->session->userdata[SITE_NAME . '_admin_user_data']['username'] ? ucfirst($this->session->userdata[SITE_NAME . '_admin_user_data']['username']) : ''; ?></li>
                                <li><a href="<?php echo base_url("admin/myaccount") ?>">My Account</a></li>
                                <li><a href="<?php echo base_url('admin/auth/logout'); ?>">Logout</a></li>
                            </ul>
                            <br />
                            <!--<span class="small grey">Last Login: 3 hours ago</span>-->
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div> 
            <div class="grid_12 menu-article">
                <ul class="menu-module makelist">

                    <?php
                    if ($menus = $this->session->userdata(SITE_NAME . '_menus')) {
                        $ModulesRights = $this->session->userdata(SITE_NAME . '_userRights');
                        foreach ($menus as $mk => $mv) {
                            if ($mk !== MYACCOUNT) {
                                ?>
                                <li class="main-btn">
                                    <a href="<?php echo base_url('admin/' . strtolower($mv) . '/list') ?>"><?php echo $mv; ?></a>
                                    <?php if (isset($ModulesRights[$mk]) && (ADD_MODULE & $ModulesRights[$mk]) && $mv != 'orders' && $mv != 'Bulkupload') { ?>
                                    <?php } ?>
                                </li>
                                <?php
                            }
                        }
                    }
                    ?>
                    <!--<li class="main-btn"><a href="<?php // echo base_url() ?>admin/reports/list">Reports</a></li>-->
                    <!--<li class="main-btn"><a href="<?php // echo base_url() ?>admin/dbbackups/list">Database Backup</a></li>-->
                </ul>
            </div>
            <div class="grid_10">
                <div class="box round first fullpage">