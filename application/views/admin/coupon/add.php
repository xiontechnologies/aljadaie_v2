<script>
    $(document).ready(function() {
        $(".asignto").on('change', function() {
            if (this.value == 1) {
                $.ajax({
                    url: "<?php echo base_url('admin/coupons/getusers') ?>"
                }).done(function(d) {
                    $("#dropdown-user").html(d);
                });
                $("#dropdown-user").show();
            } else {
                $("#dropdown-user").hide();
            }
        });

        $(".asigntogroup").on('change', function() {
            if (this.value == 1) {
                $.ajax({
                    url: "<?php echo base_url('admin/coupons/getusergroups') ?>"
                }).done(function(d) {
                    $("#dropdown-group").html(d);
                });
                $("#dropdown-group").show();
            } else {
                $("#dropdown-group").hide();
            }
        });
        $(".type").on('change', function() {
            if (this.value == 1) {
                $(".no-time").show();
            } else {
                $(".no-time").hide();
            }
        });
    });
</script>

<?php
$singleuserSlected = $percentageSlected = $amountSlected = $multipleuserSlected = $asigntoYSelected = $asigntoNSelected = $asigntogroupYSelected = $asigntogroupNSelected = '';
$displayUser = "none";
if (isset($postdata['type']) && !$postdata['type']) {
    $singleuserSlected = TRUE;
} else {
    $multipleuserSlected = TRUE;
}

if (isset($postdata['discount_type']) && $postdata['discount_type']) {
    $percentageSlected = TRUE;
} else {
    $amountSlected = TRUE;
}
if (isset($postdata['asignto']) && $postdata['asignto']) {
    $displayUser = "block";
    $asigntoYSelected = TRUE;
} else {
    $displayUser = "none";
    $asigntoNSelected = TRUE;
}

if (isset($postdata['asigntogroup']) && $postdata['asigntogroup']) {
    $displayUser = "block";
    $asigntogroupYSelected = TRUE;
} else {
    $displayUser = "none";
    $asigntogroupNSelected = TRUE;
}
$code = array(
    'name' => 'code',
    'id' => 'code',
    'value' => isset($postdata['code']) ? $postdata['code'] : '',
    'class' => 'inp-form'
);
$singleuser = array(
    'name' => 'type',
    'value' => '0',
    'checked' => $singleuserSlected,
    'class' => 'fancy-radio type'
);
$multipleuser = array(
    'name' => 'type',
    'value' => '1',
    'checked' => $multipleuserSlected,
    'class' => 'fancy-radio type'
);
$to_date = array(
    'name' => 'to_date',
    'id' => 'to_date',
    'value' => isset($postdata['to_date']) ? $postdata['to_date'] : '',
    'class' => 'inp-form',
    'readonly' => 'readonly'
);
$from_date = array(
    'name' => 'from_date',
    'id' => 'from_date',
    'value' => isset($postdata['from_date']) ? $postdata['from_date'] : '',
    'class' => 'inp-form',
    'readonly' => 'readonly'
);
$to_amount = array(
    'name' => 'to_amount',
    'id' => 'to_amount',
    'value' => isset($postdata['to_amount']) ? $postdata['to_amount'] : '',
    'class' => 'inp-form'
);
$from_amount = array(
    'name' => 'from_amount',
    'id' => 'from_amount',
    'value' => isset($postdata['from_amount']) ? $postdata['from_amount'] : '',
    'class' => 'inp-form'
);
$percentamt = array(
    'name' => 'discount_type',
    'id' => 'discount_type',
    'value' => '1',
    'checked' => $percentageSlected,
    'class' => 'fancy-radio'
);
$amt = array(
    'name' => 'discount_type',
    'id' => 'discount_type',
    'value' => '0',
    'checked' => $amountSlected,
    'class' => 'fancy-radio'
);
$yes = array(
    'name' => 'asignto',
    'value' => '1',
    'checked' => $asigntoYSelected,
    'class' => 'fancy-radio asignto',
);
$no = array(
    'name' => 'asignto',
    'value' => '0',
    'checked' => $asigntoNSelected,
    'class' => 'fancy-radio asignto'
);
$group_yes = array(
    'name' => 'asigntogroup',
    'value' => '1',
    'checked' => $asigntogroupYSelected,
    'class' => 'fancy-radio asigntogroup',
);
$group_no = array(
    'name' => 'asigntogroup',
    'value' => '0',
    'checked' => $asigntogroupNSelected,
    'class' => 'fancy-radio asigntogroup'
);
$no_of_time = array(
    'name' => 'no_of_time',
    'id' => 'no_of_time',
    'value' => isset($postdata['no_of_time']) ? $postdata['no_of_time'] : '',
    'class' => 'inp-form'
);
$amount = array(
    'name' => 'amount',
    'id' => 'amount',
    'value' => isset($postdata['amount']) ? $postdata['amount'] : '',
    'class' => 'inp-form'
);
?>
<h2>Add Coupon</h2>
<div class="block ">

    <?php
    echo form_open('admin/coupons/add', 'id="addcontents"');
    ?>
    <table class="form">
        <tr>
            <td valign="top"><label style="color:red;">*</label> <?php echo form_label('Code : '); ?></td>
            <td><?php echo form_input($code); ?></td>

            <?php if (form_error('code')) { ?>
                <td id="error" style="display: <?php echo (form_error('code') != '') ? 'block' : 'none'; ?>">
                    <div class="error-left"></div>
                    <div class="error-inner"><?php echo (form_error('code') != '') ? form_error('code') : 'This field is required.'; ?></div>
                </td>
            <?php } ?>
        </tr>
        <tr>
            <td valign="top"><label style="color: red">*</label> <?php echo form_label('Coupon Type :'); ?> </td>
            <td><p style="float:left"><?php echo form_radio($singleuser); ?>&nbsp;&nbsp;<b>Single Time</b> &nbsp;&nbsp;</p>
                <p><?php echo form_radio($multipleuser); ?>&nbsp;&nbsp;<b>Multiple Time </b>&nbsp;&nbsp;</p>
            </td>
            <?php if (form_error('type')) { ?>
                <td id="error" style="display: <?php echo (form_error('type') != '') ? 'block' : 'none'; ?>">
                    <div class="error-left"></div>
                    <div class="error-inner"><?php echo (form_error('type') != '') ? form_error('type') : 'This field is required.'; ?></div>
                </td>
            <?php } ?>

        </tr>
        <tr class="no-time" <?php echo $singleuserSlected ? 'style="display:none"' : '' ?>>
            <td valign="top"><?php echo form_label('Number of Times: '); ?> </td>
            <td><p style="float:left"><?php echo form_input($no_of_time); ?></p>
            </td>
            <?php if (form_error('no_of_time')) { ?>
                <td id="error" style="display: <?php echo (form_error('no_of_time') != '') ? 'block' : 'none'; ?>">
                    <div class="error-left"></div>
                    <div class="error-inner"><?php echo (form_error('no_of_time') != '') ? form_error('no_of_time') : 'This field is required.'; ?></div>
                </td>
            <?php } ?>
        </tr>
        <tr>
            <td valign="top"><?php echo form_label('To Date:'); ?> </td>
            <td><?php echo form_input($to_date); ?></td>
        </tr>

        <tr>
            <td valign="top"><?php echo form_label('From Date:'); ?> </td>
            <td><?php echo form_input($from_date); ?></td>
        </tr>
        <tr>
            <td valign="top"><?php echo form_label('Min amount:'); ?> </td>
            <td><?php echo form_input($to_amount); ?></td>
        </tr>

        <tr>
            <td valign="top"><?php echo form_label('Max amount:'); ?></td>
            <td><?php echo form_input($from_amount); ?></td>
        </tr>
        <tr>
            <td valign="top"><label style="color:red">*</label> <?php echo form_label('Discount Type :'); ?> </td>
            <td><p style="float:left"><?php echo form_radio($percentamt); ?>&nbsp;&nbsp;<b>Percent</b> &nbsp;&nbsp;</p>
                <p><?php echo form_radio($amt); ?>&nbsp;&nbsp;<b>Amount </b>&nbsp;&nbsp;</p>
            </td>
            <?php if (form_error('discount_type')) { ?>
                <td id="error" style="display: <?php echo (form_error('discount_type') != '') ? 'block' : 'none'; ?>">
                    <div class="error-left"></div>
                    <div class="error-inner"><?php echo (form_error('discount_type') != '') ? form_error('discount_type') : 'This field is required.'; ?></div>
                </td>
            <?php } ?>

        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <?php echo form_input($amount); ?>
            </td>
            <?php if (form_error('amount')) { ?>
                <td id="error" style="display: <?php echo (form_error('amount') != '') ? 'block' : 'none'; ?>">
                    <div class="error-left"></div>
                    <div class="error-inner"><?php echo (form_error('amount') != '') ? form_error('amount') : 'This field is required.'; ?></div>
                </td>
            <?php } ?>
        </tr>
        <tr>
            <td valign="top"><?php echo form_label('Asign To Group:'); ?>  </td>
            <td><p style="float:left"><?php echo form_radio($group_yes); ?>&nbsp;&nbsp;<b>Yes</b> &nbsp;&nbsp;</p>
                <p><?php echo form_radio($group_no); ?>&nbsp;&nbsp;<b>No </b>&nbsp;&nbsp;</p>
                <div id="dropdown-group">
                    <?php
                    if ($asigntogroupYSelected) {
                        $this->load->view('admin/coupon/grouplist', $this->data);
                    }
                    ?>
                </div>
                <?php if (form_error('group_option[]')) { ?>
                    <div id="error" style="display: <?php echo (form_error('group_option[]') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('group_option[]') != '') ? form_error('group_option[]') : 'Select atleast one user'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>

        <tr>
            <td valign="top"><?php echo form_label('Asign To User:'); ?>  </td>
            <td><p style="float:left"><?php echo form_radio($yes); ?>&nbsp;&nbsp;<b>Yes</b> &nbsp;&nbsp;</p>
                <p><?php echo form_radio($no); ?>&nbsp;&nbsp;<b>No </b>&nbsp;&nbsp;</p>
                <div id="dropdown-user">
                    <?php
                    if ($asigntoYSelected) {
                        $this->load->view('admin/coupon/userlist', $this->data);
                    }
                    ?>
                </div>
                <?php if (form_error('option[]')) { ?>
                    <div id="error" style="display: <?php echo (form_error('option[]') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('option[]') != '') ? form_error('option[]') : 'Select atleast one user'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <input type="submit" id="sumbit" value="Submit" class="btn btn-blue" />
                <input type="reset" value="reset" class="btn btn-blue"  />
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>
