<div style="margin-bottom: 20px;  width: 50%;">
    <div class="round first grid">
        <div class="block" style="padding: 0;">
            <?php if (isset($usergroup) && count($usergroup) > 0) { ?>
                <table id="product-table" class="data display datatable my-table" style="border:1px solid #ccc">
                    <thead>
                        <tr>
                            <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecksgroup(this);id="selectall"'); ?></th>
                            <th><a href = "javascript:void(0);">Name</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($usergroup as $u) {
                            $checked = '';
                            if (isset($postdata['group_option']) && !empty($postdata['group_option']) && in_array($u->id, $postdata['group_option'])) {
                                $checked = "checked='checked'";
                            } else if (isset($EditCoupan->user_group_id) && !empty($EditCoupan->user_group_id) && in_array($u->id, explode(',', $EditCoupan->user_group_id))) {
                                $checked = "checked='checked'";
                            }
                            ?>
                            <tr class="odd gradeX">

                                <td><input type="checkbox" value="<?php echo $u->id; ?>" <?php echo $checked; ?>id="<?php echo $u->id; ?>" name="group_option[]" class ="casegroup"/></td>
                                <td class="sorting_1"><?php echo $u->group; ?></td>
                                <?php
                            }
                            ?>

                            </td>
                        </tr>

                    </tbody>
                </table>
                <?php
            } else {
                echo "There is no data to display";
            }
            ?>
        </div>
    </div>
</div>
<div class="clear"></div>