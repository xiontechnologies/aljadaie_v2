<h2>View Coupons</h2>
<?php
if ($userRights['add']) {
    ?>
    <p class="fl "><br />
        <a href = "<?php echo base_url("admin/coupons/add") ?>" class="btn btn-blue" style="float:left">Add New Coupon</a></p>
<?php } ?>
<?php
if ($userRights['delete']) {
    $frmaction = 'admin/' . $controller . '/delete';
    $attributes = 'id="frmDelete"';
    echo form_open($frmaction, $attributes);
    ?>
    <p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="btn btn-blue" style="float:left">Delete Rows</a></p>
<?php } ?>
<?php // echo '<pre>';print_r($coupans);exit;?>
<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
<div class="block">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <?php
//            echo '<pre>';print_r($coupans);exit;
    if (isset($allcoupans) && !empty($allcoupans)) {
        ?>
        <table id="product-table" class="data display datatable" width="100%">
            <thead>
                <tr>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?>  <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   <?php } ?>
                    <th>Code</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>To Date</th>
                    <th>From Date</th>
                    <th>To Amount</th>
                    <th>From Amount </th>
                    <th class="sort">Number Of Used</th>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?><th class="sort">Action</th><?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($allcoupans as $coupans) {
                    ?>
                    <tr class="odd gradeX">
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?>
                            <td><?php echo form_checkbox("option[]", $coupans->id, '', 'class="case"'); ?></td>
                        <?php } ?>
                        <td><?php echo $coupans->code; ?></td>
                        <td><?php if ($coupans->type == '0') { ?>
                                Single Time 
                            <?php } else { ?>
                                Multiple Time
                                <?php ?>
                            <?php } ?></td>
                        <td><?php echo $coupans->amount; ?></td>
                        <td><?php echo date("d M Y", strtotime($coupans->to_date)); ?></td>
                        <td><?php echo date("d M Y", strtotime($coupans->from_date)); ?></td>
                        <td><?php echo $coupans->to_amount; ?></td>
                        <td><?php echo $coupans->from_amount; ?></td>
                        <td><?php echo $coupans->no_of_used; ?></td>
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?><td class="center">
                            <?php if ($userRights['edit']) { ?>
                                    <a href="<?php echo base_url() ?>admin/coupons/edit/<?php echo $coupans->id; ?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>
                                <?php } if ($userRights['delete']) { ?>
                                    <a href="<?php echo base_url() ?>admin/coupons/delete/<?php echo $coupans->id ?>" title="Delete" class="btn-mini btn-black btn-cross">
                                        `<span></span>Delete</a>        
                                    <?php
                                }
                                ?>
                                <!--<div>&nbsp;</div>-->
                            </td><?php } ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    } else {
        echo "There is no data to display";
    }
    ?>
</div>
<div id="dialog" title="Products Details" class="popup-module"></div> 