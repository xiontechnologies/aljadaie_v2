<?php
/*
 * @auther Shafiq
 */
?>

<div style="margin-bottom: 20px;  width: 50%;">
    <div class="round first grid">
        <div class="block" style="padding: 0;">
            <?php if (isset($user) && count($user) > 0) { ?>
                <table id="product-table" class="data display datatable" style="border:1px solid #ccc">
                    <thead>
                        <tr>
                            <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>
                            <th class = "table-header-repeat line-left minwidth-1"><a href = "javascript:void(0);">User name</a></th>
                            <th class = "table-header-repeat line-left minwidth-1"><a href = "javascript:void(0);">Email</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($user as $u) {
                            $checked = '';
                            if (isset($postdata['option']) && !empty($postdata['option']) && in_array($u->id, $postdata['option'])) {
                                $checked = "checked='checked'";
                            } else if (isset($EditCoupan->user_id) && !empty($EditCoupan->user_id) && in_array($u->id, explode(',', $EditCoupan->user_id))) {
                                $checked = "checked='checked'";
                            }
                            ?>
                            <tr>
                                <td>
                                    <!--<td><?php // echo form_checkbox("option[]", $u->id, '', 'class="case"');    ?></td>-->
                                    <input type="checkbox" value="<?php echo $u->id; ?>" <?php echo $checked; ?>id="<?php echo $u->id; ?>" name="option[]" class ="case"/></td>
                                <td><?php echo $u->firstname . ' ' . $u->lastname; ?> </td>
                                <td><?php echo $u->email; ?></td>
                            </tr>

                        </tbody>
                    <?php } ?>
                </table>
                <?php
            } else {
                echo "There is no data to display";
            }
            ?>
        </div>
    </div>
</div>
<div class="clear"></div>





