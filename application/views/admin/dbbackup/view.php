<h2>Database Backup</h2>
<?php
echo isset($add_success) ? $add_success : "";
if ($userRights['add']) {
    ?>
    <p class="fl "><br /><a href = "<?php echo base_url() ?>admin/dbbackups/export" class="btn btn-blue" style="float:left">Export</a></p>
    <p class="fl marginleft10"><br /><a href = "<?php echo base_url()  ?>admin/dbbackups/autobackup" class="btn btn-blue" style="float:left">Auto Backup</a></p>
<?php } ?>
<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
