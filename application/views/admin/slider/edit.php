<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//echo '<pre>';print_r($editdata->photo);exit;
$name = array(
    'name' => 'name',
    'id' => 'name',
//    'value' => isset($editdata) && isset($editdata->design_name) ? $editdata->design_name : isset($postdata) && (isset($postdata->design_name)) ? $postdata->design_name : "",
    'value' => isset($editdata) && isset($editdata->name) ? $editdata->name : (isset($postdata) && isset($postdata->name) ? $postdata->name : ""),
    'class' => 'mini fl validate[required]');
$submit = array(
    'name' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
$activechecked = isset($postdata->status) && $postdata->status == 1 ? true : isset($editdata->status) && $editdata->status == 1 ? true : '';
$deacstatus = isset($postdata->status) && $postdata->status == 0 ? true : isset($editdata->status) && $editdata->status == 0 ? true : '';

$active = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '1',
    'class' => 'fancy-radio validate[required]',
    'checked' => $activechecked
);
$deactive = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '0',
    'class' => 'fancy-radio validate[required]',
    'checked' => $deacstatus
);
$image = array(
    'name' => 'userfile',
    'id' => 'userfile',
    'value' => isset($editdata) && (isset($editdata->photo->file_name)) ? $editdata->photo->file_name : '',
    'class' => ''
);
?>

<?php
echo form_open_multipart('admin/sliders/edit/' . $editdata->id, 'id="addadminfrm"');
?>
<h2>Edit Slider</h2>

<div class="block ">
    <?php
    if (isset($add_error)) {
        echo $add_error;
    }
    if (isset($add_warning)) {
        echo $add_warning;
    }
    if (isset($add_info)) {
        echo $add_info;
    }
    if (isset($add_success)) {
        echo $add_success;
    }
    ?>
    <table class="form">
        <tr><td valign="top"> <?php echo form_label('Design Name : ', 'Design Name', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($name); ?>
                <?php echo form_error('name', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Image : ', 'userfile', array('class' => 'required')); ?></td></td>
            <td class="col2">  
                <?php echo form_upload($image); ?>
                <?php echo form_error('sliderimage', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                <?php
                if (isset($postdata->sliderimage) && !empty($postdata->sliderimage)) {
                    $path = explode('|', $postdata->sliderimage);
                    ?>
                    <img class="sliderimage" src="<?php echo base_url(SLIDER_PATH . '/' . $path[1]); ?>" height="100px" width="100px">
                    <?php
                } else {

                    $pathphoto = $editdata->photo->file_name;
                    $pathphotoid = $editdata->photo->id;
                    ?>
                    <img class='sliderimage' src="<?php echo base_url(SLIDER_PATH . '/' . $pathphoto); ?>" height='100px' width='100px'/>
                    <?php
                }
                ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Status : ', 'Status', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($active); ?>&nbsp;&nbsp;<?php echo form_label('Active', 'Active'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($deactive); ?>&nbsp;&nbsp;<?php echo form_label('Deactivate', 'Deactivate'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('status', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr id="submit">
            <td><input type="hidden" name="sliderimage" value="<?php echo isset($postdata->sliderimage) && !empty($postdata->sliderimage) ? $postdata->sliderimage : '' ?>" id="sliderimage"/></td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
                <input type="reset" name="reset" nalue="Reset" class="btn btn-blue">
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>

