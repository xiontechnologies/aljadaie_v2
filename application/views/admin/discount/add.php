<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//echo '<pre>';
//print_r($editdata);
//exit;

$percentage = $value = '';
if (isset($postdata->discount) && !$postdata->discount) {
    $percentage = TRUE;
} else {
    $value = TRUE;
}
//$percentage = (isset($postdata) && $postdata->discount == '0' ? $postdata->discount : TRUE);
//$value = isset($postdata) && $postdata->discount == '1' ? $postdata->discount : "";
$percentage = array(
    'name' => 'discount',
    'id' => 'discount',
    'value' => '0',
    'checked' => $percentage,
    'class' => 'fancy-radio validate[required]');
$value = array(
    'name' => 'discount',
    'id' => 'discount',
    'value' => '1',
    'checked' => $value,
    'class' => 'fancy-radio validate[required]');
$start_date = array(
    'name' => 'start_date',
    'id' => 'start_date',
    'class' => 'date',
    'value' => (isset($postdata->start_date)) ? $postdata->start_date : "",
    'class' => 'mini fl validate[required]'
);
$end_date = array(
    'name' => 'end_date',
    'id' => 'end_date',
    'class' => 'date',
    'value' => (isset($postdata->end_date)) ? $postdata->end_date : "",
    'class' => 'mini fl validate[required]'
);
$submit = array(
    'name' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
$amount = array(
    'name' => 'amount',
    'id' => 'amount',
    'class' => 'mini f1 validate[required]',
    'value' => (isset($postdata) && isset($postdata->amount) ? $postdata->amount : "")
);
?>
<?php
if ($action == 'edit') {
    $hidden = array('id' => $editdata->id);
    echo form_open('admin/discounts/edit/' . $editdata->id, 'id="addadminfrm"', $hidden);
    ?>
    <h2>Edit Discount</h2>

    <?php
} else {
    echo form_open('admin/discounts/add', 'id="addadminfrm"');
    ?>
    <h2>Add Discount</h2>

    <?php
}
?>
<div class="block ">
    <?php
    if (isset($add_error)) {
        echo $add_error;
    }
    ?>
    <?php
    if (isset($add_warning)) {
        echo $add_warning;
    }
    ?>
    <?php
    if (isset($add_info)) {
        echo $add_info;
    }
    ?>
    <?php
    if (isset($add_success)) {
        echo $add_success;
    }
    ?>
    <table class="form">
        <tr><td valign="top"> <?php echo form_label('Select Usergroup : ', 'usergroup', array('class' => 'required')); ?></td>
            <td>
                <?php
                $extra = '"id = usergroup"';
//                $selected = Array();
                $userdata = isset($postdata) && isset($postdata->usergroup) ? $postdata->usergroup : "";
                $selected = "";
                if (isset($userdata) && !empty($userdata)) {
                    foreach ($userdata as $select) {
                        $selected[] = $select;
                    }
                }
//                $selected = (isset($postdata->userdata) && !empty($postdata->userdata) && in_array($u->id, $postdata->userdata)) ?  : "";
                echo form_multiselect('usergroup[]', $usergroup, $selected, $extra);
                ?>
                <?php echo form_error('usergroup', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Discount Type : ', 'type', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($percentage); ?>&nbsp;&nbsp;<?php echo form_label('Percentage', 'Percentage'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($value); ?>&nbsp;&nbsp;<?php echo form_label('Value', 'Value'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('discount', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Amount : ', 'Amount', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($amount); ?>
                <?php echo form_error('amount', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Start Date : ', 'Start Date', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($start_date); ?>
                <?php echo form_error('start_date', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('End Date : ', 'End Date', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($end_date); ?>
                <?php echo form_error('end_date', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>
