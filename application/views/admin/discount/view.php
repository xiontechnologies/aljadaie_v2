<h2>View Discount</h2>
<?php
if ($userRights['add']) {
    ?>
    <p class="fl "><br /><a href = "<?php echo base_url() ?>admin/discounts/add" class="btn btn-blue" style="float:left">Add Discount</a></p>
<?php } ?>
<?php
if ($userRights['delete']) {
    $frmaction = 'admin/' . $controller . '/delete';
    $attributes = 'id="frmDelete"';
    echo form_open($frmaction, $attributes);
    ?>
    <p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="btn btn-blue" style="float:left">Delete Rows</a></p>
<?php } ?>
<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
<div class="block">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <?php if (isset($alldata) && count($alldata) > 0) { ?>
        <table id="product-table" class="data display datatable" width="100%">
            <thead>
                <tr>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?>  <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   <?php } ?>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Start Date</th>
                    <th class="sort">End Date</th>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?><th class="sort">Action</th><?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($alldata as $discount) { ?>
                    <tr class="odd gradeX">
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?>
                            <td><?php echo (form_checkbox("option[]", $discount->id, '', 'class="case"')); ?></td>
                        <?php } ?>
                        <td class="sorting_1">
                            <?php
                            if ($discount->type == '0') {
                                echo 'Percentage';
                            } else {
                                echo 'Value';
                            }
                            ?>
                        </td>
                        <td><?php echo $discount->amount ?></td>
                        <td class="center"><?php echo date("d-m-Y", strtotime($discount->start_date)); ?></td>
                        <td class="center"><?php echo date("d-m-Y", strtotime($discount->end_date)); ?></td>
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?><td class="center">
                            <?php if ($userRights['edit']) { ?>
                                    <a href="<?php echo base_url() ?>admin/discounts/edit/<?php echo $discount->id; ?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>
                                <?php } if ($userRights['delete']) { ?>
                                    <a href="<?php echo base_url() ?>admin/discounts/delete/<?php echo $discount->id ?>" title="Delete" class="btn-mini btn-black btn-cross">
                                        `<span></span>Delete</a>        
                                    <?php
                                }
                            }
                            ?>
                            <!--<div>&nbsp;</div>-->
                        </td><?php } ?>
                </tr>

            </tbody>
        </table>
        <?php
    } else {
        echo "There is no data to display";
    }
    ?>
</div>
