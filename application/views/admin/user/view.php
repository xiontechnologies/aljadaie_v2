
<h2>View Members</h2>
<?php
if ($userRights['add']) {
    ?>
    <p class="fl "><br /><a href = "<?php echo base_url() ?>admin/members/add" class="btn btn-blue" style="float:left">Add Member</a></p>
<?php } ?>
<?php
if ($userRights['delete']) {
    $frmaction = 'admin/' . $controller . '/delete';
    $attributes = 'id="frmDelete"';
    echo form_open($frmaction, $attributes);
    ?>
    <p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="btn btn-blue" style="float:left">Delete Rows</a></p>
<?php } ?>
<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
<div class="block">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <?php if (isset($allusers) && !empty($allusers)) { ?>
        <table id="product-table" class="data display datatable" width="100%">
            <thead>
                <tr>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?>  <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   <?php } ?>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th class="sort">Created Date</th>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?> <th class="sort">Action</th><?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allusers as $user) { ?>
                    <tr class="odd gradeX">

                        <?php if ($userRights['edit'] || $userRights['delete']) { ?> <td><?php echo form_checkbox("option[]", $user->id, '', 'class="case"'); ?></td><?php } ?>

                        <td class="sorting_1"><?php echo $user->firstname; ?>
                        <td class="sorting_1"><?php echo $user->lastname; ?>
                        <td class="sorting_1"><?php echo $user->email; ?></td>

                        <td>
                            <?php
                            if ($user->activated == '1') {
                                $statusAction = 'javascript:void(0);';

                                $statusAction = base_url("admin/members/deactivate/" . $user->id);
                                ?>
                                <a href="<?php echo $statusAction ?>" id="deactive" title="Active" class="btn-mini btn-black btn-active"><span></span>Active</a> 
                                <?php
                            } else {
                                $statusAction = '';
                                $statusAction = 'javascript:void(0);';

                                $statusAction = base_url("admin/members/activate/" . $user->id);
                                ?>
                                <a href="<?php echo $statusAction; ?>" id="active_priori" title="De-active" class="btn-mini btn-black btn-deactive"><span></span>De-active</a>                              
                            <?php } ?>
                        </td>
                        <td class="center"><?php echo date("d-m-Y", strtotime($user->created)); ?></td>
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?><td class="center">


                                <?php if ($userRights['edit']) { ?> 
                                    <a href="<?php echo base_url() ?>admin/members/edit/<?php echo $user->id; ?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>
                                <?php } ?>
                                <?php if ($userRights['delete']) { ?> 
                                    <a href="<?php echo base_url() ?>admin/members/delete/<?php echo $user->id; ?>" title="Delete" class="btn-mini btn-black btn-cross"><span></span>Delete</a>        
                                <?php } ?>
                                            <!--                                            <a href="<?php echo base_url() ?>admin/user_video/<?php echo $user->id; ?>" title="View Details"><span></span>View Details</a>        -->

                            </td><?php } ?>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            echo "There is no data to display";
        }
        ?>
</div>
