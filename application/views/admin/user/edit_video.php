<?php
//echo '<pre>';
//
//print_r($editdata);
//echo '--------------------------------------------------';
//echo '<pre>';
//print_r($editdata->category);
//exit;


$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue',
);

$activeRadio = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '1',
    'class' => 'fancy-radio',
    'checked' => (isset($postdata->status) && $postdata->status == 1) ? True : ((isset($editdata->status) && $editdata->status == 1) ? true : ''),
);
$deactiveRadio = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '0',
    'class' => 'fancy-radio',
    'checked' => (isset($postdata->status) && $postdata->status == 0) ? True : ((isset($editdata->status) && $editdata->status == 0) ? true : ''),
);
$title = array(
    'name' => 'title',
    'id' => 'title',
    'class' => form_error('title') ? 'mini fl' : 'mini',
    'value' => isset($editdata) && isset($editdata->file_title) ? $editdata->file_title :  "",
);


$yes = array(
    'name' => 'featured_video',
    'id' => 'featured_video',
    'value' => '1',
    'class' => 'fancy-radio',
    'checked' => (isset($postdata->featured_vedio) && $postdata->featured_vedio == 1) ? True : ((isset($editdata->featured_vedio) && $editdata->featured_vedio == 1) ? true : ''),
);
$no = array(
    'name' => 'featured_video',
    'id' => 'featured_video',
    'value' => '0',
    'class' => 'fancy-radio',
    'checked' => (isset($postdata->featured_vedio) && $postdata->featured_vedio == 0) ? True : ((isset($editdata->featured_vedio) && $editdata->featured_vedio == 0) ? true : ''),
);
//<input type="file" id="video" name="video" >
$video = array(
    'type' => 'file',
    'id' => 'video',
    'name' => 'video',
);


$approved = array(
    'name' => 'approved',
    'id' => 'approved',
    'value' => '1',
    'class' => 'fancy-radio',
    'checked' => (isset($postdata->approved) && $postdata->approved == 1) ? True : ((isset($editdata->approved) && $editdata->approved == 1) ? true : ''),
);
$disapproved = array(
    'name' => 'approved',
    'id' => 'approved',
    'value' => '0',
    'class' => 'fancy-radio',
    'checked' =>(isset($postdata->approved) && $postdata->approved == 0) ? True : ((isset($editdata->approved) && $editdata->approved == 0) ? true : ''),
);
?>
<div class="grid_12 full-module">
    <div class="box round first fullpage">
        <?php
        if ($action == 'edit') {
            $hidden = array(
                'name' => 'id',
                'value' => $editdata->id,
            );
            echo form_open_multipart("admin/uservideo/edit/$editdata->id/$user_id", 'id="addadminfrm"', $hidden);
            ?>
            <h2>Edit Video</h2>
            <?php
        } else {
            echo form_open_multipart('admin/videos_add', 'id="addvideofrm"');
            ?>
            <h2>Add Video</h2>
        <?php } ?>
        <div class="block ">
            <?php echo $add_error; ?>
            <?php echo $add_warning; ?>
            <?php echo $add_info; ?>
            <?php echo $add_success; ?>
            <table class="form">
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Title : ', 'title', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($title); ?>
                        <?php echo form_error('title', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr><td valign="top"><?php echo form_label('Category: ', 'category', array('class' => 'required')); ?></td>
                    <td>
                        <?php
                        $extra = "id=category";
                        $select_cat = isset($editdata->category[0]->id) && $editdata->category[0]->id ? $editdata->category[0]->id : "";
//                        echo '<pre>';print_r($select_cat);exit;
                        echo form_dropdown('category_id', $category, $select_cat, $extra);
                        ?>
                        <?php echo form_error('category_id', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td valign="top"> <?php echo form_label('Select Video  : ', 'video', array('class' => 'required')); ?></td>
                    <td>
                        <?php echo form_upload($video); ?>
                        <?php if (isset($editdata) && isset($editdata->file_name)) { ?>
                            <video width="100" height="100" controls>
                                <source src="<?php echo base_url() ?><?php echo VIDEO_UPLOAD?><?php echo $editdata->file_name ?>" type="<?php echo $editdata->file_type; ?>">
                                Your browser does not support the video tag.
                            </video>
                        <?php } ?>
                        <?php echo form_error('video', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>  

                <tr><td valign="top"> <?php echo form_label('Status : ', 'Status', array('class' => 'required')); ?></td>
                    <td>
                        <p class="fl"><?php echo form_radio($activeRadio); ?>&nbsp;&nbsp;<?php echo form_label('Active', 'Active'); ?>&nbsp;&nbsp;</p>
                        <p class="fl"><?php echo form_radio($deactiveRadio); ?>&nbsp;&nbsp;<?php echo form_label('Deactivate', 'Deactivate'); ?>&nbsp;&nbsp;</p>
                        <?php echo form_error('status', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>            

                <tr><td valign="top"> <?php echo form_label('Featured Video : ', 'featured_video', array('class' => 'required')); ?></td>
                    <td>
                        <p class="fl"><?php echo form_radio($yes); ?>&nbsp;&nbsp;<?php echo form_label('Yes', 'Yes'); ?>&nbsp;&nbsp;</p>
                        <p class="fl"><?php echo form_radio($no); ?>&nbsp;&nbsp;<?php echo form_label('No', 'No'); ?>&nbsp;&nbsp;</p>
                        <?php echo form_error('featured_video', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                
                 <tr><td valign="top"> <?php echo form_label('Approved : ', 'Approved', array('class' => 'required')); ?></td>
                    <td>
                        <p class="fl"><?php echo form_radio($approved); ?>&nbsp;&nbsp;<?php echo form_label('Approved', 'Approved'); ?>&nbsp;&nbsp;</p>
                        <p class="fl"><?php echo form_radio($disapproved); ?>&nbsp;&nbsp;<?php echo form_label('Decline', 'Decline'); ?>&nbsp;&nbsp;</p>
                        <?php echo form_error('Approved', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr> 

                <tr id="submit">
                    <td>&nbsp;</td>
                    <td valign="top">
                        <?php echo form_submit($submit); ?>
                    </td>
                    <td></td>
                </tr>
            </table>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<div class="clear"> </div>
