<div class="grid_10">
    <div class="box round first grid">
        <h2>View Videos</h2>
        
<!--//        echo'<pre>';print_r($alldata[0]->category[0]->name);exit;
//    echo'<pre>';print_r($this->$alldata[0]->video[0]->category);exit;-->
       
        
        <?php
        if ($userRights['delete']) {
            $frmaction = 'admin/' . $controller . '/delete';
            $attributes = 'id="frmDelete"';
            echo form_open($frmaction, $attributes);
            ?>
            <p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="btn btn-blue" style="float:left">Delete Rows</a></p>
        <?php } ?>
        <div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
        <div class="block">
            <?php echo $add_error; ?>
            <?php echo $add_warning; ?>
            <?php echo $add_info; ?>
            <?php echo $add_success; ?>
            <?php 
           
          
              
            if (isset($alldata) && !empty($alldata)) { 
              
                ?>
                <table id="product-table" class="data display datatable" width="100%">
                    <thead>
                        <tr>
                            <?php if ($userRights['edit']) { ?>  <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   <?php } ?>
                            <th>Title</th>
                            <th>Category Name</th>
                            <th>Video</th>
                            <th>Status</th>
                            <th>Approved</th>
                            <th>Created Date</th>
                            <th class="sort">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($alldata as $video) {
                          // echo'<pre>';print_r($alldata);exit;
                            ?>
                            <tr class="odd gradeX">
                                <?php if ($userRights['edit']) {?>
                                    <td><?php echo form_checkbox("option[]", $video->id, '', 'class="case"'); ?></td>
                                <?php } ?>
                                <td class="sorting_1"><?php echo $video->file_title; ?></td>
                                <td class="sorting_1"><?php echo isset($alldata[0]->category[0]->name) ? $alldata[0]->category[0]->name :'' ; ?></td>
                                <td class="sorting_1">
                                    <video width="100" height="100" controls>
                                        <source src="<?php echo base_url() ?><?php echo VIDEO_UPLOAD?><?php echo $video->file_name ?>" type="<?php echo $video->file_type; ?>">
                                        <!--<source src="<?php echo base_url() ?>uploads/admin/videos<?php echo $video->file_name ?>" type="video/wmv">-->
                                        <!--<source src="movie.ogg" type="video/ogg">-->
                                        Your browser does not support the video tag.
                                    </video>
                                </td>
                                <td>
                                    <?php
                                    
                                    //echo'<pre>';print_r($alluser->id);exit;
                                    if ($video->status == '1') {
                                        $statusAction = 'javascript:void(0);';
                                        if ($userRights['edit']) {
                                            $statusAction = base_url("admin/uservideo/deactivate/" . $video->id ."/" .$alluser->id );
                                        }
                                        ?>
                                        <a href="<?php echo $statusAction ?>" id="deactive" title="Active" class="btn-mini btn-black btn-active"><span></span>Active</a> 
                                        <?php
                                    } else {
                                        $statusAction = '';
                                        $statusAction = 'javascript:void(0);';
                                        if ($userRights['edit']) {
                                            $statusAction = base_url("admin/uservideo/activate/" . $video->id ."/" .$alluser->id );
                                        }
                                        ?>
                                        <a href="<?php echo $statusAction; ?>" id="active_priori" title="De-active" class="btn-mini btn-black btn-deactive"><span></span>De-active</a>                              
                                    <?php } ?>
                                </td>
                                
                                <td>
                                    <?php
                                    if ($video->approved == '1') {
                                        $statusAction = 'javascript:void(0);';
                                        if ($userRights['edit']) {
                                            $statusAction = base_url("admin/uservideo/videos_disapproved/" . $video->id ."/" .$alluser->id);
                                        }
                                        ?>
                                        <a href="<?php echo $statusAction ?>" id="deactive" title="Approved" class="btn-mini btn-black btn-active"><span></span>Arroved</a> 
                                        <?php
                                    } else {
                                        $statusAction = '';
                                        $statusAction = 'javascript:void(0);';
                                        if ($userRights['edit']) {
                                            $statusAction = base_url("admin/uservideo/videos_approved/" . $video->id ."/" .$alluser->id);
                                        }
                                        ?>
                                        <a href="<?php echo $statusAction; ?>" id="active_priori" title="Disapproved" class="btn-mini btn-black btn-deactive"><span></span>disapproved</a>                              
                                    <?php } ?>
                                </td>
                                
                                <td class="center"><?php echo date("d-m-Y", strtotime($video->created)); ?></td>
                                <td class="center">
                                    <?php
                                    if ($userRights['edit']) {
                                        ?>
                                        <a href="<?php echo base_url() ?>admin/uservideo/edit/<?php echo $video->id; ?>/<?php echo $alluser->id ;?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>
                                    <?php } if ($userRights['delete']) { ?>
                                        <a href="javascript:void(0)" onclick="deleteAction(<?php echo $video->id; ?>,'<?php echo $controller; ?>')" title="Delete" class="btn-mini btn-black btn-cross"><span></span>Delete</a>        
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <?php
            } else {
                echo "There is no data to display";
            }
            ?>
        </div>
    </div>
</div>
<div class="clear"></div>