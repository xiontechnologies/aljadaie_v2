<?php
$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue',
);
$reset = array(
    'name' => 'reset',
    'id' => 'reset',
    'value' => 'Reset',
    'class' => 'btn btn-blue'
);
$activate = isset($postdata->status) && ($postdata->status == '1') ? $postdata->status : "checked";
$deactivate = isset($postdata->status) && ($postdata->status == '0') ? $postdata->status : "";
$activeRadio = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '1',
    'class' => 'fancy-radio',
    'checked' => $activate,
);
$deactiveRadio = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '0',
    'class' => 'fancy-radio',
    'checked' => $deactivate,
);
$firstname = array(
    'name' => 'firstname',
    'id' => 'firstname',
    'class' => form_error('firstname') ? 'mini fl' : 'mini',
    'value' => isset($postdata) && isset($postdata->firstname) ? $postdata->firstname : '',
);
$email = array(
    'name' => 'email',
    'id' => 'email',
    'class' => form_error('email') ? 'mini fl' : 'mini',
    'value' => isset($postdata->email) ? $postdata->email : "",
);
$lastname = array(
    'name' => 'lastname',
    'id' => 'lastname',
    'class' => form_error('lastname') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$male = isset($postdata->gender) && ($postdata->gender == 'male') ? $postdata->gender : "checked";
$female = isset($postdata->gender) && ($postdata->gender == 'female') ? $postdata->gender : "";
$male = array(
    'name' => 'gender',
    'id' => 'gender',
    'value' => 'male',
    'class' => 'fancy-radio',
    'checked' => $male,
);
$female = array(
    'name' => 'gender',
    'id' => 'gender',
    'value' => 'female',
    'class' => 'fancy-radio',
    'checked' => $female,
);
$dob = array(
    'id' => 'dob',
    'name' => 'dob',
    'class' => form_error('dob') ? 'mini fl' : 'mini',
    'value' => isset($postdata->dob) ? $postdata->dob : '',
);
$password = array(
    'name' => 'password',
    'id' => 'password',
    'class' => form_error('password') ? 'mini f1' : 'mini'
);
$confirmpassword = array(
    'name' => 'confirmpassword',
    'id' => 'confirmpassword',
    'class' => form_error('confirmpassword') ? 'mini f1' : 'mini'
);
$aboutme = array(
    'name' => 'aboutme',
    'id' => 'aboutme',
    'class' => form_error('aboutme') ? 'mini fl' : 'mini',
    'value' => isset($postdata->aboutme) ? $postdata->aboutme : "",
);
$telephone = array(
    'name' => 'telephone',
    'id' => 'telephone',
    'class' => form_error('telephone') ? 'mini fl' : 'mini',
    'value' => isset($postdata->telephone) ? $postdata->telephone : "",
);
$mobiles = array(
    'name' => 'mobiles',
    'id' => 'mobiles',
    'class' => form_error('mobiles') ? 'mini fl' : 'mini',
    'value' => isset($postdata->mobiles) ? $postdata->mobiles : "",
);
$photo = array(
    'name' => 'userfile',
    'id' => 'userfile',
    'value' => isset($postdata->photo->file_name) ? $postdata->photo->file_name : ""
);
?>
<?php
echo form_open_multipart('admin/members/add', 'id="adduserfrm"');
?>
<h2>Add User</h2>
<div class="block ">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <table class="form">
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Group : ', 'Group', array('class' => 'required')); ?></td>
            <td class="col2">
                <?php
                //echo'<pre>';print_r($group);exit;
                $select = (isset($postdata->group_id) && ($postdata->group_id)) ? $postdata->group_id : '';
                echo form_dropdown('group_id', $group, $select, 'class ="styledselect_form_1 fl" id="admingroup"');
                ?>

                <?php echo form_error('group_id', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>

        <tr>
            <td class="col1" valign="top"> <?php echo form_label('First Name : ', 'firstname', array('class' => 'required')); ?></td>
            <td class="col2">  
                <?php echo form_input($firstname); ?>
                <?php echo form_error('firstname', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Last Name : ', 'lastname', array('class' => 'required')); ?></td>
            <td class="col2">  
                <?php echo form_input($lastname); ?>
                <?php echo form_error('lastname', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Email : ', 'email', array('class' => 'required')); ?></td>
            <td class="col2">  
                <?php echo form_input($email); ?>
                <?php echo form_error('email', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>

        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Password : ', 'password', array('class' => 'required')); ?></td>
            <td class="col2">  
                <?php echo form_password($password); ?>
                <?php echo form_error('password', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Confirm Password : ', 'confirmpassword', array('class' => 'required')); ?></td>
            <td class="col2">  
                <?php echo form_password($confirmpassword); ?>
                <?php echo form_error('confirmpassword', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Date of Birth : ', 'confirmpassword', array('class' => 'required')); ?></td>
            <td class="col2">  
                <?php echo form_input($dob); ?>
                <?php echo form_error('dob', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>  

        <tr><td valign="top"> <?php echo form_label('Status : ', 'Status', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($activeRadio); ?>&nbsp;&nbsp;<?php echo form_label('Active', 'Active'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($deactiveRadio); ?>&nbsp;&nbsp;<?php echo form_label('Deactivate', 'Deactivate'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('status', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr> 
        <tr><td valign="top"> <?php echo form_label('Gender : ', 'gender', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($male); ?>&nbsp;&nbsp;<?php echo form_label('Male', 'Male'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($female); ?>&nbsp;&nbsp;<?php echo form_label('Female', 'Female'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('gender', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr> 

        <tr><td valign="top"> <?php echo form_label('Telephone : ', 'telephone', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($telephone); ?>
                <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Profile Picture : ', 'userfile'); ?></td>
            <td class="col2">  
                <?php echo form_upload($photo); ?>
                <?php echo form_error('userfile', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Mobile : ', 'mobiles'); ?></td>
            <td>
                <?php echo form_input($mobiles); ?>
                <?php echo form_error('mobiles', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('About Me : ', 'aboutme'); ?></td>
            <td>
                <?php echo form_textarea($aboutme); ?>
                <?php echo form_error('aboutme', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
                <?php echo form_reset($reset); ?>
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>

