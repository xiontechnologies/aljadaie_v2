<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo SITE_NAME; ?> | <?php echo $add_title; ?></title>
        <?php
        echo $add_css;
        echo $add_js;
        ?>
    </head>
    <body id="login-bg"> 
        <?php
        $uservalue = (isset($postdata['username']) && $postdata['username'] != '') ? $postdata['username'] : '';
        $userName = array(
            'name' => 'username',
            'class' => 'login-inp',
            'id' => 'username',
            'value' => $uservalue
        );
        $password = array(
            'name' => 'password',
            'class' => 'login-inp',
            'id' => 'password'
        );
        ?>
        <!-- Start: login-holder -->
        <div id="login-holder">
            <!--  start loginbox ................................................................................. -->
            <div id="loginbox">

                <!--  start login-inner -->
                <div id="login-inner">
                    <!-- start logo -->
                    <div id="logo-login" class="centerAll">
                        <a href="<?php echo base_url(); ?>admin/admin"><img src="<?php echo base_url() ?>assets/admin/images/logo.png"  alt="" /></a>
                    </div>
                    <!-- end logo -->
                    <div class="clr">&nbsp;</div>
                    <?php echo form_open('admin', array('name' => 'login')); ?>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <?php echo $add_error; ?>
                        <?php echo $add_warning; ?>
                        <?php echo $add_info; ?>
                        <?php echo $add_success; ?>
                        <tr>
                            <td>
                                <label class="padding20">Enter Email</label>
                                <?php echo form_input($userName); ?>                            
                                <div class="login-error"><?php echo form_error('username'); ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label >Password</label>
                                <?php
                                echo form_password($password);
                                ?>
                                <div class="login-error"><?php echo form_error('password'); ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="Login" class="submit-login"  />
                            </td>
                        </tr>
                    </table>
                    <?php echo form_close(); ?>
                </div>
                <!--  end login-inner -->
                <div class="clear"></div>
                <!--<a href="" class="forgot-pwd">Forgot Password?</a>-->
            </div>
            <!--  end loginbox -->
            <!--  start forgotbox ................................................................................... -->
            <div id="forgotbox">
                <div id="forgotbox-text">Please send us your email and we'll reset your password.</div>
                <!--  start forgot-inner -->
                <div id="forgot-inner">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Email address:</th>
                            <td><input type="text" value=""   class="login-inp" /></td>
                        </tr>
                        <tr>
                            <th> </th>
                            <td><input type="button" class="submit-login"  /></td>
                        </tr>
                    </table>
                </div>
                <!--  end forgot-inner -->
                <div class="clear"></div>
                <a href="" class="back-login">Back to login</a>
            </div>
            <!--  end forgotbox -->
        </div>
        <!-- End: login-holder -->
    </body>
</html>