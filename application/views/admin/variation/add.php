<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$name = array(
    'name' => 'name',
    'id' => 'name',
    'class' => 'mini fl validate[required]',
    'value' => isset($postdata) && isset($postdata->name) ? $postdata->name : ''
);
$active = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '1',
    'class' => 'fancy-radio validate[required]',
    'checked' => (isset($postdata->status) && ($postdata->status == 1) ? TRUE : TRUE)
);
$deactive = array(
    'name' => 'status',
    'id' => 'status',
    'value' => '0',
    'class' => 'fancy-radio validate[required]',
    'checked' => (isset($postdata->status) && ($postdata->status == '0') ? TRUE : FALSE)
);
$submit = array(
    'name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
?>
<?php
if ($action == 'edit') {
    $hidden = array('id' => $postdata->id);
    echo form_open('admin/variations/edit/' . $postdata->id, 'id="addadminfrm"', $hidden);
    ?>
    <h2>Edit Variation</h2>

    <?php
} else {
    echo form_open('admin/variations/add', 'id="addadminfrm"');
    ?>
    <h2>Add Variation</h2>

    <?php
}
?>
<div class="block ">
    <?php
    if (isset($add_error)) {
        echo $add_error;
    }
    ?>
    <?php
    if (isset($add_warning)) {
        echo $add_warning;
    }
    ?>
    <?php
    if (isset($add_info)) {
        echo $add_info;
    }
    ?>
    <?php
    if (isset($add_success)) {
        echo $add_success;
    }
    ?>
    <table class="form">
        <tr><td valign="top"> <?php echo form_label('Name : ', 'Name', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($name); ?>
                <?php echo form_error('name', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Category Name : ', 'Category Name', array('class' => 'required')); ?></td>
            <td>
                <?php
                $extra = "class=validate[required]";
                $category_id = $category;
                $select = ((isset($postdata->category_id) && ($postdata->category_id != '') ? $postdata->category_id : ""));
                echo form_dropdown('category_id', $category_id, $select, $extra);
                ?>
                <?php echo form_error('category_id', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Status : ', 'Status', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($active); ?>&nbsp;&nbsp;<?php echo form_label('Active', 'Active'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($deactive); ?>&nbsp;&nbsp;<?php echo form_label('Deactivate', 'Deactivate'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('status', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>

