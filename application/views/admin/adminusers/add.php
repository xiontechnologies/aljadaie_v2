<?php
$password = array(
    'name' => 'password',
    'id' => 'password',
    'class' => form_error('password') ? 'mini fl' : 'mini'
);
$Cpassword = array(
    'name' => 'Cpassword',
    'id' => 'Cpassword',
    'class' => form_error('Cpassword') ? 'mini fl' : 'mini'
);
$contact_num = array(
    'name' => 'contact_number',
    'id' => 'contact_number',
    'value' => isset($postdata->contact_number) ? $postdata->contact_number : (isset($adminusers->contact_number) ? $adminusers->contact_number : ''),
    'class' => form_error('contact_number') ? 'mini fl' : 'mini'
);
$userName = array(
    'name' => 'username',
    'id' => 'username',
    'value' => isset($postdata->username) ? $postdata->username : (isset($adminusers->username) ? $adminusers->username : ''),
    'readonly' => true,
    'class' => form_error('username') ? 'mini fl' : 'mini'
);
$emailAd = array(
    'name' => 'email',
    'id' => 'email',
    'value' => isset($postdata->email) ? $postdata->email : (isset($adminusers->email) ? $adminusers->email : ''),
    'class' => form_error('email') ? 'mini fl' : 'mini',
);
$image = array(
    'name' => 'photo',
    'id' => 'photo',
    'class' => 'mini fl',
);
$activated = (isset($postdata->activated) && ($postdata->activated == '1')) ? TRUE : $adminusers->activated && ($adminusers->activated == '1') ? TRUE : FALSE;
$deactivated = $adminusers->activated && ($adminusers->activated == '0') ? TRUE : (isset($postdata->activated) && ($postdata->activated == '0')) ? TRUE : TRUE;
$activeRadio = array(
    'name' => 'activated',
    'id' => 'status',
    'value' => '1',
    'checked' => $activated,
//    'checked' => isset($postdata) && isset($postdata->acivated) && ($postdata->acivated == '1') ? TRUE : $adminusers->activated ? TRUE : '',
    'class' => 'fancy-radio'
);
$deactiveRadio = array(
    'name' => 'activated',
    'id' => 'status',
    'value' => '0',
    'checked' => $deactivated,
//    'checked' => isset($postdata) && isset($postdata->acivated) && ($postdata->activated == '0') ? true : !$adminusers->activated ? TRUE : '',
    'class' => 'fancy-radio'
);
$banRadioYes = array(
    'name' => 'ban',
    'id' => 'ban',
    'value' => '1',
    'class' => 'fancy-radio'
);
$banRadioNo = array(
    'name' => 'ban',
    'id' => 'ban',
    'value' => '0',
    'class' => 'fancy-radio'
);
$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
$reset = array(
    'name' => 'reset',
    'id' => 'reset',
    'value' => 'Reset',
    'class' => 'btn btn-blue'
);
?>
<?php
if ($action == 'edit') {
    $hidden = array(
        'name' => 'id',
        'value' => $adminusers->id,
    );
    echo form_open_multipart("admin/edit/$adminusers->id", 'id="addadminfrm"', $hidden);
    ?>
    <h2>Edit Admin</h2>
    <?php
} else {
    echo form_open_multipart('admin/admin/add', 'id="addadminfrm"');
    ?>
    <h2>Add Admin</h2>
<?php } ?>
<div class="block ">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <table class="form">
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Group : ', 'group', array('class' => 'required')); ?></td>
            <td class="col2">
                <?php
                $select = isset($postdata->admingroup_id) ? $postdata->admingroup_id : (isset($adminusers->admingroup_id) ? $adminusers->admingroup_id : '');

                echo form_dropdown('admingroup_id', $admingp, $select, 'class ="styledselect_form_1 fl" id="admingroup"');
                ?>
                <?php echo form_error('admingroup_id', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"><?php echo form_label('Email : ', 'email', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($emailAd); ?>
                <?php echo form_error('email', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Password : ', 'password', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_password($password); ?> 
                <?php echo form_error('password', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Confirm Password : ', 'Confirm Password', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_password($Cpassword); ?>
                <?php echo form_error('Cpassword', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>

        <tr><td valign="top"> <?php echo form_label('Name : ', 'Name', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($userName); ?>
                <?php echo form_error('username', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Contact number : ', 'Contact number', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($contact_num); ?>
                <?php echo form_error('contact_number', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Status : ', 'Status', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($activeRadio); ?>&nbsp;&nbsp;<?php echo form_label('Active', 'Active'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($deactiveRadio); ?>&nbsp;&nbsp;<?php echo form_label('Deactivate', 'Deactivate'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('activated', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Profile image : ', 'image', array('class' => 'lefpad10')); ?></td>
            <td><?php echo form_upload($image); ?>
                <?php if (isset($uploadError) && !empty($uploadError)) { ?>
                    <div id="error"><div class="error-left"></div><div class="error-inner">
                            <?php echo $uploadError; ?>
                        </div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <?php if ($action == 'edit' && $adminusers->image_name) { ?>
            <tr>
                <td valign="top">&nbsp;</td>
                <td><img width="200" height="180" src="<?php echo base_url($adminusers->image_path . '/' . $adminusers->image_name); ?>" title="" /></td>
            </tr>
        <?php } ?>

        <?php if ($action == 'edit' && $this->session->userdata[SITE_NAME . '_admin_user_data']['user_id'] == $adminusers->id) { ?>
        <?php } else { ?>
            <tr>
                <td><?php echo form_input(array('type' => 'button', 'name' => 'check', 'class' => 'btn btn-blue', 'value' => 'Check All', 'id' => 'check')); ?></td>
                <td colspan="1">
                    <table class="rights-tables">
                        <tbody>
                            <tr>
                                <th><?php echo form_label('Module/Rights', 'headers', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Read', 'Read', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Add', 'Add', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Edit', 'Edit', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Delete', 'Delete', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Other', 'Other', array('class' => 'lefpad10')); ?> </th>
    <!--                                <th><?php // echo form_label('Copy Product', 'Copy Product', array('class' => 'lefpad10'));                                                  ?> </th>
                                <th><?php // echo form_label('Download Product', 'Download Product', array('class' => 'lefpad10'));                                                  ?> </th>-->
                                <th><?php echo form_label('Landing Page', 'Landing Page', array('class' => 'lefpad10')); ?> </th>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                            foreach ($allmodules as $k => $v) {
                                $read = $add = $edit = $delete = $sendemail = $copyproduct = $download = $other = '';
                                if ($action == 'edit') {
                                    $userRights = $adminusers->getRights($k);
                                    if ($userRights & READ_MODULE) {
                                        $read = "checked";
                                    }
                                    if ($userRights & ADD_MODULE) {
                                        $add = "checked";
                                    }
                                    if ($userRights & EDIT_MODULE) {
                                        $edit = "checked";
                                    }
                                    if ($userRights & DELETE_MODULE) {
                                        $delete = "checked";
                                    }
                                    if ($userRights & SEND_EMAIL_MODULE || $userRights & COPY_PRODUCT_MODULE || $userRights & DOWNLOAD_MODULE) {
                                        $other = "checked";
                                    }
                                    //if ($userRights & SEND_EMAIL_MODULE) {
//                                        $sendemail = "checked";
//                                    }
//                                    if ($userRights & COPY_PRODUCT_MODULE) {
//                                        $copyproduct = "checked";
//                                    }
//                                    if ($userRights & DOWNLOAD_MODULE) {
//                                        $download = "checked";
//                                    }
                                }
                                if (isset($postdata->rights) && !empty($postdata->rights)) {
                                    if (isset($postdata->rights[$k]) && isset($postdata->rights[$k]['read'])) {
                                        $read = "checked";
                                    }
                                    if (isset($postdata->rights[$k]) && isset($postdata->rights[$k]['add'])) {
                                        $add = "checked";
                                    }
                                    if (isset($postdata->rights[$k]) && isset($postdata->rights[$k]['edit'])) {
                                        $edit = "checked";
                                    }
                                    if (isset($postdata->rights[$k]) && isset($postdata->rights[$k]['delete'])) {
                                        $delete = "checked";
                                    }
                                    if (isset($postdata->rights[$k]) && isset($postdata->rights[$k]['other'])) {
                                        $other = "checked";
                                    }
                                }
                                ?>
                                <tr>
                                    <td><?php echo form_label($v, $v, array('class' => 'lefpad10')); ?></td>
                                    <td><?php echo $k != MYACCOUNT ? form_checkbox("rights[$k][read]", '', $read, 'class="case"') : ''; ?></td>
                                    <td><?php echo $k != MYACCOUNT ? ($k != BULKUPLOAD ? form_checkbox("rights[$k][add]", '', $add, 'class="case"') : '' ) : ''; ?></td>
                                    <td><?php echo $k != MYACCOUNT ? ($k != BULKUPLOAD ? form_checkbox("rights[$k][edit]", '', $edit, 'class="case"') : '') : ''; ?></td>
                                    <td><?php echo $k != MYACCOUNT ? form_checkbox("rights[$k][delete]", '', $delete, 'class="case"') : ''; ?></td>
                                    <td><?php echo $k != MYACCOUNT && $v == 'Admin' || $v == 'Reports' || $v == 'Dbbackups' || $v == 'Newsletters' ? form_checkbox("rights[$k][other]", '', $other, 'class="case"') : ''; ?></td>
        <!--                                    <td><?php // echo $k != MYACCOUNT && $v == 'Newsletters' ? form_checkbox("rights[$k][sendemail]", '', $sendemail, 'class="case"') : '';                                                 ?></td>
                                    <td><?php // echo $k != MYACCOUNT && $v == 'Products' ? form_checkbox("rights[$k][copyproduct]", '', $copyproduct, 'class="case"') : '';                                           ?></td>
                                    <td><?php // echo $k != MYACCOUNT && $v == 'Reports' || $k == 'Dbbackups' ? form_checkbox("rights[$k][download]", '', $download, 'class="case"') : '';                                          ?></td>-->
                                    <?php
                                    $radioCheck = FALSE;
                                    if (isset($landingPage) && !empty($landingPage) && $landingPage == $k) {
                                        $radioCheck = true;
                                    } else if ($k == MYACCOUNT) {
                                        $radioCheck = true;
                                    }
                                    ?>
                                    <td><?php echo $k != BULKUPLOAD ? form_radio("landing", $k, $radioCheck, 'class="case"') : ''; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?php } ?>
        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
                <?php echo form_reset($reset); ?>
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>
