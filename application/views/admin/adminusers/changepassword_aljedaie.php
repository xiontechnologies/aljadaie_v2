<script>
    $(document).ready(function(){
        $("#sumbit").live('click',function(){
            $flag =true;
            if($("#password").val() == ''){
                $flag = false;
                $("#password").removeClass('inp-form');
                $("#password").addClass('inp-form-error');
                $("#passworderror").css('display', 'block');
            }else{
                $("#passworderror").css('display', 'none');
            }
            if($("#conpassword").val() == ''){
                $flag = false;
                $("#conpassword").removeClass('inp-form');
                $("#conpassword").addClass('inp-form-error');
                $("#cpassworderror").css('display', 'block');
            }else{
                $("#cpassworderror").css('display', 'none');
            } 
            if($("#password").val() != $("#conpassword").val()){
                $flag = false;
                $("#conpassword").removeClass('inp-form');
                $("#conpassword").addClass('inp-form-error');
                $("#cpassworderror").css('display', 'block');
            }else{
                $("#cpassworderror").css('display', 'none');
            }
            
            if($flag)
                $("#addadminfrm").submit();
        });
    });
    
</script>
<div id="page-heading"><h1></h1></div>
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
    <tr>
        <th rowspan="3" class="sized"><img src="<?php echo base_url() ?>assets/admin/images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="<?php echo base_url() ?>assets/admin/images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
    </tr>
    <tr>
        <td id="tbl-border-left"></td>
        <td>
            <!--  start content-table-inner -->
            <div id="content-table-inner">

                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr valign="top">
                        <td>
                            <?php echo form_open('admin/user/changepassword/', 'id=addadminfrm', $hidden = array('mid' => $mid)); ?>
                            <!-- start id-form -->
                            <table border="0" cellpadding="0" cellspacing="0"  id="id-form">                                
                                <tr>
                                    <th valign="top">Password : <label>*</label></th>
                                    <td> <input type="password" name="password" id="password" class="inp-form" /></td>                                  
                                    <td id="passworderror" style="display:none" >

                                        <div class="error-left"></div>
                                        <div class="error-inner">This field is required.</div></td>
                                </tr>
                                <tr>
                                    <th valign="top">Conform Password: <label>*</label></th>
                                    <td><input type="password" name="conpassword" id="conpassword" class="inp-form" /></td>                                               
                                    <td id="cpassworderror" style="display:none" >                                    
                                        <div class="error-left"></div>
                                        <div class="error-inner">This field is required.</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td valign="top">
                                        <input type="button" id="sumbit" value="submit" class="form-submit" />
                                        <input type="reset" value="reset" class="form-reset"  />
                                    </td>
                                    <td></td>
                                </tr>
                            </table>                            

                            <!-- end id-form  -->
                            <?php echo form_close(); ?>
                        </td>

                    </tr>
                    <tr>
                        <td><img src="<?php echo base_url() ?>assets/admin/images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                        <td></td>
                    </tr>
                </table>

                <div class="clear"></div>


            </div>
            <!--  end content-table-inner  -->
        </td>
        <td id="tbl-border-right"></td>
    </tr>
    <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
    </tr>
</table>