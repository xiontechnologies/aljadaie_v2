<?php
$class = '';
if ($action != 'edit') {
    $class = 'validate[required]';
}
$password = array(
    'name' => 'password',
    'id' => 'password',
    'class' => 'mini fl ' . $class
);
if ($action != 'edit') {
    $class = 'validate[required,matches[password]]';
}
$Cpassword = array(
    'name' => 'Cpassword',
    'id' => 'Cpassword',
    'class' => 'mini fl ' . $class
);
$contact_num = array(
    'name' => 'contact_number',
    'id' => 'contact_number',
    'value' => $adminusers->contact_number,
    'class' => 'mini fl validate[required,custom[onlyNumberSp],maxSize[10]]'
);
$userName = array(
    'name' => 'username',
    'id' => 'username',
    'value' => $adminusers->username,
    'class' => 'mini fl validate[required]'
);
$emailAd = array(
    'name' => 'email',
    'id' => 'email',
    'value' => $adminusers->email,
//    'class' => form_error('email') ? 'mini fl validate[required,custom[email]]' : 'mini',
    'class' => 'mini fl validate[required,custom[email]]',
);
$image = array(
    'name' => 'photo',
    'id' => 'photo',
    'class' => 'mini fl',
);
$activeRadio = array(
    'name' => 'activated',
    'id' => 'status',
    'value' => '1',
    'checked' => $adminusers->activated ? TRUE : '',
    'class' => 'fancy-radio'
);
$deactiveRadio = array(
    'name' => 'activated',
    'id' => 'status',
    'value' => '0',
    'checked' => !$adminusers->activated ? TRUE : '',
    'class' => 'fancy-radio'
);
$banRadioYes = array(
    'name' => 'ban',
    'id' => 'ban',
    'value' => '1',
    'class' => 'fancy-radio'
);
$banRadioNo = array(
    'name' => 'ban',
    'id' => 'ban',
    'value' => '0',
    'class' => 'fancy-radio'
);
$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
?>
<?php
if ($action == 'edit') {
    $hidden = array(
        'name' => 'id',
        'value' => $adminusers->id,
    );
    echo form_open_multipart("admin/edit/$adminusers->id", 'id="addadminfrm"', $hidden);
    ?>
    <h2>Edit Admin</h2>
    <?php
} else {
    echo form_open_multipart('admin/add', 'id="addadminfrm"');
    ?>
    <h2>Add Admin</h2>
<?php } ?>
<div class="block ">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <table class="form">
        <tr>
            <td class="col1" valign="top"> <?php echo form_label('Group : ', 'group', array('class' => 'required')); ?></td>
            <td class="col2">
                <?php echo form_dropdown('admingroup_id', $admingp, $adminusers->admingroup_id, 'class ="styledselect_form_1 fl validate[required]" id="admingroup"'); ?>
                <?php echo form_error('admingroup_id', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"><?php echo form_label('Email : ', 'email', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($emailAd); ?>
                <?php echo form_error('email', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Password : ', 'password', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_password($password); ?> 
                <?php echo form_error('password', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Confirm Password : ', 'Confirm Password', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_password($Cpassword); ?>
                <?php echo form_error('Cpassword', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>

        <tr><td valign="top"> <?php echo form_label('Name : ', 'Name', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($userName); ?>
                <?php echo form_error('username', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Contact number : ', 'Contact number', array('class' => 'required')); ?></td>
            <td>
                <?php echo form_input($contact_num); ?>
                <?php echo form_error('contact_number', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr><td valign="top"> <?php echo form_label('Status : ', 'Status', array('class' => 'required')); ?></td>
            <td>
                <p class="fl"><?php echo form_radio($activeRadio); ?>&nbsp;&nbsp;<?php echo form_label('Active', 'Active'); ?>&nbsp;&nbsp;</p>
                <p class="fl"><?php echo form_radio($deactiveRadio); ?>&nbsp;&nbsp;<?php echo form_label('Deactivate', 'Deactivate'); ?>&nbsp;&nbsp;</p>
                <?php echo form_error('activated', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
            </td>
        </tr>
        <tr>
            <td valign="top"> <?php echo form_label('Profile image : ', 'image', array('class' => 'lefpad10')); ?></td>
            <td><?php echo form_upload($image); ?>
                <?php if (isset($uploadError) && !empty($uploadError)) { ?>
                    <div id="error"><div class="error-left"></div><div class="error-inner">
                            <?php echo $uploadError; ?>
                        </div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <?php if ($action == 'edit' && $adminusers->image_name) { ?>
            <tr>
                <td valign="top">&nbsp;</td>
                <td><img width="200" height="180" src="<?php echo base_url($adminusers->image_path . '/' . $adminusers->image_name); ?>" title="" /></td>
            </tr>
        <?php } ?>

        <?php if ($action == 'edit' && $this->session->userdata[SITE_NAME . '_admin_user_data']['user_id'] == $adminusers->id) { ?>
        <?php } else { ?>
            <tr>
                <td><?php echo form_input(array('type' => 'button', 'name' => 'check', 'class' => 'btn btn-blue', 'value' => 'Check All', 'id' => 'check')); ?></td>
                <td colspan="1">
                    <table class="rights-tables">
                        <tbody>
                            <tr>
                                <th><?php echo form_label('Module/Rights', 'headers', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Read', 'Read', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Add', 'Add', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Edit', 'Edit', array('class' => 'lefpad10')); ?> </th>
                                <th><?php echo form_label('Delete', 'Delete', array('class' => 'lefpad10')); ?> </th>
                            </tr>
                            <?php
                            foreach ($allmodules as $k => $v) {
                                $read = $add = $edit = $delete = '';
                                if ($action == 'edit') {
                                    $userRights = $adminusers->getRights($k);
                                    if ($userRights & READ_MODULE) {
                                        $read = "checked";
                                    }
                                    if ($userRights & ADD_MODULE) {
                                        $add = "checked";
                                    }
                                    if ($userRights & EDIT_MODULE) {
                                        $edit = "checked";
                                    }
                                    if ($userRights & DELETE_MODULE) {
                                        $delete = "checked";
                                    }
                                }
                                ?>
                                <tr>
                                    <td><?php echo form_label($v, $v, array('class' => 'lefpad10')); ?></td>
                                    <td><?php echo form_checkbox("rights[$k][read]", '', $read, 'class="case"'); ?></td>
                                    <td><?php echo form_checkbox("rights[$k][add]", '', $add, 'class="case"'); ?></td>
                                    <td><?php echo form_checkbox("rights[$k][edit]", '', $edit, 'class="case"'); ?></td>
                                    <td><?php echo form_checkbox("rights[$k][delete]", '', $delete, 'class="case"'); ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?php }
        ?>
        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <?php echo form_submit($submit); ?>
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>

