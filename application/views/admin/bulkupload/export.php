<?php

/*
 * First need to creat header for all data
 * as product column is fixed so we can used that first
 * then marge category column
 */
$productCol = array(
    'category',
    'sku',
    'colorcode',
    'title',
    'description',
    'shortdescription',
    'price',
    'discountprice',
    'weight',
    'stock',
    'washcare',
    'tags',
    'metakeywords',
    'designname',
    'imagename'
);

$categoryCol = array();
if (isset($category) && !empty($category)) {
    foreach ($category as $cat => $catv)
        $categoryCol[$catv->name] = $catv->name;
}

$columns = array_merge($productCol, $categoryCol);
$col = '';
foreach ($columns as $ck => $cv) {
    $col.= $cv . "\t";
}
//header("Content-Type: application/vnd.ms-excel");
echo $col;
foreach ($products as $pk => $pv) {
    echo "\n";
    echo $pv->section->name . "\t";
    echo $pv->sku . "\t";
    echo $pv->colorcode . "\t";
    echo $pv->title . "\t";
    echo $pv->description . "\t";
    echo $pv->shortdescription . "\t";
    echo $pv->price . "\t";
    echo $pv->discountprice . "\t";
    echo $pv->weight . "\t";
    echo $pv->stock . "\t";
    echo $pv->washcare . "\t";
    echo $pv->tags . "\t";
    echo $pv->metakeywords . "\t";
    /*
     * Product table end here
     */
    echo ($pv->design[0]->file_name) . "\t";
    echo ($pv->photo->file_name) . "\t";
    /*
     * design and image table end here
     */
    foreach ($pv->productvariation as $variation) {
        if (isset($arr[$variation->subcategory->category->name]) && !empty($arr[$variation->subcategory->category->name])) {
            $arr[$variation->subcategory->category->name] = $arr[$variation->subcategory->category->name] . ' , ' . $variation->subcategory->name;
        } else {
            $arr[$variation->subcategory->category->name] = $variation->subcategory->name;
        }
    }
    foreach ($categoryCol as $catpk => $catpv) {
        if (isset($arr[$catpv]) && !empty($arr[$catpv])) {
            echo $arr[$catpv] . "\t";
        } else {
            echo "\t";
        }
    }
}
$total = 0;
header('Content-Description: File Transfer');
header('Content-Type: application/vnd.ms-excel');
header('Content-Transfer-Encoding: binary');
header("Content-disposition: attachment; filename=spreadsheet");
?>