<?php
/*
 * @auther Shafiq
 */
echo form_open_multipart('admin/bulkupload/import', 'id=uploadfile');
$data = array(
    'name' => 'userfile',
    'id' => 'userfile'
);
?>
<h2>Bulk Upload</h2>
<?php if ($userRights['add']) { ?>
    <div class="block ">
        <?php echo $add_error; ?>
        <?php echo $add_warning; ?>
        <?php echo $add_info; ?>
        <?php echo $add_success; ?>
        <table class="form">
            <tr><td valign="top"> <?php echo form_label('Upload data : ', 'Upload data', array('class' => 'required')); ?></td>
                <td>
                    <?php echo form_upload($data);
                    ?>
                </td>
                <td valign="top">
                    <?php echo form_submit('SUBMIT', 'SUBMIT');
                    ?>
                </td>
                <td valign="top">
                    <a href="<?php echo base_url('admin/bulkupload/export'); ?>"> Export</a>
                </td>
            </tr>
        </table>
        <?php echo form_close(); ?>
    </div>
<?php } ?>
<table id="product-table" class="data display datatable" width="100%">
    <thead>
        <tr>

            <th>Category</th>
            <th>Sku</th>
            <th>Color code</th>
            <th>Name</th>
            <th>Price</th>
            <th>Stock </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($products as $prodk => $prodv) { ?>
            <tr class="odd gradeX">
                <td class="sorting_1"><?php echo $prodv->section->name; ?></td>
                <td class="sorting_1"><?php echo $prodv->colorcode; ?></td>
                <td class="sorting_1"><?php echo $prodv->sku; ?></td>
                <td class="sorting_1"><?php echo $prodv->title; ?></td>
                <td class="sorting_1"><?php echo $prodv->price; ?></td>
                <td class="sorting_1"><?php echo $prodv->stock; ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

