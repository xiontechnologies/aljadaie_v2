<h2>View Content</h2>
<?php if ($userRights['add']) { ?>
    <p class="fl "><br /><a href = "<?php echo base_url() ?>admin/contents/add" class="btn btn-blue" style="float:left">Add Content</a></p>
    <?php
}
?>
<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
<div class="block">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <?php if (isset($alldata) && !empty($alldata)) { ?>
        <table id="product-table" class="data display datatable" width="100%">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Short Description</th>
                    <th>Status</th>
                    <th class="sort">Created Date</th>
                    <?php if ($userRights['edit'] || $userRights['delete']) { ?><th class="sort">Action</th><?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($alldata as $user) {
                    ?>
                    <tr class="odd gradeX">
                        <td class="sorting_1"><?php echo $user->title; ?></td>
                        <td class="sorting_1"><?php echo substr($user->short_desc, 0, 50); ?></td>
                        <td>
                            <?php
                            if ($user->status == '1') {
                                $statusAction = 'javascript:void(0);';
                                if ($userRights['edit']) {
                                    $statusAction = base_url("admin/contents/deactivate/" . $user->id);
                                }
                                ?>
                                <a href="<?php echo $statusAction ?>" id="deactive" title="Active" class="btn-mini btn-black btn-active"><span></span>Active</a> 
                                <?php
                            } else {
                                $statusAction = '';
                                $statusAction = 'javascript:void(0);';
                                if ($userRights['edit']) {
                                    $statusAction = base_url("admin/contents/activate/" . $user->id);
                                }
                                ?>
                                <a href="<?php echo $statusAction; ?>" id="active_priori" title="De-active" class="btn-mini btn-black btn-deactive"><span></span>De-active</a>                              
                            <?php } ?>
                        </td>
                        <td class="center"><?php echo date("d-m-Y", strtotime($user->created)); ?></td>
                        <?php if ($userRights['edit'] || $userRights['delete']) { ?><td class="center">
                            <?php
                            if ($userRights['edit']) {
                                ?>
                                    <a href="<?php echo base_url() ?>admin/contents/edit/<?php echo $user->id; ?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>
                                    <?php
                                }
                                ?>
                            </td><?php } ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    } else {
        echo "There is no data to display";
    }
    ?>
</div>
