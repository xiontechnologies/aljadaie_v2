<?php
//echo'<pre>';print_r($postdata);exit;
$title = array('name' => 'title',
    'id' => 'title',
    'value' => isset($postdata->title) ? $postdata->title : ((isset($editdata->title)) ? $editdata->title : ((isset($postdata['title'])) ? $postdata['title'] : '')),
    'class' => 'mini');
$short_desc = array('name' => 'short_desc',
    'id' => 'short_desc',
    'value' => isset($postdata->short_desc) ? $postdata->short_desc : ((isset($editdata->short_desc)) ? $editdata->short_desc : ((isset($postdata['short_desc'])) ? $postdata['short_desc'] : '')),
    'style' => 'vertical-align:top;',
    'class' => 'tinymce',
    'rows' => '6',
    'cols' => '40');
$long_desc = isset($postdata->long_desc) ? $postdata->long_desc : ((isset($editdata->long_desc)) ? $editdata->long_desc : ((isset($postdata['long_desc'])) ? $postdata['long_desc'] : ''));
?>
<h2>Edit Content</h2>
<div class="block ">
    <flag style="color:red"><?php echo (isset($img_error)) ? $img_error : ''; ?></flag>
    <?php if ($this->session->flashdata('error_message')) { ?> <div class="message error"><h5>Error!</h5><p> <?php echo $this->session->flashdata('error_message'); ?></p></div><?php } ?>
    <?php echo form_open_multipart('admin/contents/edit/' . $editdata->id, 'id="addcmsfrm"'); ?>
    <table class="form">
        <tr>
            <td valign="top">
                <?php echo form_label('Page Title'); ?><label style="color:red;">*</label> 
            </td>
            <td>
                <?php echo form_input($title); ?>
                <?php if (form_error('title')) { ?>
                    <div id="error" style="display: <?php echo (form_error('title') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('title') != '') ? form_error('title') : 'This field is required.'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <?php echo form_label('Short Description'); ?><label style="color:red;">*</label> 
            </td>
            <td>
                <!--<textarea id="short_desc" name="short_desc"></textarea>-->
                <?php echo form_textarea($short_desc); ?>
                <?php if (form_error('short_desc')) { ?>
                    <div id="error" style="display: <?php echo (form_error('short_desc') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('short_desc') != '') ? form_error('short_desc') : 'This field is required.'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <?php echo form_label('Long Description'); ?>
            </td>
            <td>
                <textarea name="long_desc" id="long_desc" style="width:100%"><?php echo $long_desc; ?></textarea>
                <?php // form_textarea($long_desc) ?>
            </td>
        </tr>

        <?php
        $activechecked = isset($postdata->status) && $postdata->status == 1 ? true : isset($editdata->status) && $editdata->status == 1 ? true : '';
        $deacstatus = isset($postdata->status) && $postdata->status == 0 ? true : isset($editdata->status) && $editdata->status == 0 ? true : '';
        ?>
        <tr>
            <td valign="top">
                <?php echo form_label('Status'); ?><label style="color:red;">*</label> 
            </td>
            <td>
                <?php echo form_radio('status', '1', $activechecked); ?> Active
                <?php echo form_radio('status', '0', $deacstatus); ?> Deactive
            </td>
            <td><?php echo form_error('status', '<span style="font-size:10px; color=red; padding:0px 0px 0px 8px >', '</span>'); ?></td>
        </tr>
        <?php
        $positionTop = isset($postdata->position) && $postdata->position == 0 ? true : isset($editdata->position) && $editdata->position == 0 ? true : '';
        $positionBottom = isset($postdata->position) && $postdata->position == 1 ? true : isset($editdata->position) && $editdata->position == 1 ? true : '';
//                if ($editdata->position == '0') {
//                    $positionTop = TRUE;
//                    $positionBottom = FALSE;
//                } else {
//                    $positionTop = FALSE;
//                    $positionBottom = TRUE;
//                }
        ?>
        <tr>
            <td valign="top">
                <?php echo form_label('Position'); ?><label style="color:red;">*</label> 
            </td>
            <td>
                <?php echo form_radio('position', '0', $positionTop); ?> Top
                <?php echo form_radio('position', '1', $positionBottom); ?> Footer
            </td>
            <td><?php echo form_error('position', '<span style="font-size:10px; color=red; padding:0px 0px 0px 8px >', '</span>'); ?></td>
        </tr>

        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <input type="submit" id="sumbit" value="Submit" class="btn btn-blue" />
                <input type="reset" value="reset" class="btn btn-blue"  />
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>
