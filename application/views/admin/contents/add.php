<?php
//echo '<pre>';print_r($postdata);exit;
$title = array('name' => 'title',
    'id' => 'title',
    'value' => isset($postdata->title) ? $postdata->title : "",
    'class' => 'mini');
$short_desc = array('name' => 'short_desc',
    'id' => 'short_desc',
    'value' => (isset($postdata->short_desc)) ? $postdata->short_desc : "",
    'style' => 'vertical-align:top;',
    'rows' => '6',
    'cols' => '40');
$long_desc = isset($postdata->long_desc) ? $postdata->long_desc : "";

$active = array(
    'name' => 'status',
    'id' => 'active',
    'value' => "1",
    'checked' => (isset($postdata->status) && ($postdata->status == '1')) ? TRUE : isset($editdata->status) && ($editdata->status == '1') ? TRUE : True,
    'class' => 'fancy-radio'
);
$deactive = array(
    'name' => 'status',
    'id' => 'deactive',
    'value' => "0",
    'checked' => (isset($postdata->status) && ($postdata->status == '0')) ? TRUE : isset($editdata->status) && ($editdata->status == '0') ? TRUE : FALSE,
    'class' => 'fancy-radio'
);
?>
<h2>Add Content</h2>
<div class="block ">

    <?php
    if ($userRights == 'edit') {
        echo form_open_multipart("admin/contents/edit", "id=addcontents");
    }
    echo form_open_multipart('admin/contents/add', 'id="addcontents"');
    ?>
    <table class="form">
        <tr>
            <td valign="top">
                <?php echo form_label('Page Title'); ?><label style="color:red;">*</label> 
            </td>
            <td>
                <?php echo form_input($title); ?>
                <?php if (form_error('title')) { ?>
                    <div id="error" style="display: <?php echo (form_error('title') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('title') != '') ? form_error('title') : 'This field is required.'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <?php echo form_label('Short Description'); ?><label style="color:red;">*</label> 
            </td>
            <td>
                <?php echo form_textarea($short_desc); ?>
                <?php if (form_error('short_desc')) { ?>
                    <div id="error" style="display: <?php echo (form_error('short_desc') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('short_desc') != '') ? form_error('short_desc') : 'This field is required.'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <?php echo form_label('Long Description'); ?>
            </td>
            <td>
                <textarea name="long_desc" id="long_desc" style="width:100%"><?php echo $long_desc; ?></textarea>
                <?php // form_textarea($long_desc)    ?>
                <?php if (form_error('long_desc')) { ?>
                    <div id="error" style="display: <?php echo (form_error('long_desc') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('long_desc') != '') ? form_error('long_desc') : 'This field is required.'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td class="col1" valign="top">
                <?php echo form_label('Status'); ?><label style="color:red;">*</label>
            </td>
            <td class="col2">
                <?php echo form_radio($active); ?>&nbsp;&nbsp;<?php echo "Active"; ?>
                <?php echo form_radio($deactive); ?>&nbsp;&nbsp;<?php echo "Deactive"; ?>
                <?php if (form_error('status')) { ?>
                    <div id="error" style="display: <?php echo (form_error('status') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('status') != '') ? form_error('status') : 'This field is required.'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td class="col1" valign="top">
                <?php echo form_label('Position'); ?><label style="color:red;">*</label>
            </td>
            <td class="col2">
                <?php
                $top = (isset($postdata->position) && ($postdata->position == '0')) ? TRUE : isset($editdata->position) && ($editdata->position == '0') ? TRUE : True;
                $footer = (isset($postdata->position) && ($postdata->position == '1')) ? TRUE : isset($editdata->position) && ($editdata->position == '1') ? TRUE : FALSE;
                ?>
                <?php echo form_radio('position', '0', $top); ?>&nbsp;&nbsp;<?php echo "Top"; ?>
                <?php echo form_radio('position', '1', $footer); ?>&nbsp;&nbsp;<?php echo "Footer"; ?>
                <?php if (form_error('position')) { ?>
                    <div id="error" style="display: <?php echo (form_error('position') != '') ? 'block' : 'none'; ?>">
                        <div class="error-left"></div>
                        <div class="error-inner"><?php echo (form_error('position') != '') ? form_error('position') : 'This field is required.'; ?></div>
                    </div>
                <?php } ?>
            </td>
        </tr>
        <tr id="submit">
            <td>&nbsp;</td>
            <td valign="top">
                <input type="submit" id="sumbit" value="Submit" class="btn btn-blue" />
                <input type="reset" value="reset" class="btn btn-blue"  />
            </td>
            <td></td>
        </tr>
    </table>
    <?php echo form_close(); ?>
</div>
