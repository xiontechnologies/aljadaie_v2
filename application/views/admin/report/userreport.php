<?php
header("Content-Type: application/vnd.ms-excel");
echo 'Order Date' . "\t" . 'Order Number' . "\t" . 'Order Status' . "\t" . 'User Name' . "\t" . 'Total Amount' . "\t" . 'Product Name' . "\t" . 'Sku' . "\t" . 'Price' .  "\n";
if (isset($order) && !empty($order)) {
    echo "\n";
    echo date("d-m-y", strtotime($order->created_date)) . "\t";
    echo $order->id . "\t";
    echo $order->order_status . "\t";
    echo $order->first_name . " " . $order->last_name . "\t";
    echo $order->total_amt . "\t";
    if ($order->orderdetail) {
        foreach ($order->orderdetail as $pk => $pv) {
            if ($pk) {
                echo "\n";
                echo "\t";
                echo "\t";
                echo "\t";
                echo "\t";
                echo "\t";
               }
            echo $pv->product->title . "\t";
            echo $pv->product->sku . "\t";
            echo $pv->product->price . "\t";
            echo "\t";
            if ($pk) {
                echo "\n";
            }
        }
    }

    echo "\n" ;
    echo "\t";
    echo "\t";
    echo "\t";
    echo "\t";
    echo "\t";
    echo "\t";
    echo "\t";


    echo "\n";
} else {
    echo 'no data found';
}

header("Content-disposition: attachment; filename=spreadsheet.xls");
?>