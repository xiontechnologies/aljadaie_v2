
<h2>View Reports</h2>      
<?php

$username = array(
    'name' => 'username',
    'id' => 'username',
//    'value' => isset($postdata) && !empty($postdata->username) ? $postdata->username : "",
    'value' => '',
    'class' => 'mini'
);
$from_date = array(
    'name' => 'from_date',
    'id' => 'from_date',
//    'value' => isset($postdata) && !empty($postdata->from_date) ? $postdata->from_date : "",
    'value' => '',
    'class' => 'mini'
);
$to_date = array(
    'name' => 'to_date',
    'id' => 'to_date',
//    'value' => isset($postdata) && !empty($postdata->to_date) ? $postdata->to_date : "",
    'value' => '',
    'class' => 'mini'
);
$submit = array(
    'name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue'
);
?>
<?php echo form_open('admin/reports/index', 'id="addadminfrm"') ?>
<table class="form">
    <tr><td valign="top"> <?php echo form_label('From Date : ', 'From Date'); ?></td>
        <td>
            <?php echo form_input($from_date); ?>
        </td>
        <td valign="top"> <?php echo form_label('To Date : ', 'To Date'); ?></td>
        <td>
            <?php echo form_input($to_date); ?>
        </td>
        <td valign="top"> <?php echo form_label('User Name : ', 'User Name'); ?></td>
        <td>
            <?php echo form_input($username); ?>
        </td>
        <td>&nbsp;</td>
        <td valign="top">
            <?php echo form_submit($submit); ?>
        </td>
    <input type="hidden" name="fromdate" id="fromdate" value="<?php echo isset($postdata) && !empty($postdata->from_date) ? $postdata->from_date : "" ?>"/>
    <input type="hidden" name="todate" id="todate" value="<?php echo isset($postdata) && !empty($postdata->to_date) ? $postdata->to_date : "" ?>"/>
    <input type="hidden" name="user_name" id="user_name" value="<?php echo isset($postdata) && !empty($postdata->username) ? $postdata->username : "" ?>"/>
</tr>
</table>
<?php echo form_close() ?>
<?php if (isset($allorders) && count($allorders) > 0) { ?>
    <p class="fl marginleft10"><br /><button class="btn btn-blue" style="float:left" id="downloadreport">Download</button></p>
<?php } ?>

<div style="clear:both; visiblity:hidden; height:1px">&nbsp;</div>
<table class="form">
    <?php if (isset($postdata) && !empty($postdata->from_date)) { ?>
        <tr><td valign="top"> <?php echo form_label('From Date : ', 'From Date'); ?></td>
            <td>
                <?php echo $postdata->from_date; ?>
            </td>
        </tr>
    <?php } ?>
    <?php if (isset($postdata) && !empty($postdata->to_date)) { ?>
        <tr>
            <td valign="top"> <?php echo form_label('To Date : ', 'To Date'); ?></td>
            <td>
                <?php echo $postdata->to_date; ?>
            </td>
        </tr>
    <?php } ?>
    <?php if (isset($postdata) && !empty($postdata->username)) { ?>
        <tr>
            <td valign="top"> <?php echo form_label('User Name : ', 'User Name'); ?></td>
            <td>
                <?php echo ($postdata->username); ?>
            </td>
        </tr>
    <?php } ?>
</table>
<div class="block">
    <?php echo $add_error; ?>
    <?php echo $add_warning; ?>
    <?php echo $add_info; ?>
    <?php echo $add_success; ?>
    <?php if (isset($allorders) && count($allorders) > 0) {
        ?>
        <table id="product-table" class="data display datatable" width="100%">
            <thead>
                <tr>
                    <th>User Name</th>
                    <th>Order Date</th>
                    <th>Order Status</th>
                    <th>Total Amount</th>
                    <th class="sort">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allorders as $order) { ?>
                    <tr class="odd gradeX">

                        <td class="sorting_1"><?php echo $order->first_name; ?></td>
                        <td class="center"><?php echo date("d-m-Y", strtotime($order->created_date)); ?></td>
                        <td class="sorting_1"><?php echo $order->order_status; ?></td>
                        <td class="center"><?php echo $order->total_amt; ?></td>
                        <td class="center">
                            <?php // if ($userRights['edit']) { ?>
                                <!--<a href="<?php // echo base_url() ?>admin/reports/edit/<?php // echo $order->id; ?>" title="Edit" class="btn-mini btn-black btn-edit"><span></span>Edit</a>-->
                                <a href="<?php echo base_url() ?>admin/reports/userdownloadproducts/<?php echo $order->id ?>">download product</a>           
                                <?php
//                            }
                        }
                        ?>



                        <div>&nbsp;</div>
                    </td>
                </tr>

            </tbody>
        </table>
        <?php
    } else {
        echo "There is no data to display";
    }
    ?>
</div>
