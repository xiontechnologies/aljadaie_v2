<?php
if ($this->session->userdata(SITE_NAME . '_story_board')) {
    $temp = json_decode($this->session->userdata(SITE_NAME . '_story_board'));
    $temp = array_reverse($temp);
    foreach ($temp as $k => $v) {
        ?>
        <div class="item story-count" > <!--listing listing-img-->
            <a href="javascript:void(0)" class="delete-storyboard" data="<?php echo $k; ?>" >
            <div class="color-circle "><img src="<?php echo base_url();?>assets/frontend/images/close.png"></div>
                <img src="<?php echo $v->canvasUrl; ?>" class="img-responsive width-100">
            </a>
        </div>

        <?php
    }
} else {
    
}
?>