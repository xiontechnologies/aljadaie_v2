<?php
/*
 * @auther Shafiq
 */

if (isset($product) && !empty($product)) {
    //echo '<pre>'; print_r($product);
    foreach ($product as $pk => $pvalue) {


        /*
         * Post is taken when some input is checked in filter and result come from variation
         * if empty then all products will show there and result come from products
         */
        if ( isset($post) && $post )
            $pv = $pvalue->product;
        else
            $pv = $pvalue;
        
        $selected = ''; $selected1='';
        $cartselected = '';
        $carthide = '';
        $cartselected1 = '';
        if ($pk == 5)
            $hide = "style='display:block'";
        if ($this->session->userdata(SITE_NAME . '_user_catalog') && array_key_exists($pv->id, $this->session->userdata(SITE_NAME . '_user_catalog'))) {
            $selected = 'selected';
            $selected1 = 'selected1';
        }
        if ($this->session->userdata(SITE_NAME . '_productIds') && in_array($pv->id, $this->session->userdata(SITE_NAME . '_productIds'))) {
            $cartselected = 'selected';
            $carthide = 'cart-hide';
            $cartselected1 = 'selected1';
        }
        ?>

        <div class="col-md-3 col-sm-4 col-xs-12 fabric-box">
            <!--<h3 class="prd-title" style="display:none"><?php  //echo $pv->sku; ?></h3>-->
            <!--<p class="list-info" style="display:none"><?php //echo $pv->shortdescription; ?></p>-->
            <?php
            if(gettype($pv) == 'object'){
               $userdata = $this->session->userdata(SITE_NAME . '_user_data');
                if ($userdata) {
                    if ($pv->get_frv($userdata['user_id'], $pv->id)) { ?>
            <button class="btn btn-default btn-disabled" data="<?php echo $pv->id; ?>" ><img src="<?php echo base_url(); ?>assets/frontend/images/heart-red-full.png"></button>
               <?php   }else{ ?>
                        <button class="btn btn-default fab-fav" data="<?php echo $pv->id; ?>" ><span class="icon-heart" aria-hidden="true"></span></button>
         <?php          }
                }
                }else{
?>   <button class="btn btn-default fab-fav" data="<?php echo $pv->id; ?>" ><span class="icon-heart" aria-hidden="true"></span></button>
            <?php } ?>
           
            <span class="qty-container <?php echo $carthide; ?>" id="qty-container<?php echo $pv->id; ?>"><input class="form-control cart-value-sm qty1 " type="text" placeholder="1" value="1" id="qty<?php echo $pv->id; ?>" data="<?php echo $pv->stock; ?>"></span>
            <div class="product-info-grid">
                <ul>
                    <li><a href="javascript:void(0)" title="PRODUCT INFORMATION" class="info-fab" data="<?php echo $pv->id; ?>" >
                            <span  aria-hidden="true"title="" class="icon-info "></span></a>
                        
                    </li>

                    <li ><a href="javascript:void(0)" class="pro-color color-fab" title="AVAILABLE COLORS" data="<?php echo $pv->id; ?>" style="color:white" >

                        <h4> <?php echo '+' . count($pv->colors); ?></h4><p>colors</p></a>
                    </li>

                    <?php if ($selected) { ?>
                    <li><a href="#" class="catlg-icon1 <?php echo $selected; ?>" data="<?php echo $pv->id; ?>" aria-hidden="true"><span class="icon-wallet <?php echo $selected1; ?>"  ></span></a></li>
                    <?php } else { ?>
                        <li><a href="#"  class="catlg-icon1"  data="<?php echo $pv->id; ?>"><span class="icon-wallet" aria-hidden="true"></span></a></li>
                    <?php } ?>
                    <?php if ($cartselected) { ?>
                        <li><a href="#" class="cart-icon1 <?php echo $cartselected; ?> " data="<?php echo $pv->id; ?>" ><span class="icon-basket-loaded <?php echo $cartselected1; ?>" aria-hidden="true"></span></a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo base_url('addtocart') . '/' . $pv->id; ?>" title="Add to cart" class="cart-icon1 add-to-cart" data="<?php echo $pv->id ?>"><span class="icon-basket-loaded" aria-hidden="true"></span></a></li>
                    <?php } ?>
                </ul>
                <div class="clearfix"></div>
            </div>
<!--            <a href="javascript:void(0)" class="" ><img alt="<?php echo $pv->photo->file_name; ?>"  src="http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0=<?php echo $pv->thumimagepath; ?>&q=100&tw0=10&th0=10&w=300" data="<?php //echo $pv->id ?>" class="img-responsive width-100 fabric1" ></a>-->
            <a href="javascript:void(0)" class="" ><img alt="<?php echo $pv->photo->file_name; ?>"  src="<?php echo $pv->thumimagepath; ?>" data="<?php echo $pv->id ?>" class="img-responsive width-100 fabric1" ></a>
        </div>





        <?php
    }
} else {
    if (isset($type) && $type != 'scroll') {
        echo 'Sorry no result found';
    }
}
?>