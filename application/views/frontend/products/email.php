<?php
$login = array(
    'name' => 'email',
    'id' => 'email',
    'value' => set_value('login'),
    'maxlength' => 80,
    'size' => 30,
    'class' => 'rigister-input',
    'placeholder' => 'Enter email'
);

$subject = array(
    'name' => 'subject',
    'id' => 'subject',
    'value' => isset($catalogue) && ($catalogue->title != "") ? $catalogue->title : "",
    'class' => 'rigister-input'
);
$message = array(
    'name' => 'message',
    'id' => 'message',
    'placeholder' => 'Message',
    'class' => 'rigister-input'
);

$login_label = 'Email';
?>
<div id="validate-error"></div>
<div id="loader_int"></div>
<div class="email-container" style="border: 1; color: #fff; font-size: 15">
    <h1>Email</h1>
    <table style="margin-bottom: 30px;" cellpadding="10" cellspacing="0" border="0">
        <tr>
            <td><?php echo form_input($login); ?></td>
        </tr>
        <tr>
            <td><?php echo form_input($subject); ?></td>
        </tr>
        <tr>
            <td><?php echo form_textarea($message); ?></td>
        </tr>
        <tr>
            <?php
            $id = "";
            if (isset($catalogue) && !empty($catalogue->id)) {
                $id = $catalogue->id;
            }
            ?>
            <td><input type="hidden" id="<?php echo $id ?>" name="<?php echo $id ?>"/></td>
        </tr>
    </table>
</div>
