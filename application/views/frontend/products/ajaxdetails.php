
<?php
//echo $embed;
?>
<script src="<?php echo base_url(); ?>assets/frontend/js/zoom.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){
         $('.stock').tooltip();
        $("#zoom_01").elevateZoom({
                cursor: "crosshair",
                zoomType : "inner"
            }); 
       $('#info1').on('hidden.bs.modal', function () {
            $('.zoomContainer').remove();
});
$('#qty1<?php echo $product->id; ?>').keyup(function(){
    var total_stock = <?php echo $product->stock; ?>;
    if(parseInt(total_stock)< parseInt($(this).val())){
        alert("Stock Limit is : "+total_stock );
        $('#qty1<?php echo $product->id; ?>').val('1');
        $('#qty1<?php echo $product->id; ?>').focus();
        return false;
    }
});
    });
    
</script>
<!-- product gallery -->
<div class="row">
    <div class="col-md-6">
        <div><img src="<?php echo $product->imagepath ?>" class="img-responsive width-100" style="height: 350px" data-zoom-image="<?php echo $product->imagepath ?>" id="<?php echo $product->productphoto->photo->id != '1' ? "zoom_01" : "" ?>"></div>
<!--        <div><img src="http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0=<?php //echo $product->imagepath ?>&q=100&tw0=10&th0=10&w=300" class="img-responsive width-100" style="height: 350px" data-zoom-image="http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0=<?php //echo $product->imagepath ?>&q=100&tw0=10&th0=10" id="<?php //echo $product->productphoto->photo->id != '1' ? "zoom_01" : "" ?>"></div>-->
    </div>

    <!-- product info -->
    <div class="col-md-6">
        <h2 class="heading-bd"><?php echo $product->sku; ?> </h2>
        <p class="h3" style="margin-top: -10px"><span class="currency-symbol" style="font-size: 14px"><?php echo DEFUELT_CURRENCY; ?></span> <b><?php echo $product->price; ?></b></p>
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" data-original-title="" title="" class="collapsed">
                    <h4 class="panel-title">
                            DESCRIPTION
                            <span class="pull-right"></span>
                    </h4>
                        </a>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body" style="height:125px; overflow:auto; text-transform:capitalize; letter-spacing:normal;">
                        <?php echo strip_tags($product->description); ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" data-original-title="" title="" class="collapsed">
                    <h4 class="panel-title">
                       
                            TECHNICAL SPECIFICATIONS
                            <span class="pull-right"></span>
                    </h4>
                </a>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" >
                    <div class="panel-body" style="height:125px; overflow:auto; text-transform:capitalize; letter-spacing:normal;">
                        <div class="col-sm-12 table-responsive" >
                            <table width="100%" class="">
                                <tbody>
                                    <tr class="odd gradeX">
                                        <th>Title</th><td class="sorting_1"><?php echo $product->title; ?></td>
                                    </tr>
                                    <tr class="odd gradeX">
                                        <th>Category</th><td class="sorting_1"><?php echo $product->section->name; ?></td>
                                    </tr>
                                    <?php
                                    foreach ($product->productvariation as $k => $v) {
                                        if ($v->subcategory && $v->subcategory->category)
                                            $temp[$v->subcategory->category->name][] = $v->subcategory->name;
                                    }
                                    foreach ($category as $k => $v) {
                                        ?>
                                        <tr class="odd gradeX">
                                            <th><?php echo $v->name; ?> </th><td class="sorting_1">
                                                <?php if (isset($temp[$v->name]) && !empty($temp[$v->name])) { ?>
                                                    <table class="table table-hover">
                                                        <?php
                                                        foreach ($temp[$v->name] as $varik => $variv) {
                                                            echo '<tr><td>' . $variv . '</td></tr>';
                                                        }
                                                        ?>
                                                    </table>
                                                    <?php
                                                } else {
                                                    ?>
                                                    N-A
                                                <?php } ?>

                                            </td>
                                        </tr>    
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" data-original-title="" title="" class="collapsed">
                    <h4 class="panel-title">
                            WASH CARE
                            <span class="pull-right"></span>
                    </h4>
                    </a>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body" style="height:125px; overflow:auto; text-transform:capitalize; letter-spacing:normal;">
                        <?php echo $product->washcare; ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour" data-original-title="" title="" class="collapsed">
                    <h4 class="panel-title">
                            TAGS
                            <span class="pull-right"></span>
                    </h4>
                    </a>
                </div>
                <div id="collapsefour" class="panel-collapse collapse">
                    <div class="panel-body" style="height:125px; overflow:auto; text-transform:capitalize; letter-spacing:normal;">
                        <?php
                        $prod = explode(",", $product->tags);
                        foreach ($prod as $value) {
                            ?>
                            <?php if (isset($value) && !empty($value)) { ?><span class="label label-default"><?php echo $value; ?> </span><?php } ?>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="row info-buttons-popup">
            <div class="col-sm-12" id="manual-example">
            
                <button class="btn btn-danger stock" type="button" data-trigger='click' data-toggle="tooltip" title="<?php
                echo ($product->stock) <= 100 ? $product->stock.' meters available' : '+ 100 meters available' ; 
                ?>" >Check Stock</button>
            
            
                <?php if ($this->session->userdata(SITE_NAME . '_user_catalog') && array_key_exists($product->id, $this->session->userdata(SITE_NAME . '_user_catalog'))) { ?>
                    <button class="btn btn-primary deactive">Added to Catalogue</button>

                <?php } else { ?>
                    <button class="btn btn-primary catlg-icon11" data="<?php echo $product->id; ?>" > Add to Catalogue</button>
                <?php } ?>
            
                <?php
                /*
                 * if session is available
                 * then call get_frv()
                 *  if is frv product then apply class else leave it
                 * else do nothing 
                 */
                ?>
                <?php
                $userdata = $this->session->userdata(SITE_NAME . '_user_data');

                if ($userdata) {
                    if ($product->get_frv($userdata['user_id'], $product->id)) {
                        ?>

                        <a href=<?php echo base_url(); ?>favoriteslist class="deactive btn btn-warning ">View Favorite</a>  
                    <?php } else { ?>
                        <a class="fav-icon btn btn-warning" href="javascript:void(0)" data="<?php echo $product->id; ?>">Add to Favorites</a>
                        <?php
                    }
                }
                ?>
                        
                </div>
            
            <div class="col-sm-12">
                <?php if (isInCart($this->cart->contents(), 'id', $product->id)) { ?>
               
                    <a href="<?php echo base_url('cartlist') ?>" class="deactive btn btn-success pull-left margin-right-10"><span aria-hidden="true" class="icon-basket-loaded"></span> View Cart</a>
                    
                <?php } else { ?>
            <span id="qty-container" class="qty-container"><input type="text" id="qty1<?php echo $product->id; ?>" value="0"  class="form-control margin-bottom-5 pull-left margin-right-10 qty1" name="qty" style="width: 100px"/>  </span>
              <!-- cart-icon add-to-cart-details -->
              <a class="btn btn-custom add-to-cart-details pull-left margin-right-10" data="<?php echo $product->id; ?>" href="<?php echo base_url('addtocart') . '/' . $product->id; ?>" ><span aria-hidden="true" class="icon-basket-loaded"></span> Add to Cart </a>   
                <?php } ?>
              <button class="btn btn-default info-fab-visul" style="background-color: #e6e6e6;" alt="<?php echo $product->sku; ?>.jpg" data="<?php echo $product->id ?>" >Visualize</button>
            </div>
            <div class="col-sm-12">
                <span class="pull-left">(Quantity In Meters)</span>
            </div>
        </div>
        

    </div>
   
    <div class="col-md-12">
        <h2 class="heading-bd">Available  Collections</h2>
        <div class="product-options-grid">
            <?php if ($product->collections) {
                ?>
                
                    <?php foreach ($product->collections as $k => $v) { ?>
                     <div class="col-sm-2 col-xs-6"> <!--maketabs-->
                        <a href="javascript:void(0)" class="avail-color" data="<?php echo $v->id; ?>"><img src="<?php echo $v->thumimagepath; ?>" class="img-responsive" alt="" title="" />
                        </a>
<!--                         <a href="javascript:void(0)" class="avail-color" data="<?php //echo $v->id; ?>"><img src="http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0=<?php //echo $v->thumimagepath; ?>&q=100&tw0=10&th0=10&w=300" class="img-responsive" alt="" title="" />
                        </a>-->
                     </div> 
                    <?php } ?>
                
            <?php } else { ?> <div class="col-sm-6 col-xs-12"> No Collections Available For This Fabric</div> <?php } ?>
        </div>
    </div>
     <!-- Available colors -->
    <div class="col-md-12">
        <h2 class="heading-bd">Available  Colors</h2>
        <div class="product-options-grid">
            <?php if ($product->colors) { ?>


                <?php foreach ($product->colors as $k => $v) { ?>
                    <div class="col-sm-2 col-xs-6"> <!--maketabs-->
                        <a href="javascript:void(0)" class="avail-color" data="<?php echo $v->id; ?>"><img src="<?php echo $v->thumimagepath; ?>" class="img-responsive" alt="" title="" />
                        </a>
<!--                        <a href="javascript:void(0)" class="avail-color" data="<?php //echo $v->id; ?>"><img src="http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0=<?php //echo $v->thumimagepath; ?>&q=100&tw0=10&th0=10&w=300" class="img-responsive" alt="" title="" />
                        </a>-->
                    </div>    
                <?php } ?>


            <?php } else { ?><div class="col-sm-6 col-xs-12">No Color Available For This Fabric</div><?php } ?>
        </div>
    </div> 
</div>



