<?php
/*
 * @auther Shafiq
 */
$userdata = $this->session->userdata(SITE_NAME . '_user_data');
?>

<?php //$this->load->view('frontend/layout/leftwidgets', $this->data); ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<!-- Filter module -->
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div class="panel-body center-content">
                <div class="row">
                    <!-- End Filter module --> 
                    <div id="shopping-page" class="col-sm-12 shadow-main">

                        <?php if ($this->cart->total_items()) { ?>
                            <?php echo form_open('cart/update'); ?>
                            <div class="col-sm-3 margin-bottom-10 margin-top-10">
                                <input type ="submit" name="delete" value="Delete" class="btn btn-custom">
                            </div>

                            <div align="right" class="col-sm-9 margin-bottom-10 margin-top-10">
                                
                                <button type="submit" class="btn btn-success" name="update" title="Update your Cart" id="update" value="Update your Cart"><span class="icon-reload" aria-hidden="true"></span> Update your Cart</button>
                                <a href="<?php echo base_url(); ?>" class="btn btn-info"><span class="icon-basket-loaded" aria-hidden="true"></span> Continue shopping </a>
                                <a href="<?php echo base_url('checkout'); ?>" class="btn btn-warning"><span class="icon-arrow-right" aria-hidden="true"></span> Check out</a>

                            </div>
                            <div class="col-sm-12">
                                <div id="cart-table-new" class="table-responsive">
                                    <table width="100%" class="table-bordered table-striped table-condensed cf">
                                        <thead class="cf">
                                            <tr>
                                                <th align="center"><?php echo form_checkbox("", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                                                <th>Image</th>
                                                <th class="numeric">Item Number</th>
                                                <th class="numeric">Quantity</th>
                                                <th class="numeric">Price</th>  
                                                <th class="numeric">Discount</th>
                                                <th class="numeric">Price After Discount</th>
                                                <th class="numeric">Sub-Total</th>
                                                <th class="numeric">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($this->cart->contents() as $items) { ?>
                                                <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
                                                <tr>
                                                    <td data-title="checkbox" align="center"><?php echo (form_checkbox("option[]", $items['rowid'], '', 'class="case"')); ?></td>
                                                    <td data-title="Image"><img src="http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0=<?php echo base_url(); ?>/<?php echo PRODUCTIMAGE_PATH; ?>/thumb/<?php echo $items['options']['file_name']; ?>&q=100&tw0=10&th0=10&w=100&h=70"></td>
                                                    <td data-title="Item Number" class="numeric"><?php echo $items['options']['sku']; ?></td>
                                                    <td data-title="Quantity" class="numeric"><?php echo form_input(array('name' => $i . '[qty]', 'value' => $items['qty'], 'class' => 'form-control qty', 'size' => '5')); ?></td>
                                                    <?php // if (!$this->session->userdata('change-currency-to') || $this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                <!--                                <td class="sorting_1"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;          ?></span><?php // echo $this->cart->format_number($items['price']);          ?></td>
                                                        <td style="text-align:right"><span class="currency-symbol"></span><?php // echo $this->cart->format_number($items['subtotal']);          ?></td>-->
                                                    <?php // } else { ?>
                                <!--                                <td class="sorting_1"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');          ?></span><?php // echo convertCurrency($this->cart->format_number($items['price']), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));           ?></td>
                                                        <td style="text-align:right"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');          ?></span><?php // echo convertCurrency($this->cart->format_number($items['subtotal']), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));           ?></td>-->
                                                    <?php // } ?>
                                                    <td data-title="Price" class="numeric shorting_1"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span> <?php echo $this->cart->format_number($items['price']); ?></td>
                                                    <td data-title="Discount" class="numeric">0%</td>
                                                    <td data-title="Price After Discount" class="numeric shorting_1"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span> <?php echo $this->cart->format_number($items['price']); ?></td>
                                                    <td data-title="Sub-Total" class="numeric"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span> <?php echo $this->cart->format_number($items['subtotal']); ?></td>
                                                    <td data-title="Action" class="numeric" align="center">
                                                        <a href="<?php echo base_url('cart/delete') . '/' . $items['rowid']; ?>" alt="Delete" title="Delete" class="btn-mini">
                                                            <span class="icon-trash" aria-hidden="true"></span> Delete</a>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 total-price pull-left">
                                    <div class="row">
                                        <div class="col-md-6">Total Meters :</div>
                                        <div class="col-md-6"> <?php $total_meter=0;
                                        foreach ($this->cart->contents() as $items) {
                                            $total_meter = $total_meter + $items['qty'];
                                        }
                                        echo $total_meter;
                                                ?> <span style="font-size: 10px; font-style: oblique; font-weight: bold;" >meters</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 total-price pull-right">
                                    <div class="row">
                                        <div class="col-md-6">Total Amount:</div>
                                        <div class="col-md-6"><span class="currency-symbol"></span> <?php
//                if (isset($totalamount) && !empty($totalamount))
//                    echo convertCurrency($totalamount, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));
//                else if ($this->session->userdata('change-currency-to') && $this->session->userdata('change-currency-to') != DEFUELT_CURRENCY)
//                    echo convertCurrency($this->cart->format_number($this->cart->total()), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));
//                else
                                        echo $this->cart->format_number($this->cart->total());
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:-15px;margin-bottom:20px">
                                    <span style="color: red" class="pull-right">*The price communicated above is a tentative estimation and not a final price</span>
                                </div>
                                <div class="clearfix"></div>
                                
                            </div>


                            <?php
//                $coupon = array(
//                    'name' => 'coupon',
//                    'id' => 'coupon',
//                );
//                echo form_label('Coupon: ', 'coupon');
//                echo form_input($coupon);
                            ?>

                            <!--                <a href="javascript:void(0);" class='apply-coupon'>Apply Coupon</a>-->


                            <div align="right" class="col-sm-12 margin-bottom-10">

                                <button type="submit" class="btn btn-success" name="update" title="Update your Cart" id="update" value="Update your Cart"><span class="icon-reload" aria-hidden="true"></span> Update your Cart</button>
                                <a href="<?php echo base_url(); ?>" class="btn btn-info"><span class="icon-basket-loaded" aria-hidden="true"></span> Continue shopping </a>
                                <a href="<?php echo base_url('checkout'); ?>" class="btn btn-warning"><span class="icon-arrow-right" aria-hidden="true"></span> Check out</a>

                            </div>


                            <?php echo form_close(); ?>
                        <?php } else { ?>
                            <div class="col-sm-12">
                                <div class="empty-page">
                                    <div class="margin-bottom-10"><span class="icon-basket" aria-hidden="true"></span> Cart is empty</div>
                                    <a href="<?php echo base_url(); ?>" class="btn btn-custom">Continue shopping </a>
                                </div>
                            </div>
                            <div class="col-sm-12 margin-bottom-10" align="right"></div>
                        <?php } ?>

                    </div> 


                    <!-- Right widgets -->
                    <?php //$this->load->view('frontend/layout/rightwidgets', $this->data); ?>
                    <!-- end widgets --> 
                    <!-- Popup -->
                    <div id="dialog" class="popup-module"></div>
                    <!-- end Popup --> 
                </div>

                <input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
                <input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
                <input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>

            </div>
        </section>
    </div>
</div>
<script>
    function toggleChecks(obj) {
        $('.case').prop('checked', obj.checked);
    }
    function checkValid() {
        if ($(".case:checked").length > 0) {
            document.getElementById('frmDelete').submit();
        } else {
            alert("You didn't select any row");
        }
    
    
    }
</script>