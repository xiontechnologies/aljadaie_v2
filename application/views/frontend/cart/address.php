<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section" class="panel-body center-content "> 
                <div class="row address-content">
                    <div id="login-page" class="col-sm-12 shadow-main" style="margin-top:20px">

                        <?php
                        $submit = array('name' => 'submit',
                            'id' => 'submit',
                            'value' => 'Submit',
                            'class' => 'btn btn-custom',
                        );
                        $billingcompany = array(
                            'name' => 'billing[company]',
                            'id' => 'billingcompany',
                            'required' => 'required ',
                            'class' => form_error('billingcompany') ? 'form-control mini fl' : 'form-control mini',
                            'value' => $user->fullname ? $user->fullname : '',
                            'placeholder'=> 'Name'
                        );
                        $billingaddress = array(
                            'name' => 'billing[address]',
                            'id' => 'billingaddress',
                            'required' => 'required',
                            'class' => form_error('billingaddress') ? 'form-control mini fl' : 'form-control mini',
                            'value' => $user->user_profile ? $user->user_profile->address : '',
                            'placeholder'=> 'Address'
                        );
                        $billingstate = array(
                            'name' => 'billing[state]',
                            'id' => 'billingstate',
                            'required' => 'required',
                            'class' => form_error('billingstate') ? 'form-control mini fl' : 'form-control mini',
                            'value' => $user->user_profile ? $user->user_profile->state : '',
                            'placeholder'=> 'State'
                        );
                        $billingcity = array(
                            'name' => 'billing[city]',
                            'id' => 'billingcity',
                            'required' => 'required',
                            'class' => form_error('billingcity') ? 'form-control mini fl' : 'form-control mini',
                            'value' => $user->user_profile ? $user->user_profile->city : '',
                            'placeholder'=> 'City'
                        );
                        $billingcountry = array(
                            'name' => 'billing[country]',
                            'id' => 'billingcountry',
                            'required' => 'required',
                            'class' => form_error('billingcountry') ? 'form-control mini fl' : 'form-control mini',
                            'value' => $user->user_profile ? $user->user_profile->country : '',
                            'placeholder'=> 'Country'
                        );
                        $billingpostal = array(
                            'name' => 'billing[postal]',
                            'id' => 'billingpostal',
                            'required' => 'required',
                            'class' => form_error('billingpostal') ? 'form-control mini fl' : 'form-control mini',
                            'value' => $user->user_profile ? $user->user_profile->zip : '',
                            'placeholder'=> 'Posta/Zip Code'
                        );
                        $billingfax = array(
                            'name' => 'billing[fax]',
                            'id' => 'billingfax',
                            'class' => form_error('billingfax') ? 'form-control mini fl' : 'form-control mini',
                            'value' => $user->user_profile ? $user->user_profile->fax : '',
                            'placeholder'=> 'Fax Number'
                        );
                        $billingtelephone = array(
                            'name' => 'billing[telephone]',
                            'id' => 'billingtelephone',
                            'class' => form_error('billingtelephone') ? 'form-control mini fl' : 'form-control mini',
                            'value' => $user->user_profile ? $user->user_profile->telephone : '',
                            'placeholder'=> 'Telephone'
                        );

                        $shippingcompany = array(
                            'name' => 'shipping[company]',
                            'id' => 'shippingcompany',
                            'required' => 'required',
                            'class' => form_error('shippingcompany') ? 'form-control mini fl' : 'form-control mini',
                            'value' => isset($postdata) && isset($postdata->firstname) ? $postdata->firstname : '',
                            'placeholder'=> 'Name'
                        );

                        $shippingaddress = array(
                            'name' => 'shipping[address]',
                            'id' => 'shippingaddress',
                            'required' => 'required',
                            'class' => form_error('shippingaddress') ? 'form-control mini fl' : 'form-control mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Address'
                        );
                        $shippingstate = array(
                            'name' => 'shipping[state]',
                            'id' => 'shippingstate',
                            'required' => 'required',
                            'class' => form_error('shippingstate') ? 'form-control mini fl' : 'form-control mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'State'
                        );
                        $shippingcity = array(
                            'name' => 'shipping[city]',
                            'id' => 'shippingcity',
                            'required' => 'required',
                            'class' => form_error('shippingcity') ? 'form-control mini fl' : 'form-control mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'City'
                        );
                        $shippingcountry = array(
                            'name' => 'shipping[country]',
                            'id' => 'shippingcountry',
                            'required' => 'required',
                            'class' => form_error('shippingcountry') ? 'form-control mini fl' : 'form-control mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Country'
                        );
                        $shippingpostal = array(
                            'name' => 'shipping[postal]',
                            'id' => 'shippingpostal',
                            'required' => 'required',
                            'class' => form_error('shippingpostal') ? 'form-control mini fl' : 'form-control mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Posta/Zip Code'
                        );
                        $shippingfax = array(
                            'name' => 'shipping[fax]',
                            'id' => 'shippingfax',
                            'class' => form_error('shippingfax') ? 'form-control mini fl' : 'form-control mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Fax Number'
                        );
                        $shippingtelephone = array(
                            'name' => 'shipping[telephone]',
                            'id' => 'shippingtelephone',
                            'class' => form_error('shippingtelephone') ? 'form-control mini fl' : 'form-control mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Telephone'
                        );
//billing-add
//fullwid
                        ?>

                        <?php echo form_open_multipart(base_url('cart/address'), 'id="adduserfrm"'); ?>
                        <div class="col-sm-3 col-md-3 display-none mainaddress1"></div>
                        <div class="col-sm-6 col-md-6" id="mainaddress">       
                            <h2 class="heading-bd"><span class="icon-directions" aria-hidden="true"></span> Billing Address</h2>
                            <div class="row login-page-left">
                                <div class="col-sm-4"><?php echo form_label('Name: ', 'company', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($billingcompany); ?></div>
                                <div class="col-sm-12"><?php echo form_error('Name', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"><?php echo form_label('Address: ', 'address', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_textarea($billingaddress); ?></div>
                                <div class="col-sm-12"><?php echo form_error('address', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('State: ', 'state', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"> <?php echo form_input($billingstate); ?> </div>
                                <div class="col-sm-12">    <?php echo form_error('state', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"><?php echo form_label('City: ', 'city', array('class' => 'required')); ?></div>
                                <div class="col-sm-8">  <?php echo form_input($billingcity); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('city', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"><?php echo form_label('Country: ', 'country', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($billingcountry); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('country', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"><?php echo form_label('Postal/Zip Code: ', 'postal', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"> <?php echo form_input($billingpostal); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('postal', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Fax: ', 'fax', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($billingfax); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('fax', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Telephone: ', 'telephone', array('class' => 'required')); ?></div>
                                <div class="col-sm-8">  <?php echo form_input($billingtelephone); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>

                                <div class="col-sm-12">Shipping address same as billing address<input name="sameaddress" type="checkbox" class="check-add" style="margin: 0 15px" /></div>

                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3 display-none mainaddress1" ></div>
                        <div class="col-sm-6 col-md-6" id="shipaddress">
                            <h2 class="heading-bd"><span class="icon-directions" aria-hidden="true"></span> Shipping Address</h2>
                            <div class="row login-page-left">

                                <div class="col-sm-4"> <?php echo form_label('Name: ', 'company', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($shippingcompany); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('company', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Address: ', 'address', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_textarea($shippingaddress); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('address', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"><?php echo form_label('State: ', 'state', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($shippingstate); ?></div>
                                <div class="col-sm-12"><?php echo form_error('state', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('City: ', 'city', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($shippingcity); ?></div>
                                <div class="col-sm-12">   <?php echo form_error('city', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Country: ', 'country', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($shippingcountry); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('country', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Postal/Zip Code: ', 'postal', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"> <?php echo form_input($shippingpostal); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('postal', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Fax: ', 'fax', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"> <?php echo form_input($shippingfax); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('fax', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"><?php echo form_label('Telephone: ', 'telephone', array('class' => 'required')); ?></div>
                                <div class="col-sm-8">  <?php echo form_input($shippingtelephone); ?></div>
                                <div class="col-sm-12">   <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                
                            </div>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12 margin-bottom-10" align="center">
                            <?php echo form_submit($submit); ?>
                                </div>
                        <?php echo form_close(); ?>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>
<script>
    $(document).on('change', '.check-add', function(event) {
   
        if ($('.check-add').is(":checked")) {
            $(".address-content").addClass('billing-add');
            $(".mainaddress1").removeClass('display-none');
            $('#shipaddress').hide();
            $("#shipaddress :input").removeAttr('required');
        } else {
            $(".address-content").removeClass('billing-add');
            $(".mainaddress1").addClass('display-none');
            $('#shipaddress').show();
            $("#shipaddress :input").attr('required', 'required');
        }
   
});
    </script>