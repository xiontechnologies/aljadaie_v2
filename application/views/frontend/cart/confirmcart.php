<?php
/*
 * @auther Shafiq
 */
?>

<?php //$this->load->view('frontend/layout/leftwidgets', $this->data); ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div class="panel-body center-content">
                <div class="row">
                    <!-- Filter module -->

                    <!-- End Filter module --> 
                    <div id="shopping-page" class="col-sm-12 shadow-main" style="margin-top:20px">
                         <h2 class="heading-bd" style="margin-left:15px"><span class="icon-check" aria-hidden="true"></span> Order Confirmation</h2>
                        <?php if ($this->cart->total_items()) { ?>
                            <div class="col-sm-3 margin-bottom-10 margin-top-10">
                                <a href="<?php echo base_url('cart/order'); ?>" class="btn btn-success">Confirm Order</a>
                            </div>   
                            <div class="col-sm-12">
                                <div id="cart-table-new" class="table-responsive">
                                    <table width="100%" class="table-bordered table-striped table-condensed cf">
                                        <thead class="cf">
                                            <tr>
                                                <th>Product Name</th>
                                                <th class="numeric">SKU</th>
                                                <th class="numeric">Colorcode</th>
                                                <th class="numeric">Product</th>
                                                <th class="numeric">Quantity</th>
                                                <th class="numeric">Price</th>  
                                                <th class="numeric">Sub-Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($this->cart->contents() as $items) { ?>
                                                <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
                                                <tr>
                                                    <td data-title="Product Name"><?php echo $items['name']; ?></td>
                                                    <td data-title="SKU" class="numeric"><?php echo $items['options']['sku']; ?></td>
                                                    <td data-title="Colorcode" class="numeric"><?php echo $items['options']['colorcode']; ?></td>
                                                    <td data-title="Product" class="numeric"><img src="<?php echo base_url(); ?>/<?php echo PRODUCTIMAGE_PATH; ?>/thumb/<?php echo $items['options']['file_name']; ?>" height="50" width="50"></td>
                                                    <td data-title="Quantity" class="numeric"><?php echo form_label($items['qty']); ?></td>
                                                    <td data-title="Price" class="numeric"><span class="currency-symbol"></span> <?php echo $this->cart->format_number($items['price']); ?></td>
                                                    <td data-title="Sub-Total" class="numeric"><span class="currency-symbol"></span> <?php echo $this->cart->format_number($items['subtotal']); ?></td>
                                                    <?php // if (!$this->session->userdata('change-currency-to') || $this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                <!--                                <td class="sorting_1"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo $this->cart->format_number($items['price']);   ?></td>
                                                        <td style="text-align:right"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo $this->cart->format_number($items['price']);   ?></td>-->
                                                    <?php // } else { ?>
                                <!--                                <td class="sorting_1"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency($this->cart->format_number($items['price']), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));   ?></td>
                                                        <td style="text-align:right"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency($this->cart->format_number($items['price']), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));   ?></td>-->
                                                    <?php // } ?>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 total-price pull-right">
                                    <div class="row">
                                        <div class="col-md-6">Total</div>
                                        <div class="col-md-6"><span class="currency-symbol"></span> <?php
//                if (isset($totalamount) && !empty($totalamount))
//                    echo convertCurrency($totalamount, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));
//                else if ($this->session->userdata('change-currency-to') && $this->session->userdata('change-currency-to') != DEFUELT_CURRENCY)
//                    echo convertCurrency($this->cart->format_number($this->cart->total()), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));
//                else
                                        echo $this->cart->format_number($this->cart->total());
                                            ?>
                                        </div>
                                    </div>
                                    <?php if ($this->session->userdata('discount')) { ?>
                                        <div class="row">
                                            <div class="col-md-6">Discount</div>
                                            <div class="col-md-6"><span class="currency-symbol"></span> - <?php echo $this->session->userdata('discount'); ?></div>

                                        </div>
                                        <<div class="row">
                                            <div class="col-md-6">Final Price</div>
                                            <div><span class="currency-symbol"></span> <?php echo ($this->cart->format_number($this->cart->total()) - $this->session->userdata('discount')); ?></div>

                                        </div>
                                    <?php } ?>
                                </div>

                                <div class="coupon">
                                    <?php
                                    if (!$this->session->userdata('discount')) {
                                        $coupon = array(
                                            'name' => 'coupon',
                                            'id' => 'coupon', 'class' => 'form-control', 'style' => 'width:200px; float:left; margin-right:15px;  margin-top:10px;'
                                        );
                                        $style = array(
                                            'style' => 'float:left; margin-right:15px;  margin-top: 16px;'
                                        );
                                        echo form_label('Coupon: ', 'coupon', $style);
                                        echo form_input($coupon);
                                        ?>
                                        <a href="javascript:void(0);" class='apply-coupon apply-coupon btn btn-info margin-top-10'>Apply Coupon</a>
                                    <?php } ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div align="right" class="col-sm-12 margin-bottom-10">

                                <a id ="confirm-order" href="<?php echo base_url('cart/order'); ?>" class="btn btn-success">Confirm Order</a>


                                <?php echo form_close(); ?>
                            <?php } else { ?>
                                <div class="col-sm-12">
                                    <div class="empty-page">
                                        <div class="margin-bottom-10"><span class="icon-basket" aria-hidden="true"></span> Cart is empty</div>
                                        <a href="<?php echo base_url(); ?>" class="btn btn-custom">Continue shopping </a>
                                    </div>
                                </div>
                                <div class="col-sm-12 margin-bottom-10" align="right"></div>
                            <?php } ?>
                        </div>

                        <!-- Right widgets -->
                        <?php //$this->load->view('frontend/layout/rightwidgets', $this->data);    ?>
                        <!-- end widgets --> 
                        <!-- Popup -->
                        <div id="dialog" class="popup-module"></div>
                        <!-- end Popup --> 
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
