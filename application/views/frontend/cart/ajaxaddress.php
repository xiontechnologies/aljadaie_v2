<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row address-content">
                    <div id="login-page" class="col-sm-12 shadow-main" style="margin-top:20px">
                    
                        <?php
                        $submit = array('name' => 'submit',
                            'id' => 'submit',
                            'value' => 'Submit',
                            'class' => 'btn btn-custom btn-block',
                        );
                        $billingcompany = array(
                            'name' => 'billingcompany',
                            'id' => 'billingcompany',
                            'class' => form_error('billingcompany') ? 'mini fl' : 'mini',
                            'value' => isset($postdata) && isset($postdata->firstname) ? $postdata->firstname : '',
                            'placeholder'=> 'Name'
                        );

                        $billingaddress = array(
                            'name' => 'billingaddress',
                            'id' => 'billingaddress',
                            'class' => form_error('billingaddress') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Address'
                        );
                        $billingstate = array(
                            'name' => 'billingstate',
                            'id' => 'billingstate',
                            'class' => form_error('billingstate') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'State'
                        );
                        $billingcity = array(
                            'name' => 'billingcity',
                            'id' => 'billingcity',
                            'class' => form_error('billingcity') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'City'
                        );
                        $billingcountry = array(
                            'name' => 'billingcountry',
                            'id' => 'billingcountry',
                            'class' => form_error('billingcountry') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Country'
                        );
                        $billingpostal = array(
                            'name' => 'billingpostal',
                            'id' => 'billingpostal',
                            'class' => form_error('billingpostal') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Posta/Zip Code'
                        );
                        $billingfax = array(
                            'name' => 'billingfax',
                            'id' => 'billingfax',
                            'class' => form_error('billingfax') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Fax Number'
                        );
                        $billingtelephone = array(
                            'name' => 'billingtelephone',
                            'id' => 'billingtelephone',
                            'class' => form_error('billingtelephone') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Telephone'
                        );

                        $shippingcompany = array(
                            'name' => 'shippingcompany',
                            'id' => 'shippingcompany',
                            'class' => form_error('shippingcompany') ? 'mini fl' : 'mini',
                            'value' => isset($postdata) && isset($postdata->firstname) ? $postdata->firstname : '',
                            'placeholder'=> 'Name'
                        );

                        $shippingaddress = array(
                            'name' => 'shippingaddress',
                            'id' => 'shippingaddress',
                            'class' => form_error('shippingaddress') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Address'
                        );
                        $shippingstate = array(
                            'name' => 'shippingstate',
                            'id' => 'shippingstate',
                            'class' => form_error('shippingstate') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'State'
                        );
                        $shippingcity = array(
                            'name' => 'shippingcity',
                            'id' => 'shippingcity',
                            'class' => form_error('shippingcity') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'City'
                        );
                        $shippingcountry = array(
                            'name' => 'shippingcountry',
                            'id' => 'shippingcountry',
                            'class' => form_error('shippingcountry') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Country'
                        );
                        $shippingpostal = array(
                            'name' => 'shippingpostal',
                            'id' => 'shippingpostal',
                            'class' => form_error('shippingpostal') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Posta/Zip Code'
                        );
                        $shippingfax = array(
                            'name' => 'shippingfax',
                            'id' => 'shippingfax',
                            'class' => form_error('shippingfax') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Fax Number'
                        );
                        $shippingtelephone = array(
                            'name' => 'shippingtelephone',
                            'id' => 'shippingtelephone',
                            'class' => form_error('shippingtelephone') ? 'mini fl' : 'mini',
                            'value' => isset($postdata->lastname) ? $postdata->lastname : "",
                            'placeholder'=> 'Telephone'
                        );
                        ?>
                        
                        <?php
                        echo form_open_multipart('frontend/Mycart/addaddress', 'id="adduserfrm"');
                        ?>
                            <div class="col-sm-6 col-md-6" id="mainaddress1">       
                        <?php echo $add_error; ?>
                        <?php echo $add_warning; ?>
                            <?php echo $add_info; ?>
                            <?php echo $add_success; ?>
                                <h2 class="heading-bd"><span class="icon-directions" aria-hidden="true"></span> Billing Address</h2>
                            <div class="row login-page-left">
                                    <div class="col-sm-4"><?php echo form_label('Name: ', 'company', array('class' => 'required')); ?></div>
                                    <div class="col-sm-8"><?php echo form_input($billingcompany); ?></div>
                                    <div class="col-sm-12"><?php echo form_error('Name', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                    <div class="col-sm-4"><?php echo form_label('Address: ', 'address', array('class' => 'required')); ?></div>
                                    <div class="col-sm-8"><?php echo form_textarea($billingaddress); ?></div>
                                    <div class="col-sm-12"><?php echo form_error('address', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                    <div class="col-sm-4"> <?php echo form_label('State: ', 'state', array('class' => 'required')); ?></div>
                                    <div class="col-sm-8"> <?php echo form_input($billingstate); ?> </div>
                                    <div class="col-sm-12">    <?php echo form_error('state', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                    <div class="col-sm-4"><?php echo form_label('City: ', 'city', array('class' => 'required')); ?></div>
                                    <div class="col-sm-8">  <?php echo form_input($billingcity); ?></div>
                                    <div class="col-sm-12">    <?php echo form_error('city', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                    <div class="col-sm-4"><?php echo form_label('Country: ', 'country', array('class' => 'required')); ?></div>
                                    <div class="col-sm-8"><?php echo form_input($billingcountry); ?></div>
                                    <div class="col-sm-12">    <?php echo form_error('country', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                    <div class="col-sm-4"><?php echo form_label('Postal/Zip Code: ', 'postal', array('class' => 'required')); ?></div>
                                    <div class="col-sm-8"> <?php echo form_input($billingpostal); ?></div>
                                    <div class="col-sm-12">    <?php echo form_error('postal', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                    <div class="col-sm-4"> <?php echo form_label('Fax: ', 'fax', array('class' => 'required')); ?></div>
                                    <div class="col-sm-8"><?php echo form_input($billingfax); ?></div>
                                    <div class="col-sm-12">    <?php echo form_error('fax', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                    <div class="col-sm-4"> <?php echo form_label('Telephone: ', 'telephone', array('class' => 'required')); ?></div>
                                    <div class="col-sm-8">  <?php echo form_input($billingtelephone); ?></div>
                                    <div class="col-sm-12">    <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>

                                    <div class="col-sm-12">Shipping address same as billing address<input name="sameaddress" type="checkbox" class="check-add1" style="margin: 0 15px" /></div>

                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6" id="shipaddress1">
<?php echo $add_error; ?>
<?php echo $add_warning; ?>
<?php echo $add_info; ?>
<?php echo $add_success; ?>
                                <h2 class="heading-bd"><span class="icon-directions" aria-hidden="true"></span> Shipping Address</h2>
                            <div class="row login-page-left">

                                   <div class="col-sm-4"> <?php echo form_label('Name: ', 'company', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($shippingcompany); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('company', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Address: ', 'address', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_textarea($shippingaddress); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('address', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"><?php echo form_label('State: ', 'state', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($shippingstate); ?></div>
                                <div class="col-sm-12"><?php echo form_error('state', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('City: ', 'city', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($shippingcity); ?></div>
                                <div class="col-sm-12">   <?php echo form_error('city', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Country: ', 'country', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"><?php echo form_input($shippingcountry); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('country', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Postal/Zip Code: ', 'postal', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"> <?php echo form_input($shippingpostal); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('postal', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"> <?php echo form_label('Fax: ', 'fax', array('class' => 'required')); ?></div>
                                <div class="col-sm-8"> <?php echo form_input($shippingfax); ?></div>
                                <div class="col-sm-12">    <?php echo form_error('fax', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>


                                <div class="col-sm-4"><?php echo form_label('Telephone: ', 'telephone', array('class' => 'required')); ?></div>
                                <div class="col-sm-8">  <?php echo form_input($shippingtelephone); ?></div>
                                <div class="col-sm-12">   <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?></div>
</div>
                              
                            </div>
                            <div class="clr"></div>
                            <div class="col-sm-12 margin-bottom-10" align="right">
                                    <?php echo form_submit($submit); ?>
                                </div>
<?php echo form_close(); ?> 
                        </div>
                    </div>
              
            </div>
        </section>
    </div>
</div>
<script>
    $(document).on('change', '.check-add1', function(event) {
   
        if ($('.check-add1').is(":checked")) {
            $(".address-content").addClass('billing-add');
            $("#mainaddress1").removeClass('col-md-6 col-sm-6');
            $("#mainaddress1").addClass('col-md-12 col-sm-12');
            $('#shipaddress1').hide();
            $("#shipaddress1 :input").removeAttr('required');
        } else {
            $(".address-content").removeClass('billing-add');
            $("#mainaddress1").removeClass('col-md-12 col-sm-12');
            $("#mainaddress1").addClass('col-md-6 col-sm-6');
            $('#shipaddress1').show();
            $("#shipaddress1 :input").attr('required', 'required');
        }
   
});
    </script>

