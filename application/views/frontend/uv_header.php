<?php
ob_start();
/*
 * @auther Shafiq
 */
?>
<!DOCTYPE html>
<html lang="en-gb">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo SITE_NAME; ?> | <?php echo $add_title; ?></title>
        <script type="text/javascript">var switchTo5x = true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <?php
        echo $add_css;
        echo $add_js;
        ?>
    </head>

    <body>

        <!--Arrow Navigation-->
        <a id="prevslide" class="load-item"></a>
        <a id="nextslide" class="load-item"></a>

        <!-- Header -->
        <header>
            <div id="header-section">
                <h1 class="logo fl"><a href="<?php echo base_url(); ?>" title="<?php echo SITE_NAME; ?>"><?php echo SITE_NAME; ?></a></h1>
                <div class="fr header-right">
                    <ul class="maketabs">
                        <?php
                        if ($this->session->userdata(SITE_NAME . '_user_data')) {
                            $userdata = $this->session->userdata(SITE_NAME . '_user_data');
                            $user = Userprofile::find_by_user_id($this->session->userdata[SITE_NAME . '_user_data']['user_id']);
                            $path = "";
                            if (isset($user->photo_id) && !empty($user->photo_id)) {
                                ?>
                                <li><a href="#"><img src="<?php echo base_url(PROFILEIMAGE); ?>/<?php echo $user->photo->file_name; ?>" alt="<?php echo $user->photo->raw_name; ?>" style="width:32px; height: 32px" class="fl" title="<?php echo $user->photo->raw_name; ?>" /></a></li> 
                            <?php } else { ?>
                                <li><a href="#">
                                        <img src="<?php echo base_url(); ?>assets/frontend/images/user.png" alt="user" style="width:32px; height: 32px" class="fl" title="user" />
                                    </a></li>
                            <?php } ?>
                            <li class="user-myaccount drop"><span>Welcome <a href="<?php echo base_url('myaccount') ?>"><?php echo isset($userdata['firstname']) ? $userdata['firstname'] : 'Guest !'; ?></a></span>
                                <ul class="dropdown">
                                    <li class="user-myaccount"><span><a href="<?php echo base_url('myaccount') ?>">Myaccount</a></span></li>
                                    <li class="user-myaccount"><span><a href="<?php echo base_url('changepassword') ?>">Change Password</a></span></li>
                                    <li class="user-myaccount"><span><a href="<?php echo base_url('home/coupon') ?>">Coupon</a></span></li>
                                    <li class="user-myaccount"><span><a href="<?php echo base_url('home/orders') ?>">Order</a></span></li>
                                    <li class="user-myaccount"><span><a href="<?php echo base_url('cataloguelist') ?>">catalogue</a></span></li>
                                    <li class="user-myaccount"><span><a href="<?php echo base_url('favoriteslist') ?>">Favorites</a></span></li>
                                    <li class="user-myaccount"><span><a href="<?php echo base_url('storyboardlist') ?>">Storyboard</a></span></li>
                                    <li class="user-myaccount"><a href="<?php echo base_url('frontend/auth/logout'); ?>" tabindex="-1">Logout</a></li> 

                                </ul>                            
                            </li>

                        <?php } else { ?>
                            <li class="user-myaccount"><a href="<?php echo base_url('login') ?>">Login</a> 
        <!--                                / <a href="<?php //echo base_url('login')    ?>">Register</a>-->
                            </li>
                        <?php } ?>

                        <?php if ($this->session->userdata(SITE_NAME . '_user_data')) { ?>
                            <li style="padding:0 10px">|</li>
                            <li class="user-myaccount"><span class="cart-count"><?php echo count($this->cart->contents()); ?></span>&nbsp;<a href="<?php echo base_url('cartlist') ?>">Item in Cart</a></li>
                        <?php } ?>
                        <li class="language-module">
                            <select class="select-lng">
                                <option>English</option>
                                <option>Arabic</option>
                            </select>
                        </li>
                        <!--<li class="country"><span class="country-id"><?php // echo form_dropdown('country', $country, $this->session->userdata('change-currency-to') ? $this->session->userdata('change-currency-to') : DEFUELT_CURRENCY, 'class="currency-country"');   ?></span>&nbsp;</li>-->
                    </ul>
                </div>
            </div>
        </header>
        <!-- end Header --> 