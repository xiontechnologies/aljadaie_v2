
<?php // $this->load->view('frontend/layout/leftwidgets', $this->data);   ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<?php
//        $frmaction = 'frontend/' . $controller . '/delete_catalogue';
//        $attributes = 'id="frmDelete"';
//        echo form_open($frmaction, $attributes);
?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row">

                    <div class="col-sm-12 shadow-main" id="shopping-page" style="margin-top:20px">
                        <div class="col-sm-12"><h2 class="heading-bd"><span aria-hidden="true" class="icon-credit-card"></span> Coupon</h2></div>
                        <div class="col-sm-3 margin-bottom-10">
                            <a class="btn btn-custom" href="javascript:history.go(-1)" title="Back" ><span class="icon-arrow-left" aria-hidden="true"></span> Back</a>
                        </div>  

                        <div class="col-sm-12">

                            <?php
                            if (isset($coupon) && count($coupon) > 0) {
                                ?>
                                                            <!--<p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="default-btn" style="float:left">Delete Rows</a></p>-->

                                <table class="table-bordered table-striped table-condensed cf" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="numeric">Code</th>
                                            <th class="numeric">Type</th>
                                            <th class="numeric">Amount</th>
                                            <th class="numeric">To Date</th>
                                            <th class="numeric">From Date</th>
                                            <th class="numeric">To Amount</th>
                                            <th class="numeric">From Amount </th>
                                            <th class="numeric">Number Of Used</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    foreach ($coupon as $k => $coupans) {
//                        echo '<pre>';print_r($items);exit;
                                        ?>
                                        <tr class="odd gradeX">
                                            <td class="numeric" data-title="Code"><?php echo $coupans->coupon->code; ?></td>
                                            <td class="numeric" data-title="Type"><?php if ($coupans->coupon->type == '0') { ?>
                                                    Single Time 
                                                <?php } else { ?>
                                                    Multiple Time
                                                    <?php ?>
                                                <?php } ?></td>
                                            <td class="numeric" data-title="Amount"><?php echo $coupans->coupon->amount; ?></td>
                                            <td class="numeric" data-title="To Date"><?php echo date("d M Y", strtotime($coupans->coupon->to_date)); ?></td>
                                            <td class="numeric" data-title="From Date"><?php echo date("d M Y", strtotime($coupans->coupon->from_date)); ?></td>
                                            <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                        <!--                                <td><span class="currency-symbol">SAR</span><?php // echo $coupans->coupon->to_amount;       ?></span></td>
                                                <td><span class="currency-symbol">SAR</span><?php // echo $coupans->coupon->from_amount;       ?></span></td>-->
                                            <?php // } else { ?>
                        <!--                                <td><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');       ?></span><?php // echo convertCurrency($coupans->coupon->to_amount, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));       ?></span></td>
                                                <td><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');       ?></span><?php // echo convertCurrency($coupans->coupon->from_amount, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));       ?></span></td>-->
                                            <?php // } ?>
                                            <td class="numeric" data-title="To Amount"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY ?></span><?php echo $coupans->coupon->to_amount; ?></span></td>
                                            <td class="numeric" data-title="From Amount"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY ?></span><?php echo $coupans->coupon->from_amount; ?></span></td>
                                            <td class="numeric" data-title="Number Of Used"><?php echo $coupans->coupon->no_of_used; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            <?php } else {
                                ?>
                                <div class="col-sm-12">
                                    <div class="empty-page">
                                        <div class="margin-bottom-10"><span class="icon-credit-card" aria-hidden="true"></span> No coupon found</div>

                                    </div>
                                </div>

                            <?php }
                            ?>
                            <div class="col-sm-12 margin-bottom-10" align="right"></div>
                        </div><!--Personal Info content-->
                        <div class="clr">&nbsp;</div>


                        <!-- Right widgets -->
                        <?php // $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
                        <!-- end widgets --> 
                        <!-- Popup -->
                        <div id="dialog" class="popup-module"></div>
                        <!-- end Popup --> 
                    </div>
                    <input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
                    <input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
                    <input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
                </div>
            </div>
        </section>
    </div>
</div>

