
<?php // $this->load->view('frontend/layout/leftwidgets', $this->data);   ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row">

                    <div class="col-sm-12 shadow-main" id="shopping-page" style="margin-top:20px">
                        <div class="col-sm-12"><h2 class="heading-bd"><span aria-hidden="true" class="icon-notebook"></span> Story List</h2></div>
                        <div class="col-sm-3 margin-bottom-10">
                            <a class="btn btn-custom" href = "javascript:void(0)" onclick="checkValid();" title="Delete Rows" ><span class="icon-trash" aria-hidden="true"></span> Delete Rows</a>
                        </div>  
                        <?php
                        $frmaction = 'frontend/' . $controller . '/delete_storyboard';
                        $attributes = 'id="frmDelete"';
                        echo form_open($frmaction, $attributes);
                        ?>
                        <div class="col-sm-12">
                            <div id="cart-table-new" class="table-responsive margin-bottom-10">
                                <?php
                                if (isset($storyboard) && count($storyboard) > 0) {
                                    ?>


                                    <table class="table-bordered table-striped table-condensed cf" style="width:100%" >
                                        <thead>
                                            <tr>
                                                <th ><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                                                <th class="numeric">Name</th>
                                                <th class="numeric">Image</th>
                                                <th class="numeric">Create Date</th>
                                                <th class="numeric">Action</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        foreach ($storyboard as $k => $items) {
//                        echo '<pre>';print_r($items);exit;
                                            ?>
                                            <tbody>
                                                <tr>
                                                    <td data-title="checkbox"><?php echo (form_checkbox("option[]", $items->id, '', 'class="case"')); ?></td>
                                                    <td data-title="Name" class="numeric"><?php echo $items->name; ?></td>
                                                    <td data-title="Image" class="numeric"><img src = "<?php echo $items->canvasurl ?>" alt = "" style = "width:70px; height:70px; border:2px solid #25AAE1;" title = "" /></td>
                                                    <td data-title="Create Date" class="numeric"><?php echo userdateformat($items->created, 'Y-m-d') ?></td>
                                                    <td data-title="Action" class="numeric">
                                                        <a href="<?php echo base_url() ?>storyboarddelete/<?php echo $items->id; ?>" alt="Delete" title="Delete" class="btn btn-custom" style="color: white"><span class="icon-trash delete-btn" aria-hidden="true" style="color: white"></span></a>
                                                        <a href="<?php echo base_url() ?>storyboardpdf/<?php echo $items->id ?>" target="_blank" class="btn btn-custom"><span class="icon-book-open" aria-hidden="true" style="color: white"></span></a>
                                                        <!--<a href="javascript:void()" class="email_pdf btn-mini" data="<?php // echo $items->id    ?>"><span class="icons email-icon">Email Pdf</span></a>-->
                                                        <a class="btn btn-custom org-btn default-btn mrg-n" href="<?php echo base_url() ?>storyboarddetails/<?php echo $items->id; ?>" style="color: white" >View storyboard</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        <?php } ?>
                                    </table>
                                <?php } else {
                                    ?>
                                    <div class="col-sm-12">
                                        <div class="empty-page">
                                            <div class="margin-bottom-10"><span class="icon-notebook" aria-hidden="true"></span> No Board found</div>
                                            <a href="<?php echo base_url(); ?>" class="btn btn-custom">Create new board </a>
                                        </div>
                                    </div>

                                <?php } ?>
                                <div class="col-sm-12 margin-bottom-10" align="right"></div>
                            </div><!--Personal Info content-->
                            <div class="clr">&nbsp;</div>


                            <!-- Right widgets -->
                            <?php // $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
                            <!-- end widgets --> 
                            <!-- Popup -->
                            <div id="dialog" class="popup-module"></div>
                            <!-- end Popup --> 
                        </div>


                        <input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
                        <input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
                        <input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    function toggleChecks(obj) {
        $('.case').prop('checked', obj.checked);
    }
    function checkValid() {
        if ($(".case:checked").length > 0) {
            document.getElementById('frmDelete').submit();
        } else {
            alert("You didn't select any row");
        }
    
    
    }
</script>