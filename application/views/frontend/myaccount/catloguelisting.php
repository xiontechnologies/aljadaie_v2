
<?php // $this->load->view('frontend/layout/leftwidgets', $this->data);   ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<?php
$frmaction = 'frontend/' . $controller . '/delete_catalogue';
$attributes = 'id="frmDelete"';
echo form_open($frmaction, $attributes);
?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div class="panel-body center-content"> 
                <div class="row">
                    <div class="col-sm-12 shadow-main" id="shopping-page" style="margin-top:20px">
                        <div class="col-sm-12"><h2 class="heading-bd"><span aria-hidden="true" class="icon-folder-alt"></span> Catalogue list</h2></div>
                        <div class="col-sm-3 margin-bottom-10">
                            <button class="btn btn-custom" type="button" onclick="checkValid();" ><span class="icon-trash" aria-hidden="true"></span> Delete Rows</button>
                        </div>         
                        <div class="col-sm-12">
                            <?php
                            if (isset($catalogue) && count($catalogue) > 0) {
                                ?>
                                        <!--<p class="fl marginleft10"><br /></p>-->
                                <div id="cart-table-new" class="table-responsive margin-bottom-10">
                                    <table class="table-bordered table-striped table-condensed cf" style="width:100%">
                                        <thead class="cf">
                                            <tr>
                                                <th align="center"><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                                                <th >Catalogue Name</th>
                                                <th class="numeric">Created Date</th>
                                                <th class="numeric">Action</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        foreach ($catalogue as $k => $items) {
//                        echo '<pre>';print_r($items);exit;
                                            ?>
                                            <tbody>
                                                <tr>
                                                    <td data-title="Checkbox"><?php echo (form_checkbox("option[]", $items->id, '', 'class="case"')); ?></td>
                                                    <td data-title="Catalogue Name" class="numeric"><?php echo $items->title; ?></td>
                                                    <td data-title="Created Date" class="numeric"><?php echo userdateformat($items->created) ?></td>
                                                    <td data-title="Action" class="numeric">
                                                        <a href="<?php echo base_url() ?>cataloguedelete/<?php echo $items->id; ?>" alt="Delete" title="Delete" class="btn btn-custom"><span class="icon-trash" aria-hidden="true" style="color: white"></span></a>
                                                        <a href="<?php echo base_url() ?>cataloguepdf/<?php echo $items->id ?>" target="_blank" class="btn btn-custom"><span class="icons pdf-icon icon-book-open" aria-hidden="true" style="color: white"></span></a>
                                                        <a href="javascript:void(0)" class="email_pdf btn btn-custom" data="<?php echo $items->id ?>"><span class="icons email-icon icon-login" aria-hidden="true" style="color: white"></span></a>
                                                        <a class="btn btn-custom org-btn default-btn mrg-n" href="<?php echo base_url() ?>cataloguedetails/<?php echo $items->id; ?>"><span class="delete-btn" style="color: white">View Catalogue</span></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        <?php } ?>
                                    </table>
                                <?php } else {
                                    ?>
                                    <div class="col-sm-12">
                                        <div class="empty-page">
                                            <div class="margin-bottom-10"><span class="icon-folder-alt" aria-hidden="true"></span> No catalogue found</div>
                                            <a href="<?php echo base_url(); ?>" class="btn btn-custom">Add products to catalogue </a>
                                        </div>
                                    </div>

                                <?php }
                                ?>
                                <div class="col-sm-12 margin-bottom-10" align="right"></div>
                            </div><!--Personal Info content-->
                            <div class="clr">&nbsp;</div>


                            <!-- Right widgets -->
                            <?php // $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
                            <!-- end widgets --> 
                            <!-- Popup -->
                            <div id="dialog" class="popup-module"></div>
                            <!-- end Popup --> 
                        </div>

                        <input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
                        <input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
                        <input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade main-popup col-md-6" id="emailpdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin: 0 auto 0 auto;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="product-info-popup">
                        <div class="modal-header margin-bottom-10">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="icon-close"></span></button>
                            <h4 class="modal-title">Email</h4>
                        </div>
                        <div class="modal-body" id="">
                            <form name="frme21mail" id="frme21mail" action="" method="get" class="form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <label for="email1"><span style="color: red">*</span>Email address</label> <span id="email-error1" style="color: red"></span>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="email" class="form-control" placeholder="Email address" id="email1" required="required" value="" name="email">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="subject1"><span style="color: red">*</span>Subject</label><span id="subject-error1" style="color: red"></span>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" placeholder="Subject" id="subject1" required="required" value="" name="subject">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="message1">Message</label>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="message" id="message1" class="form-control" ></textarea>
                                        </div>
                                        <span id="response11"></span>
                                    </div>
                                </div>
                                <input type="hidden" value="" name="cid" id="cid" />
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button name="send" value="send" class="btn btn-custom" id="send21mail" >Send Mail</button>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
<script>
    var site_url = '<?php echo base_url(); ?>';
    function toggleChecks(obj) {
        $('.case').prop('checked', obj.checked);
    }
    function checkValid() {
        if ($(".case:checked").length > 0) {
            document.getElementById('frmDelete').submit();
        } else {
            alert("You didn't select any row");
        }
    
    
    }
    $(document).on("click", '.email_pdf', function(e) {
    var id = $(this).attr('data');
    $('#cid').val(id);
    $('#emailpdf').modal('show');
   
});
 $(document).on('click','#send21mail', function(){
        var id = $('#cid').val();
        
        var re = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
        var email =  $('#email1').val();
        var subject = $('#subject1').val();
        var i = 0;
        if(!re.test(email)){
            $('#email-error').html("Please enter valid email");
            
            i++;
        }
        if(subject == ''){
            $('#subject-error').html("Please Enter Subject");
           
            i++
        }
        if(i == 0){
            $('#email-error').html(' ');
            $('#subject-error').html(' ');
            //$('#ajax-loader').modal('show');
            $('.ui-loader').show();
            $.ajax({
                'url' : site_url + "catalogue/emailpdf/",
                'data' : $('form#frme21mail').serialize(),
                success: function(data){
                    // $('#ajax-loader').modal('hide');
                    $('.ui-loader').hide();
                    if(data == 1){
                        alert('send mail');
                        $('#emailpdf').modal('hide');
                    }else{
                        alert('email not sent');
                       
                        $('#emailpdf').modal('hide');
                    }
                    $('form#frme21mail').each (function(){
                        this.reset();
                    });
                }
            });
            return false;
        }
        
    });
</script>