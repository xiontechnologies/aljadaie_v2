
<?php //$this->load->view('frontend/layout/leftwidgets', $this->data);   ?>
<?php
$this->load->view('frontend/layout/homewidgets', $this->data);
?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row">
                    <div class="col-sm-12" style="margin-top:20px">
                        <div class="col-sm-12"><h2 class="page-header heading-bd"><span class="icon-note" aria-hidden="true"></span> Personal Info</h2></div>
                        <div class="col-sm-12 table-responsive">
                            <?php
                            echo isset($add_error) ? $add_error : '';
                            echo isset($add_success) ? $add_success : '';
                            ?>

                            <?php echo form_open_multipart('myaccount'); ?>
                            <div class="login-form">
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> Firstname', 'firstname'); ?>
                                        <?php
                                        echo form_input(
                                                array(
                                                    'name' => 'firstname',
                                                    'class' => 'form-control',
                                                    'id' => 'firstname',
                                                    'placeholder' => 'First Name',
                                                    'value' => (isset($postdata) && isset($postdata['firstname'])) ? $postdata['firstname'] : (isset($user->firstname) ? $user->firstname : ''),
                                                    'required' => 'required'
                                        ));
                                        echo form_error('firstname');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> Lastname', 'lastname'); ?>
                                        <?php
                                        echo form_input(
                                                array(
                                                    'name' => 'lastname',
                                                    'class' => 'form-control',
                                                    'id' => 'lastname',
                                                    'placeholder' => 'Last Name',
                                                    'value' => (isset($postdata) && isset($postdata['lastname'])) ? $postdata['lastname'] : (isset($user->lastname) ? $user->lastname : ''),
                                                    'required' => 'required'
                                        ));
                                        echo form_error('lastname');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('Email', 'email'); ?>
                                        <?php
                                        echo form_input(
                                                array(
                                                    'name' => 'email',
                                                    'class' => 'form-control',
                                                    'id' => 'email',
                                                    'placeholder' => 'Email',
                                                    'readonly' => TRUE,
                                                    'value' => (isset($user->email) ? $user->email : ''),
                                                    'required' => 'required'
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> Address', 'email'); ?>
                                        <?php
                                        echo form_textarea(
                                                array(
                                                    'name' => 'address',
                                                    'class' => 'form-control',
                                                    'id' => 'address',
                                                    'placeholder' => 'Address',
                                                    'value' => (isset($postdata) && isset($postdata['address'])) ? $postdata['address'] : (($user->user_profile->address) ? $user->user_profile->address : ''),
                                                    'required' => 'required'
                                        ));
                                        echo form_error('address');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> City', 'city'); ?>
                                        <?php
                                        echo form_input(
                                                array(
                                                    'name' => 'city',
                                                    'class' => 'form-control',
                                                    'id' => 'city',
                                                    'placeholder' => 'City',
                                                    'value' => (isset($postdata) && isset($postdata['city'])) ? $postdata['city'] : (($user->user_profile) ? $user->user_profile->city : ''),
                                                    'required' => 'required'
                                        ));
                                        echo form_error('lastname');
                                        ?>
                                    </div>
                                </div>
                                <?php
//                echo convertCurrency('10', 'SAR', 'INR');
//                exit;
                                ?>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> Date of Birth', 'dob'); ?>
                                        <?php
                                        echo form_input(array('name' => 'dob',
                                            'id' => 'dob',
                                            'class' => 'dob form-control',
                                            'palceholder' => 'Date Of Birth',
                                            'value' => (isset($postdata) && isset($postdata['dob'])) ? $postdata['dob'] : (isset($user) && $user->user_profile && ($user->user_profile->dateofbirth != "") ? userdateformat($user->user_profile->dateofbirth, 'Y-m-d') : ''),
                                            'required' => 'required'
                                        ));
                                        echo form_error('dob');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> Gender', 'gender'); ?>

                                        <?php
                                        $male = (isset($postdata) && ($postdata['gender'] == 'male')) ? $postdata['gender'] : (($user->user_profile) && ($user->user_profile->gender == 'male') ? ($user->user_profile->gender) : '');
                                        $male = array(
                                            'name' => 'gender', 'id' => 'male', 'value' => 'male', 'checked' => $male,
                                        );
                                        $female = (isset($postdata) && ($postdata['gender'] == 'female')) ? $postdata['gender'] : (($user->user_profile) && ($user->user_profile->gender == 'female') ? ($user->user_profile->gender) : '');
                                        $female = array(
                                            'name' => 'gender', 'id' => 'female', 'value' => 'female', 'checked' => $female,
                                        );
                                        $extra = array('style' => 'min-width:50px;')
                                        ?>
                                        <br>
                                        <?php echo form_label('Male', 'male'); ?>
                                        <?php echo form_radio($male); ?>
                                        <?php echo form_label('Female', 'female'); ?>
                                        <?php echo form_radio($female); ?>








                                        <?php echo form_error('gender'); ?>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> Telephone', 'telephone'); ?>
                                        <?php
                                        echo form_input(array('name' => 'telephone', 'class' => 'form-control', 'placeholder' => 'Telephone',
                                            'value' => (isset($postdata) && isset($postdata['telephone'])) ? $postdata['telephone'] : (isset($user) && $user->user_profile ? $user->user_profile->telephone : '')
                                        ));
                                        ?>
                                        <?php echo form_error('telephone'); ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('Mobile', 'mobile'); ?>
                                        <?php
                                        echo form_input(array('name' => 'mobile', 'class' => 'form-control', 'placeholder' => 'Mobile',
                                            'value' => (isset($postdata) && isset($postdata['mobile'])) ? $postdata['mobile'] : (isset($user) && $user->user_profile ? $user->user_profile->mobile : '')
                                        ));
                                        ?>
                                        <?php echo form_error('mobile'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('Company', 'company'); ?>
                                        <?php
                                        echo form_input(array('name' => 'company',
                                            'id' => 'company',
                                            'palceholder' => 'Company', 'class' => 'form-control',
                                            'value' => (isset($postdata) && isset($postdata['company'])) ? $postdata['company'] : (isset($user) ? ($user->user_profile->company) : ''),
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> State/Province', 'state'); ?>
                                        <?php
                                        echo form_input(array('name' => 'state',
                                            'id' => 'state',
                                            'palceholder' => 'state', 'class' => 'form-control',
                                            'value' => (isset($postdata) && isset($postdata['state'])) ? $postdata['state'] : (isset($user) ? ($user->user_profile->state) : ''),
                                            'required' => 'required'
                                        ));
                                        echo form_error('state');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> Country', 'country'); ?>
                                        <?php
                                        echo form_input(array('name' => 'country',
                                            'id' => 'country',
                                            'palceholder' => 'country', 'class' => 'form-control',
                                            'value' => (isset($postdata) && isset($postdata['country'])) ? $postdata['country'] : (isset($user) ? ($user->user_profile->country) : ''),
                                            'required' => 'required'
                                        ));
                                        echo form_error('country');
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('<span style="color: red">*</span> Postal/Zip Code', 'zip'); ?>
                                        <?php
                                        echo form_input(array('name' => 'zip',
                                            'id' => 'zip',
                                            'palceholder' => 'zip', 'class' => 'form-control',
                                            'value' => (isset($postdata) && isset($postdata['zip'])) ? $postdata['zip'] : (isset($user) ? ($user->user_profile->zip) : ''),
                                            'required' => 'required'
                                        ));
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('Fax', 'fax'); ?>
                                        <?php
                                        echo form_input(array('name' => 'fax',
                                            'id' => 'fax',
                                            'palceholder' => 'fax', 'class' => 'form-control',
                                            'value' => (isset($postdata) && isset($postdata['fax'])) ? $postdata['fax'] : (isset($user) ? ($user->user_profile->fax) : ''),
                                        ));
                                        echo form_error('fax');
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php echo form_label('About Me', 'aboutme'); ?>
                                        <?php
                                        echo form_textarea(array('name' => 'aboutme', 'cols' => "15", 'id' => 'aboutme', 'class' => 'form-control', 'placeholder' => 'About Me', 'row' => '3',
                                            'value' => (isset($postdata) && isset($postdata['aboutme'])) ? $postdata['aboutme'] : (isset($user) && $user->user_profile ? $user->user_profile->aboutme : '')
                                        ));
                                        ?>
                                        <?php echo form_error('aboutme'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 clearfix">
                                        <?php echo form_label('Profile Picture', 'profilepicture'); ?>
                                        <?php
                                        echo form_upload('userfile');
                                        ?>
                                        <?php if (isset($user) && $user->user_profile && $user->user_profile->photo_id) { ?>
                                            <img class="my-profile" src="<?php echo PROFILEIMAGE . '/' . $user->user_profile->photo->file_name; ?>" height="100" width="100"/>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-12">

                                    <?php echo form_submit('', 'Submit', 'class="btn btn-custom"'); ?>

                                </div>
                            </div>
                            <?php echo form_close(); ?>
                            <div class="col-sm-12 margin-bottom-10" align="right"></div>
                        </div><!--Personal Info content-->
                        <div class="clr">&nbsp;</div>


                        <!-- Right widgets -->
                        <?php //$this->load->view('frontend/layout/rightwidgets', $this->data);       ?>
                        <!-- end widgets --> 
                        <!-- Popup -->
                        <div id="dialog" class="popup-module"></div>
                        <!-- end Popup --> 
                    </div>

                    <input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
                    <input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
                    <input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
                </div>
            </div>
        </section>
    </div>
</div>
