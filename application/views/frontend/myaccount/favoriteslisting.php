

<?php // $this->load->view('frontend/layout/leftwidgets', $this->data); ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<?php
$frmaction = 'frontend/' . $controller . '/delete_catalogue';
$attributes = 'id="frmDelete"';
echo form_open($frmaction, $attributes);
?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row">
                    
                    <div class="col-sm-12 shadow-main" id="shopping-page" style="margin-top:20px">
                        <div class="col-sm-12"><h2 class="heading-bd"><span aria-hidden="true" class="icon-star"></span> Favorite list</h2></div>
                        <div class="col-sm-3 margin-bottom-10">
                            <button class="btn btn-custom" type="button" onclick="checkValid();" ><span class="icon-trash" aria-hidden="true"></span> Delete Rows</button>
                        </div>             


                        <div class="col-sm-12">
                            <div id="cart-table-new" class="table-responsive margin-bottom-10">
                                <?php
                                if (isset($fav) && count($fav) > 0) {
                                    ?>

                                    <table width="100%" class="table-bordered table-striped table-condensed cf">
                                        <thead class="cf">
                                            <tr>
                                                <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                                                <th>product Name</th>
                                                <th class="numeric">Photo</th>
                                                <th class="numeric">Price</th>      
                                                <th class="numeric">Stock</th>      
                                                <th class="numeric">Action</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        foreach ($fav as $k => $items) {
                                            ?>
                                            <tbody>
                                                <tr>
                                                    <td data-title="Checkbox" align="center"><?php echo (form_checkbox("option[]", $items->id, '', 'class="case"')); ?></td>
                                                    <td data-title="product Name"  ><?php echo $items->product->title; ?></td>
                                                    <td data-title="Photo" class="sorting_1 numeric"><img src="<?php echo $items->product->thumimagepath ?>" height="50" width="50"></td>
                                                    <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                                        <!--<td class="sorting_1"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;    ?></span><?php // echo$items->product->price;    ?></td>-->
                                                    <?php // } else { ?>
                                                        <!--<td class="sorting_1"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');    ?></span><?php // echo convertCurrency($items->product->price, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));    ?></td>-->
                                                    <?php // } ?>
                                                    <td data-title="Price" class="sorting_1 numeric"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $items->product->price; ?></td>
                                                    <td data-title="Stock" class="sorting_1 numeric"><?php echo $items->product->stock ?></td>
                                                    <td data-title="Action" class="numeric">
                                                        <a href="<?php echo base_url() ?>favdelete/<?php echo $items->id; ?>" alt="Delete" title="Delete" class="btn btn-custom" ><span class="icon-trash" aria-hidden="true" style="color: white"></span></a>
                                                        <?php if (isInCart($this->cart->contents(), 'id', $items->product->id)) { ?>
                                                            <a href="<?php echo base_url('cartlist') ?>" class="btn btn-success" style="color: white">View Cart</a>
                                                        <?php } else { ?>
                                                            <a class="btn btn-custom add-to-cart-details" href="<?php echo base_url('addtocart') . '/' . $items->product->id; ?>" style="color: white" >Add to Cart </a>  
                                                        <?php } ?>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        <?php } ?>
                                    </table>
                                <?php } else {
                                    ?>
                                <div class="col-sm-12">
                                    <div class="empty-page">
                                        <div class="margin-bottom-10"><span class="icon-star" aria-hidden="true"></span> No favorite products found</div>
                                        <a href="<?php echo base_url(); ?>" class="btn btn-custom">Add products to favorites</a>
                                    </div>
                                </div>
                                    

                                <?php } ?>
                                <div class="col-sm-12 margin-bottom-10" align="right"></div>
                            </div><!--Personal Info content-->
                            <div class="clr">&nbsp;</div>


                            <!-- Right widgets -->
                            <?php // $this->load->view('frontend/layout/rightwidgets', $this->data);   ?>
                            <!-- end widgets --> 
                            <!-- Popup -->
                            <div id="dialog" class="popup-module"></div>
                            <!-- end Popup --> 
                        </div>

                        <input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
                        <input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
                        <input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    function toggleChecks(obj) {
        $('.case').prop('checked', obj.checked);
    }
    function checkValid() {
        if ($(".case:checked").length > 0) {
            document.getElementById('frmDelete').submit();
        } else {
            alert("You didn't select any row");
        }
    
    
    }
    
</script>