
<?php // $this->load->view('frontend/layout/leftwidgets', $this->data);  ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row">
                    
                    <div class="col-sm-12 shadow-main" id="shopping-page" style="margin-top:20px">
                        <div class="col-sm-12"><h2 class="heading-bd"><span aria-hidden="true" class="icon-folder-alt"></span> Catlogue</h2></div>
                        <div class="col-sm-6 margin-bottom-10">
                            <a class="btn btn-custom" href="javascript:history.go(-1)" title="Back" ><span class="icon-arrow-left" aria-hidden="true"></span> Back</a>
                            <a class="btn btn-custom" href = "javascript:void(0)" onclick="checkValid();" title="Delete Rows" ><span class="icon-trash" aria-hidden="true"></span> Delete Rows</a>
                        </div>  
                        <?php if (isset($signuperror) && !empty($signuperror)) { ?>
                            <span class="error"><?php echo $signuperror; ?></span>
                        <?php } ?>
                        <?php if (isset($success) && !empty($success)) { ?>
                            <span class="error"><?php echo $success; ?></span>
                        <?php } ?>
                        <?php
                        $frmaction = 'frontend/' . $controller . '/delete_product_list';
                        $attributes = 'id="frmDelete"';
                        echo form_open($frmaction, $attributes);
                        ?>

                        <div class="col-sm-12">
<div id="cart-table-new" class="table-responsive margin-bottom-10">
                            <?php
                            if (isset($catalogue) && count($catalogue) > 0) {
                                ?>
                                <table class="table-bordered table-striped table-condensed cf" style="width:100%">
                                    <thead class="cf">
                                        <tr>
                                            <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                                            <th class="numeric">Product Name</th>
                                            <th class="numeric">SKU</th>
                                            <th class="numeric">Colorcode</th>
                                            <th class="numeric">Product</th>
                                            <th class="numeric">Price</th>  
                                            <th class="numeric">Action</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    foreach ($catalogue as $k => $items) {
//                        echo '<pre>';print_r($items);exit;
                                        ?>
                                        <tbody>
                                            <tr>
                                                <td data-title="Checkbox" class="numeric"><?php echo (form_checkbox("option[]", $items->product_id, '', 'class="case"')); ?></td>
                                                <td data-title="Product Name" class="numeric"><?php echo $items->title; ?></td>
                                                <td data-title="SKU" class="numeric"><?php echo $items->sku; ?></td>
                                                <td data-title="Colorcode" class="numeric"><?php echo $items->colorcode; ?></td>
                                                <td data-title="Product"class="numeric"><img src="<?php echo base_url(); ?>/<?php echo PRODUCTIMAGE_PATH; ?>/thumb/<?php echo $items->file_name; ?>" height="50" width="50"></td>
                                                <td data-title="Price" class="center numeric"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $items->price; ?></td>
                                                <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                                    <!--<td class="center"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo $items->price;   ?></td>-->
                                                <?php // } else { ?>
                                                    <!--<td class="center"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency($items->price, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));   ?></td>-->
                                                <?php // } ?>

                                                <td data-title="Action" class="numeric"><a href="<?php echo base_url() ?>cataloguedeleteproduct/<?php echo $items->product_id; ?>" alt="Delete" title="Delete" class="btn btn-custom">
                                                        <span class="icon-trash delete-btn" aria-hidden="true" style="color: white"></span></a></td>
                                            </tr>
                                        </tbody>
                                    <?php } ?>
                                </table>
                            <?php } else {
                                ?>
                                 <div class="col-sm-12">
                                        <div class="empty-page">
                                            <div class="margin-bottom-10"><span class="icon-folder-alt" aria-hidden="true"></span> No catalogue found</div>
                                            <a href="<?php echo base_url(); ?>" class="btn btn-custom">Add products to catalogue </a>
                                        </div>
                                    </div>

                            <?php }
                            ?>
                            <div class="col-sm-12 margin-bottom-10" align="right"></div>
                        </div><!--Personal Info content-->
                        <div class="clr">&nbsp;</div>


                        <!-- Right widgets -->
                        <?php // $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
                        <!-- end widgets --> 
                        <!-- Popup -->
                        <div id="dialog" class="popup-module"></div>
                        <!-- end Popup --> 
                    </div>
                    <input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
                    <input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
                    <input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    function toggleChecks(obj) {
        $('.case').prop('checked', obj.checked);
    }
    function checkValid() {
        if ($(".case:checked").length > 0) {
            document.getElementById('frmDelete').submit();
        } else {
            alert("You didn't select any row");
        }
    
    
    }
</script>