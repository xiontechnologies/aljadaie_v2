<?php
/*
 * @auther Shafiq
 */
?>


<?php $this->load->view('frontend/layout/leftwidgets', $this->data); ?>
<?php $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <?php
        echo form_open('home/search', array('id' => 'search', 'class' => 'form-inline search-top'));
        ?>
        <div class="form-group">
            <label>Search Fabric by Item:</label>
        </div>
        <?php
        $searchitem = array(
            'name' => 'item_kind',
            'type' => 'text',
            'id' => 'item_kind',
            'value' => '',
            'placeholder' => '',
            'class' => 'form-control txt1',
            'maxlength' => 2
        );
        $searchdefination = array(
            'name' => 'item_definition',
            'id' => 'item_definition',
            'value' => '',
            'placeholder' => '',
            'class' => 'form-control txt2',
            'maxlength' => 2
        );
        $searchcode = array(
            'name' => 'collection',
            'id' => 'collection',
            'value' => '',
            'placeholder' => '',
            'class' => 'form-control input-big txt3',
            'maxlength' => 6
        );
        $searchdesign = array(
            'name' => 'product_design',
            'id' => 'product_design',
            'value' => '',
            'placeholder' => '',
            'class' => 'form-control txt4',
            'maxlength' => 2
        );
        $searchcolor = array(
            'name' => 'colors',
            'id' => 'searchcolor',
            'value' => '',
            'placeholder' => '',
            'class' => 'form-control txt5',
            'maxlength' => 3
        );

        $searchbutton = array(
            'name' => 'searchbutton',
            'value' => 'Search',
            'class' => 'btn red-btn',
        );
        ?>

        <div class="form-group"><?php echo form_input($searchitem); ?></div>
        <div class="form-group"><?php echo form_input($searchdefination); ?></div>
        <div class="form-group"><?php echo form_input($searchcode); ?></div>
        <div class="form-group"><?php echo form_input($searchdesign); ?></div>
        <div class="form-group"><?php echo form_input($searchcolor); ?></div>
        
        <button class="btn btn-custom" type="submit"  style="margin-top: 5px"><span aria-hidden="true" class="icon-magnifier"></span></button>
        <button class="btn btn-custom" type="button"  data-toggle="modal" data-target="#adv-search" style="margin-top: 5px">Advance Filter </button>
        
        <?php
        echo form_close();
?>




        <section class="panel" style="background:none;">
            <div class="panel-body center-content">
                <div class="banner-text">
                    <div class="media"> <a class="pull-left" href="#">
                            <div class="icon-left"> <span class="icon-layers" aria-hidden="true"></span> </div>
                        </a>
                        <div class="media-body">
                            <p>Step1</p>
                            <h3>Choose Fabric</h3>
                        </div>
                    </div>
                    <div class="media"> <a class="pull-left" href="#">
                            <img src="<?php echo base_url(); ?>/assets/frontend/images/arrow.png" class="img-responsive">
                        </a>
                    </div>

                    <div class="media"> <a class="pull-left" href="#">
                            <div class="icon-left"> <span class="icon-settings" aria-hidden="true"></span> </div>
                        </a>
                        <div class="media-body">
                            <p>Step2</p>
                            <h3>Use Visualizer</h3>
                        </div>
                    </div>
                    <div class="media"> <a class="pull-left" href="#">
                            <img src="<?php echo base_url(); ?>/assets/frontend/images/arrow.png" class="img-responsive">
                        </a>
                    </div>
                    <div class="media"> <a class="pull-left" href="#">
                            <div class="icon-left"> <span class="icon-basket-loaded" aria-hidden="true"></span> </div>
                        </a>
                        <div class="media-body">
                            <p>Step3</p>
                            <h3>Add to Cart</h3>
                        </div>
                    </div>
                </div>

                <div class="flexslider">
                    <ul class="slides">

                        <?php
                        foreach ($slider as $k => $v) {
                            ?>
                            <li><img class="img-responsive width-100"  src="<?php echo base_url('uploads/slider') . '/' . $v->photo->file_name; ?>" /></li>
<?php } ?>
                    </ul>
                </div>

                <article class="visualizer <?php if ($action != 'editstoryboard') { ?>display-none<?php } ?>">
                    <div class="visualizer-module centerAll">
                        <div class="image-article">
        <!--                    <span id="fullscreen" ><img src="<?php //echo base_url('assets/frontend/images/full screen.png')     ?>" alt="full sreen"/></span>-->
                            <!--<a href="<?php // echo base_url('storyboard');                        ?>" id="save-visulizer">Save</a>-->
                            <img id="canvaspreloader" style="display:none" src="<?php echo base_url('assets/images/loading.gif') ?>" alt="" title="" class="img" />
                            <img id="canvas" src="#" alt="" title="" class="img-responsive width-100 fab-modal-body" /></div>
                            <img id="canvaszoom" src="#" alt="" title="" class="" style="display: none"/></div>
                    </div>
                </article>

        </section>
            
            <!-- Popup -->

      
        <div id="imgContainer" style="display:none">
            <a class="btn btn-large pull-right" style="margin-top:7px !important;" id="maskClose"><span aria-hidden="true" class="icon-close" style="font-size: 20px"></span></a>
            <img src="#" title="" alt="" id="imageFullScreen">
            <div id="positionButtonDiv">
                <p>Zoom : 
                    <span>
                        <img id="zoomInButton" class="zoomButton" src="<?php echo base_url() ?>assets/frontend/images/zoomIn.png" title="zoom in" alt="zoom in" />
                        <img id="zoomOutButton" class="zoomButton" src="<?php echo base_url() ?>assets/frontend/images/zoomOut.png" title="zoom out" alt="zoom out" />
                    </span>
                </p>
                <p>
                    <span class="positionButtonSpan">
                        <map name="positionMap" class="positionMapClass">
                            <area id="topPositionMap" shape="rect" coords="20,0,40,20" title="move up" alt="move up"/>
                            <area id="leftPositionMap" shape="rect" coords="0,20,20,40" title="move left" alt="move left"/>
                            <area id="rightPositionMap" shape="rect" coords="40,20,60,40" title="move right" alt="move right"/>
                            <area id="bottomPositionMap" shape="rect" coords="20,40,40,60" title="move bottom" alt="move bottom"/>
                        </map>
                        <img src="<?php echo base_url() ?>assets/frontend/images/position.png" usemap="#positionMap" />
                    </span>
                </p>
            </div>
        </div>
        <div class="mask"  style="display:none">&nbsp;</div>
        <div id="dialog" class="popup-module"></div>
        <!-- end Popup --> 

    </div>
</div>
    <input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
<input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
<input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
<script type="text/javascript">
$(document).on('keyup','.txt1',function() {
     if(this.value.length == $(this).attr('maxlength')) {
         $('.txt2').focus();
     }
 });
 $(document).on('keyup','.txt2',function() {
     if(this.value.length == $(this).attr('maxlength')) {
         $('.txt3').focus();
     }
 });
 $(document).on('keyup','.txt3',function() {
     if(this.value.length == $(this).attr('maxlength')) {
         $('.txt4').focus();
     }
 });
 $(document).on('keyup','.txt4',function() {
     if(this.value.length == $(this).attr('maxlength')) {
         $('.txt5').focus();
     }
 });
</script>