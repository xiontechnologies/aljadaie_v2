<?php
ob_start();
/*
 * @auther Shafiq
 */
?>
<!DOCTYPE html>
<html lang="en-gb">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo SITE_NAME; ?> | <?php echo $add_title; ?></title>
		
		<!-- below css added by urvisha -->
		<!--<link href="<?php //echo base_url();?>/assets/frontend/css/style_adj.css" rel="stylesheet">-->
		<!--<link href="<?php //echo base_url();?>/assets/frontend/css/basic.css" rel="stylesheet">-->
		<!--  end --->
                <script type="text/javascript">var switchTo5x = true; var site_url='<?php echo base_url(); ?>';</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url();?>/assets/frontend/css/bootstrap.css" rel="stylesheet">
		
        <!-- Aljedaie theme -->
        <link href="<?php echo base_url();?>/assets/frontend/css/style.css" rel="stylesheet">
        <!--<link href="<?php // echo base_url();?>/assets/frontend/css/old_edit_style.css" rel="stylesheet">-->
        <link href="<?php echo base_url();?>/assets/frontend/css/bootstrap-mod.css" rel="stylesheet">
        <link href="<?php echo base_url();?>/assets/frontend/css/simple-line-icons.css" rel="stylesheet">
        <link href="<?php echo base_url();?>/assets/frontend/css/owl.carousel.css" rel="stylesheet">
		<link href="<?php echo base_url();?>/assets/frontend/css/flexslider.css" rel="stylesheet">

        <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>-->
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="<?php echo base_url();?>/assets/frontend/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<?php echo base_url();?>/assets/frontend/js/ie-emulation-modes-warning.js"></script>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?php echo base_url();?>/assets/frontend/js/ie10-viewport-bug-workaround.js"></script>
        
	
        
		

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		
		
      
        <?php
       // echo $add_css;
			echo $add_js; 
        ?>
        
        <!-- select2 jquery -->
<!--        <link href="<?php //echo base_url();?>assets/frontend/select2/select2.css" rel="stylesheet">
        <link href="<?php //echo base_url();?>assets/frontend/select2/select2-bootstrap.css" rel="stylesheet">
        <script src="<?php //echo base_url();?>assets/frontend/select2/select2.js"></script>-->
        <link href="<?php echo base_url();?>assets/frontend/css/bootstrap-multiselect.css" rel="stylesheet">
        <script src="<?php echo base_url();?>assets/frontend/js/bootstrap-multiselect.js"></script>
        <!-- tooltip -->
        
    </head>

    <body style="overflow-x: hidden !important;">

        <!--Arrow Navigation-->
        <a id="prevslide" class="load-item"></a>
        <a id="nextslide" class="load-item"></a>

        <!-- Header -->
        <!--<header>-->
            <!--<div id="header-section">-->
			<?php $page_uri = $_SERVER["REQUEST_URI"];
			$page_array = explode('/', $page_uri);
			$page = array_pop($page_array); ?>
                <div class="navbar navbar-inverse red-navigation" role="navigation">
                <div class="container">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>" title="<?php echo SITE_NAME; ?>"><?php echo SITE_NAME; ?><img src="<?php echo base_url();?>assets/frontend/images/<?php if ($this->session->userdata('logo-image')){ echo $this->session->userdata('logo-image'); } else { echo 'logo-en.jpg'; }?>" class="img-responsive" id="logo-image"></a>
                    <!--<a href="index.html"><img src="<?php echo base_url();?>/assets/frontend/images/logo.png" class="img-responsive"></a>-->
                  </div>
                  <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <?php if ($this->session->userdata(SITE_NAME . '_user_data')) { 
                             $userdata = $this->session->userdata(SITE_NAME . '_user_data');
                            $user = Userprofile::find_by_user_id($this->session->userdata[SITE_NAME . '_user_data']['user_id']);
                            $path = "";
                            ?>
						<?php if($page == 'cataloguelist'){ echo "<li class='active'>"; } else{ echo '<li>'; } ?>	
                        <a href="<?php echo base_url('cataloguelist') ?>"><span class="icon-folder-alt" aria-hidden="true"></span> Catalogue</a></li>
						<?php if($page == 'favoriteslist'){ echo "<li class='active'>"; } else{ echo '<li>'; } ?>
                      <a href="<?php echo base_url('favoriteslist') ?>"><span class="icon-star" aria-hidden="true"></span> Favorites</a></li>
					 <?php if( ($page != 'cartlist') && ($page != 'favoriteslist') && ($page != 'cataloguelist') ){ $active = 'active'; } else{ $active = ''; } ?>
                      <li class='<?php echo $active; ?> dropdown'><a href="<?php //echo base_url(); ?>" class="dropdown-toggle" data-toggle="dropdown"><!--<span>
                                  <?php //if (isset($user->photo_id) && !empty($user->photo_id)) { ?>
                      <img src="<?php //echo base_url(PROFILEIMAGE); ?>/<?php //echo $user->photo->file_name; ?>" alt="<?php //echo $user->photo->raw_name; ?>" style="width:32px; height: 32px" class="fl" title="<?php //echo $user->photo->raw_name; ?>" />
                      <?php //} else { ?>
                      <img src="<?php //echo base_url(); ?>assets/frontend/images/user.png" alt="user" style="width:32px; height: 32px" class="fl" title="user" />
                     <?php //} ?>
                              </span>-->Welcome <?php echo isset($userdata['firstname']) ? $userdata['firstname'] : 'Guest !'; ?> <span class="caret"></span></a>
                          <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo base_url('myaccount') ?>" style="text-align:left;">My Account</a></li>
                                <li><a href="<?php echo base_url('changepassword') ?>" style="text-align:left;">Change Password</a></li>
                                <!--<li><a href="<?php //echo base_url('home/coupon') ?>" style="text-align:left;">Coupon</a></li>-->
                                <li><a href="<?php echo base_url('home/orders') ?>" style="text-align:left;">My Orders</a></li>
<!--                                <li><a href="<?php //echo base_url('cataloguelist') ?>" style="text-align:left;">Catalogue</a></li>
                                <li><a href="<?php //echo base_url('favoriteslist') ?>" style="text-align:left;">Favorites</a></li>-->
                                <li><a href="<?php echo base_url('storyboardlist') ?>" style="text-align:left;">Storyboard</a></li>
                                <li><a href="<?php echo base_url('frontend/auth/logout'); ?>" style="text-align:left;">Logout</a></li>
                            </ul>
                        </li>
						<?php if($page == 'cartlist'){ echo "<li class='active'>"; } else{ echo '<li>'; } ?>
                      <a href="<?php echo base_url('cartlist') ?>"><span class="icon-basket-loaded" aria-hidden="true"></span> <?php if ($this->session->userdata(SITE_NAME . '_user_data')) { 
                          echo '<span class="cart-count">'.count($this->cart->contents())."</span>&nbsp;";
                      }
?> Item in Cart</a></li>
                      <?php } ?>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="lang-title"><?php if ($this->session->userdata('lang-title')){ echo $this->session->userdata('lang-title'); } else { ?>English <span class="caret"></span><?php } ?></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="lang-select pull-left" id="ab1">Arabic</a> </li>
                          <li><a href="#" class="lang-select pull-left" id="en1">English</a> </li>
                        </ul>
                      </li>
                      
                      
                    </ul>
                  </div><!--/.nav-collapse -->
                </div>
              </div>
            <div class="white-navigation">
             <?php if ($this->session->userdata(SITE_NAME . '_user_data')) { ?>
            <div class="container">
          <ul class="pull-right">  <!--st_sharethis-->
                <li id="add-a">
                        <a href="javascript:void(0)" title="SHARE" class="image-share-lib" displayText='ShareThis' id="button_1"><span class=" icon-share" aria-hidden="true"></span> <b>Share</b></a>
                </li>
                <li class="select-fabric-view">
                    <a href="#" class="downloadpdf"><span class="icon-cloud-download" aria-hidden="true"></span> <b>Download</b></a>
                </li>
                <li class="select-fabric-view">
                    <a href="#" class="emailpdf"><span class="icon-envelope" aria-hidden="true"></span> <b>Email</b></a>
                </li>
                <li class="select-fabric-view">
                    <a href="#"><span class="icon-printer" aria-hidden="true"></span> <b>HD Print</b></a>
                </li>
<!--                <li class="select-fabric-view">
                    <a href="#"><span class="icon-check" aria-hidden="true"></span> <b>Save</b></a>
                </li>-->
                <li class="select-fabric-view">
                    <a href="#" id="fullscreen"><span class="icon-size-fullscreen" aria-hidden="true"></span> <b>Fullscreen</b></a>
                </li>
            </ul>
            <?php } ?>
            <div class="clearfix"></div>
            </div>
        </div>
         <!-- /container -->
         <script>
             $(document).on('click','.lang-select', function (){
                 var id = $(this).attr('id');
                 $.ajax({
                    'url': "<?php echo base_url();?>" + "product/storeval",
                    'data': {
                        val: id
                    },
                    success: function (data) {
                        //window.location.reload();
                        var str = data.split('_');
                        var lan = str[0]+' <span class="caret"></span>';
                        $('#lang-title').html(lan);
                        
                        var img = '<?php echo base_url();?>assets/frontend/images/'+str[1];
                        $('#logo-image').attr('src',img);
                       
                    }
                });
             });
         </script>
         
	
	  
			
			
			   
      
        <!--</div>-->
        <!--</header>-->
        <!-- end Header --> 
        <div class="modal fade main-popup col-md-6" id="email2pdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin: 0 auto 0 auto;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="product-info-popup">
                        <div class="modal-header margin-bottom-10">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="icon-close"></span></button>
                            <h4 class="modal-title">Email</h4>
                        </div>
                        <div class="modal-body" id="">
                            <form name="frmemail" id="frmemail" action="" method="get" class="form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <label for="email"><span style="color: red">*</span>Email address</label> <span id="email-error" style="color: red"></span>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="email" class="form-control" placeholder="Email address" id="email" required="required" value="" name="email">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="subject"><span style="color: red">*</span>Subject</label><span id="subject-error" style="color: red"></span>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" placeholder="Subject" id="subject" required="required" value="" name="subject">
                                        </div>
                                        <div class="col-md-12">
                                            <label for="message">Message</label>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="message" id="message" class="form-control" ></textarea>
                                        </div>
                                        <span id="response1"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button name="send" value="send" class="btn btn-custom" id="send-mail" >Send Mail</button>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
