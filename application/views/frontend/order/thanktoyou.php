
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row">
                    
                    <div class="col-sm-12 shadow-main" id="shopping-page" style="margin-top:20px">
                        <div class="col-sm-12">
                            <h2 class="heading-bd">
                                <span aria-hidden="true" class="icon-flag"></span> Thank You For Order 
                                <span class="pull-right" style="margin-top:-10px"><a href="<?php echo base_url(); ?>" class="btn btn-custom" alt="Continue Shopping">Continue Shopping</a></span>
                            </h2>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <p><b><?php echo $this->session->userdata[SITE_NAME . '_user_data']['name']; ?></b>, Thank you for ordering with us.</p>
                                <p>A mail with complete details of your order will be sent to you shortly.
                                Your order will be despatched within 7days from now.</p>
                                <p>Should you need to speak to us regarding to your order, please feel free to contact us at order@aljedaie.com</p>
                                <p>We hope you love your custom fabric product and we look forward to seeing you more often here.</p>
                                <p>Thanks & Keep visualizing.<br>
                                team@aljedaie.com</p>
                            </div>
                            <div class="col-sm-6">
                                <!--<img src="<?php //echo base_url();?>assets/frontend/images/thankyou.png" class="img-responsive" >-->
                            </div>
                           
                        </div>
                   
                        <div class="col-sm-12">
                                 <hr>
                            <h2 class="heading-bd">
                                <span aria-hidden="true" class="icon-notebook"></span> Order Details
                            </h2>
                        </div>
                        <div class="order-table">
                            <?php
//                            if ($add_error)
//                                echo $add_error;
//                            if ($add_success)
//                                echo $add_success;
                            ?>

                            <div class="col-sm-12 table-responsive">
                                <?php
                                if (isset($allorders) && count($allorders) > 0) {
                                    ?>

                                    <table class="table-bordered table-striped table-condensed cf" style="width:100%">
                                        <thead>
                                            <tr>                            
                                                <th class="numeric">Order Number</th>
                                                <th class="numeric">Order Date</th>
                                                <th class="numeric">Order Status</th>
                                                <th class="numeric">Amount</th>
                                                <th class="numeric">Action</th>
                                            </tr>
                                        </thead>
                                        <?php foreach ($allorders as $k => $items) { ?>
                                            <tbody>
                                                <tr>                               
                                                    <td class="numeric"><?php echo date("Ymd", strtotime($items->created_date)) . '000' . $items->id; ?></td>
                                                    <td class="numeric"><?php echo userdateformat($items->created_date); ?></td>
                                                    <td class="numeric"><span class="label label-warning"><?php echo $items->order_status; ?></span></td>
                                                    <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                                        <!--<td><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo $items->total_amt;   ?></td>-->
                                                    <?php // } else { ?>
                                                        <!--<td><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency($items->total_amt, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));   ?></td>-->
                                                    <?php // } ?>
                                                    <td class="numeric"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span> <?php echo $items->total_amt; ?></td>
                                                    <td class="numeric"><a href="<?php echo base_url() ?>productlisting/<?php echo $items->id; ?>" title="products" class="btn btn-custom org-btn default-btn mrg-n">
                                                            View Products</a> </td>
                                                </tr>
                                            </tbody>
                                        <?php } ?>
                                    </table>
                                <?php } else {
                                    ?>
                                <div class="col-sm-12">
                                        <div class="empty-page">
                                            <div class="margin-bottom-10"><span class="icon-notebook" aria-hidden="true"></span> No order found</div>
                                            <a href="<?php echo base_url(); ?>" class="btn btn-custom">Continue shopping </a>
                                        </div>
                                    </div>

                                <?php } ?>
                                <div class="col-sm-12 margin-bottom-10" align="right"></div>
                            </div><!--Personal Info content-->

                        </div>
                        <div class="col-sm-6">
                                 <hr>
                            <h2 class="heading-bd">
                                <span aria-hidden="true" class="icon-directions"></span> Billing Address:
                            </h2>
                                 <div class="col-sm-12">
                                <?php foreach ($allorders as $k => $items) { 
//                                   echo $items->first_name.' '.$items->last_name.'<br>';
//                                   echo $items->address.',<br>';
//                                   echo $items->city.'<br>';
//                                   echo $items->state.'<br>';
//                                   echo $items->country.'-'.$items->postal_code."<br>";
//                                   echo 'Mo:.'.$items->phone_number;
                                   echo '<p>'.$items->first_name.' '.$items->last_name.', ';
                                   echo $items->address.', ';
                                   echo $items->city.', ';
                                   echo $items->state.', ';
                                   echo $items->country.','.$items->postal_code.", ";
                                   echo $items->phone_number.'</p>';
                                   
                                 } ?>
                                 </div>
                        </div>
                        <div class="col-sm-6">
                                 <hr>
                            <h2 class="heading-bd">
                                <span aria-hidden="true" class="icon-directions"></span> Shipping Address:
                            </h2>
                                 <div class="col-sm-12">
                                 <?php foreach ($allorders as $k => $items) { 
                                     echo '<p>'.str_replace(",", ", ", $items->shipping_address).'</p>';
                                  
                                 } ?>
                                 </div>
                        </div>
                         <div class="col-sm-12 margin-bottom-10" align="right"></div>
                    </div>
                </div>
        </section>
    </div>
</div>