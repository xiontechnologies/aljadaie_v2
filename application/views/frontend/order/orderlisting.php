<?php // $this->load->view('frontend/layout/leftwidgets', $this->data);                          ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row">
                    
                    <div class="col-sm-12 shadow-main" id="shopping-page" style="margin-top:20px">
                        <div class="col-sm-12"><h2 class="heading-bd"><span aria-hidden="true" class="icon-notebook"></span> Order History</h2></div>
                        <div class="order-table">
                            <?php
                            if ($add_error)
                                echo $add_error;
                            if ($add_success)
                                echo $add_success;
                            ?>

                            <div class="col-sm-12 table-responsive">
                                <?php
                                if (isset($allorders) && count($allorders) > 0) {
                                    ?>

                                    <table class="table-bordered table-striped table-condensed cf" style="width:100%">
                                        <thead>
                                            <tr>                            
                                                <th class="numeric">Order Number</th>
                                                <th class="numeric">Order Date</th>
                                                <th class="numeric">Order Status</th>
                                                <th class="numeric">Amount</th>
                                                <th class="numeric">Action</th>
                                            </tr>
                                        </thead>
                                        <?php foreach ($allorders as $k => $items) { ?>
                                            <tbody>
                                                <tr>                               
                                                    <td class="numeric"><?php echo date("Ymd", strtotime($items->created_date)) . '000' . $items->id; ?></td>
                                                    <td class="numeric"><?php echo userdateformat($items->created_date); ?></td>
                                                    <td class="numeric"><span class="label label-warning"><?php echo $items->order_status; ?></span></td>
                                                    <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                                        <!--<td><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo $items->total_amt;   ?></td>-->
                                                    <?php // } else { ?>
                                                        <!--<td><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency($items->total_amt, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));   ?></td>-->
                                                    <?php // } ?>
                                                    <td class="numeric"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span> <?php echo $items->total_amt; ?></td>
                                                    <td class="numeric"><a href="<?php echo base_url() ?>productlisting/<?php echo $items->id; ?>" title="products" class="btn btn-custom org-btn default-btn mrg-n">
                                                            View Products</a> </td>
                                                </tr>
                                            </tbody>
                                        <?php } ?>
                                    </table>
                                <?php } else {
                                    ?>
                                <div class="col-sm-12">
                                        <div class="empty-page">
                                            <div class="margin-bottom-10"><span class="icon-notebook" aria-hidden="true"></span> No order found</div>
                                            <a href="<?php echo base_url(); ?>" class="btn btn-custom">Continue shopping </a>
                                        </div>
                                    </div>

                                <?php } ?>
                                <div class="col-sm-12 margin-bottom-10" align="right"></div>
                            </div><!--Personal Info content-->

                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>