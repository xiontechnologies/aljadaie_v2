<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="main-content">
    <div class="container">
        <section class="panel">
            <div id="content-section " class="panel-body center-content"> 
                <div class="row">
                    <div class="col-sm-12 shadow-main" id="shopping-page" style="margin-top:20px">
                        <div class="col-sm-3 margin-bottom-10 margin-top-10">
                            <a class="btn btn-custom" href="javascript:void(0)" onclick="history.go(-1);return false;"  title="Back" ><span class="icon-arrow-left" aria-hidden="true"></span> Back</a>
                        </div>

                        <?php foreach ($allproducts as $order) { ?>
                            <div class="col-sm-12" >
                                <div class="col-sm-6">
                                    <h2 class="heading2"><span>Order Number : <?php echo date("Ymd", strtotime($order->created_date)) . '000' . $order->id; ?></h2>
                                </div>
                                <div class="col-sm-6">
                                    <h2 class="heading2"> <span class="fr"> Status : <?php echo $order->order_status; ?></span> 
                                </div>
                                <h2 class="page-header heading-bd"><span style="color: #ffffff">.</span></h2>
                                <div class="col-sm-12 table-responsive">

                                    <div class="Billing Address clearfix" style="margin-bottom: 20px;">
                                        <div class="fl col-sm-12">
                                            <div class="col-sm-6">
                                                <h4>Billing Address</h4>
                                                <span><?php echo $order->address; ?></span>
                                                <span><?php echo $order->city; ?></span>
                                                <span><?php echo $order->state; ?></span>
                                                <span><?php echo $order->country; ?></span>
                                            </div>
                                            <div class="fr col-sm-6">
                                                <h4>Shipping Address</h4>
                                                <span><?php echo $order->shipping_address; ?></span>      
                                            </div>
                                        </div>

                                    </div>  
                                <?php } ?>
                                <?php
                                if (isset($allproducts) && count($allproducts) > 0) {
                                    ?>               

                                    <table  class="table-bordered table-striped table-condensed" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th class="numeric">Product Name</th>
                                                <th class="numeric">Order Date</th>
                                                <th class="numeric">sku</th>
                                                <th class="numeric">colorcode</th>
                                                <th class="numeric">collection</th> 
                                                <th class="numeric">price</th>                           
                                                <th class="numeric">Quantity</th>  
                                                <th class="numeric">Total</th>
                                            </tr>
                                        </thead>                    
                                        <?php
                                        $amount = 0;
                                        foreach ($allproducts as $order) {//                          
                                            foreach ($order->orderdetail as $orderdetail) {
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td class="sorting_1 numeric"><?php echo $orderdetail->product->title; ?></td>
                                                    <td class="center numeric"><?php echo userdateformat($order->created_date); ?></td>
                                                    <td class="sorting_1 numeric"><?php echo $orderdetail->product->sku; ?></td>
                                                    <td class="center numeric"><?php echo $orderdetail->product->colorcode; ?></td>
                                                    <td class="center numeric"><?php echo $orderdetail->product->collection; ?></td>
                                                    <td class="center numeric"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $orderdetail->product->price; ?></td>
                                                    <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                                        <!--<td class="center"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo  $orderdetail->product->price;   ?></td>-->
                                                    <?php // } else { ?>
                                                        <!--<td class="center"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency( $orderdetail->product->price, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));   ?></td>-->
                                                    <?php // } ?>
                                                    <td class="center numeric"><?php echo $orderdetail->quantity; ?></td>
                                                    <?php
                                                    $total = ($orderdetail->quantity) * ($orderdetail->product->price);
                                                    $amount = $amount + $total;
//            echo $total;
                                                    ?>
                                                    <td class="center numeric" ><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $total; ?></td>
                                                    <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                                        <!--<td class="center"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo $total;   ?></td>-->
                                                    <?php // } else { ?>
                                                        <!--<td class="center"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency($total, 'SAR', $this->session->userdata('change-currency-to'));   ?></td>-->
                                                    <?php // } ?>

                                                <?php }
                                                ?>
                                            </tr>
                                            <tr>
                                                <td colspan="8"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="background: transparent"></td>  <td colspan="3" style="color: #B41E22"><b><span><h4 align="right">Total Amount :<span>&nbsp;&nbsp;</span><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $amount; ?></h4></span></b></td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    <?php }
                                    ?>


                                    </table>



                                    <!--        <div class="col-sm-12">
                                                
                                                <div class="col-sm-6 "></div>
                                    <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                                    <li><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo $amount;   ?></li>
                                    <?php // } else { ?>
                                                    <li><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency($amount, 'SAR', $this->session->userdata('change-currency-to'));   ?></li>
                                    <?php // } ?>
                                        <li><span class="currency-symbol">SAR</span><?php // echo $amount;     ?>                        </li>
                                            </div>-->

                                <?php } else {
                                    ?>
                                    <div class="col-sm-12">
                                        <p style="box-shadow: 0px 1px 5px 1px #680C13; text-align: center;">No product found</p><br/>
                                        <a href="<?php echo base_url(); ?>" class="btn btn-large btn-block btn-custom">Continue shopping </a>
                                    </div>

                                <?php } ?>
                                <div class="col-sm-12 margin-bottom-10" align="right"></div>
                            </div><!--Personal Info content-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>




