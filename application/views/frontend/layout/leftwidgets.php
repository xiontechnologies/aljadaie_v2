	<div class="left-sidebar select-fabric-view">
    	<ul>
        	<li id="fabric-style">
            	<a data-target="#style" data-toggle="modal" href="#"><span aria-hidden="true" class="icon-puzzle margin-bottom-5"></span> <b>Style</b></a>
            </li>
            <li>
                <a href="<?php echo base_url('storyboard'); ?>" class="storyboard" id="save-visulizer"><span aria-hidden="true" class="icon-book-open margin-bottom-5 "></span> <b>Add to<br>story<br>board</b></a>
            </li>
        </ul>
    </div>
	
	<div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="style" class="modal fade main-popup in" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true" class="icon-close"></span></button>
            <h4 id="myModalLabel" class="modal-title">Select a Style </h4>
          </div>
          <div class="modal-body">
		  
		 
		   <div class="tab-article">
			  <?php
						if (isset($style_category) && !empty($style_category)) {
							foreach ($style_category as $k => $stylecategory) {
								?>
			  <div class="fl"><a href="javascript:void(0)" id ="selectestyle" class="selected" data="<?php echo $stylecategory->id ?>"><?php echo $k ? '/' . $stylecategory->name : "$stylecategory->name"; ?></a></div>
			  <?php
							}
						}
						?>
			  <div class="clr">&nbsp;</div>
			</div>
    
           
            <div class="tab-listing" id="style-grid">
                 
			  <?php $this->load->view("frontend/home/style_image", $this->data); ?>
			</div>

          </div>
          
        </div>
      </div>
    </div>
