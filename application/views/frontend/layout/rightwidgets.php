
<div class="right-sidebar">
    <ul>
        <li>
            <a data-target="#fabric" data-toggle="modal" href="#"><span aria-hidden="true" class="icon-layers margin-bottom-5"></span> <b>Fabric</b></a>
        </li>
    </ul>
</div>

<div class="modal fade main-popup " id="fabric" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button" style="margin-top:7px !important;"><span aria-hidden="true" class="icon-close"></span></button>
                <h4 id="myModalLabel" class="modal-title" style="margin-top:7px !important;">Select a Fabric</h4>
                <div id="accordion" role="tablist" aria-multiselectable="true" class="">
                    <h6 class="heading-bd custom-hide-heading margin-top-10">
                        <a data-toggle="collapse" data-parent="#accordion" href="#mobile-search" aria-expanded="true" aria-controls="mobile-search">
                            Search Filters<span class="pull-right"></span>
                        </a>
                    </h6>
                    <div id="mobile-search" class="panel-collapse collapse in " role="tabpanel" aria-labelledby="headingOne">
                        <form role="form" class="form-inline select-fabric-filter" id="modal-search-form">

                            <?php
                            $searchcode = array(
                                'name' => 'collection',
                                'id' => 'fcollection',
                                'value' => '',
                                'placeholder' => 'Collection Number',
                                'class' => 'form-control txt6',
                                'maxlength' => 6);
                            $searchdefination = array(
                                'name' => 'item_definition',
                                'id' => 'fitem_definition',
                                'value' => '',
                                'placeholder' => 'Item Definition',
                                'class' => 'form-control txt7',
                                'maxlength' => 2
                            );
                            ?>

                            <div class="form-group search-box">
                                <?php echo form_input($searchcode); ?>
                            </div>
                            <div class="form-group search-box">
                                <?php echo form_input($searchdefination); ?>
                            </div>

                            <?php
                            $i = 0;
                            foreach ($category as $ck => $cv) {

                                $i++;
                                if (strtoupper($cv->name) != 'ITEM DEFINITION') {
                                    ?>
                                    <div class="form-group search-box" style="margin-top: -2px"> <!--search-box-->
                                            <select id="e<?php echo $i; ?>" name="serchvariation[]" data-placeholder="<?php echo strtoupper($cv->name); ?>" multiple="multiple" class="form-control"> <!--id="e<?php //echo $i;  ?>"-->
                                            <?php
                                            foreach ($cv->subcategory as $subk => $subv) {
                                                if ($subv->productcount) {
                                                    $id = 'id=' . 'rightwigdth' . $subv->id;
                                                    ?>
                                                    <option value="<?php echo $subv->id; ?> "><?php echo $subv->name . ' (' . $subv->productcount . ') '; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                <?php }
                            }
                            ?><?php ?>
                            <?php
                            $filtersearch = array(
                                'name' => 'filtersearch',
                                'id' => 'filtersearch',
                                'placeholder' => 'SEARCH',
                                'value' => '',
                                'class' => 'form-control txt8',
                                'type' => 'text'
                            );
                            ?>
                            <a href="#" class="icon-bttn refresh-btn form-group" style="margin-bottom:7px;"><span aria-hidden="true" class="icon-reload "></span></a>
                            <br>

                            <div class="form-group search-box" > <!--search-box-->
                                <div class="input-group" >
<?php echo form_input($filtersearch); ?>
                                    <div class="input-group-addon"><a href="#" id="search-main"><span aria-hidden="true" class="icon-magnifier"></span></a></div>
                                </div>
                            </div>
                            <a id="search-fav-fabric" class="form-group btn btn-primary" style="" href="#">
                                <span class="icon-heart" aria-hidden="true"></span>
                                View My Favourite
                            </a>

                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-body change-view fab-modal-body" style="height:400px; overflow:auto;"> <!--  -->

                <div class="row pagination-data" id="grid-fabric" >
<?php $this->load->view('frontend/products/ajaxpagination', $this->data); ?>
                </div>
                <input type="hidden" id="fab-flag" value="0" />
            </div>
            <div class="modal-footer" style="height: 50px">
                <img src="<?php echo base_url(); ?>assets/images/fab_loading.gif" style='margin-left:auto;margin-right:auto;margin-top: 0;background: transparent;' id="fabric-loader" class=""/>
                <div class="pagination rightAlign strong" style="margin: 0;"> Showing <span id="page"><?php echo isset($page) ? $page : 0; ?></span> of <span id="total"><?php echo isset($total) ? $total : '0'; ?></span> </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade main-popup" id="info1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="product-info-popup">
                <div class="modal-header margin-bottom-10">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="icon-close"></span></button>
                    <h4 class="modal-title">Product Information</h4>
                </div>
                <div class="modal-body info-modal-body" id="response" style="overflow:auto;">

                </div>
            </div> 
        </div>
    </div>
</div>

<div class="modal fade main-popup" id="adv-search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="product-info-popup">
                <div class="modal-header margin-bottom-10">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="icon-close"></span></button>
                    <h4 class="modal-title">Advance Search</h4>
                </div>
                <div class="modal-body fab-modal-body" id="response" style=" overflow:auto;">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            if ($product) {
                                echo form_open('home/advsearch', 'id=advsearch');
                                ?>
                                <div class="panel-group" id="accordion">


                                    <!-- Advance Filters -->
                                    <!--<h1>Advance <span class="blue-color">Filters:</span></h1>-->
                                    <div id="search-widgt">
                                        
                                            <?php
                                            //$catcount = count($category);
                                            $width = '100%';
                                            $colleps = 1;
                                            foreach ($category as $ck => $cv) {
                                                $class = '';
//                                if ($catcount > 7 && ($ck + 1) % 7 == 0)
//                                    $class = 'last';
//                                else if ($catcount < 7 && $catcount == ($ck + 1)) {
//                                    $class = 'last';
//                                }
                                                ?>
<div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $colleps ?>" data-original-title="" title="" class="collapsed">
                                                        <h4 class="panel-title">
        <?php echo $cv->name; ?>
                                                           <span class="pull-right"></span>
                                                        </h4>
                                                        
                                                    </a>
                                                </div>
                                                <div id="collapse<?= $colleps ?>" class="panel-collapse collapse">
                                                    <div class="panel-body" style="text-transform:capitalize; letter-spacing:normal;">



                                                        <?php
                                                        $i = 0;
                                                        foreach ($cv->subcategory as $subk => $subv) {
//                                            if ($subv->products) {
                                                            $i++;
                                                            $id = 'id=' . $subv->id;
                                                            ?>
                                                            <div class="col-md-4">
                                                            <?php echo form_checkbox('variation[]', $subv->id, '', $id); ?>
                                                            <?php echo form_label($subv->name, $subv->id); ?>
                                                            </div>
                                                            <?php
//                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>     
</div>
        <?php $colleps++;
    } ?>
                                        
                                    </div>
                                </div>
                                <?php
                                $submit = array(
                                    'name' => 'submit',
                                    'id' => 'advsubmit',
                                    'value' => 'Search',
                                    'placeholder' => 'Search',
                                    'class' => 'btn btn-custom',
                                    'style' => "margin-top:5px"
                                );
                                $reset = array(
                                    'name' => 'reset',
                                    'id' => 'resetfilter',
                                    'value' => 'Reset',
                                    'placeholder' => 'Reset',
                                    'class' => 'btn btn-info'
                                );
                                ?>

    <?php echo form_close(); ?>
                                <input type="hidden" name="hidden" value="1" class="hidden"/>
<?php } ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
<?php echo form_submit($submit); ?>
<?php echo form_reset($reset); ?>
<?php echo form_close(); ?>
                </div>
            </div> 
        </div>
    </div>
</div>


<div class="ui-loader ui-corner-all ui-body-a ui-loader-default"><span class="ui-icon-loading"></span></div>

<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div id="ajax-loader" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" style="">
        <img src="<?php echo base_url(); ?>assets/images/loading.gif" style='display: block;margin-left:auto;margin-right:auto;margin-top:40%;background: transparent' />
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

<script>
    $(document).on('keyup','.txt6',function() {
        if(this.value.length == $(this).attr('maxlength')) {
            $('.txt7').focus();
        }
    });
    $(document).on('keyup','.txt7',function() {
        if(this.value.length == $(this).attr('maxlength')) {
            $('.txt8').focus();
        }
    });
</script>