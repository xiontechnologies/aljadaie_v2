<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="right-widget">
    <ul class="makelist widget">
        <li class="icon-btns" id="select-fabric"><a href="javascript:void(0)" class="bluetip" title="SELECT A FABRIC"><span class="icons fabric-icon">SELECT A FABRIC</span></a></li>
    </ul>
    <div class="clr">&nbsp;</div>

    <!-- Overlay module -->
    <div id="overlay" class="select-fabric"> <span class="box-arrow">&nbsp;</span> <span class="box-drag right-wid">&nbsp;</span>
        <h1>Select a <span class="blue-color">Fabric</span>  <span class="ui-dialog-titlebar-close" id="close-pop">&nbsp;</span></h1>
        <div id="fabric-tabs">
            <ul class="maketabs fr tab-btn fabric-tabs">
                <li class="active grid-view"><a href="javascript:void(0);" title="Grid View">Grid View</a></li>
                <li class="list-view"><a href="javascript:void(0);" title="List View">list View</a></li>
            </ul>
            <div class="tab-article" id="search-widgt">
                <div class="fabric-filter"> 
                    <?php ?><?php
                    foreach ($category as $ck => $cv) {
                        $params = 'class="fl"';
                        if ($ck >= 1) {
                            $params = 'class="filter-fullview" style="float:left"';
                        }
                        ?>
                        <span <?php echo $params; ?> >
                            <div class="select-search">
                                <span class="select-opiton"><?php echo strtoupper($cv->name); ?></span>
                                <span class="select-arrow"></span>
                                <ul class="option-list-ul filter_Category">
                                    <?php
                                    foreach ($cv->subcategory as $subk => $subv) {
                                        if ($subv->productcount) {
                                            $id = 'id=' . 'rightwigdth' . $subv->id;
                                            ?>
                                            <li>
                                                <div class="select-search-checkbox">
                                                    <?php echo form_checkbox('serchvariation[]', $subv->id, '', $id); ?>
                                                    <?php echo form_label($subv->name . ' (' . $subv->productcount . ') ', 'rightwigdth' . $subv->id); ?>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </span>
                    <?php } ?><?php ?>
                    <?php
                    $filtersearch = array(
                        'name' => 'filtersearch',
                        'id' => 'filtersearch',
                        'placeholder' => 'SEARCH',
                        'value' => '',
                        'class' => ''
                    );
                    echo form_input($filtersearch);
                    ?>
                    <a href="javascript:void(0)" class="refresh-btn">Refresh</a>
                    <div class="clr">&nbsp;</div>
                </div>
                <div class="clr">&nbsp;</div>
            </div>
            <!-- Tab Content -->
            <div class="tab-listing" id="grid-fabric"> 
                <!--<ul class="maketabs listing-table fabric-fullview">--> <!-- Uncomment this ul for full view -->
                <ul class="maketabs listing-table search-filter change-view">
                    <?php $this->load->view('frontend/products/ajaxpagination', $this->data); ?>
                </ul>
                <!-- Pagination -->
<!--                        <div class="pagination rightAlign strong"> Showing <span>04</span> of <span>24</span> <span class="prev-arrow"><a href="#" title="Previous">&nbsp;</a></span> <span class="next-arrow"><a href="#" title="Next">&nbsp;</a></span> </div>-->
                <div class="pagination rightAlign strong"> Showing <span id="page"><?php echo isset($page) ? $page : 0; ?></span> of <span id="total"><?php echo isset($total) ? $total : '0'; ?></span> </div>

            </div>
            <!-- Fabric List View -->
        </div>
    </div>
</div>