<section>
    <div id="content-section" class="clearfix"> 
        <div class="order-table">
            <?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
            <?php
            if ($add_error)
                echo $add_error;
            if ($add_success)
                echo $add_success;
            ?>
            <?php if (isset($signuperror) && !empty($signuperror)) { ?>
                <span class="error" style="float:none"><?php echo $signuperror; ?></span>
            <?php } ?>
            <?php if (isset($success) && !empty($success)) { ?>
                <span class="success" style="float:none"><?php echo $success; ?></span>
            <?php } ?>
            <h3 class="widget-title">Newsletter Subscription</h3>
            <div class="order-tablular clearfix">
                <?php
                $InputEmail1 = array(
                    'name' => 'email',
                    'id' => 'email',
                    'type' => 'email',
                    'placeholder' => 'Enter email',
                    'value' => '',
                    'class' => 'form-control',
                    'required' => 'required'
                );
                $submit = array('name' => 'submit',
                    'id' => 'submit',
                    'value' => 'Submit',
                    'class' => 'btn btn-blue',
                );
                ?>
                <?php echo form_open('frontend/newsletters/newsletter', 'id="newsletter"'); ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <?php echo form_input($InputEmail1); ?>
                    <?php echo form_error('email', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                <!--    <input type="email" placeholder="Enter email" id="InputEmail1" class="form-control">-->

                </div>
                <?php echo form_submit($submit); ?>
                <?php echo form_close(); ?>
            </div>
        </div> 
</section>
