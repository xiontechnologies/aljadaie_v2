<?php
/*
 * @auther Shafiq
 */
if ($this->data['controller'] == 'home' || $this->data['action'] == 'editstoryboard') {
    ?>

    <!-- Footer Panel -->
    <section>
        <div id="dialogmodel">&nbsp;</div>
        <div id="footer">
            <div id="tabs">
			
                <ul class="maketabs tab-panel tab-content-template">
                    <li class="tab-surfaces" style="<?php echo $this->data['action'] == 'editstoryboard' ? "" : 'display:none' ?>"><a href="#tabs-1" class="orangetip_left" title="View Surfaces">Surfaces</a></li>
                    <li class="tab-storyboard" style="<?php echo $this->data['action'] == 'editstoryboard' ? "" : 'display:none' ?>"><a href="#tabs-3" title="Storyboard">Storyboard</a></li>
                    <li class="tab-catalogue"><a href="#tabs-2" class="orangetip_right" title="View catelogue">Catalogue</a></li>
                    <!--<li class="tab-hanger"><a href="javascript:void(0)" title="Hanger">Hanger</a></li>-->
                </ul>

                <div id="tabs-1" class="surfaces-article panel"></div>
                <div id="tabs-2" class="catalogue-article panel" >
                    <?php if ($this->session->userdata(SITE_NAME . '_user_catalog')) { ?>
                        <div class="cataloge-options">
                            <h3>CATALOGUE TITLE</h3>
                            <?php
                            $titleCat = array(
                                'name' => 'cateloguetitle',
                                'class' => 'catelogue-title',
                                'id' => 'catelogue-title',
                                'placeholder' => 'Catalogue Title',
                            );
                            if ($this->session->userdata(SITE_NAME . '_save_user_catalog')) {
                                ?>
                                <label id="catelogue-title-save"><?php echo $this->session->userdata(SITE_NAME . '_save_user_catalog') ? '<b>' . $this->session->userdata[SITE_NAME . '_save_user_catalog']['name'] . '</b><a class="catelogue-option-btn create-catalogue" href="javascript:void(0)">CREATE NEW CATALOGUE</a>' : ''; ?></label>
                                <?php
                            } else {
                                echo form_input($titleCat);
                                ?>
                                <a href="javascript:void(0)" class="catelogue-option-btn save-catalogue">SAVE CATALOGUE</a>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="cataloge-options" style="display:none;">
                            <h3>CATALOGUE TITLE</h3>
                            <?php
                            $titleCat = array(
                                'name' => 'cateloguetitle',
                                'class' => 'catelogue-title',
                                'id' => 'catelogue-title',
                                'placeholder' => 'Catalogue Title',
                            );
                            echo form_input($titleCat);
                            ?>
                            <a href="javascript:void(0)" class="catelogue-option-btn save-catalogue">SAVE CATALOGUE</a>
                        </div>
                    <?php } ?>
                    <div class="ftr-slider">

                        <?php if ($this->session->userdata(SITE_NAME . '_user_catalog')) { ?>
                            <a class="prev browse left">prev</a> 
                            <div class="scroll-container">
                                <ul class="maketabs listing-table lsit-view">
                                    <?php
                                    $catalogueSession = $this->session->userdata(SITE_NAME . '_user_catalog');
                                    foreach ($catalogueSession as $k => $v) {
                                        $data['v'] = $v;
                                        $this->load->view('frontend/products/ajaxcatalog', $data);
                                    }
                                    ?>
                                </ul>
                            </div>
                            <a class="next browse right">next</a> 
                        <?php } else { ?>
                            <a class="prev browse left" style="display:none;">prev</a> 
                            <div class="scroll-container"> <p class="catnotavail"> No Product available in Catalogue    </p></div>
                            <a class="next browse right" style="display:none;">next</a> 

                        <?php } ?>

                    </div>
                    <div class="catlg-property fr">
                        <h1>NUMBER OF FABRIC(S): <span class="blue-color"><?php echo $this->session->userdata(SITE_NAME . '_user_catalog') ? count($this->session->userdata(SITE_NAME . '_user_catalog')) : '0' ?></span></h1>
                        <h1><a href="javascript:void(0)" class="downloadpdf orangetip" title="DOWNLOAD PDF"><span class="icons pdf-icon">&nbsp;</span>DOWNLOAD PDF</a></h1>
                        <h1><a href="javascript:void(0)" class="emailpdf orangetip" title="EMAIL PDF"><span class="icons email-icon">&nbsp;</span>EMAIL PDF</a></h1>
                        <input id="barcode-new" class="bar-code" type="input" name="cateloguetitle" placeholder="Enter barcode">
                    </div>
                    <div class="clr">&nbsp;</div>
                </div>

                <div id="tabs-3" class="tab-storyboard panel">

                    <?php if (json_decode($this->session->userdata(SITE_NAME . '_story_board'))) { ?>
                        <!--                        <div class="storyborad-property">
                                                    <h1><a href="javascript:void(0)" class="downloadpdf-storyboard orangetip" title="DOWNLOAD PDF"><span class="icons pdf-icon">&nbsp;</span>DOWNLOAD PDF</a></h1>
                                                </div>-->
                        <div class="storyboard-options " >
                            <h3>Storyboard TITLE</h3>
                            <?php
                            $titlestory = array(
                                'name' => 'storyboardtitle',
                                'class' => 'storyboard-title',
                                'id' => 'storyboard-title',
                                'placeholder' => 'Storyboard Title',
                            );

                            if ($this->session->userdata(SITE_NAME . '_save_user_storyboard')) {
//                                echo '<pre>';print_r($this->session->userdata[SITE_NAME . '_save_user_storyboard']['name']);exit;
                                ?>
                                <label id="storyboard-title-save"><?php echo $this->session->userdata(SITE_NAME . '_save_user_storyboard') ? '<b>' . $this->session->userdata[SITE_NAME . '_save_user_storyboard']['name'] . '</b><a class="catelogue-option-btn create-storyboard" href="javascript:void(0)">CREATE NEW Storyboard</a>' : ''; ?></label>
                                <?php
                            } else {
                                echo form_input($titlestory);
                                ?>
                                <!--<p style="margin:5px 0 0; text-align: left;"><a href="javascript:void(0)" class="catelogue-option-btn save-storyboard">SAVE Storyboard</a></p>-->
                                <a href="javascript:void(0)" class="catelogue-option-btn save-storyboard">SAVE Storyboard</a>
                            <?php } ?>

                        </div>

                    <?php } else {
                        ?>

                        <div class="storyboard-options" style="display:none;">
                            <h3>Storyboard TITLE</h3>
                            <?php
                            $titlestory = array(
                                'name' => 'storyboardtitle',
                                'class' => 'storyboard-title',
                                'id' => 'storyboard-title',
                                'placeholder' => 'Storyboard Title',
                            );

                            echo form_input($titlestory);
                            ?>
                            <a href="javascript:void(0)" class="catelogue-option-btn save-storyboard">SAVE Storyboard</a>
                        </div>
                        <!--<div class="storyborad-property fr" style="display:none;">-->
                        <!--                                                                                   <h1>NUMBER OF FABRIC(S): <span class="blue-color"><?php // echo $this->session->userdata(SITE_NAME . '_user_catalog') ? count($this->session->userdata(SITE_NAME . '_user_catalog')) : '0'         ?></span></h1>-->
                            <!--<h1><a href="javascript:void(0)" class="downloadpdf-storyboard orangetip" title="DOWNLOAD PDF"><span class="icons pdf-icon">&nbsp;</span>DOWNLOAD PDF</a></h1>-->
                        <!--</div>-->
                    <?php } ?>
                    <div class="ftr-slider">

                        <?php if (json_decode($this->session->userdata(SITE_NAME . '_story_board'))) { ?>
                            <a class="prev browse left storyboard-prev">prev</a> 
                            <div class="scroll-container storyboard-div" id="storyboard">
                                <ul class="maketabs listing-table lsit-view storyboard-ul">
                                    <?php $this->load->view('frontend/products/ajaxstoryboard'); ?>
                                </ul>
                            </div>
                            <a class="next browse right storyboard-next">next</a> 
                        <?php } else { ?>
                            <a class="prev browse left storyboard-prev" style="display:none;">prev</a> 
                            <div class="scroll-container storyboard-div"> <p class="storyborad-notavail"> No Story available    </p></div>
                            <a class="next browse right storyboard-next" style="display:none;">next</a> 

                        <?php } ?>

                    </div>
                    <div class="story-property fr">
                        <?php $array = ($this->session->userdata(SITE_NAME . '_story_board') != "") ? json_decode($this->session->userdata(SITE_NAME . '_story_board')) : array(); ?>
                        <h1>NUMBER OF BOARD(S): <span class="blue-color">
                                <?php echo count($array); ?>
                            </span></h1>
                        <h1><a href="javascript:void(0)" class="downloadpdf-storyboard orangetip" title="DOWNLOAD PDF"><span class="icons pdf-icon">&nbsp;</span>DOWNLOAD PDF</a></h1>
                        <h1><a href="javascript:void(0)" class="emailpdf orangetip" title="EMAIL PDF"><span class="icons email-icon">&nbsp;</span>EMAIL PDF</a></h1>

                    </div>
                    <div class="clr">&nbsp;</div>
                </div>



            <?php } ?>

            </body>
            </html>
