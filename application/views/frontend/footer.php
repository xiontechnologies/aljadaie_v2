<?php
/*
 * @auther Shafiq
 */
if ($this->data['controller'] == 'home' || $this->data['action'] == 'editstoryboard') {
    ?>
    <!--
         Footer Panel -->
    <div class="footer-main">
        <div class="">
            <div id="dialogmodel">&nbsp;</div>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        <li id="surfaces_content" class="select-fabric-view"><a data-toggle="tab" id="link_surfaces" href="#Surfaces"><span aria-hidden="true" class="icon-grid"></span> <span title="View Surfaces">Surfaces</span></a></li>
                        <li id="storyboard_content" class="select-fabric-view"><a data-toggle="tab" id="link_storyboard" href="#Storyboard"><span aria-hidden="true" class="icon-notebook"></span> <span title="Storyboard">Storyboard</span></a></li>
                        <li id="catalogue_content" class="active"><a data-toggle="tab" id="link_catalogue" href="#Catalogue"><span aria-hidden="true" class="icon-folder-alt"></span> <span title="View catelogue">Catalogue</span></a></li>
                    </ul>
                    <div class="tab-content" >
                        <div id="Surfaces" class="tab-pane tabs-1">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="owl-surfaces" class="owl-carousel owl-theme">
                                        <div class="item">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Storyboard" class="tab-pane tabs-2">
                            <div class="row" id="storyboard_view">
                                <div class="col-sm-12">
                                    <ul class="storyboard-top">
                                        <?php if (json_decode($this->session->userdata(SITE_NAME . '_story_board'))) { ?>

                                            <li style="padding-top:16px;">Storyboard TITLE</li>
                                            <?php
                                            if ($this->session->userdata(SITE_NAME . '_save_user_storyboard')) {
                                                ?>
                                                                                                                    <!--<li><input type="text" class="form-control" placeholder="Storyboard Title"></li>-->
                                                <li style="padding-top:16px;"><a href="javascript:void(0)" id="storyboard-title-save"><?php echo $this->session->userdata(SITE_NAME . '_save_user_storyboard') ? '<b>' . $this->session->userdata[SITE_NAME . '_save_user_storyboard']['name'] . '</b><a class="catelogue-option-btn create-storyboard" href="javascript:void(0)">CREATE NEW Storyboard</a>' : ''; ?></a></li>
                                                <?php
                                            } else {
                                                ?>
                                                <li><input type="text" class="form-control storyboard-title" name="storyboardtitle" id="storyboard-title1" placeholder="Storyboard Title"></li>
                                                <!--<li><input type="text" class="form-control" placeholder="Storyboard Title"></li>-->
                                                <li style="padding-top:16px;"><a href="javascript:void(0)" class="catelogue-option-btn save-storyboard" >SAVE Storyboard</a></li>
                                                <!--<a href="javascript:void(0)" class="catelogue-option-btn save-storyboard">SAVE Storyboard</a>-->
                                                <?php
                                            }
                                        } else {
                                            ?>

                                            <li style="padding-top:16px;">Storyboard TITLE</li>
                                            <li><input type="text" class="form-control storyboard-title" name="storyboardtitle" id="storyboard-title1" placeholder="Storyboard Title"></li>
                                            <li style="padding-top:16px;"><a href="javascript:void(0)" class="catelogue-option-btn save-storyboard">SAVE Storyboard</a></li>
                                        <?php } ?>
                                    </ul>
                                    <ul class="storyboard-top-right">
                                        <li>
                                            <a href="#" class="emailpdf"><span aria-hidden="true" class="icon-envelope"></span> <b>Email</b></a>
                                        </li>
                                        <li>
                                            <a href="#" class="downloadpdf-storyboard"><span aria-hidden="true" class="icon-cloud-download "></span> <b>Download PDF</b></a>
                                        </li>
                                        <?php $array = ($this->session->userdata(SITE_NAME . '_story_board') != "") ? json_decode($this->session->userdata(SITE_NAME . '_story_board')) : array(); ?>
                                        <li>NUMBER OF BOARD(S): <span id="property-count"><?php echo count($array); ?></span></li>
                                    </ul>

                                </div>

                                <div class="col-sm-12">
                                    <hr style="margin-top: 0;border-color:black !important;"> 
                                    <div id="owl-storyboard" class="owl-carousel owl-theme">
                                        <?php
                                        if (json_decode($this->session->userdata(SITE_NAME . '_story_board'))) {

                                            $this->load->view('frontend/products/ajaxstoryboard');
                                        } else {
                                            ?>
                                            <div style="height: 100px; width: 200px">                                    	
                                                No Story selected 
                                            </div>
                                        <?php } ?>

                                    </div>
                                    <div class="customNavigation col-lg-12">
                                        <a class="btn btn-custom prev"><span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span></a>
                                        <a class="btn btn-custom next"><span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span></a>
                                    </div>
                                </div>

                            </div>
                            <!--                            <div class="row" id="null-storyboard">
                                                            <div class="col-sm-12">
                                                                <p> No Product available in Storyboard </p>
                                                            </div>
                                                        </div>-->
                        </div>

                        <div id="Catalogue" class="tab-pane active">
                            <div class="row">
                                <div class="col-sm-12">

                                    <ul class="storyboard-top catelogue-top">
                                        <?php if ($this->session->userdata(SITE_NAME . '_user_catalog')) { ?>
                                            <li style="padding-top:16px;">CATALOGUE TITLE :</li>
                                            <?php if ($this->session->userdata(SITE_NAME . '_save_user_catalog')) { ?>
                                                <li><label id="catelogue-title-save"><?php echo $this->session->userdata(SITE_NAME . '_save_user_catalog') ? '<b>' . $this->session->userdata[SITE_NAME . '_save_user_catalog']['name'] . '</b></label> </li><li style="padding-top:16px;"><a class="catelogue-option-btn create-catalogue" href="javascript:void(0)">CREATE NEW CATALOGUE</a>' : ''; ?></li>
                                            <?php } else { ?>
                                                <li><input type="text" class="form-control catelogue-title" placeholder="Catelogue Title" name="cateloguetitle" id="catelogue-title"></li>
                                                <li style="padding-top:16px;"><a href="#" class="catelogue-option-btn save-catalogue">SAVE CATALOGUE</a></li>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <li style="padding-top:16px;">CATALOGUE TITLE :</li>
                                            <li><input type="text" class="form-control catelogue-title" placeholder="Catelogue Title" name="cateloguetitle" id="catelogue-title"></li>
                                            <li style="padding-top:16px;"><a href="#" class="catelogue-option-btn save-catalogue">SAVE CATALOGUE</a></li>

                                        <?php } ?>
                                    </ul>
                                    <ul class="storyboard-top-right" style="padding-left: 0">
                                        <li>
                                            <a href="#" class="emailpdf orangetip" title="EMAIL PDF"><span aria-hidden="true" class="icon-envelope"></span> <b>Email</b></a>
                                        </li>
                                        <li>
                                            <a href="#" class="downloadpdf orangetip" title="DOWNLOAD PDF"><span aria-hidden="true" class="icon-cloud-download"></span> <b>Download</b></a>
                                        </li>
                                        <li><span>NUMBER OF FABRIC(S): <span class="blue-color1"><?php echo $this->session->userdata(SITE_NAME . '_user_catalog') ? count($this->session->userdata(SITE_NAME . '_user_catalog')) : '0' ?></span></span></li>
                                        <li><input id="barcode-new" class="bar-code" type="input" name="barcodenumber" placeholder="Enter Barcode"></li>

                                    </ul>

                                </div>

                                <div class="col-sm-12">
                                    <hr style="margin-top: 0; border-color:black !important;"> 
                                    <div id="owl-catalogue" class="owl-carousel owl-theme">

                                        <?php if ($this->session->userdata(SITE_NAME . '_user_catalog')) { ?>

                                            <?php
                                            $catalogueSession = $this->session->userdata(SITE_NAME . '_user_catalog');
                                            foreach ($catalogueSession as $k => $v) {
                                                $data['v'] = $v;
                                                $this->load->view('frontend/products/ajaxcatalog', $data);
                                            }
                                            ?>

                                        <?php } else { ?>
                                            <div style="height: 100px; width: 200px">                                    	
                                                No catalogue selected 
                                            </div>
                                        <?php } ?>

                                    </div>
                                    <div class="customNavigation col-lg-12">
                                        <a class="btn btn-custom prev1"><span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span></a>
                                        <a class="btn btn-custom next1"><span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
<?php } ?>


<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/slider_js/jquery.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/slider_js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/slider_js/jquery.flexslider.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/js/slider_js/owl.carousel/owl.carousel.js"></script>
<script>
    $("#link_surfaces").on('click', function () {
        $("#Surfaces").toggleClass("active");
        $("#Catalogue").removeClass('active');
        $("#Storyboard").removeClass('active');
        if($("#surfaces_content").hasClass('activetab')){
            $("#surfaces_content").removeClass('activetab');
        }else{
            $("#surfaces_content").addClass('activetab');
        }
        $('#catalogue_content').removeClass('activetab');
        $("#storyboard_content").removeClass('activetab');
    });

    $("#link_catalogue").on('click', function () {
        $("#Catalogue").toggleClass("active");
        $("#Surfaces").removeClass('active');
        $("#Storyboard").removeClass('active');
        if($('#catalogue_content').hasClass('activetab')){
            $('#catalogue_content').removeClass('activetab');
        }else{
            $('#catalogue_content').addClass('activetab');
        }
        $("#surfaces_content").removeClass('activetab');
        $("#storyboard_content").removeClass('activetab');
    });

    $("#link_storyboard").on('click', function () {

        $("#Storyboard").toggleClass("active");
        $("#Surfaces").removeClass('active');
        $("#Catalogue").removeClass('active');
        if( $("#storyboard_content").hasClass('activetab')){
            $("#storyboard_content").removeClass('activetab');
        }else{
            $("#storyboard_content").addClass('activetab');
        }
        $('#catalogue_content').removeClass('activetab');
        $("#surfaces_content").removeClass('activetab');
        
    });

</script>

<script type="text/javascript">
    var glob = 1; var share_iamge = '';
    var site_url = '<?php echo base_url(); ?>';
    $(window).resize(function() {
        var modal_height = parseInt($(window).height()) - parseInt(300);
        var modal_height1 = parseInt($(window).height()) - parseInt(125);
        $('.fab-modal-body').css("max-height",modal_height+"px");
        $('.info-modal-body').css("max-height",modal_height1+"px");
    });
    $(function () {
        // SyntaxHighlighter.all();
        //$('#e1').select2();
        $('#fabric-loader').hide();
    });
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
    });
    $(document).ready(function () {
        
        var modal_height = parseInt($(window).height()) - parseInt(300);
        var modal_height1 = parseInt($(window).height()) - parseInt(125);
        $('.fab-modal-body').css("height",modal_height+"px");
        $('.info-modal-body').css("height",modal_height1+"px");
        //share();
        
        $('#fabric-loader').hide();
        $("#Catalogue").removeClass('active');
        $("#catalogue_content").removeClass('active');
        $('#e1').multiselect({
            maxHeight: '150',
            buttonText: function(options, select) {
                if (options.length == 0) {
                    return 'PRIMARY COLOR <span class="caret" style="color: black"></span>';
                }
                else if (options.length > 0) {
                    return 'PRIMARY COLOR <span class="caret" style="color: black"></span>';
                }
                else {
                    
                    return 'PRIMARY COLOR <span class="caret" style="color: black"></span>';
                }
            }
        });
        $('#e2').multiselect({
            maxHeight: '150',
            buttonText: function(options, select) {
                if (options.length == 0) {
                    return 'SECONDARY COLOR <span class="caret" style="color: black"></span>';
                }
                else if (options.length > 0) {
                    return 'SECONDARY COLOR <span class="caret" style="color: black"></span>';
                }
                else {
                    
                    return 'SECONDARY COLOR <span class="caret" style="color: black"></span>';
                }
            }
        });
        $('#e3').multiselect({
            maxHeight: '150',
            buttonText: function(options, select) {
                if (options.length == 0) {
                    return 'DESIGN <span class="caret" style="color: black"></span>';
                }
                else if (options.length > 0) {
                    return 'DESIGN <span class="caret" style="color: black"></span>';
                }
                else {
                    
                    return 'DESIGN <span class="caret" style="color: black"></span>';
                }
            }
        });
        //        $('#e4').multiselect({
        //            maxHeight: '150',
        //            buttonText: function(options, select) {
        //                if (options.length == 0) {
        //                    return 'ITEM DEFINITION';
        //                }
        //                else if (options.length > 0) {
        //                    return 'ITEM DEFINITION';
        //                }
        //                else {
        //                    
        //                    return 'ITEM DEFINITION';
        //                }
        //            }
        //        });
        var owl_surfaces = $("#owl-surfaces");

        owl_surfaces.owlCarousel({
            items: 10, //10 items above 1000px browser width
            itemsDesktop: [1000, 5], //5 items between 1000px and 901px
            itemsDesktopSmall: [900, 3], // betweem 900px and 601px
            itemsTablet: [600, 2], //2 items between 600 and 0
            itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
        });
        
        
        var owl_storyboard = $("#owl-storyboard");
        // Custom Navigation Events
        $(".next").click(function () {
            owl_storyboard.trigger('owl.next');
        })
        $(".prev").click(function () {
            owl_storyboard.trigger('owl.prev');
        })
        $(".play").click(function () {
            owl_storyboard.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
        })
        $(".stop").click(function () {
            owl_storyboard.trigger('owl.stop');
        })
        
        var owl_catalogue = $("#owl-catalogue");
        // Custom Navigation Events
        $(".next1").click(function () {
            owl_catalogue.trigger('owl.next');
        })
        $(".prev1").click(function () {
            owl_catalogue.trigger('owl.prev');
        })
        $(".play1").click(function () {
            owl_catalogue.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
        })
        $(".stop1").click(function () {
            owl_catalogue.trigger('owl.stop');
        })

        $(".currency-symbol").text('SAR');
        var imgpath = '';
        var style = $('#style-grid-view li:nth-child(2)').find('.img123').attr('data');
        var current_object = 0;
        var object_count = 0;
        var server = "http://vivek2.picarisplatform.com/picaris";
        var qtypath = "&q=100";
        var querystring = {};
        var querytn = {};
        var fabric = "";
        var surface = "";
        var alt = "";
        var imgwidth = "250";
        var imgheight = "250";
        var designpath = 'uploads/productimage/';
        //var site_url = 'http://www.prismartec.in/project/aljedaie_v2/';
        
        var fabricId = '';
        var dpi = 72;
        var storyboardIds = '';
        var data = new Array();
        
        

        $('.img123').click(function () {
            $('.flexslider').hide();
            //$("#ajax-loader").modal('show');
            
            $('.ui-loader').show();
            style = $(this).attr('data');
            reset($(this).attr('data'));
            load_scene_coords();
            //  share();
            addSpan();
            
        });
        var flag = true;
        function logArrayElements(element, index, array) {
            data.push($.trim(element));
        }
        $("#e1").change(function() {
            while(data.length){
                data.pop();
            }
            var drp1 = $(this).val();
            var drp2 = $('#e2').val();
            var drp3 = $('#e3').val();
            if(drp1 != null){
                drp1.forEach(logArrayElements);
            }
            if(drp2 != null){
                drp2.forEach(logArrayElements);
            }
            if(drp3 != null){
                drp3.forEach(logArrayElements);
            }
            $('.ui-loader').show();
            $('#grid-fabric').html('');
            if ($("#canvas").attr('src') == '#') {
                reset($('#style-grid-view li:nth-child(2)').find('.img123').attr('data'));
                load_scene_coords();
                share();
            }
            //tabsoption();
            if (products('filter', data, 0, $("#filtersearch").val())) {
                $('.ui-loader').hide();
            }
        });
        $("#e2").change(function() {
            while(data.length){
                data.pop();
            }
            var drp1 = $(this).val();
            var drp2 = $('#e1').val();
            var drp3 = $('#e3').val();
            if(drp1 != null){
                drp1.forEach(logArrayElements);
            }
            if(drp2 != null){
                drp2.forEach(logArrayElements);
            }
            if(drp3 != null){
                drp3.forEach(logArrayElements);
            }
            $('.ui-loader').show();
            $('#grid-fabric').html('');
            if ($("#canvas").attr('src') == '#') {
                reset($('#style-grid-view li:nth-child(2)').find('.img123').attr('data'));
                load_scene_coords();
                share();
            }
            //tabsoption();
            if (products('filter', data, 0, $("#filtersearch").val())) {
                $('.ui-loader').hide();
            }
        });
        $("#e3").change(function() {
            while(data.length){
                data.pop();
            }
            var drp1 = $(this).val();
            var drp2 = $('#e2').val();
            var drp3 = $('#e1').val();
            if(drp1 != null){
                drp1.forEach(logArrayElements);
            }
            if(drp2 != null){
                drp2.forEach(logArrayElements);
            }
            if(drp3 != null){
                drp3.forEach(logArrayElements);
            }
            $('.ui-loader').show();
            $('#grid-fabric').html('');
            if ($("#canvas").attr('src') == '#') {
                reset($('#style-grid-view li:nth-child(2)').find('.img123').attr('data'));
                load_scene_coords();
                share();
            }
            //tabsoption();
            if (products('filter', data, 0, $("#filtersearch").val())) {
                $('.ui-loader').hide();
            }
        });
        $(document).on("submit", "#search", function(event) {
            event.preventDefault();
            var postData = $(this).serializeArray();
            $('#fabric').modal('show');
            $('.ui-loader').show();
            $('#grid-fabric').html('');
            if ($("#canvas").attr('src') == '#') {
                reset($('#style-grid-view li:nth-child(2)').find('.img123').attr('data'));
                load_scene_coords();
                share();
            }
            //tabsoption();
            if (products('topsearch', '', 0, postData)) {
           
            }
        });
        $('.change-view').scroll(function() {
	
	
            totalheight = parseInt($(this)[0].scrollHeight);
            scrollposition = parseInt($(this).scrollTop()) + parseInt($(this).innerHeight());
            if (scrollposition >= totalheight && flag) {
                flag = false;
                //console.log(scrollposition +">="+ totalheight);
                //alert('scroll');
                /*First check its filter result or not
                 *if yes then show result for filter only
                 *else
                 *calling ajax for more data
                 */
                var pagenumber = $("#page").html();
                var totalrecords = $("#total").html();
                
                
                if (parseInt(pagenumber) < parseInt(totalrecords)) {
                    if($('#fab-flag').val() == '0'){
                        //                        var data = new Array();
                        //                        $('#modal-search-form option:selected').each(function(){
                        //                            data.push($(this).val());
                        //                        });
                        //$('#ajax-loader').modal('show');
                        if (products('scroll', data, pagenumber, $("#filtersearch").val())) {
                        
                        }
                    }
                    else{
                    
                        $('#fabric-loader').show();
                        $.ajax({
                            'url': site_url + "product/ajaxfavorites",
                            'dataType': 'json',
                            'data': {
                                'pagintation': pagenumber
                            },
                            success: function(data) {
                                share();
                                $('#fabric-loader').hide();
                                $(".pagintation").val(data.offset);
                                $("#page").html(data.page);
                                $("#total").html(data.total);
                                $('.pagination-data').append(data.view);
                            }
                        });
                    }
                }
            }
        });
        $('.refresh-btn').click(function() {
        
            $('.search-box').removeClass('select-fabric-view1');
            $('.change-fab-view').addClass('change-view');
            $('.change-fab-view').removeClass('change-fab-view');
            $('#fab-flag').val('0');
            $("#filtersearch").val('');
            $('#fitem_definition').val('');
            $('#fcollection').val('');
            while(data.length){
                data.pop();
            } 
            $('#e1').multiselect('deselectAll', false);
            $('#e1').multiselect('updateButtonText');
            $('#e2').multiselect('deselectAll', false);
            $('#e2').multiselect('updateButtonText');
            $('#e3').multiselect('deselectAll', false);
            $('#e3').multiselect('updateButtonText');
            $('#e4').multiselect('deselectAll', false);
            $('#e4').multiselect('updateButtonText');
        
            $('.pagination-data').html('');
            if (products('filter', '', 0, '')) {
            
            }
        });
        $(document).on("click", "#search-main", function() {
            clearTimeout($.data('#filtersearch', 'timer'));
            $('#grid-fabric').html('');
            var wait = setTimeout(textfilter, 10);
            $(this).data('timer', wait);
        });
        $(document).on("keyup", "#fcollection", function() {
            clearTimeout($.data('#fcollection', 'timer'));
            $('#grid-fabric').html('');
            var wait = setTimeout(textfilter, 500);
            $(this).data('timer', wait);
        });
        $(document).on("keyup", "#fitem_definition", function() {
            clearTimeout($.data('#fitem_definition', 'timer'));
            $('#grid-fabric').html('');
            var wait = setTimeout(textfilter, 500);
            $(this).data('timer', wait);
        });
        $(document).on("click","#search-fav-fabric", function(){
            if($('.search-box').hasClass('select-fabric-view1')){
                $('.search-box').removeClass('select-fabric-view1');
                $('.change-fab-view').addClass('change-view');
                $('.change-fab-view').removeClass('change-fab-view');
                $('#fab-flag').val('0');
                if (products('filter', '', 0, '')) {
            
                }
            }else{
                $('.search-box').addClass('select-fabric-view1');
                $('.change-view').addClass('change-fab-view');
                $('.change-view').removeClass('change-view');
                $('#fab-flag').val('1');
                var pagination = '0';
                //$('#ajax-loader').modal('show');
                $('.ui-loader').show();
                $.ajax({
                    'url': site_url + "product/ajaxfavorites",
                    'dataType': 'json',
                    'data': {
                        'pagintation': pagination
                    },
                    success: function(data) {
                        share();
                        //$('#ajax-loader').modal('hide');
                        $('.ui-loader').hide();
                        $(".pagintation").val(data.offset);
                        $("#page").html(data.page);
                        $("#total").html(data.total);
                        $('.pagination-data').html('');
                        $('.pagination-data').append(data.view);
                    }
                });
            }
        
       
        });
        $(document).on('click','#advsubmit', function(event) {
            //console.log('hello advance search');
            event.preventDefault();
            while(data.length){
                data.pop();
            } 
             
            $.each($("#adv-search input[type=checkbox]:checked"), function(index, value) {
                
                //                $('#modal-search-form input[type=checkbox]').each(function(){
                //                     
                //                      if( parseInt($(this).val()) == parseInt(value.value)){
                //                          console.log('loop'+value.value+" =>"+ $(this).val());
                //                          $(this).parent('li').addClass('active');
                //                      }
                //                 });
                data.push(value.value);
            });
            $('#e1').multiselect('select',data); //$('#e1').multiselect('refresh');
            $('#e2').multiselect('select',data); //$('#e2').multiselect('refresh');
            $('#e3').multiselect('select',data); //$('#e3').multiselect('refresh');
            $('#e4').multiselect('select',data); //$('#e4').multiselect('refresh');
            $('#adv-search').modal('hide');
            $('#fabric').modal('show');
            $('.pagination-data').html('');
            if (products('advancesearch', data, 0, '')) {
            
            }
        });
        /*
         * For advance filter reset 
         */
        $(document).on('click', '#resetfilter', function() {
            while(data.length){
                data.pop();
            }
            $.each($("#adv-search input[type=checkbox]"), function(index, value) {
                $('#' + value.value).attr('checked', false);
            });
        });
        function textfilter() {
            while(data.length){
                data.pop();
            }
            var drp1 = $('#e1').val();
            var drp2 = $('#e2').val();
            var drp3 = $('#e3').val();
            var txtitem = $('#fitem_definition').val();
            var txtcollec = $('#fcollection').val();
            if(drp1 != null){
                drp1.forEach(logArrayElements);
            }
            if(drp2 != null){
                drp2.forEach(logArrayElements);
            }
            if(drp3 != null){
                drp3.forEach(logArrayElements);
            }
            if(txtitem != null || txtitem != '' ){
                data.push(txtitem);
            }
            if(txtcollec != null || txtcollec != '' ){
                data.push(txtcollec);
            }
            $('.ui-loader').show();
        
            $('#grid-fabric').html('');
            if (products('filter', data, 0, $("#filtersearch").val())) {
                $('.ui-loader').hide();
            }
        }
        function products(type, data, pagination, val) {
            // $('#ajax-loader').modal('show');
            $('#fabric-loader').show();
            $.ajax({
                'url': site_url + "product/ajaxview",
                'dataType': 'json',
                'data': {
                    'variation': data,
                    'type': type,
                    'pagintation': pagination,
                    'val': val
                },
                success: function(data) {
                    share();
                    //$('#ajax-loader').modal('hide');
                    $('.ui-loader').hide();
                    $(".pagintation").val(data.offset);
                    $("#page").html(data.page);
                    $("#total").html(data.total);
                    if (type == 'filter') {
                        $('.pagination-data').html('');
                    }
                    if (type == 'advancesearch') {
                        $('.pagination-data').html('');
                        
                    }
                    $('.pagination-data').append(data.view);
                    $('.fabric1').click(function () {
                        if ($("#canvas").attr('src') == '#') {
                            reset($('#style-grid-view li:nth-child(2)').find('.img123').attr('data'));
                            
                            load_scene_coords();
                            share();
                        }
                        alt = $(this).attr('alt');
                        fabricId = $(this).attr('data');
                        fabric = site_url + designpath + alt;
                        backgroudImag = site_url + designpath + 'thumb/' + alt;
            
                        // $("body").css('background-image', 'url(' + backgroudImag + ')');
                        //  $("body").css('padding-top', '0');
                        // $("body").css('background-repeat', 'repeat');
                        $.ajax({
                            url: site_url + "products/getdetails?imge=" + fabric,
                            dataType: "json",
                            success: function (data) {
                                imgwidth = data[0];
                                imgheight = data[1];
                            }
                        });
                        if ($(".hidden").val() == '1') {
                            $(".flexslider").remove();
                            $(".banner-text").remove();
                            $("#add-storyboard").parent().removeClass('display-none');
                            $("#save-visulizer").parent().removeClass('display-none');
                            $("#surfaces_content").show();
                            $(".visualizer").removeClass('display-none');
                            $(".storyboard").attr("id", 'save-visulizer');
                            $("#Surfaces").addClass('active');
                            $("#surfaces_content").addClass('active');
                            $("#Storyboard").removeClass('active');
                            $("#catalogue_content").removeClass('active');
                            $("#storyboard_content").removeClass('active');
                            $("#Catalogue").removeClass('active');

                        }
                    });
                    //console.log(data.view);
                    //$('#ajax-loader').modal('hide');
                    $('#fabric-loader').hide();
                    $('.storyboard').attr('id', 'save-visulizer');
                    flag = true;
                    //tooltipcall();
                }
            });
            return true;
        }
        $(document).on('click','.info-fab-visul',function(){
            $('#fabric').modal('hide');
            $('#info1').modal('hide');
            $('.select-fabric-view').removeClass('select-fabric-view');
            $('#surfaces_content').addClass('activetab');
            if ($("#canvas").attr('src') == '#') {
                reset($('#style-grid-view li:nth-child(2)').find('.img123').attr('data'));
                load_scene_coords();
                share();
            }
            alt = $(this).attr('alt');
            fabricId = $(this).attr('data');
            fabric = site_url + designpath + alt;
            $.ajax({
                url: site_url + "products/getdetails?imge=" + fabric,
                dataType: "json",
                success: function (data) {
                
                    imgwidth = data[0];
                    imgheight = data[1];
                 
                }
            });
            
            $(".flexslider").remove();
            $(".banner-text").remove();
            $("#add-storyboard").parent().removeClass('display-none');
            $("#save-visulizer").parent().removeClass('display-none');
            $("#surfaces_content").show();
            $(".visualizer").removeClass('display-none');
            $(".storyboard").attr("id", 'save-visulizer');
            $("#Surfaces").addClass('active');
            $("#surfaces_content").addClass('active');
            $("#Storyboard").removeClass('active');
            $("#catalogue_content").removeClass('active activetab');
            $("#storyboard_content").removeClass('active');
            $("#Catalogue").removeClass('active');

        });
        $(document).on('click','.cat-fab',function(){
            $('.select-fabric-view').removeClass('select-fabric-view');
            $('#surfaces_content').addClass('activetab');
            if ($("#canvas").attr('src') == '#') {
                reset($('#style-grid-view li:nth-child(2)').find('.img123').attr('data'));
                load_scene_coords();
                share();
            }
            alt = $(this).attr('alt');
            fabricId = $(this).attr('data');
            fabric = site_url + designpath + alt;
            $.ajax({
                url: site_url + "products/getdetails?imge=" + fabric,
                dataType: "json",
                success: function (data) {
                
                    imgwidth = data[0];
                    imgheight = data[1];
                 
                }
            });
            
            $(".flexslider").remove();
            $(".banner-text").remove();
            $("#add-storyboard").parent().removeClass('display-none');
            $("#save-visulizer").parent().removeClass('display-none');
            $("#surfaces_content").show();
            $(".visualizer").removeClass('display-none');
            $(".storyboard").attr("id", 'save-visulizer');
            $("#Surfaces").addClass('active');
            $("#surfaces_content").addClass('active');
            $("#Storyboard").removeClass('active');
            $("#catalogue_content").removeClass('active activetab');
            $("#storyboard_content").removeClass('active');
            $("#Catalogue").removeClass('active');

            
        });
        $(document).on('click','.fabric1',function () {
            $('.select-fabric-view').removeClass('select-fabric-view');
            $('#fabric').modal('hide');
            $('#surfaces_content').addClass('activetab');
            if ($("#canvas").attr('src') == '#') {
                reset($('#style-grid-view li:nth-child(2)').find('.img123').attr('data'));
                load_scene_coords();
                share();
				
            }
            alt = $(this).attr('alt');
            fabricId = $(this).attr('data');
            fabric = site_url + designpath + alt;
            backgroudImag = site_url + designpath + 'thumb/' + alt;
            //    $(".visualizer-module").css('background-image', 'url(' + backgroudImag + ')');
            //    $(".visualizer-module").css('padding-top', '0');
            //    $(".visualizer-module").css('background-repeat', 'repeat');
            // $("body").css('background-image', 'url(' + backgroudImag + ')');
            //$("body").css('padding-top', '0');
            //$("body").css('background-repeat', 'repeat');
            $.ajax({
                url: site_url + "products/getdetails?imge=" + fabric,
                dataType: "json",
                success: function (data) {
                
                    imgwidth = data[0];
                    imgheight = data[1];
                 
                }
            });
            if ($(".hidden").val() == '1') {
                $(".flexslider").remove();
                $(".banner-text").remove();
                //        $("#filter-module").remove();

                $("#add-storyboard").parent().removeClass('display-none');
                $("#save-visulizer").parent().removeClass('display-none');
                //$(".catalogue_content").show();
                $("#surfaces_content").show();
                //$(".storyboard_content").show();
                $(".visualizer").removeClass('display-none');
                $(".storyboard").attr("id", 'save-visulizer');
                $("#Surfaces").addClass('active');
                $("#surfaces_content").addClass('activetab active');
                $("#Storyboard").removeClass('active');
                $("#catalogue_content").removeClass('activetab active');
                $("#storyboard_content").removeClass('activetab active');
                $("#Catalogue").removeClass('active');

            }
        });
        $('#save-visulizer').click(function (event) {

            event.preventDefault();
            var url = $(this).attr('href');
            
            $.ajax({
                url: url,
                dataType: "json",
                data: {
                    'fabrics': querytn,
                    'style': style,
                    'url': $("#canvas").attr('src')
                },
                success: function (data) {
                    if (data.success) {
                        
                        $("#owl-storyboard").empty();
                        var HTML = data.view;
                        owl_storyboard.data('owlCarousel').addItem(HTML);

                        var count = $('.story-count').length; //$('.item').size();blue-color1

                        count = parseInt(count);
                        $("#property-count").html(count);
                       

                    } else if (data.error) {
                        alert(data.error); // dialogbox
                    }

                }

            });
        });
        
        function reset(s) {
            //    imgpath = fabric = surface = style = "";
            style = s;
            querystring = {};
            querytn = {};
            fabricId = '';
            var img = new Image();
            img.src = server + '/getimage.ashx?ft=1&fn=' + style + qtypath;
            $("#canvas").attr('src', img.src).load(function () {
                //$("#ajax-loader").modal('hide');
                $('.ui-loader').hide();
                $('#style').modal('hide');
                load_scene_visu_coords();
            });
            $("#canvaszoom").attr('src', img.src).load(function (){});
			addSpan();
        }

        function count_objects(coords) {
            var oa = [];
            for (var i = a_start_index; i <= a_last_index; i++)
                for (var x = 0; x < coords[i].length; x++)
                    if (coords[i][x][1] != -1 && ($.inArray(coords[i][x][1], oa) == -1))
                        oa.push(coords[i][x][1]);
            return (oa.length);
        }
        function load_scene_coords() {

            //$("#Surfaces").html('<img src="' + site_url + 'assets/images/loading.gif" id="surfacepreloader"></img>');
            $('#surfaces_content').addClass('activetab');
            $("#owl-surfaces").html('<img src="' + site_url + 'assets/images/loading.gif" id="surfacepreloader"></img>');
            $.getScript(server + "/getcoords.ashx?fn=" + style + "&ot=3&w=600&h=540", function (data, textStatus, jqxhr) {
                if (a != undefined && a.length > 0) {
                    var icount = 0;
                    while (icount < 2000)
                        if (a[icount++] != undefined)
                            break;
                    a_start_index = icount - 1;
                    a_last_index = a.length - 1;
                    object_count = count_objects(a);
                    load_object_thumbnails();
                }
            });
        }
        function load_scene_visu_coords() {
	
            var imageWidth = $('#canvas').width();
            var imageHeight = $('#canvas').height();
            console.log(imageWidth+" "+imageHeight);
            console.log(server + "/getcoords.ashx?fn=" + style + "&ot=3&w="+imageWidth+"&h="+imageHeight);
            $.getScript(server + "/getcoords.ashx?fn=" + style + "&ot=3&w="+imageWidth+"&h="+imageHeight, function() {});
	
        }
        $('#canvas').click(function(e) {
            var ImageMouseY = e.pageY;
            var ImageMouseX = e.pageX;
		
            var ObjNr = -1;
            if (typeof (a) != "undefined") {
                console.log('defined');
                if (a.length > 0) {
                    var LineY = a[ImageMouseY];
                    if (LineY) {
                        for (i = 0; i < LineY.length; i++) {
                            var LineX = LineY[i];
                            if (ImageMouseX >= LineX[0]) {
                                ObjNr = LineX[1];
                            }
                        } 
                    }
                }
            }

            alert("Object "+ObjNr+" Selected");
        });
		
        function load_object_thumbnails() {

            $("#owl-surfaces").empty();
            for (i = 0; i < object_count; i++) {
                //$("#Surfaces").append('<div style="width:96px; height: auto; border: 0px solid grey; float: left; margin-top: 10px; background-color: #f3f3f4;"><div class="thumbclick1" id="' + i + '" style="width: 80px; height: 80px; border: 1px solid grey; margin: 0 0px 10px 10px; background-color: white; background-image: url(' + server + '/getimage.ashx?ft=4&on=' + i + '&fn=' + style + '&h=80&w=80&bg=white&q=50);" ></div></div>');
                var HTML = '<div class="item"><a href="#" class="thumbclick1" id="' + i + '"><div class="color-circle fabric-div" id="fabric-div' + i + '"><img src="" id="fabric' + i + '"></div><img src="' + server + '/getimage.ashx?ft=4&on=' + i + '&fn=' + style + '&h=80&w=80&bg=white&q=50" class="img-responsive width-100" /></a></div>';
                owl_surfaces.data('owlCarousel').addItem(HTML);
                $('.fabric-div').hide();
                // console.log(HTML1);
                $("#surfacepreloader").remove();
            }
            $('.thumbclick1').click(function () {
                var tn = $(this).attr('id');
                if (fabric) {
                    var img_src = site_url + 'uploads/productimage/thumb/' + alt ;
                    $("#fabric"+ tn).attr("src",img_src);
                    $("#fabric-div"+ tn).show();
                    // $(".tabs-1").children("div").eq(tn).find('.fabric-choose').remove();
                    //$(".tabs-1").children("div").eq(tn).append('<div class="fabric-choose" style="background-image:url(' + site_url + 'uploads/productimage/thumb/' + alt + ')"></div>');
                    var temp = 'tn' + tn;
                    var tw = 'tw' + tn;
                    var th = 'th' + tn;
                    querytn[tn] = fabricId;
                    querystring[temp] = fabric;
                    querystring[tw] = imgwidth * 25.4 / dpi;
                    querystring[th] = imgheight * 25.4 / dpi;
                    imgpath = server + '/getimage.ashx?ft=1&fn=' + style + '&' + $.param(querystring);

                    createscreen(imgpath);
                } else {
                    display_activity('Please select Fabric first');
                }
            });
            if (storyboardIds)
                thumbsStoryBoard();
        }
        function createscreen(imgpath) {
            //console.log(imgpath);
            //share();
            var img = new Image();
            //$("#ajax-loader").modal('show');
            $('.ui-loader').show();
            var zoom = imgpath + qtypath;
            img.src = imgpath + qtypath + '&h=500';
            $("#canvas").attr('src', img.src).load(function () {
                //$("#ajax-loader").modal('hide');
                $('.ui-loader').hide();
                load_scene_visu_coords();
            });
            $("#canvaszoom").attr('src', zoom).load(function () {
                //$("#ajax-loader").modal('hide');
            });
			addSpan();
        }
       
        
    });
    var owl_storyboard = $("#owl-storyboard");

    owl_storyboard.owlCarousel({
        items: 10, //10 items above 1000px browser width
        itemsDesktop: [1000, 5], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 3], // betweem 900px and 601px
        itemsTablet: [600, 2], //2 items between 600 and 0
        itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
    });
    $(document).on('click','.save-storyboard',function (){
        if ($("#storyboard-title1").val() == '') {
            alert("Please choose name for your Storyboard"); //dailogmessage
        } else {
            var sel = $("#storyboard-title1").val();
            $.ajax({
                'url': site_url + "product/storyboardsave?val=" + sel + "+",
                'data': {
                    val: sel
                },
                success: function (data) {
                    //console.log(data);
                    if (data == 1) {
                        alert("Storyboard saved in my account"); //dailogmessage
                        $('.storyboardnotavail').remove();
                        //                        ele.parents('ul').remove(); 
                        //$(".next.browse.right,.prev.browse.left,.cataloge-options").hide();
                        var html = '<p class=storyboardnotavail> Storyboard : ' + sel + ' save to my account </p>';
                        $("#storyboard-title1").remove();
                        $(".catelogue-option-btn.save-storyboard").before('<b><label id="storyboard-title-save">' + sel + '</label></b><a class="catelogue-option-btn create-storyboard" href="javascript:void(0)" style="margin-left:10px;">CREATE NEW Storyboard</a>');
                        $(".catelogue-option-btn.save-storyboard").remove();
                        //$('.scroll-container').html(html);
                    } else {
                        //                        dailogmessage("sorry we can not save your catalogue at this time");
                        alert("Please Login First"); //dailogmessage
                    }
                }
            });
        }
    });
    $(document).on('click','.create-storyboard',function (){
        $.ajax({
            'url': site_url + "product/ajaxnewstoryboard",
            success: function (data) {
                //console.log(data);
                $("#owl-storyboard").empty();
                var input = '<li style="padding-top:16px;">Storyboard TITLE</li>' +
                    '<li><input type="text" class="form-control storyboard-title" name="storyboardtitle" id="storyboard-title1" placeholder="Storyboard Title"></li>'+
                    '<li style="padding-top:16px;"><a href="javascript:void(0)" class="catelogue-option-btn save-storyboard">SAVE Storyboard</a></li>';
                $(".storyboard-top").html(input);
                $("#property-count").html('0');
            }
        });
    });

    $(document).on('click','.downloadpdf-storyboard',function () {
       
        var count = $('.story-count').length; //$('.item').size();
        count = parseInt(count);
        if (count) {       
            /*
             * ask for title and save
             */
            if ($("#storyboard-title-save").html()) {
                var url = site_url + "storyboard/createpdf";
                //                $(this).target = "_blank";
                window.open(url);
            } else {
                alert("Please first save storyboard"); //dialogbox
            }
        } else {
            alert("Product not available in storyboard"); //dialogbox
        }
    });
    $(document).on('click','.delete-storyboard',function (){
       
        element = $(this);
        $.ajax({
            url: site_url + "storyboard/delete?id=" + $(this).attr('data'),
            dataType: "json",
            success: function (data) {
                
                if (data) {
                        
                    var count = $('.story-count').length; //$('.item').size();
                    count = parseInt(count);
                   
                    if (count != 1) {
                        element.parents("div.owl-item").remove();
                        var count1 = parseInt(count) - parseInt(1);
                        $("#property-count").html(count1);
                    } else {
                        $("#owl-storyboard").empty();
                        var count1 = parseInt(count) - parseInt(1);
                        $("#property-count").html(count1);
                        var html = '<div style="height: 100px; width: 200px"> No Story selected </div>';
                        $("#owl-storyboard").data('owlCarousel').addItem(html);
                            
                    }
                    // element.parents("li:first").remove();
                }
            }
        });
    });
    $(document).on("click", ".info-fab, .color-fab", function() {
        $(".zoomContainer").remove();
        id = $(this).attr('data');
        if (id) {
            $('#info1').modal('show');
            $("#response").html("<img class='preloader' src='" + site_url + "assets/images/loading.gif'/>");
            $.ajax({
                'url': site_url + "product/ajaxdetails/" + id,
                success: function(data) {
                    $("#response").html(data);
                }
            });
        }
    });
    $(document).on("click",".avail-color",function(){
        $('.zoomContainer').remove();
        id = $(this).attr("data");
        $("#response").html("<img class='preloader' src='" + site_url + "assets/images/loading.gif'/>");
        $.ajax({
            url:site_url+"product/ajaxdetails/"+id,
            success:function(data){
                $("#response").html(data);
            }
        });
    });
    var owl_catalogue = $("#owl-catalogue");
    var cat =0;
    owl_catalogue.owlCarousel({
        items: 10, //10 items above 1000px browser width
        itemsDesktop: [1000, 5], //5 items between 1000px and 901px
        itemsDesktopSmall: [900, 3], // betweem 900px and 601px
        itemsTablet: [600, 2], //2 items between 600 and 0
        itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
    });
    $(document).on("click", ".catlg-icon1", function() {
    
        if( cat == 0 && parseInt($(".blue-color1").html()) == 0){
            owl_catalogue.empty();
        }
        element = $(this);
        if ($(this).hasClass('selected')) {
            //commeneted by urvisha dailogmessage("Already added to catalogue");
        } else {
            id = $(this).attr('data');
            $.ajax({
                'url': site_url + "product/ajaxcatalog/" + id,
                'dataType': 'json',
                success: function(data) {
                    //console.log(data.view);
                    if (data == '') {
                        alert("Already added");
                        element.children('span').css({ color: "grey" });
                    } else if (data.result) {
                        alert("Product not found");
                    } else{
                        element.children('span').css({ color: "grey" });
                        owl_catalogue.data('owlCarousel').addItem(data.view);
                        var count = $('.cataloge-count').length; //$('.item').size();blue-color1
                        count = parseInt(count);
                        $(".blue-color1").html(count);
                        //aftersuccess(data, element);
                        element.parents('div#cat-info-view').html('<button class="btn btn-default deactive">Catalogue Product</button>')
                    }      
                }
            });
        }
    });
    $(document).on("click", ".catlg-icon11", function() {
    
        if( cat == 0 && parseInt($(".blue-color1").html()) == 0){
            owl_catalogue.empty();
        }
        element = $(this);
        if ($(this).hasClass('selected')) {
            //commeneted by urvisha dailogmessage("Already added to catalogue");
        } else {
            id = $(this).attr('data');
            $.ajax({
                'url': site_url + "product/ajaxcatalog/" + id,
                'dataType': 'json',
                success: function(data) {
                    //console.log(data.view);
                    if (data == '') {
                        alert("Already added");
                        $("")
                        element.html('<span class="icon-heart" aria-hidden="true"></span> Added to Catalog');
                    } else if (data.result) {
                        alert("Product not found");
                    } else{
                        element.children('span').css({ color: "grey" });
                        owl_catalogue.data('owlCarousel').addItem(data.view);
                        var count = $('.cataloge-count').length; //$('.item').size();blue-color1
                        count = parseInt(count);
                        $(".blue-color1").html(count);
                        element.html('Added to Catalog');
                        //aftersuccess(data, element);
                        element.parents('div#cat-info-view').html('<button class="btn btn-default deactive">Catalogue Product</button>')
                    }      
                }
            });
        }
    });
    $(document).on('click', ".delete-icon1", function() {
        id = $(this).attr('data');
        ele = $(this);
        $(".catlg-icon1").filter(function() {
            if ($(this).attr('data') == id) {
                $(this).append('<a title="Add to Catalogue" href="javascript:void(0)">&nbsp;</a>');
                $(this).removeClass('selected');
                $(this).children('span').attr("style"," ");
            }
        });
        $.ajax({
            'url': site_url + 'product/ajaxcatalogdelete/' + id,
            success: function(data) {
                
                var count = $('.cataloge-count').length; //$('.item').size();blue-color1
                        
                $(".catlg-property span.blue-color").text(count);
                if (count>1) {
                    ele.parents("div.owl-item").remove();
                    var count1 = parseInt(count) - parseInt(1);
                    $(".blue-color1").html(count1);
                } else {
                    owl_catalogue.empty();
                    var html = '<div style="height: 100px; width: 200px"> No catalogue selected </div>';
                    var count1 = parseInt(count) - parseInt(1);
                    $(".blue-color1").html(count1);
                    owl_catalogue.data('owlCarousel').addItem(html);
                }
            }
        });
    });
    /*
     * Cart js
     */
    $(document).on('click', '.add-to-cart', function(event) {
        event.preventDefault();
        element = $(this);
        var id = element.attr('data');
        // alert(element.find('INPUT.qty1').val()); return false;
     
        if ($(this).hasClass('selected'))
            alert("Already Added to cart");
        else
            qty = $('#qty'+id).val();
        // qty = element.next('.qty-container').find('input').val();
        var flag = element.hasClass('add-to-cart');
        if (qty == '') {
            alert("Qty cannot be empty");
        } else if (qty == 0) {
            alert("Qty cannot be zero");
        } else if (qty != 0 || qty != '') {
            $.ajax({
                url: element.attr('href'),
                dataType: "json",
                data: {
                    'qty': qty
                },
                success: function(data) {
                    if (data.result == 1 && !flag) {
                        var html = '<a class="btn btn-success" href="' + site_url + 'cartlist" >View Cart </a>';
                        element.after(html);
                        element.remove();
                        $('#qty1'+id).remove();
                        $('#qty-container').remove();
                        $('.cart-count').text(parseInt($('.cart-count').text()) + parseInt(1));
                        var changeIcon = true;
                    } else if (data.result == 1 && flag) {
                        $('#qty'+id).remove();
                        element.addClass('selected');
                        element.children('span').addClass('selected1');
                        element.children('span').css({ color: "grey" });
                        $('.cart-count').text(parseInt($('.cart-count').text()) + parseInt(1));
                        var changeIcon = true;
                    } else {
                        var changeIcon = false;
                        alert("Sorry we can not add this product");
                    }
                    if (changeIcon && flag) {
                        $('#qty-container'+id).addClass('cart-hide');
                        element.next('.qty-container').hide();
                    } else if (changeIcon && !flag) {
                        popupElement = $(".cart-parent-span[data='" + element.attr('data') + "']");
                        popupElement.html('');
                        popupElement.removeClass('brd-none cart-parent-span');
                        popupElement.addClass('cart-icon1');
                        popupElement.removeAttr('title');
                        popupElement.addClass('selected');
                    }
                }
            });
        } else {

        }
    });
    $(document).on('click', '.add-to-cart-details', function(event) {
        event.preventDefault();
        element = $(this);
        var id = element.attr('data');
        // alert(element.find('INPUT.qty1').val()); return false;
     
        if ($(this).hasClass('selected'))
            alert("Already Added to cart");
        else
            qty = $('#qty1'+id).val();
        // qty = element.next('.qty-container').find('input').val();
        var flag = element.hasClass('add-to-cart');
        if (qty == '') {
            alert("Qty cannot be empty");
        } else if (qty == 0) {
            alert("Qty cannot be zero");
        } else if (qty != 0 || qty != '') {
            $.ajax({
                url: element.attr('href'),
                dataType: "json",
                data: {
                    'qty': qty
                },
                success: function(data) {
                    if (data.result == 1 && !flag) {
                        var html = '<a class="btn btn-success" href="' + site_url + 'cartlist" style="color:white">View Cart </a>';
                        element.after(html);
                        element.remove();
                        $('#qty1'+id).remove();
                        $('#qty-container').remove();
                        $('.cart-count').text(parseInt($('.cart-count').text()) + parseInt(1));
                        var changeIcon = true;
                    } else if (data.result == 1 && flag) {
                        element.addClass('selected');
                        $('.cart-count').text(parseInt($('.cart-count').text()) + parseInt(1));
                        var changeIcon = true;
                    } else {
                        var changeIcon = false;
                        alert("Sorry we can not add this product");
                    }
                    if (changeIcon && flag) {
                        element.next('.qty-container').hide();
                    } else if (changeIcon && !flag) {
                        popupElement = $(".cart-parent-span[data='" + element.attr('data') + "']");
                        popupElement.html('');
                        popupElement.removeClass('brd-none cart-parent-span');
                        popupElement.addClass('cart-icon1');
                        popupElement.removeAttr('title');
                        popupElement.addClass('selected');
                    }
                }
            });
        } else {

        }
    });
    $(document).on("click", ".save-catalogue", function() {
        if ($("#catelogue-title").val() == '') {
            alert("Please choose name for your catalogue"); //dailogmessage
        } else {
            var value = $("#catelogue-title").val();
            $.ajax({
                'url': site_url + "product/ajaxcatalogsave",
                'data': {
                    val: value
                },
                success: function(data) {
                    if (data == 1) {
                        alert("Catalogue saved in my account"); //dailogmessage
                        $("#catelogue-title").remove();
                        $(".catelogue-option-btn.save-catalogue").before('<b><label id="catelogue-title-save">' + value + '</label></b>  <a class="catelogue-option-btn create-catalogue" href="javascript:void(0)" style="margin-left:10px">CREATE NEW CATALOGUE</a>');
                        $(".catelogue-option-btn.save-catalogue").remove();
                    } else {
                        //                        dailogmessage("sorry we can not save your catalogue at this time");
                        alert("Please Login First");
                    }
                }
            });
        }
    });
    $(document).on("click", '.create-catalogue', function() {
        var ele = $(this);
        $.ajax({
            'url': site_url + "product/ajaxcatalognew",
            success: function(data) {
                owl_catalogue.empty();
                var input = '<li style="padding-top:16px;">CATALOGUE TITLE :</li>'+
                    '<li><input type="text" class="form-control catelogue-title" placeholder="Catelogue Title" name="cateloguetitle" id="catelogue-title"></li>' +
                    '<li style="padding-top:16px;"><a href="#" class="catelogue-option-btn save-catalogue">SAVE CATALOGUE</a></li>';
                $(".catelogue-top").html(input);
                $(".blue-color1").html('0');
            }
        });
    });
    $(document).on('click','.emailpdf',function () {
        var count = $('.cataloge-count').length;
        count = parseInt(count);
        if (count) {
            /*
             * ask for title and save
             */
            if ($("#catelogue-title-save").html()) {
                $.ajax({
                    'url' : site_url + "frontend/home/email",
                 
                    success: function(data){
                        $('#email2pdf').modal('show');
                    }
                });
            } else {
                alert("Please first save catalogue"); //dailogmessage
            }
        } else {
            alert("Product not available in catalogue"); //dailogmessage
        }

    });
    $(document).on('click','#send-mail', function(){
        var re = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
        var email =  $('#email').val();
        var subject = $('#subject').val();
        var i = 0;
        if(!re.test(email)){
            $('#email-error').html("Please enter valid email");
            
            i++;
        }
        if(subject == ''){
            $('#subject-error').html("Please Enter Subject");
           
            i++
        }
        if(i == 0){
            $('#email-error').html(' ');
            $('#subject-error').html(' ');
            //$('#ajax-loader').modal('show');
            $('.ui-loader').show();
            $.ajax({
                'url' : site_url + "catalogue/emailpdf",
                'data' : $('form#frmemail').serialize(),
                success: function(data){
                    // $('#ajax-loader').modal('hide');
                    $('.ui-loader').hide();
                    if(data == 1){
                        alert('send mail');
                        $('#email2pdf').modal('hide');
                    }else{
                        alert('email not sent');
                       
                        $('#email2pdf').modal('hide');
                    }
                    $('form#frmemail').each (function(){
                        this.reset();
                    });
                }
            });
        }
        
    });
    $(document).on('click','.downloadpdf',function () {
   
        /*
         * Check catalogue products
         */
        if ($('.cataloge-count').length) {
            /*
             * ask for title and save
             */
            if ($("#catelogue-title-save").html()) {

                var url = site_url + "catalogue/createpdf";
                //                $(this).target = "_blank";
                window.open(url);
            } else {
                alert("Please first save catalogue"); //dailogmessage
            }
        } else {
            alert("Product not available in catalogue"); //dailogmessage
        }
    });
    $(document).on('click', '.fav-icon', function(event) {
        event.preventDefault();
        element = $(this);
        var product_id = $(this).attr('data');
        $.ajax({
            url: 'favorities',
            dataType: "json",
            data: {
                'product_id': product_id
            },
            success: function(data) {
                if (data.id) {
                    var html = '<a href="' + site_url + 'favoriteslist" class="btn btn-warning deactive">View Favorite</a>';
                    element.after(html);
                    element.remove();
                } else {
                    alert(data.message);
                }
            }
        });

    });
    $(document).on('click', '.fab-fav', function(event) {
        event.preventDefault();
        element = $(this);
        var product_id = $(this).attr('data');
        $.ajax({
            url: 'favorities',
            dataType: "json",
            data: {
                'product_id': product_id
            },
            success: function(data) {
                if (data.id) {
                    element.addClass('btn-disabled');
                    //element.attr('disabled','');
                    element.removeClass('fab-fav');
                    element.html('<img src="<?php echo base_url(); ?>assets/frontend/images/heart-red-full.png">');
                } else {
                    alert(data.message);
                }
            }
        });

    });
    $(document).on('click','#maskClose',function(){
        $("#imageFullScreen").attr('src','#');
        $('#imgContainer').hide();
        $('#positionButtonDiv').hide();
        $('.mask').hide();
    });
   
    $(document).on('hidden.bs.modal', '#info1',function () {
        $('.zoomContainer').remove();
        zoomclose();
			
    });
    $(document).on('hidden.bs.modal', '#fabric',function () {
        $('.zoomContainer').remove();
			
    });
    function zoomclose(){
        setTimeout(function(){ $('.zoomContainer').remove(); },2000);
    }
    //Zoom script:
    $(document).on('click','#fullscreen',function () {
			
        $("#imageFullScreen").attr('src', $('#canvaszoom').attr('src')).load(function () {
            //$("#ajax-loader").modal('hide');
            $('.ui-loader').hide();
            $('#imgContainer').show();
            $('#positionButtonDiv').show();
            $('.mask').show();
            zoom();
				
        });        
    });
    $(document).on('click','.mask',function () {
        $("#imageFullScreen").attr('src','#');
        $('#imgContainer').hide();
        $('#positionButtonDiv').hide();
        $('.mask').hide();
    });

    //key event when press esc then close popup
    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $("#imageFullScreen").attr('src','#');
            $('#imgContainer').hide();
            $('#positionButtonDiv').hide();
            $('.mask').hide();
        }   // esc
    });

    function zoom() {
        $('#imageFullScreen').smartZoom("destroy");
        //    $('#imageFullScreen').smartZoom('Reset') ;
        $('#imageFullScreen').smartZoom({
            'containerClass': 'zoomableContainer'
        });

        $('#topPositionMap,#leftPositionMap,#rightPositionMap,#bottomPositionMap').bind("click", moveButtonClickHandler);
        $('#zoomInButton,#zoomOutButton').bind("click", zoomButtonClickHandler);

        function zoomButtonClickHandler(e) {
            var scaleToAdd = 0.8;
            //console.log("zoombutton" + e.target.id)
            if (e.target.id == 'zoomOutButton')
                scaleToAdd = -scaleToAdd;
            $('#imageFullScreen').smartZoom('zoom', scaleToAdd);
        }

        function moveButtonClickHandler(e) {
            var pixelsToMoveOnX = 0;
            var pixelsToMoveOnY = 0;
            //console.log("zoombuttonmove" + e.target.id)
            switch (e.target.id) {
                case "leftPositionMap":
                    pixelsToMoveOnX = 50;
                    break;
                case "rightPositionMap":
                    pixelsToMoveOnX = -50;
                    break;
                case "topPositionMap":
                    pixelsToMoveOnY = 50;
                    break;
                case "bottomPositionMap":
                    pixelsToMoveOnY = -50;
                    break;
            }
            $('#imageFullScreen').smartZoom('pan', pixelsToMoveOnX, pixelsToMoveOnY);
        }

    }
    $(document).on('keyup','.cart-value-sm',function(){
        var stock = $(this).attr('data');
        if(parseInt(stock)< parseInt($(this).val())){
            alert("Stock Limit is : "+stock );
            $(this).val('1');
            $(this).focus();
            return false;
        }
    });
    function share() {}
    addSpan();
    function addSpan(){
			//alert('share');
        var id= 'button_'+glob;
			
        $('#add-a').html('<a href="javascript:void(0)" title="SHARE" class="image-share-lib" displayText="ShareThis" id="'+id+'"><span class=" icon-share" aria-hidden="true"></span> <b>Share</b></a>');
        addMore(id);
    }
			
    function addMore(id){
			
        var id = id;
        var img_path_share=document.getElementById('canvas').getAttribute('src');
           
        if(img_path_share == '#'){
            share_iamge = site_url+'assets/frontend/images/logo-en.jpg';
        }else{
            share_iamge = img_path_share;
        }
        stWidget.addEntry({
            "service":"sharethis",
            "element":document.getElementById(id),
            "url":site_url+glob,
            "title":"Aljedaie",
            "type":"large",
            "text":site_url ,
            "image":share_iamge,
            "summary":"this is description"+glob   
        });
        glob++;
    }
</script>
</body>
</html>