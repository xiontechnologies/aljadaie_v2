
            <?php // $this->load->view('frontend/layout/leftwidgets', $this->data); ?>
            <?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
</div>
</div>
<div class="col-sm-6 shadow-main login-form" >
                 <div class="col-sm-12"> 
            <?php
            if ($add_error)
                echo $add_error;
            if ($add_success)
                echo $add_success;
            ?>
            <?php if (isset($signuperror) && !empty($signuperror)) { ?>
                <span class="error" style="float:none"><?php echo $signuperror; ?></span>
            <?php } ?>
            <?php if (isset($success) && !empty($success)) { ?>
                <span class="success" style="float:none"><?php echo $success; ?></span>
            <?php } ?>
                 <h2 class="page-header heading-bd"><span>Unsubscribe Newsletter</span></h2>
            <div class="row login-page-left">
            
            <?php
            $login = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'email',
                'placeholder' => 'Enter email',
                'value' => '',
                'class' => 'form-control',
                'required' => 'required'
            );
            ?>
            <?php echo form_open($this->uri->uri_string()); ?>
            
                <div class="col-sm-4">
                    <span style="color: red">*</span> <?php echo form_label('Email', 'email'); ?>
                </div>
                <div class="col-sm-8">
                    <?php echo form_input($login); ?>
                </div>
                <div class="col-sm-12">
                    <span style="color: red;"><?php echo form_error('email'); ?></span>
                </div>
                <div class="col-sm-12">
                    <?php echo form_submit('unsubscribe', 'Unsubscribe', 'class="btn btn-custom"'); ?>

                </div>
            <?php echo form_close(); ?>
            </div>
            
        </div>
    </div>
</div>
</div>
</section>
</div>
</div>
