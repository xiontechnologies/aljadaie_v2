
<div class="col-sm-6">
    <h2 class="heading-bd"><span class="icon-user-following" aria-hidden="true"></span> Sign Up</h2>
    <div class="row login-page-left">
        <?php
        echo form_open('register');
        $email = array(
            'name' => 'remail',
            'type' => 'email',
            'id' => 'remail',
            'placeholder' => 'Enter email',
            'value' => isset($postdata['remail']) ? $postdata['remail'] : '',
            'class' => 'form-control',
            'required' => 'required'
        );
        $password = array(
            'name' => 'rpassword',
            'id' => 'rpassword',
            'placeholder' => 'Password',
            'value' => '',
            'class' => 'form-control',
            'required' => 'required'
        );
        $cpassword = array(
            'name' => 'cpassword',
            'id' => 'cpassword',
            'placeholder' => 'Password',
            'value' => '',
            'class' => 'form-control',
            'required' => 'required'
        );
        ?>
        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> First Name', 'firstname'); ?>
            <?php
            echo form_input(
                    array(
                        'name' => 'firstname',
                        'class' => 'form-control',
                        'id' => 'firstname',
                        'placeholder' => 'First Name',
                        'value' => isset($postdata['firstname']) ? $postdata['firstname'] : (isset($user->firstname) ? $user->firstname : ''),
                        'required' => 'required'
            ));
            echo form_error('firstname');
            ?>
        </div>
        <div class="col-sm-4"> 
            <?php echo form_label('<span style="color: red">*</span> Last Name', 'lastname'); ?>
        </div>
        <div class="col-sm-8">
            <?php
            echo form_input(
                    array(
                        'name' => 'lastname',
                        'class' => 'form-control',
                        'id' => 'lastname',
                        'placeholder' => 'Last Name',
                        'value' => isset($postdata['lastname']) ? $postdata['lastname'] : (isset($user->lastname) ? $user->lastname : ''),
                        'required' => 'required'
            ));
            ?>
        </div>
        <div class="col-sm-12">
            <?php
            echo form_error('lastname');
            ?>
        </div>
        <div class="col-sm-4">
<?php echo form_label('<span style="color: red">*</span> Email address', 'email'); ?>
        </div>
        <div class="col-sm-8">
<?php echo form_input($email); ?>
        </div>
        <div class="col-sm-4">
<?php echo form_label('<span style="color: red">*</span> Password', 'password'); ?>
        </div>
        <div class="col-sm-8">
<?php echo form_password($password); ?>
        </div>
        <div class="col-sm-4">
            <?php echo form_label('<span style="color: red">*</span> Confirm Password', 'cpassword'); ?>
        </div><div class="col-sm-8">
<?php echo form_password($cpassword); ?>
        </div>
        <div class="col-sm-4">
            <?php echo form_label('Company', 'company'); ?>
        </div><div class="col-sm-8">
            <?php
            echo form_input(array('name' => 'company',
                'id' => 'company',
                'palceholder' => 'Company', 'class' => 'form-control',
                'value' => isset($postdata['company']) ? $postdata['company'] : (isset($user) && $user->company ? ($user->userprofile->company) : ''),
            ));
            ?>
        </div>
        <div class="col-sm-4">
            <?php echo form_label('<span style="color: red">*</span> Address', 'address'); ?>
        </div><div class="col-sm-8">
            <?php
            echo form_textarea(array('name' => 'address',
                'id' => 'address',
                'palceholder' => 'address', 'class' => 'form-control',
                'value' => isset($postdata['address']) ? $postdata['address'] : (isset($user) && $user->address ? ($user->userprofile->address) : ''),
                'required' => 'required'
            ));
            ?>
        </div><div class="col-sm-12">
            <?php
            echo form_error('address');
            ?>
        </div>
        <div class="col-sm-4">
            <?php echo form_label('<span style="color: red">*</span> City', 'city'); ?>
        </div><div class="col-sm-8">
            <?php
            echo form_input(array('name' => 'city',
                'id' => 'city',
                'palceholder' => 'city', 'class' => 'form-control',
                'value' => isset($postdata['city']) ? $postdata['city'] : (isset($user) && $user->userprofile->city ? ($user->userprofile->city) : ''),
                'required' => 'required'
            ));
            ?>
        </div><div class="col-sm-12">
<?php
echo form_error('city');
?>
        </div>
        <div class="col-sm-4">
            <?php echo form_label('<span style="color: red">*</span> State/Province', 'state'); ?>
        </div><div class="col-sm-8">
            <?php
            echo form_input(array('name' => 'state',
                'id' => 'state',
                'palceholder' => 'state', 'class' => 'form-control',
                'value' => isset($postdata['state']) ? $postdata['state'] : (isset($user) && $user->state ? ($user->userprofile->state) : ''),
                'required' => 'required'
            ));
            ?>
        </div><div class="col-sm-12">
            <?php echo form_error('state');
            ?>
        </div>
        <div class="col-sm-4">
            <?php echo form_label('<span style="color: red">*</span> Country', 'country'); ?>
        </div><div class="col-sm-8">
            <?php
            echo form_input(array('name' => 'country',
                'id' => 'country',
                'palceholder' => 'country', 'class' => 'form-control',
                'value' => isset($postdata['country']) ? $postdata['country'] : (isset($user) && $user->country ? ($user->userprofile->country) : ''),
                'required' => 'required'
            ));
            ?>
        </div><div class="col-sm-12">
            <?php echo form_error('country');
            ?>
        </div>

        <div class="col-sm-4">
            <?php echo form_label('<span style="color: red">*</span> Postal/Zip Code', 'zip'); ?>
        </div>
        <div class="col-sm-8">
            <?php
            echo form_input(array('name' => 'zip',
                'id' => 'zip',
                'palceholder' => 'zip', 'class' => 'form-control',
                'value' => isset($postdata['zip']) ? $postdata['zip'] : (isset($user) && $user->zip ? ($user->userprofile->zip) : ''),
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="col-sm-4">
            <?php echo form_label('Fax', 'fax'); ?>
        </div>
        <div class="col-sm-8">
            <?php
            echo form_input(array('name' => 'fax',
                'id' => 'fax',
                'palceholder' => 'fax', 'class' => 'form-control',
                'value' => isset($postdata['fax']) ? $postdata['fax'] : (isset($user) && $user->fax ? ($user->userprofile->fax) : ''),
            ));
            ?>
        </div><div class="col-sm-12">
            <?php echo form_error('fax');
            ?>
        </div>

        <div class="col-sm-4">
            <?php echo form_label('<span style="color: red">*</span> Telephone', 'telephone'); ?>
        </div><div class="col-sm-8">
            <?php
            echo form_input(array('name' => 'telephone',
                'id' => 'telephone',
                'palceholder' => 'telephone', 'class' => 'form-control',
                'value' => isset($postdata['telephone']) ? $postdata['telephone'] : (isset($user) && $user->telephone ? ($user->userprofile->telephone) : ''),
                'required' => 'required'
            ));
            ?>
        </div><div class="col-sm-12">
            <?php echo form_error('telephone');
            ?>
        </div>
        <div class="col-sm-12">
<?php
echo form_submit('submit', 'Register', 'class="org-btn default-btn"');

echo form_close();
?>
        </div>
        <div class="col-sm-12 margin-bottom-10">
            <div class="row">
                <div class="col-sm-8 margin-top-10">Login with a Social Network</div>
                <div class="col-sm-4">
                    <a href="<?php echo base_url('login/facebook'); ?>"><img title="Sign In with Facebook" alt="Sign In with Facebook" src="<?php echo base_url('assets/frontend/images/fb-log-pop.jpg') ?>"></a>
                    <a href="<?php echo base_url('login/google'); ?>"><img title="Sign In with Google+" alt="Sign In with Google+" src="<?php echo base_url('assets/frontend/images/gplus-log-pop.jpg'); ?>" ></a>
                </div>
            </div>
        </div>
    </div>
</div>
