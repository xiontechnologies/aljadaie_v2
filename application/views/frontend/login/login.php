<?php
echo form_open('frontend/auth/index');
?>
<div id="login-page" class="col-sm-12 shadow-main" >
    <div class="col-sm-3"></div>
    <div class="col-sm-6" style="padding: 20px">
        <h2 class="heading-bd" style="color: #333333"><span class="icon-lock" aria-hidden="true"></span> Login</h2>
        <div class="row login-page-left">
            <div class="col-sm-4">
                <label for="email"><span style="color: red">*</span>Email address</label>
            </div>
            <div class="col-sm-8">
                <input type="email" class="form-control" placeholder="Email address" id="email" required="required" value="" name="email">
            </div>
            <div class="col-sm-12"> <?php echo form_error('email', '<span class="error">', '</span>'); ?></div>
            <div class="col-sm-4">
                <label for="Password"><span style="color: red">*</span>Password</label>
            </div>
            <div class="col-sm-8">
                <input type="password" class="form-control" placeholder="Password" id="password" required="required" value="" name="password">
            </div>
            <div class="col-sm-12"><?php echo form_error('password', '<span class="error">', '</span>'); ?></div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <input type="submit" class="btn btn-custom" name="submit" value="Submit">
            </div>
            <div style="padding-top:7px;" class="col-sm-4"><a href="javascript:void(0)" id="forgotpassword" >Forgot Password</a></div>
            <?php
            echo form_close();
            ?>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-8 margin-top-10" style="text-align: right">Login with a Social Network</div>
                    <div class="col-sm-4">
                        <a href="#"><img src="<?php echo base_url(); ?>/assets/frontend/images/fb-log-pop.jpg"></a>
                        <a href="#"><img src="<?php echo base_url(); ?>/assets/frontend/images/gplus-log-pop.jpg"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3"></div>
</div>

<!--    <div class="login-form social">
        <p style="margin:0">Login with a Social Network  &nbsp;&nbsp;<a href="<?php //echo base_url('login/facebook');    ?>" ><img title="Sign In with Facebook" alt="Sign In with Facebook" src="<?php //echo base_url('assets/frontend/images/fb-log-pop.jpg')    ?>"></a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php // echo base_url('login/google');    ?>" ><img title="Sign In with Google+" alt="Sign In with Google+" src="<?php //echo base_url('assets/frontend/images/gplus-log-pop.jpg');    ?>"></a></p>
    </div>-->
<!--forgot password-->

<?php
$login = array(
    'name' => 'email',
    'id' => 'email',
    'type' => 'email',
    'placeholder' => 'Enter email',
    'value' => '',
    'class' => 'form-control',
    'required' => 'required'
);
?>
<div class="col-sm-6 shadow-main login-form forgot-pass" style="display: none;" >
    <div class="col-sm-12">
        <h2 class="heading-bd"><span class="icon-lock" aria-hidden="true"></span> Forget Password</h2>
        <div class="row login-page-left">
            <div class="col-sm-12">
                <div class="row">
                    <?php echo form_open("forgotpassword", 'id="forgotpassword"') ?>  
                    <div class="col-sm-4">
                        <label for="email"><span style="color: red">*</span>Email address</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="Email address" id="email" required="required" value="" name="email">
                    </div>
                    <div class="col-sm-12">
                        <?php echo form_error('password', '<span class="error">', '</span>'); ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-8">
                        <input type="submit" class="btn btn-custom" name="submit" value="Submit">
                    </div>
                </div>
                <?php
                echo form_close();
                ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click", "#forgotpassword", function () {
        $('.login-form').hide();
        $('.forgot-pass').show();
    });
</script>