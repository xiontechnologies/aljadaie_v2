<?php
/*
 * @auther Shafiq
 */
?>
<!-- Body Text -->
<section>
    <div id="content-section"> 
        <!-- left widgets -->
        <div class="left-widget">
            <ul class="makelist widget">
                <li class="icon-btns" id="fabric-style"><a href="javascript:void(0)"><span class="icons style-icon"  title="SELECT A STYLE">SELECT A STYLE</span></a></li>
                <li class="icon-btns"> <a href="javascript:void(0)"><span class="icons share-icon" onclick='postToFeed(); return false;'  title="SHARE">SHARE</span></a> <a href="javascript:void(0)" title="DOWNLOAD PDF"><span class="icons pdf-icon">DOWNLOAD PDF</span></a> <a href="javascript:void(0)" title="EMAIL"><span class="icons email-icon">EMAIL</span></a> </li>
<!--                <li class="icon-btns"><a href="javascript:void(0)" title="ADD TO STORYBOARD"><span class="icons storyB-icon">ADD TO STORYBOARD</span></a></li>-->
            </ul>

            <!-- Select a Style -->
            <div id="overlay" class="select-style"> <span class="box-arrow">&nbsp;</span> <span class="box-drag">&nbsp;</span>
                <h1>Select a <span class="blue-color">Style</span> <span class="ui-dialog-titlebar-close" id="close-pop">&nbsp;</span></h1>
                <div id="fabric-tabs-style">
                    <ul class="maketabs fr tab-btn">
                        <li class="active grid-view"><a href="#style-grid" title="Grid View">Grid View</a></li>
                        <li class="list-view"><a href="#style-list" title="List View">list View</a></li>
                    </ul>
                    <div class="tab-article">
                        <div class="fl"><a href="javascript:void(0)" class="selected">Suits</a> / <a href="">Jackets</a> / <a href="javascript:void(0)">Trousers</a></div>
                        <div class="clr">&nbsp;</div>
                    </div>
                    <!-- Tab Content -->
                    <div class="tab-listing" id="style-grid">
                        <ul class="maketabs listing-table">
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa1.jpg') ?>" alt="grandsofa1" class="img" data="grandsofa1" title="" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa2.jpg') ?>" alt="grandsofa2" class="img" title="" data="grandsofa2" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa3.jpg') ?>" alt="grandsofa3" class="img" title="" data="grandsofa3" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa4.jpg') ?>" alt="grandsofa4" class="img" title="" data="grandsofa4" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa5.jpg') ?>" alt="grandsofa5" class="img" title="" data="grandsofa5" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa6.jpg') ?>" alt="grandsofa6" class="img" title="" data="grandsofa6" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa7.jpg') ?>" alt="grandsofa7" class="img" title="" data="grandsofa7" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa8.jpg') ?>" alt="grandsofa8" class="img" title="" data="grandsofa8" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa9.jpg') ?>" alt="grandsofa9" class="img" title="" data="grandsofa9" /></a></div></li>
                        </ul>
                        <!-- Pagination -->
                        <div class="pagination rightAlign strong"> Showing <span>04</span> of <span>24</span> <span class="prev-arrow"><a href="javascript:void(0)" title="Previous">&nbsp;</a></span> <span class="next-arrow"><a href="javascript:void(0)" title="Next">&nbsp;</a></span> </div>
                    </div>
                    <!-- List view for fabric style  -->
                    <div id="style-list">
                        <ul class="maketabs listing-table">
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa1.jpg') ?>" alt="grandsofa1" class="img" data="grandsofa1" title="" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa2.jpg') ?>" alt="grandsofa2" class="img" title="" data="grandsofa2" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa3.jpg') ?>" alt="grandsofa3" class="img" title="" data="grandsofa3" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa4.jpg') ?>" alt="grandsofa4" class="img" title="" data="grandsofa4" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa5.jpg') ?>" alt="grandsofa5" class="img" title="" data="grandsofa5" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa6.jpg') ?>" alt="grandsofa6" class="img" title="" data="grandsofa6" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa7.jpg') ?>" alt="grandsofa7" class="img" title="" data="grandsofa7" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa8.jpg') ?>" alt="grandsofa8" class="img" title="" data="grandsofa8" /></a></div></li>
                            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url('assets/' . FRONTENDFOLDERNAME . '/images/grandsofa9.jpg') ?>" alt="grandsofa9" class="img" title="" data="grandsofa9" /></a></div></li>
                        </ul>
                        <!-- Pagination -->
                        <div class="pagination rightAlign strong"> Showing <span>04</span> of <span>24</span> <span class="prev-arrow"><a href="javascript:void(0)" title="Previous">&nbsp;</a></span> <span class="next-arrow"><a href="javascript:void(0)" title="Next">&nbsp;</a></span> </div>
                    </div>
                </div>
            </div>
            <!-- End Select a Style --> 

        </div>
        <!-- end widgets --> 

        <!-- Visualizer module -->
        <article>
            <div class="visualizer-module centerAll">
                <div class="image-article"><img id="canvaspreloader" style="display:none" src="<?php echo base_url('assets/images/loading.gif') ?>" alt="" title="" class="img" /><img id="canvas" src="http://vivek2.picarisplatform.com/picaris/grandsofa.ashx?ft=1&on=0&fn=suit&h=500" alt="" title="" class="img" /></div>
                <!--                <canvas class="image-article" id="canvas"></canvas>-->
            </div>
        </article>
        <!-- end Visualizer module --> 

        <!-- Right widgets -->
        <div class="right-widget">
            <ul class="makelist widget">
                <li class="icon-btns" id="select-fabric"><a href="javascript:void(0)" title="SELECT A FABRIC"><span class="icons fabric-icon">SELECT A FABRIC</span></a></li>
            </ul>
            <div class="clr">&nbsp;</div>

            <!-- Overlay module -->
            <div id="overlay" class="select-fabric" style="display:block"> <span class="box-arrow">&nbsp;</span> <span class="box-drag">&nbsp;</span>
                <h1>Select a <span class="blue-color">Fabric</span>  <span class="ui-dialog-titlebar-close" id="close-pop">&nbsp;</span></h1>
                <div id="fabric-tabs">
                    <ul class="maketabs fr tab-btn fabric-tabs">
                        <li class="active grid-view"><a href="#grid-fabric" title="Grid View">Grid View</a></li>
                        <li class="list-view"><a href="#list-fabric" title="List View">list View</a></li>
                    </ul>
                    <div class="tab-article">
                        <div class="fl fabric-filter"> <span class="fl">
                                <select name="">
                                    <option>Select Color</option>
                                </select>
                                <select name="">
                                    <option>Select Weave</option>
                                </select>
                            </span> <span class="filter-fullview" style="float:left">
                                <select name="">
                                    <option>Select Category</option>
                                </select>
                                <select name="">
                                    <option>Select Blend</option>
                                </select>
                                <select name="">
                                    <option>Select Occation</option>
                                </select>
                                <select name="">
                                    <option>Select Collection</option>
                                </select>
                                <input name="" value="Search " type="text">
                            </span>
                            <div class="clr">&nbsp;</div>
                        </div>
                        <div class="clr">&nbsp;</div>
                    </div>
                    <!-- Tab Content -->
                    <div class="tab-listing" id="grid-fabric"> 
                        <!--<ul class="maketabs listing-table fabric-fullview">--> <!-- Uncomment this ul for full view -->
                        <ul class="maketabs listing-table">
                            <?php
                            if ($product) {
                                foreach ($product as $pk => $pvalue) {
                                    /*
                                     * Post is taken when some input is checked in filter and result come from variation
                                     * if empty then all products will show there and result come from products
                                     */
                                    if (isset($post) && $post)
                                        $pv = $pvalue->product;
                                    else
                                        $pv = $pvalue;
                                    if ($pk == 5)
                                        $hide = "style='display:block'";
                                    ?>
                                    <li class="listing"> <div class="listing-img"><a href="javascript:void(0)"><img src="<?php echo $pv->thumimagepath; ?>" data="<?php echo $pv->id ?>" design="<?php echo $pv->designpath; ?>" alt="" class="img close-pop" title="" /></a></div>
                                        <div class="info"> <span class="info-icon" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)"  title="Color"> <?php echo '+' . count($pv->colors); ?></a></span> <span class="catlg-icon brd-none" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" title="Add to Catalogue" >&nbsp;</a></span> </div>
                                    </li>
                                    <?php
                                }
                            } else {
                                ?>
                                No Product available as this time
                            <?php } ?>
                        </ul>

                        <!-- Pagination -->
                        <div class="pagination rightAlign strong"> Showing <span>04</span> of <span>24</span> <span class="prev-arrow"><a href="javascript:void(0)" title="Previous">&nbsp;</a></span> <span class="next-arrow"><a href="javascript:void(0)" title="Next">&nbsp;</a></span> </div>
                    </div>
                    <!-- Fabric List View -->
                    <div id="list-fabric">
                        <ul class="maketabs listing-table lsit-view">
                            <?php
                            if ($product) {
                                foreach ($product as $pk => $pvalue) {
                                    /*
                                     * Post is taken when some input is checked in filter and result come from variation
                                     * if empty then all products will show there and result come from products
                                     */
                                    if (isset($post) && $post)
                                        $pv = $pvalue->product;
                                    else
                                        $pv = $pvalue;
                                    if ($pk == 5)
                                        $hide = "style='display:block'";
                                    ?>
                                    <li class="listing"> <div class="listing-img"><a href="javascript:void(0)"><img src="<?php echo $pv->thumimagepath; ?>" data="<?php echo $pv->id ?>" design="<?php echo $pv->designpath; ?>" alt="" class="img close-pop" title="" /></a></div>
                                        <div class="info">
                                            <h3 class="prd-title"><?php echo $pv->sku; ?>-<span class="prd-code"><?php echo $pv->colorcode; ?></span></h3>
                                            <p class="list-info"><?php echo $pv->shortdescription; ?></p>
                                            <span class="info-icon" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)"  title="Color"> <?php echo '+' . count($pv->colors); ?></a></span> <span class="catlg-icon brd-none"><a href="javascript:void(0)" title="Add to Catalogue">&nbsp;</a></span> </div>
                                    </li>
                                    <?php
                                }
                            } else {
                                ?>
                                No Product available as this time
                            <?php } ?>
                        </ul>
                        <div class="clr">&nbsp;</div>
                        <div class="pagination rightAlign strong"> Showing <span>04</span> of <span>24</span> <span class="prev-arrow"><a href="javascript:void(0)" title="Previous">&nbsp;</a></span> <span class="next-arrow"><a href="javascript:void(0)" title="Next">&nbsp;</a></span> </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end widgets --> 
    </div>

    <!-- Popup -->
    <div id="dialog" class="popup-module">

    </div>
    <!-- end Popup --> 
</section>

<!-- End Body Text --> 
<input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
<input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
