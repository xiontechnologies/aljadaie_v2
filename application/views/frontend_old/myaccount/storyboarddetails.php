<?php
/*
 * @auther Shafiq
 */
if($edit_story_board->storyboardsurface){
$temp = array();
foreach ($edit_story_board->storyboardsurface as $k => $v) {
    $temp[] = $v->surface;
}
//echo '<pre>';print_r($temp);exit;
}
?>
<section>
    <div id="content-section"> 
        <?php $this->load->view('frontend/layout/leftwidgets', $this->data); ?>

        <!-- Filter module -->
        <div id="filter-module">
            <div class="search-field">
                <?php
                echo form_open('home/search', 'id=search');
                $searchfeild = array(
                    'name' => 'searchfield',
                    'id' => 'searchfield',
                    'value' => '',
                    'placeholder' => 'Search your Fabric here...',
                    'class' => 'search-textfield',
                );
                $searchbutton = array(
                    'name' => 'searchbutton',
                    'value' => '',
                    'class' => 'search-btn',
                );
                echo form_input($searchfeild);
                echo form_submit($searchbutton);
                echo form_close();
                ?>

            </div>
            <div class="clr">&nbsp;</div>
            <!--            If no product then hide all-->
            <?php if ($product) { ?>
                <?php echo form_open('home/advsearch', 'id=advsearch'); ?>
                <!-- Advance Filters -->
                <h1>Advance <span class="blue-color">Filters:</span></h1>
                <ul class="maketabs filter-list" id="format">
                    <?php
                    $catcount = count($category);
                    if ($catcount < 7) {
                        $width = 100 / count($category);
                    } else {
                        $width = 100 / 7;
                    }
                    $width .= '%';
                    foreach ($category as $ck => $cv) {
                        $class = '';
                        if ($catcount > 7 && ($ck + 1) % 7 == 0)
                            $class = 'last';
                        else if ($catcount < 7 && $catcount == ($ck + 1)) {
                            $class = 'last';
                        }
                        ?>
                        <li class="filter-colm <?php echo $class; ?>" <?php echo 'style=width:' . $width; ?>>
                            <div class="filter-text"><h2><?php echo $cv->name; ?></h2>
                                <?php
                                $i = 0;
                                foreach ($cv->subcategory as $subk => $subv) {
                                    if ($subv->products && $i < 7) {
                                        $i++;
                                        $id = 'id=' . $subv->id;
                                        ?>
                                        <div>
                                            <?php echo form_checkbox('variation[]', $subv->id, '', $id); ?>
                                            <?php echo form_label($subv->name, $subv->id); ?>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <div class="clr">&nbsp;</div>
                <div class="search-article">
                    <?php
                    $submit = array(
                        'name' => 'submit',
                        'id' => 'submit',
                        'value' => 'Search',
                        'placeholder' => 'Search',
                        'class' => 'search-btn',
                    );
                    ?>
                    <a href="javascript:void(0)"><?php echo form_submit($submit); ?></a>
                    <div class="clr">&nbsp;</div>
                </div>
                <?php echo form_close(); ?>
                <input type="hidden" name="hidden" value="1" class="hidden"/>
            <?php } ?>
        </div>
<!--        <a href="<?php // echo base_url('frontend/newsletters/add_newsletter');     ?>">Newsletter</a>-->
        <!-- End Filter module --> 
        <article class="visualizer display-none">
            <div class="visualizer-module centerAll">
                <div class="image-article">
                    <span id="fullscreen"><img src="<?php echo base_url('assets/frontend/images/full screen.png') ?>" alt="full sreen"/></span>
                    <a href="<?php echo base_url('storyboard'); ?>" id="save-visulizer">Save</a>
                    <img id="canvaspreloader" style="display:none" src="<?php echo base_url('assets/images/loading.gif') ?>" alt="" title="" class="img" />
                    <img id="canvas" src="#" alt="" title="" class="img" /></div>
            </div>
        </article>
        <!-- Right widgets -->
        <?php $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
        <!-- end widgets --> 
        <!-- Popup -->
        <div id="dialog" class="popup-module"></div>
        <!-- end Popup --> 
    </div>
</section>
<input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
<input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
<input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>