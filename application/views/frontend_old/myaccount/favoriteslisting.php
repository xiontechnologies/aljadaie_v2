
<section>
    <div id="content-section"> 
        <?php // $this->load->view('frontend/layout/leftwidgets', $this->data); ?>
        <?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
        <?php
        $frmaction = 'frontend/' . $controller . '/delete_catalogue';
        $attributes = 'id="frmDelete"';
        echo form_open($frmaction, $attributes);
        ?>
        <div class="order-table">
            <?php
            if (isset($fav) && count($fav) > 0) {
                ?>
                <p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="default-btn" style="float:left">Delete Rows</a></p>

                <table cellpadding="6" cellspacing="1" style="width:100%" border="0" class="table-module">
                    <thead>
                        <tr>
                            <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                            <th>product Name</th>
                            <th>Photo</th>
                            <th>Price</th>      
                            <th>Stock</th>      
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($fav as $k => $items) {
                        ?>
                        <tbody>
                            <tr>
                                <td><?php echo (form_checkbox("option[]", $items->id, '', 'class="case"')); ?></td>
                                <td><?php echo $items->product->title; ?></td>
                                <td class="sorting_1"><img src="<?php echo $items->product->thumimagepath ?>" height="50" width="50"></td>
                                <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                    <!--<td class="sorting_1"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;   ?></span><?php // echo$items->product->price;   ?></td>-->
                                <?php // } else { ?>
                                    <!--<td class="sorting_1"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');   ?></span><?php // echo convertCurrency($items->product->price, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));   ?></td>-->
                                <?php // } ?>
                                <td class="sorting_1"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo$items->product->price; ?></td>
                                <td class="sorting_1"><?php echo $items->product->stock ?></td>
                                <td>
                                    <a href="<?php echo base_url() ?>favdelete/<?php echo $items->id; ?>" alt="Delete" title="Delete" class="btn-mini"><span class="delete-btn">Delete</span></a>
                                    <?php if (isInCart($this->cart->contents(), 'id', $items->product->id)) { ?>
                                        <a href="<?php echo base_url('cartlist') ?>" class="deactive">View Cart</a>
                                    <?php } else { ?>
                                        <a class="cart-icon add-to-cart-details" href="<?php echo base_url('addtocart') . '/' . $items->product->id; ?>" >Add to Cart </a>  
                                    <?php } ?>

                                </td>
                            </tr>
                        </tbody>
                    <?php } ?>
                </table>
                <?php } else {
                ?>
                <div class="cart-empty"><div>No favorite products found</div>
                    <a href="<?php echo base_url(); ?>" class="default-btn org-btn">Add products to favorites</a>
                </div>
            <?php } ?>
        </div><!--Personal Info content-->
        <div class="clr">&nbsp;</div>


        <!-- Right widgets -->
        <?php // $this->load->view('frontend/layout/rightwidgets', $this->data);   ?>
        <!-- end widgets --> 
        <!-- Popup -->
        <div id="dialog" class="popup-module"></div>
        <!-- end Popup --> 
    </div>
</section>
<input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
<input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
<input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
