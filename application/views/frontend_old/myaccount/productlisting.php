<section>
    <div id="content-section"> 
        <?php // $this->load->view('frontend/layout/leftwidgets', $this->data); ?>
        <?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
        <?php if (isset($signuperror) && !empty($signuperror)) { ?>
            <span class="error"><?php echo $signuperror; ?></span>
        <?php } ?>
        <?php if (isset($success) && !empty($success)) { ?>
            <span class="error"><?php echo $success; ?></span>
        <?php } ?>
        <?php
        $frmaction = 'frontend/' . $controller . '/delete_product_list';
        $attributes = 'id="frmDelete"';
        echo form_open($frmaction, $attributes);
        ?>
        <div class="order-table">

            <?php
            if (isset($catalogue) && count($catalogue) > 0) {
                ?>

                <p class="fl marginleft10"><a href="<?php echo base_url("cataloguelist"); ?>" class="default-btn" style="margin-right: 15px;">Back</a><a href = "javascript:void(0)" onclick="checkValid();" class="default-btn">Delete Rows</a></p>
                <table cellpadding="6" cellspacing="1" style="width:100%" border="0" class="table-module">
                    <thead>
                        <tr>
                            <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                            <th>Product Name</th>
                            <th>SKU</th>
                            <th>Colorcode</th>
                            <th>Product</th>
                            <th>Price</th>  
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($catalogue as $k => $items) {
//                        echo '<pre>';print_r($items);exit;
                        ?>
                        <tbody>
                            <tr>
                                <td><?php echo (form_checkbox("option[]", $items->product_id, '', 'class="case"')); ?></td>
                                <td><?php echo $items->title; ?></td>
                                <td><?php echo $items->sku; ?></td>
                                <td><?php echo $items->colorcode; ?></td>
                                <td><img src="<?php echo base_url(); ?>/<?php echo PRODUCTIMAGE_PATH; ?>/thumb/<?php echo $items->file_name; ?>" height="50" width="50"></td>
                                <td class="center"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $items->price; ?></td>
                                <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                                    <!--<td class="center"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;  ?></span><?php // echo $items->price;  ?></td>-->
                                <?php // } else { ?>
                                    <!--<td class="center"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');  ?></span><?php // echo convertCurrency($items->price, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));  ?></td>-->
                                <?php // } ?>

                                <td><a href="<?php echo base_url() ?>cataloguedeleteproduct/<?php echo $items->product_id; ?>" alt="Delete" title="Delete" class="btn-mini">
                                        <span class="delete-btn">Delete</span></a></td>
                            </tr>
                        </tbody>
                    <?php } ?>
                </table>
                <?php } else {
                ?>
                <div class="cart-empty"><div>No catalague found</div>
                    <a href="<?php echo base_url(); ?>" class="default-btn org-btn">Add products to catalague </a>
                </div>
            <?php }
            ?>
        </div><!--Personal Info content-->
        <div class="clr">&nbsp;</div>


        <!-- Right widgets -->
        <?php // $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
        <!-- end widgets --> 
        <!-- Popup -->
        <div id="dialog" class="popup-module"></div>
        <!-- end Popup --> 
    </div>
</section>
<input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
<input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
<input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
