<section>
    <div id="content-section"> 
        <?php // $this->load->view('frontend/layout/leftwidgets', $this->data); ?>
        <?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
        <?php
        $frmaction = 'frontend/' . $controller . '/delete_storyboard';
        $attributes = 'id="frmDelete"';
        echo form_open($frmaction, $attributes);
        ?>
        <div class="order-table">
            <?php
            if (isset($storyboard) && count($storyboard) > 0) {
                ?>
                <p class="fl marginleft10"><br /><a href = "javascript:void(0)" onclick="checkValid();" class="default-btn" style="float:left">Delete Rows</a></p>

                <table cellpadding="6" cellspacing="1" style="width:100%" border="0" class="table-module">
                    <thead>
                        <tr>
                            <th><?php echo form_checkbox("selectAll", '', '', 'onclick=toggleChecks(this);id="selectall"'); ?></th>   
                            <th>Name</th>
                            <th>Image</th>
                            <th>Create Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($storyboard as $k => $items) {
//                        echo '<pre>';print_r($items);exit;
                        ?>
                        <tbody>
                            <tr>
                                <td><?php echo (form_checkbox("option[]", $items->id, '', 'class="case"')); ?></td>
                                <td><?php echo $items->name; ?></td>
                                <td><img src = "<?php echo $items->canvasurl ?>" alt = "" style = "width:70px; height:70px; border:2px solid #25AAE1;" title = "" /></td>
                                <td><?php echo userdateformat($items->created, 'Y-m-d') ?></td>
                                <td>
                                    <a href="<?php echo base_url() ?>storyboarddelete/<?php echo $items->id; ?>" alt="Delete" title="Delete" class="btn-mini"><span class="delete-btn">Delete</span></a>
                                    <a href="<?php echo base_url() ?>storyboardpdf/<?php echo $items->id ?>" target="_blank" class="btn-mini"><span class="icons pdf-icon">Download pdf</span></a>
                                    <!--<a href="javascript:void()" class="email_pdf btn-mini" data="<?php // echo $items->id  ?>"><span class="icons email-icon">Email Pdf</span></a>-->
                                    <a class="org-btn default-btn mrg-n" href="<?php echo base_url() ?>storyboarddetails/<?php echo $items->id; ?>"><span class="delete-btn">View storyboard</span></a>
                                </td>
                            </tr>
                        </tbody>
                    <?php } ?>
                </table>
                <?php } else {
                ?>
                <div class="cart-empty"><div>No Board found</div>
                    <a href="<?php echo base_url(); ?>" class="default-btn org-btn">Create new board </a>
                </div>
            <?php } ?>
        </div><!--Personal Info content-->
        <div class="clr">&nbsp;</div>


        <!-- Right widgets -->
        <?php // $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
        <!-- end widgets --> 
        <!-- Popup -->
        <div id="dialog" class="popup-module"></div>
        <!-- end Popup --> 
    </div>
</section>
<input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
<input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
<input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>
