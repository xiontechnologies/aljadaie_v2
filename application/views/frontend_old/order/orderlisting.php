<?php // $this->load->view('frontend/layout/leftwidgets', $this->data);                         ?>
<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="order-table">
    <?php
    if ($add_error)
        echo $add_error;
    if ($add_success)
        echo $add_success;
    ?>
    <?php
    if (isset($allorders) && count($allorders) > 0) {
        ?>
        <h1>Order History</h1>
        <table cellpadding="0" cellspacing="0" style="width:100%" border="0" class="table-module">
            <thead>
                <tr>                            
                    <th>Order Number</th>
                    <th>Order Date</th>
                    <th>Order Status</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <?php foreach ($allorders as $k => $items) { ?>
                <tbody>
                    <tr>                               
                        <td><?php echo date("Ymd", strtotime($items->created_date)) . '000' . $items->id; ?></td>
                        <td><?php echo userdateformat($items->created_date); ?></td>
                        <td><?php echo $items->order_status; ?></td>
                        <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                            <!--<td><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;  ?></span><?php // echo $items->total_amt;  ?></td>-->
                        <?php // } else { ?>
                            <!--<td><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');  ?></span><?php // echo convertCurrency($items->total_amt, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));  ?></td>-->
                        <?php // } ?>
                        <td><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $items->total_amt; ?></td>
                        <td><a href="<?php echo base_url() ?>productlisting/<?php echo $items->id; ?>" title="products" class="org-btn default-btn mrg-n">
                                <span></span>View Products</a> </td>
                    </tr>
                </tbody>
            <?php } ?>
        </table>
        <?php } else {
        ?>
        <div class="cart-empty"><div>No order found</div>
            <a href="<?php echo base_url(); ?>" class="default-btn org-btn">Continue shopping </a>
        </div>
    <?php } ?>
</div><!--Personal Info content-->

