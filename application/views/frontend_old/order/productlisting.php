<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<div class="order-table">
    <a href="<?php echo base_url("home/orders"); ?>" class="default-btn">Back</a>
    <?php foreach ($allproducts as $order) { ?>
        <div class="">
            <h1 class="clearfix">Order Number : <?php echo date("Ymd", strtotime($order->created_date)) . '000' . $order->id; ?><span class="fr"> Status : <?php echo $order->order_status; ?></span></h1>

        </div>
        <div class="Billing Address clearfix" style="margin-bottom: 20px;">
            <div class="fl">
                <h4>Billing Address</h4>
                <span><?php echo $order->address; ?></span>
                <span><?php echo $order->city; ?></span>
                <span><?php echo $order->state; ?></span>
                <span><?php echo $order->country; ?></span>
            </div>
            <div class="fr">
                <h4>Shipping Address</h4>
                <span><?php echo $order->shipping_address; ?></span>      
            </div>
        </div>  
    <?php } ?>
    <?php
    if (isset($allproducts) && count($allproducts) > 0) {
        ?>               

        <table cellpadding="0" cellspacing="0" style="width:100%" border="0" class="table-module">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Order Date</th>
                    <th>sku</th>
                    <th>colorcode</th>
                    <th>collection</th> 
                    <th>price</th>                           
                    <th>Quantity</th>  
                    <th>Total</th>
                </tr>
            </thead>                    
            <?php
            $amount = 0;
            foreach ($allproducts as $order) {//                          
                foreach ($order->orderdetail as $orderdetail) {
                    ?>
                    <tr class="odd gradeX">
                        <td class="sorting_1"><?php echo $orderdetail->product->title; ?></td>
                        <td class="center"><?php echo userdateformat($order->created_date); ?></td>
                        <td class="sorting_1"><?php echo $orderdetail->product->sku; ?></td>
                        <td class="center"><?php echo $orderdetail->product->colorcode; ?></td>
                        <td class="center"><?php echo $orderdetail->product->collection; ?></td>
                        <td class="center"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $orderdetail->product->price; ?></td>
                        <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                            <!--<td class="center"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;  ?></span><?php // echo  $orderdetail->product->price;  ?></td>-->
                        <?php // } else { ?>
                            <!--<td class="center"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');  ?></span><?php // echo convertCurrency( $orderdetail->product->price, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));  ?></td>-->
                        <?php // } ?>
                        <td class="center"><?php echo $orderdetail->quantity; ?></td>
                        <?php
                        $total = ($orderdetail->quantity) * ($orderdetail->product->price);
                        $amount = $amount + $total;
//            echo $total;
                        ?>
                        <td class="center"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $total; ?></td>
                        <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                            <!--<td class="center"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;  ?></span><?php // echo $total;  ?></td>-->
                        <?php // } else { ?>
                            <!--<td class="center"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');  ?></span><?php // echo convertCurrency($total, 'SAR', $this->session->userdata('change-currency-to'));  ?></td>-->
                        <?php // } ?>

                    <?php }
                    ?>
                </tr>

                </tbody>
            </table>
        <?php }
        ?>


    </table>
    <div class="total-price fr">
        <ul class="maketabs">
            <li>Total Amount</li>
            <li><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $amount; ?></li>
            <?php // if ($this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
                <!--<li><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;  ?></span><?php // echo $amount;  ?></li>-->
            <?php // } else { ?>
                <!--<li><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');  ?></span><?php // echo convertCurrency($amount, 'SAR', $this->session->userdata('change-currency-to'));  ?></li>-->
            <?php // } ?>
    <!--<li><span class="currency-symbol">SAR</span><?php // echo $amount;    ?>                        </li>-->
        </ul>
    </div>
    <?php } else {
    ?>
    <div class="cart-empty"><div>No product found</div>
        <a href="<?php echo base_url(); ?>" class="default-btn org-btn">Continue shopping </a>
    </div>
<?php } ?>
</div><!--Personal Info content-->




