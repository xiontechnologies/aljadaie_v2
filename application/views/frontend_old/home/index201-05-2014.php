<?php
/*
 * @auther Shafiq
 */
?>
<section>
    <div id="content-section"> 
        <?php $this->load->view('frontend/layout/leftwidgets', $this->data); ?>

        <!-- Filter module -->
        <?php if ($action != 'editstoryboard') { ?>
            <div id="filter-module">
                <div class="search-field">
                    <?php
                    echo form_open('home/search', 'id=search');
                    $searchfeild = array(
                        'name' => 'searchfield',
                        'id' => 'searchfield',
                        'value' => '',
                        'placeholder' => 'Search your Fabric here...',
                        //'class' => 'search-textfield',
                        'class' => 'input-field',
                        'style' => "width:575px; margin-right:20px"
                    );
//                    $searchbutton = array(
//                        'name' => 'searchbutton',
//                        'value' => '',
//                        'class' => 'search-btn',
//                    );
                    $searchbutton = array(
                        'name' => 'searchbutton',
                        'value' => 'Search',
                        'class' => 'btn red-btn',
                    );
                    echo form_input($searchfeild);
                    echo form_submit($searchbutton);
                    echo form_close();
                    ?>

                                        <!--            Search Fabric by Item:         <input name="" type="text" value="00" class="input-field" style="width:40px" />
                                        <input name="" type="text" value="00" class="input-field" style="width:40px" />
                                        <input name="" type="text" value="000000" class="input-field" style="width:170px" />
                                        <input name="" type="text" value="00" class="input-field" style="width:40px" />
                                        <input name="" type="text" value="000" class="input-field" style="width:65px" />
                                                        <input name="Search" type="button" value="Search" class="btn red-btn" />-->
                </div>
                <div class="clr">&nbsp;</div>
                <!--            If no product then hide all-->
                <?php if ($product) { ?>
                    <?php echo form_open('home/advsearch', 'id=advsearch'); ?>
                    <!-- Advance Filters -->
                    <!--<h1>Advance <span class="blue-color">Filters:</span></h1>-->
                    <div class="advance-search-module" style="display:none"><ul class="maketabs filter-list" id="format">
                            <?php
                            $catcount = count($category);
                            $width = '100%';
                            foreach ($category as $ck => $cv) {
                                $class = '';
//                                if ($catcount > 7 && ($ck + 1) % 7 == 0)
//                                    $class = 'last';
//                                else if ($catcount < 7 && $catcount == ($ck + 1)) {
//                                    $class = 'last';
//                                }
                                ?>
                                <li class="filter-colm <?php echo $class; ?>" <?php echo 'style=width:' . $width; ?>>
                                    <div class="filter-text"><h2><?php echo $cv->name; ?></h2>
                                        <?php
                                        $i = 0;
                                        foreach ($cv->subcategory as $subk => $subv) {
                                            if ($subv->products) {
                                                $i++;
                                                $id = 'id=' . $subv->id;
                                                ?>
                                        
                                                <div class="flt-att">
                                                    <?php echo form_checkbox('variation[]', $subv->id, '', $id); ?>
                                                    <?php echo form_label($subv->name, $subv->id); ?>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="clr">&nbsp;</div>
                        <div class="search-article">
                            <?php
                            $submit = array(
                                'name' => 'submit',
                                'id' => 'submit',
                                'value' => 'Search',
                                'placeholder' => 'Search',
                                'class' => 'btn red-btn',
                            );
                            ?>
                            <a href="javascript:void(0)"><?php echo form_submit($submit); ?></a>
                            <div class="clr">&nbsp;</div>
                        </div></div>
                    <?php echo form_close(); ?>
                    <input type="hidden" name="hidden" value="1" class="hidden"/>
                <?php } ?>
                <div class="advflt-togle">Advance Filter <span class="arrow-down">&nbsp;</span></div>
            </div>
        <?php } ?>
<!--        <a href="<?php // echo base_url('frontend/newsletters/add_newsletter');                ?>">Newsletter</a>-->
        <!-- End Filter module --> 

        <!-- slider -->
        <?php
        if (isset($slider) && !empty($slider)) {
            ?>
            <div class="slider-module" id="slides">
                <?php
                foreach ($slider as $k => $v) {
                    ?>
                    <img src="<?php echo base_url('uploads/slider') . '/' . $v->photo->file_name; ?>" alt="" title="" class="img" />
                <?php } ?>
            </div>
        <?php } ?>
        <!-- end slider -->

        <!-- Steps -->
        <div class="steps-module">
            <ul class="maketabs">
                <li class="steps-li">
                    <h2>
                        <span class="red">Step1</span>
                        Choose Fabric
                    </h2>
                    <img src="<?php echo base_url(); ?>assets/images/fabric_img.png" alt="" title="" class="fbc-img" />
                </li>
                <li class="steps-arrow">&nbsp;</li>
                <!--<li class="steps-li deactive-steps">-->
                <li class="steps-li">
                    <h2>
                        <span class="red">Step2</span>
                        Use Visualizer
                    </h2>
                    <img src="<?php echo base_url(); ?>assets/images/fabric_img.png" alt="" title="" class="fbc-img" />
                </li>
                <li class="steps-arrow">&nbsp;</li>
                <li class="steps-li">
                    <h2>
                        <span class="red">Step3</span>
                        Add to Cart
                    </h2>
                    <img src="<?php echo base_url(); ?>assets/images/fabric_img.png" alt="" title="" class="fbc-img" />
                </li>
            </ul>
        </div>
        <!-- end Steps -->

        <article class="visualizer <?php if ($action != 'editstoryboard') { ?>display-none<?php } ?>">
            <div class="visualizer-module centerAll">
                <div class="image-article">
                    <span id="fullscreen"><img src="<?php echo base_url('assets/frontend/images/full screen.png') ?>" alt="full sreen"/></span>
                    <!--<a href="<?php // echo base_url('storyboard');                ?>" id="save-visulizer">Save</a>-->
                    <img id="canvaspreloader" style="display:none" src="<?php echo base_url('assets/images/loading.gif') ?>" alt="" title="" class="img" />
                    <img id="canvas" src="#" alt="" title="" class="img" /></div>
            </div>
        </article>
        <!-- Right widgets -->
        <?php $this->load->view('frontend/layout/rightwidgets', $this->data); ?>
        <!-- end widgets --> 
        <!-- Popup -->
        <div id="dialog" class="popup-module"></div>
        <!-- end Popup --> 
    </div>
</section>
<input type="hidden" name="selectedfabric" value="" class="selectedfabric"/>
<input type="hidden" name="selectedstyle" value="" class="selectedstyle"/>
<input type="hidden" name="pagintation" value="<?php echo isset($offset) ? $offset : '0' ?>" class="pagintation"/>