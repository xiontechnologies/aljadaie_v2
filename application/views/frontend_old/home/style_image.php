<ul class="maketabs listing-table" id="style-grid-view">
    <?php
    if (isset($style_images) && !empty($style_images)) {
        foreach ($style_images as $k => $image) {
//                                    echo '<pre>';print_r($image);exit;
            ?>                            
            <li class="listing"><div class="style-img"><a href="javascript:void(0)"><img src="<?php echo base_url(ADMIN_STYLE_IMAGE_PATH) ?>/<?php echo $image->photo->file_name ?>" alt="<?php echo $image->design_name ?>" class="img" data="<?php echo $image->design_name ?>" title="<?php echo $image->design_name ?>" /></a></div></li>
            <?php
        }
    }
    ?>
</ul>
<div class="pagination rightAlign strong"> Showing 
    <span><?php echo isset($style_page) ? $style_page : 0; ?></span>
    of <span><?php echo isset($style_total) ? $style_total : 0; ?></span>
    <span class="prev-arrow"><a href="javascript:void(0)" title="Previous">&nbsp;</a></span>
    <span class="next-arrow"><a href="javascript:void(0)" title="Next">&nbsp;</a></span>
</div>