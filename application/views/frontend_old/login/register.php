
<div class="register-box">
    <div class="login-form">
        <h1 class="page-header">Sign Up</h1>
        <?php
        echo form_open('register');
        $email = array(
            'name' => 'remail',
            'type' => 'email',
            'id' => 'remail',
            'placeholder' => 'Enter email',
            'value' => isset($postdata['remail']) ? $postdata['remail'] : '',
            'class' => 'form-control',
            'required' => 'required'
        );
        $password = array(
            'name' => 'rpassword',
            'id' => 'rpassword',
            'placeholder' => 'Password',
            'value' => '',
            'class' => 'form-control',
            'required' => 'required'
        );
        $cpassword = array(
            'name' => 'cpassword',
            'id' => 'cpassword',
            'placeholder' => 'Password',
            'value' => '',
            'class' => 'form-control',
            'required' => 'required'
        );
        ?>
        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> First Name', 'firstname'); ?>
            <?php
            echo form_input(
                    array(
                        'name' => 'firstname',
                        'class' => 'form-control',
                        'id' => 'firstname',
                        'placeholder' => 'First Name',
                        'value' => isset($postdata['firstname']) ? $postdata['firstname'] : (isset($user->firstname) ? $user->firstname : ''),
                        'required' => 'required'
            ));
            echo form_error('firstname');
            ?>
        </div>
        <div class="form-group"> 
            <?php echo form_label('<span style="color: red">*</span> Last Name', 'lastname'); ?>
            <?php
            echo form_input(
                    array(
                        'name' => 'lastname',
                        'class' => 'form-control',
                        'id' => 'lastname',
                        'placeholder' => 'Last Name',
                        'value' => isset($postdata['lastname']) ? $postdata['lastname'] : (isset($user->lastname) ? $user->lastname : ''),
                        'required' => 'required'
            ));
            echo form_error('lastname');
            ?>
        </div>
        <div class = "form-group">
            <?php echo form_label('<span style="color: red">*</span> Email address', 'email'); ?>
            <?php echo form_input($email); ?>
        </div>
        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> Password', 'password'); ?>
            <?php echo form_password($password); ?>
        </div>
        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> Confirm Password', 'cpassword'); ?>
            <?php echo form_password($cpassword); ?>
        </div>
        <div class="form-group">
            <?php echo form_label('Company', 'company'); ?>
            <?php
            echo form_input(array('name' => 'company',
                'id' => 'company',
                'palceholder' => 'Company', 'class' => 'form-control',
                'value' => isset($postdata['company']) ? $postdata['company'] : (isset($user) && $user->company ? ($user->userprofile->company) : ''),
            ));
            ?>
        </div>
        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> Address', 'address'); ?>
            <?php
            echo form_textarea(array('name' => 'address',
                'id' => 'address',
                'palceholder' => 'address', 'class' => 'form-control',
                'value' => isset($postdata['address']) ? $postdata['address'] : (isset($user) && $user->address ? ($user->userprofile->address) : ''),
                'required' => 'required'
            ));
            echo form_error('address');
            ?>
        </div>
        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> City', 'city'); ?>
            <?php
            echo form_input(array('name' => 'city',
                'id' => 'city',
                'palceholder' => 'city', 'class' => 'form-control',
                'value' => isset($postdata['city']) ? $postdata['city'] : (isset($user) && $user->userprofile->city ? ($user->userprofile->city) : ''),
                'required' => 'required'
            ));
            echo form_error('city');
            ?>
        </div>
        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> State/Province', 'state'); ?>
            <?php
            echo form_input(array('name' => 'state',
                'id' => 'state',
                'palceholder' => 'state', 'class' => 'form-control',
                'value' => isset($postdata['state']) ? $postdata['state'] : (isset($user) && $user->state ? ($user->userprofile->state) : ''),
                'required' => 'required'
            ));
            echo form_error('state');
            ?>
        </div>
        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> Country', 'country'); ?>
            <?php
            echo form_input(array('name' => 'country',
                'id' => 'country',
                'palceholder' => 'country', 'class' => 'form-control',
                'value' => isset($postdata['country']) ? $postdata['country'] : (isset($user) && $user->country ? ($user->userprofile->country) : ''),
                'required' => 'required'
            ));
            echo form_error('country');
            ?>
        </div>

        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> Postal/Zip Code', 'zip'); ?>
            <?php
            echo form_input(array('name' => 'zip',
                'id' => 'zip',
                'palceholder' => 'zip', 'class' => 'form-control',
                'value' => isset($postdata['zip']) ? $postdata['zip'] : (isset($user) && $user->zip ? ($user->userprofile->zip) : ''),
                'required' => 'required'
            ));
            ?>
        </div>

        <div class="form-group">
            <?php echo form_label('Fax', 'fax'); ?>
            <?php
            echo form_input(array('name' => 'fax',
                'id' => 'fax',
                'palceholder' => 'fax', 'class' => 'form-control',
                'value' => isset($postdata['fax']) ? $postdata['fax'] : (isset($user) && $user->fax ? ($user->userprofile->fax) : ''),
            ));
            echo form_error('fax');
            ?>
        </div>

        <div class="form-group">
            <?php echo form_label('<span style="color: red">*</span> Telephone', 'telephone'); ?>
            <?php
            echo form_input(array('name' => 'telephone',
                'id' => 'telephone',
                'palceholder' => 'telephone', 'class' => 'form-control',
                'value' => isset($postdata['telephone']) ? $postdata['telephone'] : (isset($user) && $user->telephone ? ($user->userprofile->telephone) : ''),
                'required' => 'required'
            ));
            echo form_error('telephone');
            ?>
        </div>
        <?php
        echo form_submit('submit', 'Register', 'class="org-btn default-btn"');
        echo form_close();
        ?>
    </div>

    <div class="login-form social">
        <p style="margin:0">Login with a Social Network  &nbsp;&nbsp;<a href="<?php echo base_url('login/facebook'); ?>" ><img title="Sign In with Facebook" alt="Sign In with Facebook" src="<?php echo base_url('assets/frontend/images/fb-log-pop.jpg') ?>"></a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url('login/google'); ?>" ><img title="Sign In with Google+" alt="Sign In with Google+" src="<?php echo base_url('assets/frontend/images/gplus-log-pop.jpg'); ?>"></a></p>
    </div>


</div>


