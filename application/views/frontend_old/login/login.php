<div class="login-box">
    <div class="login-form">
        <h1 class="page-header">Login</h1>
        <?php
        echo form_open('frontend/auth/index');
        $email = array(
            'name' => 'email',
            'id' => 'email',
            'type' => 'email',
            'placeholder' => 'Enter email',
            'value' => '',
            'class' => 'form-control',
            'required' => 'required'
        );
        $password = array(
            'name' => 'password',
            'id' => 'password',
            'placeholder' => 'Password',
            'value' => '',
            'class' => 'form-control',
            'required' => 'required'
        );
        ?>
        <div class="form-group">
            <span style="color: red">*</span> <?php echo form_label('Email address', 'email'); ?>
            <?php echo form_input($email); ?>
            <?php //echo form_error('email', '<span class="error">', '</span>'); ?>
        </div>
        <div class="form-group">
            <span style="color: red">*</span> <?php echo form_label('Password', 'password'); ?>
            <?php echo form_password($password); ?>
            <?php //echo form_error('password', '<span class="error">', '</span>'); ?>
        </div>




        <?php echo form_submit('submit', 'Submit', 'class="org-btn default-btn"'); ?>
        <a href="javascript:void(0)" id="forgotpassword" style="margin-left: 30px;">Forgot Password</a>
<!--        <a href="<?php ?>">Signup</a>-->
        <?php
        echo form_close();
        ?>

    </div>

    <div class="login-form social">
        <p style="margin:0">Login with a Social Network  &nbsp;&nbsp;<a href="<?php echo base_url('login/facebook'); ?>" ><img title="Sign In with Facebook" alt="Sign In with Facebook" src="<?php echo base_url('assets/frontend/images/fb-log-pop.jpg') ?>"></a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url('login/google'); ?>" ><img title="Sign In with Google+" alt="Sign In with Google+" src="<?php echo base_url('assets/frontend/images/gplus-log-pop.jpg'); ?>"></a></p>
    </div>

    
</div><!--forgot password-->

    <?php
    $login = array(
        'name' => 'email',
        'id' => 'email',
        'type' => 'email',
        'placeholder' => 'Enter email',
        'value' => '',
        'class' => 'form-control',
        'required' => 'required'
    );
    ?>
    <div class="forgot-pass" style="display: none;">
        <div class="order-tablular forgotpass clearfix">
            <h1 class="page-header">Forget Password</h1>
            <?php echo form_open("forgotpassword", 'id="forgotpassword"') ?> 
            <div class="form-group">
                <?php echo form_label('Email Address', 'email'); ?>
                <?php echo form_input($login); ?>
                <?php //echo form_error('password', '<span class="error">', '</span>');  ?>
            </div>
            <?php
            echo form_submit('submit', 'Submit', 'class="org-btn default-btn"');
            echo form_close();
            ?>
        </div>
    </div>