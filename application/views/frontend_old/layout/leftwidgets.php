<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="left-widget">
  <ul class="makelist widget">
    <li class="icon-btns <?php echo $this->data['action'] == 'editstoryboard' ? "" : 'display-none'  ?>"><a href="javascript:void(0)" class="bluetip" title="SELECT A STYLE" id="select-style"><span class="icons style-icon">SELECT A STYLE</span></a></li>
    <!--<li class="icon-btns"> <a href="javascript:void(0)" class="greentip" title="SHARE"><span class="icons share-icon" onclick='postToFeed(); return false;'>SHARE</span></a> <a href="javascript:void(0)" class="downloadpdf greentip" title="DOWNLOAD PDF"><span class="icons pdf-icon">DOWNLOAD PDF</span></a> <a href="javascript:void(0)" title="EMAIL" class="emailpdf greentip"  title="EMAIL"><span class="icons email-icon ">EMAIL</span></a> </li>-->
    <li class="icon-btns"> <a href="javascript:void(0)" class="greentip" title="SHARE"><span class='st_sharethis icons share-icon' displayText='ShareThis' id="button_1"></span></a>
<!--        <a href="javascript:void(0)" class="downloadpdf greentip" title="DOWNLOAD PDF"><span class="icons pdf-icon">DOWNLOAD PDF</span></a> <a href="javascript:void(0)" title="EMAIL" class="emailpdf greentip"  title="EMAIL"><span class="icons email-icon ">EMAIL</span></a> -->
    </li>
    <li class="icon-btns <?php echo $this->data['action'] == 'editstoryboard' ? "" : 'display-none'  ?>"><a id="add-storyboard" href="<?php echo base_url('storyboard'); ?>" title="ADD TO STORYBOARD" class="storyboard greentip"><span class="icons storyB-icon">ADD TO STORYBOARD</span></a>
    <a href="#" title="Zoom" id="fullscreen" class="greentip display-none"><span class="icons zoom-icon">Zoom</span></a></li>
  </ul>
  <div id="overlay" class="select-style"> <span class="box-arrow">&nbsp;</span> <span class="box-drag">&nbsp;</span>
    <h1>Select a <span class="blue-color">Style</span> <span class="ui-dialog-titlebar-close" id="close-pop">&nbsp;</span></h1>
    <div class="tab-article">
      <?php
                if (isset($style_category) && !empty($style_category)) {
                    foreach ($style_category as $k => $stylecategory) {
                        ?>
      <div class="fl"><a href="javascript:void(0)" id ="selectestyle" class="selected" data="<?php echo $stylecategory->id ?>"><?php echo $k ? '/' . $stylecategory->name : "$stylecategory->name"; ?></a></div>
      <?php
                    }
                }
                ?>
      <div class="clr">&nbsp;</div>
    </div>
    <!-- Tab Content -->
    <div class="tab-listing" id="style-grid">
      <?php $this->load->view("frontend/home/style_image", $this->data); ?>
    </div>
  </div>
</div>
</div>