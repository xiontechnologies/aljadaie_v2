<?php
/*
 * @auther Shafiq
 */
$userdata = $this->session->userdata(SITE_NAME . '_user_data');
$usergroup = User_Group::find_by_user_id($userdata['user_id']);
if (isset($usergroup) && $usergroup->group_id == 2) {
    if ($product) {
        foreach ($product as $pk => $pvalue) {
            /*
             * Post is taken when some input is checked in filter and result come from variation
             * if empty then all products will show there and result come from products
             */
            $selected = '';
            if ($pk == 5)
                $hide = "style='display:block'";
            if ($this->session->userdata(SITE_NAME . '_user_catalog') && array_key_exists($pvalue->id, $this->session->userdata(SITE_NAME . '_user_catalog'))) {
                $selected = 'selected';
            }

//            if(($pvalue->product)!=''){
//                echo '<pre>';print_r($pvalue->thumimagepath);exit;
//            }else{
//                echo '<pre>';print_r($productvariation);exit;
//            }
//            exit;
            if (isset($post) && $post) {
                $pv = $pvalue;
                if ($pv) {
//                    echo '<pre>';print_r($pv->product->photo);exit;
                    ?>
                    <li class="listing"> <div class="listing-img"><a href="javascript:void(0)"><img src="<?php echo base_url(PRODUCTIMAGE_PATH); ?>/thumb/<?php echo $pv->product->photo->file_name ?>" data="<?php echo $pv->product->id ?>" design="<?php echo base_url(PRODUCTDESIGN_PATH); ?>/<?php echo $pv->product->photo->file_name ?>" alt="<?php echo $pv->product->photo->file_name; ?>" class="img close-pop" title="" /></a></div>
                        <div class="info">
                            <h3 class="prd-title"><?php echo $pv->product->sku; ?>-<span class="prd-code"><?php echo $pv->product->colorcode; ?></span></h3>
                            <p class="list-info"><?php echo $pv->product->shortdescription; ?></p>
                            <span class="info-icon orangetip_right_center" title="PRODUCT INFORMATION" data="<?php echo $pv->product->id; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon orangetip_right_center" title="AVAILABLE COLORS" data="<?php echo $pv->product->id; ?>"><a href="javascript:void(0)"  title="Color"> <?php echo '+' . (count($pv->product->colors) - 1); ?></a></span> 
                            <?php if ($selected) { ?>
                                <span class="catlg-icon <?php echo $selected; ?> brd-none" data="<?php echo $pv->product->id; ?>"></span> 
                            <?php } else { ?>
                                <span class="catlg-icon brd-none" data="<?php echo $pv->product->id; ?>"><a href="javascript:void(0)" class="orangetip_right_center" title="Add to Catalogue">&nbsp;</a></span> 
                            <?php } ?>
                        </div>
                    </li>
                    <?php
                }
            } else if (isset($pvalue) && isset($pvalue->product)) {
                $pv = $pvalue->product;
                ?>
                <li class="listing"> <div class="listing-img"><a href="javascript:void(0)"><img src="<?php echo $pv->thumimagepath; ?>" data="<?php echo $pv->id ?>" design="<?php echo $pv->designpath; ?>" alt="<?php echo $pv->photo->file_name; ?>" class="img close-pop" title="" /></a></div>
                    <div class="info">
                        <h3 class="prd-title"><?php echo $pv->sku; ?>-<span class="prd-code"><?php echo $pv->colorcode; ?></span></h3>
                        <p class="list-info"><?php echo $pv->shortdescription; ?></p>
                        <span class="info-icon orangetip_right_center" title="PRODUCT INFORMATION" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon orangetip_right_center" title="AVAILABLE COLORS" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)"  title="Color"> <?php echo '+' . (count($pv->colors) - 1); ?></a></span> 
                        <?php if ($selected) { ?>
                            <span class="catlg-icon <?php echo $selected; ?> brd-none" data="<?php echo $pv->id; ?>"></span> 
                        <?php } else { ?>
                            <span class="catlg-icon brd-none" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" class="orangetip_right_center" title="Add to Catalogue">&nbsp;</a></span> 
                        <?php } ?>
                    </div>
                </li>
                <?php
            } else if (isset($pvalue) && isset($productvariation)) {
                $pv = $pvalue->product;
                if ($pv) {
                    ?>
                    <li class="listing"> <div class="listing-img"><a href="javascript:void(0)"><img src="<?php echo $pv->thumimagepath; ?>" data="<?php echo $pv->id ?>" design="<?php echo $pv->designpath; ?>" alt="<?php echo $pv->photo->file_name; ?>" class="img close-pop" title="" /></a></div>
                        <div class="info">
                            <h3 class="prd-title"><?php echo $pv->sku; ?>-<span class="prd-code"><?php echo $pv->colorcode; ?></span></h3>
                            <p class="list-info"><?php echo $pv->shortdescription; ?></p>
                            <span class="info-icon orangetip_right_center" title="PRODUCT INFORMATION" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon orangetip_right_center" title="AVAILABLE COLORS" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)"  title="Color"> <?php echo '+' . (count($pv->colors) - 1); ?></a></span> 
                            <?php if ($selected) { ?>
                                <span class="catlg-icon <?php echo $selected; ?> brd-none" data="<?php echo $pv->id; ?>"></span> 
                            <?php } else { ?>
                                <span class="catlg-icon brd-none" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" class="orangetip_right_center" title="Add to Catalogue">&nbsp;</a></span> 
                            <?php } ?>
                        </div>
                    </li>


                    <?php
                }
            } else {
                $pv = $pvalue;
                if ($pv) {
                    ?>
                    <li class="listing"> <div class="listing-img"><a href="javascript:void(0)"><img src="<?php echo isset($pv->product) ? $pv->product->thumimagepath : $pv->designpath; ?>" data="<?php echo isset($pv->product) ? $pv->product->id : $pv->id; ?>" design="<?php echo isset($pv->product) ? $pv->product->designpath : $pv->designpath; ?>" alt="<?php echo isset($pv->product) ? $pv->product->photo->file_name : $pv->photo->file_name; ?>" class="img close-pop" title="" /></a></div>
                        <div class="info">
                            <h3 class="prd-title"><?php echo isset($pv->product) ? $pv->product->sku : $pv->sku; ?>-<span class="prd-code"><?php echo isset($pv->product) ? $pv->product->colorcode : $pv->colorcode; ?></span></h3>
                            <p class="list-info"><?php echo $pv->shortdescription; ?></p>
                            <span class="info-icon orangetip_right_center" title="PRODUCT INFORMATION" data="<?php echo isset($pv->product) ? $pv->product->id : $pv->id; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon orangetip_right_center" title="AVAILABLE COLORS" data="<?php echo isset($pv->product) ? $pv->product->id : $pv->id; ?>"><a href="javascript:void(0)"  title="Color"> <?php echo '+' . (count(isset($pv->product) ? $pv->product->colors : $pv->colors) - 1); ?></a></span> 
                            <?php if ($selected) { ?>
                                <span class="catlg-icon <?php echo $selected; ?> brd-none" data="<?php echo isset($pv->product) ? $pv->product->id : $pv->id; ?>"></span> 
                            <?php } else { ?>
                                <span class="catlg-icon brd-none" data="<?php echo isset($pv->product) ? $pv->product->id : $pv->id; ?>"><a href="javascript:void(0)" class="orangetip_right_center" title="Add to Catalogue">&nbsp;</a></span> 
                            <?php } ?>
                        </div>
                    </li>


                    <?php
                }
            }
        }
    } else {
        if (isset($type) && $type != 'scroll') {
            echo 'Sorry no result found';
        }
    }
} else {
    if ($product) {
        foreach ($product as $pk => $pvalue) {
            /*
             * Post is taken when some input is checked in filter and result come from variation
             * if empty then all products will show there and result come from products
             */
            if (isset($post) && $post) {
                $pv = $pvalue->product;
            } else {
                $pv = $pvalue;
            }
            $selected = '';
            if ($pk == 5)
                $hide = "style='display:block'";
            if ($this->session->userdata(SITE_NAME . '_user_catalog') && array_key_exists($pv->id, $this->session->userdata(SITE_NAME . '_user_catalog'))) {
                $selected = 'selected';
            }
            if ($pv) {
                ?>
                <li class="listing"> 
                    <div class="listing-img"><a href="javascript:void(0)" data="<?php echo $pv->id ?>" class="product-image"><img src="<?php echo $pv->thumimagepath; ?>" data="<?php echo $pv->id ?>" design="<?php echo $pv->designpath; ?>" alt="<?php echo $pv->photo->file_name; ?>" class="" title="" /></a></div>
                    <div class="info">
                        <h3 class="prd-title"><?php echo $pv->sku; ?>-<span class="prd-code"><?php echo $pv->colorcode; ?></span></h3>
                        <p class="list-info"><?php echo $pv->shortdescription; ?></p>
                        <span class="info-icon orangetip_right_center" title="PRODUCT INFORMATION" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon orangetip_right_center" title="AVAILABLE COLORS" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)"  title="Color"> <?php echo '+' . (count($pv->colors) - 1); ?></a></span> 
                        <?php if ($selected) { ?>
                            <span class="catlg-icon <?php echo $selected; ?> brd-none" data="<?php echo $pv->id; ?>"></span> 
                        <?php } else { ?>
                            <span class="catlg-icon brd-none" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" class="orangetip_right_center" title="Add to Catalogue">&nbsp;</a></span> 
                        <?php } ?>
                    </div>
                </li>
                <?php
            }
        }
    } else {
        if (isset($type) && $type != 'scroll') {
            echo 'Sorry no result found';
        }
    }
}
?>
