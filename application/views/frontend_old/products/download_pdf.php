<table width="520px" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1">
    <tr>
        <td class="header">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td><img src="<?php echo base_url('assets/frontend/pdf-img/logo.png') ?>" alt="" style="height:80px" title="" /></td>
                    <td><h1 style="margin:0; padding:0"><br /><?php echo $catalogue->title; ?></h1></td>
                </tr>
            </table>
        </td>
    </tr>
    <?php foreach ($catalogue->catalogueproducts as $k => $v) { ?>
        <tr>
            <td>
                <div style="color:#666; margin:0; padding:0; font-size:21px; font-weight:bold"><?php echo $v->product->title; ?></div>
                <span style="color:#6e6e70; margin:0; padding:0">SKU: <?php echo $v->product->sku . '-' . $v->product->colorcode; ?></span>
            </td>
        </tr>
        <tr>
            <td><img src="<?php echo $v->product->imagepath; ?>" alt="" style="width:520px; height:250px; border:2px solid #25AAE1;" title="" /></td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" width="100%" cellspacing="0">
                    <tr>
                        <td style="padding-right:15px;" width="250px" valign="top">
                            <h3 style="font-size:14px;border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">TECHNICAL SPECIFICATIONS
                               
                            </h3>

                            <table cellpadding="0" width="100%" cellspacing="0">
                                <?php
                                foreach ($v->product->productvariation as $kvariation => $vvariation) {
                                    $temp[$vvariation->subcategory->category->name][] = $vvariation->subcategory->name;
                                }
                                foreach ($category as $ck => $cv) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <th><?php echo $cv->name; ?> </th><td class="sorting_1">
                                            <?php if (isset($temp[$cv->name]) && !empty($temp[$cv->name])) { ?>
                                                <?php
                                                $str = array();
                                                foreach ($temp[$cv->name] as $varik => $variv) {
                                                    $str[$variv] = $variv;
                                                }
                                                echo implode(",", $str);
                                                ?>

                                                <?php
                                            } else {
                                                ?>
                                                N-A
                                            <?php } ?>

                                        </td>
                                    </tr>    
                                <?php } ?>
                            </table>
                        </td>
                        <td style="padding:0 0 0 15px" valign="top">
                            <h3 style="font-size:14px; border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">DESCRIPTION</h3>
                            <p><?php echo $v->product->description; ?></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <h3 style="font-weight:500; position:relative; border-bottom:2px solid #25AAE1">AVAILABLE COLORS</h3>
            </td>
        </tr>
        <tr>
            <td>
                <?php if (($v->product->colors)) { ?>
                    <table cellpadding = "0" width = "100%" cellspacing = "0">
                        <tr>
                            <?php
                            foreach ($v->product->colors as $avcolk => $avcolv) {
                                ?>
                                <td><img src="<?php echo base_url(PRODUCTIMAGE_PATH . '/thumb/' . $avcolv->photo->file_name); ?>" alt="" style="width:100px; height:100px" title="" /></td>
                                    <?php
                                }
                                ?>
                        </tr>
                    </table>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
</table>
