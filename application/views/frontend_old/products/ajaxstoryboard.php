<?php
if ($this->session->userdata(SITE_NAME . '_story_board')) {
    $temp = json_decode($this->session->userdata(SITE_NAME . '_story_board'));
    $temp = array_reverse($temp);
    foreach ($temp as $k => $v) {
        
        ?>
        <li class="listing"> 
            <div class="listing-img">
                <a href="javascript:void(0)"><img src="<?php echo $v->canvasUrl; ?>"></img></a>
            </div>
            <div class="info">
                <span data="<?php echo $k; ?>" class="delete-icon-storyboard brd-none"><a title="Delete" href="javascript:void(0)">&nbsp;</a></span>
            </div>
        </li>
        <?php
    }
} else {
    
}
?>