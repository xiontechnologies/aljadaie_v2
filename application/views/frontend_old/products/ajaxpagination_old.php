<?php
/*
 * @auther Shafiq
 */

if ($product) {
    foreach ($product as $pk => $pvalue) {
        /*
         * Post is taken when some input is checked in filter and result come from variation
         * if empty then all products will show there and result come from products
         */
        if (isset($post) && $post)
            $pv = $pvalue->product;
        else
            $pv = $pvalue;
        $selected = '';
        $cartselected = '';
        if ($pk == 5)
            $hide = "style='display:block'";
        if ($this->session->userdata(SITE_NAME . '_user_catalog') && array_key_exists($pv->id, $this->session->userdata(SITE_NAME . '_user_catalog'))) {
            $selected = 'selected';
        }
        if ($this->session->userdata(SITE_NAME . '_productIds') && in_array($pv->id, $this->session->userdata(SITE_NAME . '_productIds'))) {
            $cartselected = 'selected';
        }
        ?>
        <li class="listing"> <div class="listing-img"><a href="javascript:void(0)"><img src="<?php echo $pv->thumimagepath; ?>" data="<?php echo $pv->id ?>" design="<?php //echo $pv->designpath;             ?>" alt="<?php echo $pv->photo->file_name; ?>" class="img close-pop" title="" /></a></div>
            <div class="info">
                <h3 class="prd-title"><?php echo $pv->sku; ?>-<span class="prd-code"><?php echo $pv->colorcode; ?></span></h3>
                <p class="list-info"><?php echo $pv->shortdescription; ?></p>
                <span class="info-icon orangetip_right_center" title="PRODUCT INFORMATION" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon orangetip_right_center" title="AVAILABLE COLORS" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)"  title="Color"> <?php echo '+' . count($pv->colors); ?></a></span> 
                <?php if ($selected) { ?>
                    <span class="catlg-icon <?php echo $selected; ?>" data="<?php echo $pv->id; ?>"></span> 
                <?php } else { ?>
                    <span class="catlg-icon" data="<?php echo $pv->id; ?>"><a href="javascript:void(0)" class="orangetip_right_center" title="Add to Catalogue">&nbsp;</a></span> 
                <?php } ?>
                <?php if ($cartselected) { ?>
                    <span class="cart-icon <?php echo $cartselected; ?>" data="<?php echo $pv->id; ?>"></span> 
                <?php } else { ?>
                    <span class="brd-none" title="Add to cart" data="<?php echo $pv->id ?>"><a title="Add to cart" class="orangetip_right_center cart-icon add-to-cart" href="<?php echo base_url('addtocart') . '/' . $pv->id; ?>"></a></span>
                <?php } ?>

            </div>
        </li>
        <?php
    }
} else {
    if (isset($type) && $type != 'scroll') {
        echo 'Sorry no result found';
    }
}
?>
