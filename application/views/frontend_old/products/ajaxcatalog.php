<?php
/*
 * @auther Shafiq
 */
?>
<li class="listing"> <div class="listing-img"><a href="javascript:void(0)"><img design="<?php echo $v['designpath']; ?>" src="<?php echo $v['imagethumbpath']; ?>" alt="<?php echo str_replace('.JPG','.jpg',strtoupper($v['photoname'])); ?>" class="img" title="" /></a></div>
    <div class="info">
        <span class="info-icon orangetip_right_center" title="PRODUCT INFORMATION" data="<?php echo $v['id']; ?>"><a href="javascript:void(0)" title="Info">&nbsp;</a></span> <span class="color-icon orangetip_right_center" title="AVAILABLE COLORS" data="<?php echo $v['id']; ?>"><a href="javascript:void(0)" title="Color"><?php echo '+' . $v['availablecolor']; ?></a></span> <span class="delete-icon brd-none" data="<?php echo $v['id']; ?>"><a href="javascript:void(0)" title="Delete">&nbsp;</a></span> </div>
</li>