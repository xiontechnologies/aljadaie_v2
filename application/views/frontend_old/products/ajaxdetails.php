<script>
    $(function() {
        $("#available_color_coll").tabs();
    });
</script>
<?php
echo $embed;
?>
<h1>Product <span class="blue-color">Information</span></h1>
<!-- product gallery -->
<div class="fl halfwid product-img" src="<?php echo $product->imagepath ?>" id="<?php echo $product->productphoto->photo->id != '1' ? "zoom_01" : "" ?>">
    <div>
        <img src="<?php echo $product->imagepath ?>" alt="" class="img"  title="" />
    </div>
</div>

<!-- product info -->
<div class="fr halfwid prd-info">
    <h3><?php echo $product->sku; ?> </h3>
    <h3 class="prd-price"><span class="currency-symbol"><?php echo DEFUELT_CURRENCY; ?></span><?php echo $product->price; ?></h3>
    <?php // if (!$this->session->userdata('change-currency-to') || $this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
        <!--<h3 class="prd-price"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY; ?></span><?php // echo $product->price; ?></h3>-->    
    <?php // } else if ($this->session->userdata('change-currency-to')) { ?>
        <!--<h3 class="prd-price"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to'); ?></span><?php // echo convertCurrency($product->price, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to')); ?></h3>-->    
    <?php // } ?>
    <!-- Accordian -->
    <div id="accordion">
        <h3><span>DESCRIPTION</span></h3>
        <div class="panel">
            <?php echo strip_tags($product->description); ?>
        </div>
        <h3><span>TECHNICAL SPECIFICATIONS</span></h3>
        <div class="panel">
            <p> 
            <table width="100%" class="pop-table">
                <tbody>
                    <tr class="odd gradeX">
                        <th>Title</th><td class="sorting_1"><?php echo $product->title; ?></td>
                    </tr>
                    <tr class="odd gradeX">
                        <th>Category</th><td class="sorting_1"><?php echo $product->section->name; ?></td>
                    </tr>
                    <?php
                    foreach ($product->productvariation as $k => $v) {
                        if ($v->subcategory && $v->subcategory->category)
                            $temp[$v->subcategory->category->name][] = $v->subcategory->name;
                    }
                    foreach ($category as $k => $v) {
                        ?>
                        <tr class="odd gradeX">
                            <th><?php echo $v->name; ?> </th><td class="sorting_1">
                                <?php if (isset($temp[$v->name]) && !empty($temp[$v->name])) { ?>
                                    <table>
                                        <?php
                                        foreach ($temp[$v->name] as $varik => $variv) {
                                            echo '<tr><td>' . $variv . '</td></tr>';
                                        }
                                        ?>
                                    </table>
                                    <?php
                                } else {
                                    ?>
                                    N-A
                                <?php } ?>

                            </td>
                        </tr>    
                    <?php } ?>
                </tbody>
            </table>
            </p>
        </div>
        <h3><span>WASH CARE</span></h3>
        <div class="panel">
            <p> <?php echo $product->washcare; ?> </p>
        </div>
        <h3><span>TAGS</span></h3>
        <div class="panel">
            <?php
            $prod = explode(",", $product->tags);
            foreach ($prod as $value) {
                ?>
                <?php if (isset($value) && !empty($value)) { ?><span class="tags"><?php echo $value; ?> </span><?php } ?>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="clr">&nbsp;</div>
    <ul class="makelist prd-atrb">
        <li class="stock"><a href="javascript:void(0)">Check stock</a> 
            <label  class="stcokpopup"><?php echo $product->stock; ?> </label>
        </li>
        <li>
            <?php if ($this->session->userdata(SITE_NAME . '_user_catalog') && array_key_exists($product->id, $this->session->userdata(SITE_NAME . '_user_catalog'))) { ?>
                <span class="deactive">Catalogue Product</span>
            <?php } else { ?>
                <a class="catlg-icon" href="javascript:void(0)" data="<?php echo $product->id; ?>">Add to Catalogue</a>
            <?php } ?>

        </li>

        <li>
            <?php
            /*
             * if session is available
             * then call get_frv()
             *  if is frv product then apply class else leave it
             * else do nothing 
             */
            ?>
            <?php
            $userdata = $this->session->userdata(SITE_NAME . '_user_data');

            if ($userdata) {
                if ($product->get_frv($userdata['user_id'], $product->id)) {
                    ?>
                    <a href=<?php echo base_url(); ?>favoriteslist class="deactive">View Favorite</a>  
                <?php } else { ?>
                    <a class="fav-icon" href="javascript:void(0)" data="<?php echo $product->id; ?>">Add to Favorites</a>
                    <?php
                }
            }
            ?>

        </li>
        <li>
            <?php if (isInCart($this->cart->contents(), 'id', $product->id)) { ?>
                <a href="<?php echo base_url('cartlist') ?>" class="deactive">View Cart</a>
            <?php } else { ?>
                <a class="cart-icon add-to-cart-details" data="<?php echo $product->id;?>" href="<?php echo base_url('addtocart') . '/' . $product->id; ?>" >Add to Cart </a>   
                <span id="qty-container" class="qty-container"><input type="text" id="qty" value="1"  class="form-control qty" name="qty"/>  (Quantity In Meters)</span>
            <?php } ?>
        </li>    

    </ul>
</div>
<div class="clr">&nbsp;</div>
<!-- Available colors -->
<div id="available_color_coll">
    <ul>
        <li><a href="#tabs-1"><h1>Available <span class="blue-color">Colors</span></h1></a></li>
        <li><a href="#tabs-2"><h1>Available <span class="blue-color">Collections</span></h1></a></li>
    </ul>

    <div class="fl  prd-info" id="tabs-1">
        <div class="color-gallery">
            <?php if ($product->colors) { ?>
                <ul class="maketabs" >
                    <?php foreach ($product->colors as $k => $v) { ?>
                        <li><a href="javascript:void(0)" class="avail-color" data="<?php echo $v->id; ?>"><img src="<?php echo $v->thumimagepath; ?>" class="img" alt="" title="" /></a></li>    
                    <?php } ?>
                </ul>
            <?php } else { ?><p>No Color Available For This Fabric</p><?php } ?>
        </div>
    </div>
    <div class="fr prd-info" id="tabs-2">
        <div class="color-gallery">
            <?php if ($product->collections) {
                ?>
                <ul class="maketabs" >
                    <?php foreach ($product->collections as $k => $v) { ?>
                        <li><a href="javascript:void(0)" class="avail-color" data="<?php echo $v->id; ?>"><img src="<?php echo $v->thumimagepath; ?>" class="img" alt="" title="" /></a></li>    
                    <?php } ?>
                </ul>
            <?php } else { ?><p>No Collections Available For This Fabric</p><?php } ?>
        </div>
    </div>
</div>
<div class="clr">&nbsp;</div>



