<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<?php
$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-blue',
);
$billingcompany = array(
    'name' => 'billingcompany',
    'id' => 'billingcompany',
    'class' => form_error('billingcompany') ? 'mini fl' : 'mini',
    'value' => isset($postdata) && isset($postdata->firstname) ? $postdata->firstname : '',
);

$billingaddress  = array(
    'name' => 'billingaddress',
    'id' => 'billingaddress',
    'class' => form_error('billingaddress') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$billingstate  = array(
    'name' => 'billingstate',
    'id' => 'billingstate',
    'class' => form_error('billingstate') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$billingcity  = array(
    'name' => 'billingcity',
    'id' => 'billingcity',
    'class' => form_error('billingcity') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$billingcountry  = array(
    'name' => 'billingcountry',
    'id' => 'billingcountry',
    'class' => form_error('billingcountry') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$billingpostal  = array(
    'name' => 'billingpostal',
    'id' => 'billingpostal',
    'class' => form_error('billingpostal') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$billingfax  = array(
    'name' => 'billingfax',
    'id' => 'billingfax',
    'class' => form_error('billingfax') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$billingtelephone   = array(
    'name' => 'billingtelephone',
    'id' => 'billingtelephone',
    'class' => form_error('billingtelephone') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);

$shippingcompany = array(
    'name' => 'shippingcompany',
    'id' => 'shippingcompany',
    'class' => form_error('shippingcompany') ? 'mini fl' : 'mini',
    'value' => isset($postdata) && isset($postdata->firstname) ? $postdata->firstname : '',
);

$shippingaddress  = array(
    'name' => 'shippingaddress',
    'id' => 'shippingaddress',
    'class' => form_error('shippingaddress') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingstate  = array(
    'name' => 'shippingstate',
    'id' => 'shippingstate',
    'class' => form_error('shippingstate') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingcity  = array(
    'name' => 'shippingcity',
    'id' => 'shippingcity',
    'class' => form_error('shippingcity') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingcountry  = array(
    'name' => 'shippingcountry',
    'id' => 'shippingcountry',
    'class' => form_error('shippingcountry') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingpostal  = array(
    'name' => 'shippingpostal',
    'id' => 'shippingpostal',
    'class' => form_error('shippingpostal') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingfax  = array(
    'name' => 'shippingfax',
    'id' => 'shippingfax',
    'class' => form_error('shippingfax') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingtelephone   = array(
    'name' => 'shippingtelephone',
    'id' => 'shippingtelephone',
    'class' => form_error('shippingtelephone') ? 'mini fl' : 'mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);






?>
<div class="grid_10 full-module">
    <div class="box round first fullpage">
        <?php
        echo form_open_multipart('frontend/Mycart/addaddress', 'id="adduserfrm"');
        ?>
       
        <div class="block ">
            <?php echo $add_error; ?>
            <?php echo $add_warning; ?>
            <?php echo $add_info; ?>
            <?php echo $add_success; ?>
            <span>Billing Address</span>
            <table class="form">                
               
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Company: ', 'company', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($billingcompany); ?>
                        <?php echo form_error('company', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Address: ', 'address', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($billingaddress); ?>
                        <?php echo form_error('address', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('State: ', 'state', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($billingstate); ?>
                        <?php echo form_error('state', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('City: ', 'city', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($billingcity); ?>
                        <?php echo form_error('city', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Country: ', 'country', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($billingcountry); ?>
                        <?php echo form_error('country', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Postal/Zip Code: ', 'postal', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($billingpostal); ?>
                        <?php echo form_error('postal', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Fax: ', 'fax', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($billingfax); ?>
                        <?php echo form_error('fax', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Telephone: ', 'telephone', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($billingtelephone); ?>
                        <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>             
                
                <br>
            </table>
           
        </div>
        
        
        
        Shipping address same as billing address<input type="checkbox" class="check-add" />

        <div class="block-shipping ">
            <?php echo $add_error; ?>
            <?php echo $add_warning; ?>
            <?php echo $add_info; ?>
            <?php echo $add_success; ?>
            <span>Shipping Address</span>
            <table class="form">               
               
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Company: ', 'company', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingcompany); ?>
                        <?php echo form_error('company', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Address: ', 'address', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingaddress); ?>
                        <?php echo form_error('address', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('State: ', 'state', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingstate); ?>
                        <?php echo form_error('state', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('City: ', 'city', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingcity); ?>
                        <?php echo form_error('city', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Country: ', 'country', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingcountry); ?>
                        <?php echo form_error('country', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Postal/Zip Code: ', 'postal', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingpostal); ?>
                        <?php echo form_error('postal', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Fax: ', 'fax', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingfax); ?>
                        <?php echo form_error('fax', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Telephone: ', 'telephone', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingtelephone); ?>
                        <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                
                
               
              
            </table>
           
        </div>
          <tr id="submit">
                    <td>&nbsp;</td>
                    <td valign="top">
                        <?php echo form_submit($submit); ?>
                       
                    </td>
                    <td></td>
                </tr>
         <?php echo form_close(); ?>
    </div>
</div>
<div class="clear"> </div>
