<?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
<?php
$submit = array('name' => 'submit',
    'id' => 'submit',
    'value' => 'Submit',
    'class' => 'org-btn default-btn', 'style' => 'margin:20px auto; display:block;'
);
$billingcompany = array(
    'name' => 'billing[company]',
    'id' => 'billingcompany',
    'required' => 'required ',
    'class' => form_error('billingcompany') ? 'form-control mini fl' : 'form-control mini',
    'value' => $user->fullname ? $user->fullname : '',
);
$billingaddress = array(
    'name' => 'billing[address]',
    'id' => 'billingaddress',
    'required' => 'required',
    'class' => form_error('billingaddress') ? 'form-control mini fl' : 'form-control mini',
    'value' => $user->user_profile ? $user->user_profile->address : '',
);
$billingstate = array(
    'name' => 'billing[state]',
    'id' => 'billingstate',
    'required' => 'required',
    'class' => form_error('billingstate') ? 'form-control mini fl' : 'form-control mini',
    'value' => $user->user_profile ? $user->user_profile->state : '',
);
$billingcity = array(
    'name' => 'billing[city]',
    'id' => 'billingcity',
    'required' => 'required',
    'class' => form_error('billingcity') ? 'form-control mini fl' : 'form-control mini',
    'value' => $user->user_profile ? $user->user_profile->city : '',
);
$billingcountry = array(
    'name' => 'billing[country]',
    'id' => 'billingcountry',
    'required' => 'required',
    'class' => form_error('billingcountry') ? 'form-control mini fl' : 'form-control mini',
    'value' => $user->user_profile ? $user->user_profile->country : '',
);
$billingpostal = array(
    'name' => 'billing[postal]',
    'id' => 'billingpostal',
    'required' => 'required',
    'class' => form_error('billingpostal') ? 'form-control mini fl' : 'form-control mini',
    'value' => $user->user_profile ? $user->user_profile->zip : '',
);
$billingfax = array(
    'name' => 'billing[fax]',
    'id' => 'billingfax',
    'class' => form_error('billingfax') ? 'form-control mini fl' : 'form-control mini',
    'value' => $user->user_profile ? $user->user_profile->fax : '',
);
$billingtelephone = array(
    'name' => 'billing[telephone]',
    'id' => 'billingtelephone',
    'class' => form_error('billingtelephone') ? 'form-control mini fl' : 'form-control mini',
    'value' => $user->user_profile ? $user->user_profile->telephone : '',
);

$shippingcompany = array(
    'name' => 'shipping[company]',
    'id' => 'shippingcompany',
    'required' => 'required',
    'class' => form_error('shippingcompany') ? 'form-control mini fl' : 'form-control mini',
    'value' => isset($postdata) && isset($postdata->firstname) ? $postdata->firstname : '',
);

$shippingaddress = array(
    'name' => 'shipping[address]',
    'id' => 'shippingaddress',
    'required' => 'required',
    'class' => form_error('shippingaddress') ? 'form-control mini fl' : 'form-control mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingstate = array(
    'name' => 'shipping[state]',
    'id' => 'shippingstate',
    'required' => 'required',
    'class' => form_error('shippingstate') ? 'form-control mini fl' : 'form-control mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingcity = array(
    'name' => 'shipping[city]',
    'id' => 'shippingcity',
    'required' => 'required',
    'class' => form_error('shippingcity') ? 'form-control mini fl' : 'form-control mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingcountry = array(
    'name' => 'shipping[country]',
    'id' => 'shippingcountry',
    'required' => 'required',
    'class' => form_error('shippingcountry') ? 'form-control mini fl' : 'form-control mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingpostal = array(
    'name' => 'shipping[postal]',
    'id' => 'shippingpostal',
    'required' => 'required',
    'class' => form_error('shippingpostal') ? 'form-control mini fl' : 'form-control mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingfax = array(
    'name' => 'shipping[fax]',
    'id' => 'shippingfax',
    'class' => form_error('shippingfax') ? 'form-control mini fl' : 'form-control mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
$shippingtelephone = array(
    'name' => 'shipping[telephone]',
    'id' => 'shippingtelephone',
    'class' => form_error('shippingtelephone') ? 'form-control mini fl' : 'form-control mini',
    'value' => isset($postdata->lastname) ? $postdata->lastname : "",
);
//billing-add
//fullwid
?>
<div id="content-section"> 
    <div class="order-table  clearfix">
        <?php echo form_open_multipart(base_url('cart/address'), 'id="adduserfrm"'); ?>
        <div class="login-box ">
        <h1>Billing Address</h1>
            <table class="form">                

            <tr>
                <td class="col1" valign="top"> <?php echo form_label('Name: ', 'company', array('class' => 'required')); ?></td>
                <td class="col2">  
                    <?php echo form_input($billingcompany); ?>
                    <?php echo form_error('Name', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                </td>
            </tr>
            <tr>
                <td class="col1" valign="top"> <?php echo form_label('Address: ', 'address', array('class' => 'required')); ?></td>
                <td class="col2">  
                    <?php echo form_textarea($billingaddress); ?>
                    <?php echo form_error('address', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                </td>
            </tr>
            <tr>
                <td class="col1" valign="top"> <?php echo form_label('State: ', 'state', array('class' => 'required')); ?></td>
                <td class="col2">  
                    <?php echo form_input($billingstate); ?>
                    <?php echo form_error('state', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                </td>
            </tr>
            <tr>
                <td class="col1" valign="top"> <?php echo form_label('City: ', 'city', array('class' => 'required')); ?></td>
                <td class="col2">  
                    <?php echo form_input($billingcity); ?>
                    <?php echo form_error('city', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                </td>
            </tr>
            <tr>
                <td class="col1" valign="top"> <?php echo form_label('Country: ', 'country', array('class' => 'required')); ?></td>
                <td class="col2">  
                    <?php echo form_input($billingcountry); ?>
                    <?php echo form_error('country', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                </td>
            </tr>
            <tr>
                <td class="col1" valign="top"> <?php echo form_label('Postal/Zip Code: ', 'postal', array('class' => 'required')); ?></td>
                <td class="col2">  
                    <?php echo form_input($billingpostal); ?>
                    <?php echo form_error('postal', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                </td>
            </tr>
            <tr>
                <td class="col1" valign="top"> <?php echo form_label('Fax: ', 'fax', array('class' => 'required')); ?></td>
                <td class="col2">  
                    <?php echo form_input($billingfax); ?>
                    <?php echo form_error('fax', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                </td>
            </tr>
            <tr>
                <td class="col1" valign="top"> <?php echo form_label('Telephone: ', 'telephone', array('class' => 'required')); ?></td>
                <td class="col2">  
                    <?php echo form_input($billingtelephone); ?>
                    <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                </td>
            </tr>             
            <br>
        </table>
        Shipping address same as billing address<input name="sameaddress" type="checkbox" class="check-add" style="margin: 0 15px" />
        
        </div>
        <div class="register-box">
        <div class="block-shipping">
            <h1>Shipping Address</h1>
            <table class="form">               
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Name: ', 'company', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingcompany); ?>
                        <?php echo form_error('company', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Address: ', 'address', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_textarea($shippingaddress); ?>
                        <?php echo form_error('address', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('State: ', 'state', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingstate); ?>
                        <?php echo form_error('state', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('City: ', 'city', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingcity); ?>
                        <?php echo form_error('city', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Country: ', 'country', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingcountry); ?>
                        <?php echo form_error('country', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Postal/Zip Code: ', 'postal', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingpostal); ?>
                        <?php echo form_error('postal', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Fax: ', 'fax', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingfax); ?>
                        <?php echo form_error('fax', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>
                <tr>
                    <td class="col1" valign="top"> <?php echo form_label('Telephone: ', 'telephone', array('class' => 'required')); ?></td>
                    <td class="col2">  
                        <?php echo form_input($shippingtelephone); ?>
                        <?php echo form_error('telephone', '<div id="error"><div class="error-left"></div><div class="error-inner">', '</div></div>'); ?>
                    </td>
                </tr>

            </table>
        </div>
        
        </div>
        <div class="clr"></div>
<?php echo form_submit($submit); ?>
    </div>
    <?php echo form_close(); ?>
</div>