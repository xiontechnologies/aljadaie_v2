<?php
/*
 * @auther Shafiq
 */
?>
<section>
    <div id="content-section"> 
        <?php //$this->load->view('frontend/layout/leftwidgets', $this->data); ?>
        <?php $this->load->view('frontend/layout/homewidgets', $this->data); ?>
        <!-- Filter module -->

        <!-- End Filter module --> 
        <div class="order-table">
            <?php if ($this->cart->total_items()) { ?>
                <div class="confirm order cart-btn fr">
                    <div class="confirm order">
                        <a href="<?php echo base_url('cart/order'); ?>" class="org-btn default-btn">Confirm Order</a>
                    </div>
                </div>
                <div class="clr">&nbsp;</div>
                <table cellpadding="6" cellspacing="1" style="width:100%" border="0" class="table-module">
                    <tr>
                        <th>Product Name</th>
                        <th>SKU</th>
                        <th>Colorcode</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>  
                        <th style="text-align:right">Sub-Total</th>
                    </tr>
                    <?php $i = 1; ?>
                    <?php foreach ($this->cart->contents() as $items) { ?>
                        <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
                        <tr>
                            <td><?php echo $items['name']; ?></td>
                            <td><?php echo $items['options']['sku']; ?></td>
                            <td><?php echo $items['options']['colorcode']; ?></td>
                            <td><img src="<?php echo base_url(); ?>/<?php echo PRODUCTIMAGE_PATH; ?>/thumb/<?php echo $items['options']['file_name']; ?>" height="50" width="50"></td>
                            <td style="text-align: center"><?php echo form_label($items['qty']); ?></td>
                            <td><span class="currency-symbol"></span><?php echo $this->cart->format_number($items['price']);   ?></td>
                            <td style="text-align:right"><span class="currency-symbol"></span><?php echo $this->cart->format_number($items['subtotal']);   ?></td>
                            <?php // if (!$this->session->userdata('change-currency-to') || $this->session->userdata('change-currency-to') == DEFUELT_CURRENCY) { ?>
        <!--                                <td class="sorting_1"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;  ?></span><?php // echo $this->cart->format_number($items['price']);  ?></td>
                                <td style="text-align:right"><span class="currency-symbol"><?php // echo DEFUELT_CURRENCY;  ?></span><?php // echo $this->cart->format_number($items['price']);  ?></td>-->
                            <?php // } else { ?>
        <!--                                <td class="sorting_1"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');  ?></span><?php // echo convertCurrency($this->cart->format_number($items['price']), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));  ?></td>
                                <td style="text-align:right"><span class="currency-symbol"><?php // echo $this->session->userdata('change-currency-to');  ?></span><?php // echo convertCurrency($this->cart->format_number($items['price']), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));  ?></td>-->
                            <?php // } ?>
                        </tr>
                        <?php $i++; ?>
                    <?php } ?>
                </table>
                <div class="total-price fr">
                    <ul class="maketabs">
                        <li>Total</li>
                        <li><span class="currency-symbol"></span><?php
//                if (isset($totalamount) && !empty($totalamount))
//                    echo convertCurrency($totalamount, DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));
//                else if ($this->session->userdata('change-currency-to') && $this->session->userdata('change-currency-to') != DEFUELT_CURRENCY)
//                    echo convertCurrency($this->cart->format_number($this->cart->total()), DEFUELT_CURRENCY, $this->session->userdata('change-currency-to'));
//                else
                    echo $this->cart->format_number($this->cart->total());
                    ?>
                        </li>
                    </ul>
                    <?php if ($this->session->userdata('discount')) { ?>
                        <ul class="maketabs">
                            <li>Discount</li>
                            <li><span class="currency-symbol"></span> -<?php echo $this->session->userdata('discount'); ?>
                            </li>
                        </ul>
                        <ul class="maketabs">
                            <li>Final Price</li>
                            <li><span class="currency-symbol"></span> <?php echo ($this->cart->format_number($this->cart->total()) - $this->session->userdata('discount')); ?>
                            </li>
                        </ul>
                    <?php } ?>
                </div>
                <div class="coupon">
                    <?php
                    if (!$this->session->userdata('discount')) {
                        $coupon = array(
                            'name' => 'coupon',
                            'id' => 'coupon', 'class' => 'form-control', 'style' => 'width:200px; float:left; margin-right:15px;  margin-top:10px;'
                        );
                        $style = array(
                            'style' => 'float:left; margin-right:15px;  margin-top: 16px;'
                        );
                        echo form_label('Coupon: ', 'coupon', $style);
                        echo form_input($coupon);
                        ?>
                        <a href="javascript:void(0);" class='apply-coupon org-btn default-btn' style="margin-top: 10px;">Apply Coupon</a>
                    <?php } ?>
                </div>
                <div class="clr">&nbsp;</div>
                <div class="fr cart-btn">
                    <div class="confirm order">
                        <a id ="confirm-order" href="<?php echo base_url('cart/order'); ?>" class="org-btn default-btn">Confirm Order</a>
                    </div>
                </div>
                <div class="clr">&nbsp;</div>
            </div>
            <?php echo form_close(); ?>
        <?php } else { ?>
            <div>Cart is empty</div>
            <a href="<?php echo base_url(); ?>" class="default-btn">Continue shopping </a>
        <?php } ?>
    </div>

    <!-- Right widgets -->
    <?php //$this->load->view('frontend/layout/rightwidgets', $this->data);    ?>
    <!-- end widgets --> 
    <!-- Popup -->
    <div id="dialog" class="popup-module"></div>
    <!-- end Popup --> 
</div>
</section>
