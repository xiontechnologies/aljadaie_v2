<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head><title>User Order <?php echo $site_name; ?></title></head>
    <body>
        <div style="max-width: 800px; margin: 0; padding: 30px 0;">
            <table width="100%" border="0" cellspacing="20" cellpadding="0" style="background:#08588D;">
                <tr>
                    <td style="padding:20px; background:#F3F3F3;">
                        <a href="<?php echo site_url() ?>">
                            <img src="<?php echo site_url() ?>assets/frontend/images/logo.png" alt="aljedaie" title="aljedaie" />
                        </a>
                    </td>

                </tr>
                <tr>
                    <td style="padding:20px; background:#fff;">

                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="5%"></td>
                                <td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
                                    <?php if ($status == 'process' || $status == 'delivered') { ?>
                                        <h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;"><?php echo $ordernumber ?> status has been changed.</h2>
                                    <?php } else { ?>
                                        <h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Welcome to <?php echo $site_name; ?></h2>
                                    <?php } ?>
                                    <br />
                                    <br />
                                    Dear <b><?php echo $username ?>,</b><br />
                                    <br />
                                    Thank you for shopping with us. However, the transaction has been declined.
                                    <br /><br />
                                    Your order details are below. Thank you again for shopping with us.
                                    <br /><br />
                                    <h4 style="height: 25px; display: block; background: url(http://www.prismartec.in/demo/aljedaie/assets/frontend/images/header_line.gif) left bottom repeat-x; color: #78797C; font-size: 14px;font-weight: bold; margin-bottom: 20px; padding-bottom: 5px;">
                                        <span style="float:left;">Order Number<b><?php echo $ordernumber ?></b> (placed on <b><?php echo date("d-M-Y", strtotime($created_date)); ?></b> )</span>
                                        <span style="float:right;"><span>Order Status : </span> <?php echo $status; ?><br /><br /></span>

                                    </h4>
                                    <table width="100%" cellspacing="0" border="0" cellpadding="0">
                                        <tr>
                                            <td colspan="2">
                                                <b>Username : </b> <?php echo $username; ?><br /><br />
                                                <b>User Email : </b> <?php echo $useremail; ?><br /><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <div style="float: left !important;">
                                                    <b>Billing Address</b><br>
                                                    <span><?php echo $order->address; ?></span>
                                                    <span><?php echo $order->city; ?></span>
                                                    <span><?php echo $order->state; ?></span>
                                                    <span><?php echo $order->country; ?></span>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="float: right !important;">
                                                    <b>Shipping Address</b><br>
                                                    <span><?php echo $shippingaddress; ?></span>
                                                </div>
                                            </td>

                                        </tr>
                                    </table>
                                    <br /><br />
                                    <?php if (isset($product) && count($product) > 0) { ?>
                                        <table id="product-table" cellspacing="0" cellpadding="0" border="0" style="width:100%; border: 1px solid #DDDDDD;">
                                            <thead>
                                                <tr>
                                                    <th style="background-color: #666666;color: #FFFFFF;padding: 10px 5px;text-align: left;">Product Name</th>
                                                    <th style="background-color: #666666;color: #FFFFFF;padding: 10px 5px;text-align: left;">Fabric</th>
                                                    <th style="background-color: #666666;color: #FFFFFF;padding: 10px 5px;text-align: left;">sku</th>
                                                    <th style="background-color: #666666;color: #FFFFFF;padding: 10px 5px;text-align: left;">colorcode</th>
                                                    <th style="background-color: #666666;color: #FFFFFF;padding: 10px 5px;text-align: left;">price</th>                           
                                                    <th style="background-color: #666666;color: #FFFFFF;padding: 10px 5px;text-align: left;">Quantity</th>  
                                                    <th style="background-color: #666666;color: #FFFFFF;padding: 10px 5px;text-align: left;">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $amount = 0;
                                                foreach ($product as $orderdetail) {
                                                    ?>
                                                    <tr>
                                                        <td style="border-bottom: 1px solid #DDDDDD; padding: 5px;"><?php echo $orderdetail->product->title; ?></td>
                                                        <td style="border-bottom: 1px solid #DDDDDD; padding: 5px;"><img src="<?php echo $orderdetail->product->thumimagepath ?>" height="50" width="50"></td>
                                                        <td style="border-bottom: 1px solid #DDDDDD; padding: 5px;"><?php echo $orderdetail->product->sku; ?></td>
                                                        <td style="border-bottom: 1px solid #DDDDDD; padding: 5px;"><?php echo $orderdetail->product->colorcode; ?></td>
                                                        <td style="border-bottom: 1px solid #DDDDDD; padding: 5px;"><span class="currency-symbol">SAR</span> <?php echo $orderdetail->product->price; ?></td>
                                                        <td style="border-bottom: 1px solid #DDDDDD; padding: 5px;"><?php echo $orderdetail->quantity; ?></td>
                                                        <td style="border-bottom: 1px solid #DDDDDD; padding: 5px;"><span class="currency-symbol">SAR</span> <?php
                                            $total = ($orderdetail->quantity) * ($orderdetail->product->price);
                                            $amount = $amount + $total;
                                            echo $total;
                                        }
                                                ?></td>

                                                </tr>

                                            </tbody>
                                        </table>
                                        <div style="border: 1px solid #DDDDDD;margin-bottom: 15px;margin-top: 10px; float: right !important;">
                                            <ul style="background: none repeat scroll 0 0 rgba(0, 0, 0, 0);border: 0 none;margin: 0;outline: 0 none;padding: 0;">
                                                <li style=" background-color: #FFFFFF;color: #792437;font-size: 21px;font-weight: bold;padding: 15px; display: inline;float: left;">Total Amount</li>
                                                <li style=" background-color: #FFFFFF;color: #792437;font-size: 21px;font-weight: bold;padding: 15px; display: inline;float: left;"><span style="display: inline-block;font-size: 11px;padding-right: 5px;">SAR</span> <?php echo $amount; ?>  </li>                      </li>
                                            </ul>
                                        </div>
                                        <?php
                                    } else {
                                        echo "There is no data to display";
                                    }
                                    ?>
                                    <br />
                                    <div style="clear:both;">
                                        If you have any question or encounter any problems logging in, Please contact a site administrator.</div>
                                    <br />
                                    <br />
                                    <?php echo $this->lang->line('have_fun');
                                    ?>
                                    <br /> 
                                    <strong style="color:#08588D">The <?php echo $site_name; ?> Team</strong>
                                </td>
                            </tr>
                        </table>

                    </td>

                </tr>
            </table>
        </div>
    </body>
</html>
