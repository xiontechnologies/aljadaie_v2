This email confirms that your password has been changed.
<br />
To log on to the site,use the following credential.
<br />
<b>Username:</b><?php echo $username; ?>
<b>Password:</b><?php echo $password; ?>
<br />
If you have any question or encounter any problems logging in, Please contact a site administrator.
<br />
<br />

Thank you,
The <?php echo $site_name; ?> Team