<?php

/*
 * @auther Shafiq
 */

Class auth extends MY_base {

    function __construct() {
        parent::__construct();
//        array_push($this->scripts['css'], "frontend/css/validation.engine.css");
//        array_push($this->scripts['js'], "frontend/js/jquery.validationEngine-en.js", "frontend/js/jquery.validationEngine.js");
//        $validation = "$(document).ready(function(){
//                        $('#forgotpassword').validationEngine();
//                        })";
//        array_push($this->scripts['embed'], $validation);

        $this->right_menu();
    }

    function signup() {
        $view = "login/index";
        $this->metas['title'] = array("Login");
        if ($this->session->userdata(SITE_NAME . '_user_data')) {
            redirect('home');
        }
        $this->display_view('frontend', $view);
//        echo'<pre>';print_r($this->display_view('frontend', $view));exit;
    }

    function index() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
            if ($this->form_validation->run()) {
                /*
                 * Check for user email in database
                 */
                $conn = User::connection();
                $conn->transaction();
                try {
                    $user = User::find('first', array('conditions' => array('email = ?', $this->form_validation->set_value('email'))));
                    if ($user && $this->validate_password($user, $this->form_validation->set_value('password'))) {
                        if (!$user->activated) {
                            $this->data['loginerror'] = "Account is not activated";
                        } else if ($user->banned) {
                            $this->data['loginerror'] = "Account is banned";
                        } else {
//                        $lasturl = $this->session->userdata('lasturl');
                            $this->session->set_userdata(SITE_NAME . '_user_data', array(
                                'user_id' => $user->id,
                                'name' => $user->fullname,
                                'status' => $user->activated,
                                'firstname' => $user->firstname,
                            ));
                            redirect($_SERVER['HTTP_REFERER']);
//                            if ($this->session->userdata(SITE_NAME . '_cart_redirect')) {
//                                $this->session->unset_userdata(SITE_NAME . '_cart_redirect');
//                                redirect('checkout');
//                            } else {
//                                redirect('frontend/home');
//                            }
                        }
                    } else {
                        $this->_show_message("Invalid Login", "error");
                        redirect("login");
                        $this->data['loginerror'] = "Invalide login";
                    }
                } catch (Exception $e) {
//                    echo '<pre>';
//                    print_r($e->getMessage());
//                    exit;
                }
            } else {
                $this->data['postdata'] = $this->input->post();
            }
        }
        array_push($this->scripts['css'], "frontend/css/bootstrap.css", "frontend/css/bootstrap-theme.min.css", "frontend/css/style.css");
        $view = "login/login";
//        $this->load->view($view, $this->data);
        $this->display_view("frontend", $view, $this->data);
    }

    function register() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('remail', 'Email', 'trim|required|trim|xss_clean|strip_tags|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('rpassword', 'Password', 'trim|required|trim|xss_clean|strip_tags|min_length[6]');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|trim|xss_clean|strip_tags|matches[rpassword]');
            $this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('state', 'State', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('country', 'Country', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('zip', 'Zip', 'trim|required|trim|xss_clean|strip_tags|integer');
            $this->form_validation->set_rules('fax', 'Fax', 'trim|trim|xss_clean|strip_tags|integer');
            $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required|trim|xss_clean|strip_tags|integer');
            if ($this->form_validation->run()) {
                $userdata['firstname'] = $this->form_validation->set_value('firstname');
                $userdata['lastname'] = $this->form_validation->set_value('lastname');
                $userdata['email'] = $this->form_validation->set_value('remail');
                $userdata['password'] = $this->form_validation->set_value('rpassword');
                $userdata['address'] = $this->form_validation->set_value('address');
                $userdata['city'] = $this->form_validation->set_value('city');
                $userdata['state'] = $this->form_validation->set_value('state');
                $userdata['country'] = $this->form_validation->set_value('country');
                $userdata['zip'] = $this->form_validation->set_value('zip');
                $userdata['telephone'] = $this->form_validation->set_value('telephone');
                if ($this->input->post('company'))
                    $userdata['company'] = $this->input->post('company');
                if ($this->input->post('fax'))
                    $userdata['fax'] = $this->input->post('fax');
                if (!is_null($data = $this->tank_auth->create_user($userdata))) {         // success
                    $data['site_name'] = SITE_NAME;
//                    $this->load->library('parser');
                    $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;
//                    $this->parser->parse('email/activate-html', $data);
                    $this->_send_email("activate", $data['email'], $data);
                    unset($data['password']); // Clear password (just for any case)
                    $this->data['success'] = $this->lang->line('auth_message_registration_completed_1');
//                    $this->_show_message($this->lang->line('auth_message_registration_completed_1'), 'info');
                } else {
                    $formerror = '';
                    $errors = $this->tank_auth->get_error_message();
                    foreach ($errors as $k => $v) {
                        $formerror .= $this->lang->line($v);
                    }
                    $this->data['signuperror'] = $formerror;
                    //$this->_show_message($this->lang->line("change_error_and_continue") . '</br>' . $formerror, 'error');
                }
            } else {
                $formerror = '';
                $errors = $this->form_validation->get_field_data();
                foreach ($errors as $k => $v) {
                    $formerror .= $v['error'];
                }
                $this->data['signuperror'] = $formerror;
                $this->data['postdata'] = $this->input->post();
            }
        }
        array_push($this->scripts['css'], "frontend/css/bootstrap.css", "frontend/css/bootstrap-theme.min.css", "frontend/css/style.css");
//        $view = "frontend/login/register";
        $view = "login/index";
//        $this->load->view($view, $this->data);
        $this->display_view("frontend", $view, $this->data);
    }

    function activate() {
        $user_id = $this->uri->segment(2);
        $new_email_key = $this->uri->segment(3);
        // Activate user
        if ($this->tank_auth->activate_user($user_id, $new_email_key)) {  // success
            $user = User::find('first', array('conditions' => array('id = ?', $user_id)));
            $this->session->set_userdata(SITE_NAME . '_user_data', array(
                'user_id' => $user->id,
                'name' => $user->fullname,
                'status' => $user->activated,
                'firstname' => $user->firstname,
            ));
            redirect('myaccount');
        } else {                // fail
            $this->_show_message($this->lang->line('activation_code_incorrect_expire'), 'warning');
            redirect('login');
        }
    }

    public function oauth2($providername) {
        if (!in_array($providername, array('facebook', 'linkedin', 'google', 'twitter'))) {
            show_404();
        }
        $provider = $this->config->item($providername);
        $key = $provider['key'];
        $secret = $provider['secret'];
        $this->load->helper('url_helper');
        $this->load->spark('oauth2');
        $provider = $this->oauth2->provider($providername, array(
            'id' => $key,
            'secret' => $secret,
        ));

        if (!$this->input->get('code')) {
// By sending no options it'll come back here
            $provider->authorize();
        } else {
// Howzit?
            try {
                $token = $provider->access($_GET['code']);
                $user = $provider->get_user_info($token);
                $this->saveData($providername, $token, $user);
            } catch (OAuth2_Exception $e) {
                show_error('That didnt work: ' . $e);
            }
        }
    }

    private function saveData($providername, $token, $socialUser) {
        $usertoken = isset($token->access_token) ? $token->access_token : '';
        $usersecret = isset($token->secret) ? $token->secret : '';
        $uid = $socialUser['uid'];
        /*
         * Case one : 
         *              If user email is already present in both tables (social table and user table)
         *              then its already register user ask for login only
         * 
         * Case two :
         *              If user email is present in one table (user table and not in social table )
         *              its means its register normaly. So ask for password change and after that asscoaite
         *              function will write to get info of user        
         * Case third :
         *              User email is not present in both tables (user table and social table)
         *              its mean new user for registration 
         */

        /**
         * Linkedin return only these value to us which is in our database
         * firstname, lastname,email
         */
        /**
         * Case one
         */
        $oldUser = Socialnetwork::find(array('conditions' => array('provider = ? and uid = ?', $providername, $uid), 'include' => array('user')));
        $userOldObj = User::find(array('conditions' => array('email = ?', $socialUser['email'])));
        if ($oldUser) {
            /**
             * User already register
             * update its expires and send  
             * to login
             */
            $user = $oldUser->user;
        } else if ($userOldObj) {
            /**
             * case two
             * 
             */
            $this->_show_message($this->lang->line('user_already_register_with_email'), 'error');
            redirect('');
        } else {
            /**
             * Case third
             * 
             */
            $userObj = array(
                'firstname' => isset($socialUser['first_name']) ? $socialUser['first_name'] : '',
                'lastname' => isset($socialUser['last_name']) ? $socialUser['last_name'] : '',
                'email' => isset($socialUser['email']) ? $socialUser['email'] : '',
                'activated' => 1,
                'banned' => 0,
                'created' => date('Y-m-d h:i:s'),
                'modified' => date('Y-m-d h:i:s'),
            );
            $conn = User::connection();
            $conn->transaction();
            try {
                $user = User::create($userObj);
                $socialNetwork = array(
                    'user_id' => isset($user->id) ? $user->id : '',
                    'uid' => $socialUser['uid'],
                    'provider' => isset($providername) ? $providername : '',
                    'access_token' => $usertoken,
                    'secret' => $usersecret,
                    'expires' => isset($token->expires) ? $token->expires : '',
                    'email' => $user->email,
                    'location' => isset($socialUser['location']) ? $socialUser['location'] : '',
                    'email' => isset($socialUser['email']) ? $socialUser['email'] : '',
                    'created' => date('Y-m-d h:i:s'),
                    'modified' => date('Y-m-d h:i:s'),
                );
                $socialNetworkObj = Socialnetwork::create($socialNetwork);
                /**
                 * If user image is not available then default no image 
                 */
                if (isset($socialUser['image']) && !empty($socialUser['image'])) {
                    /*                     * save in photo folder* */
                    $photos = $this->save_profile_photo($socialUser['image'], $user);
                    if (!empty($photos) && count($photos) == 2) {
                        $imagename = explode('/', $photos['path'][0]);
                        $extarray = explode('/', $photos['size']['mime']);
                        $photoarray = array(
                            'file_title' => $imagename[4],
                            'file_name' => $imagename[4],
                            'full_path' => base_url($photos['path'][0]),
                            'file_path' => $photos['path'][0],
                            'file_type' => $extarray[1],
                            'raw_name' => $imagename[4],
                            'orig_name' => $imagename[4],
                            'client_name' => $imagename[4],
                            'file_ext' => $extarray[1],
                            'file_size' => $photos['size'][0] * $photos['size'][1] * $photos['size']["bits"],
                            'is_image' => 1,
                            'image_width' => $photos['size'][0],
                            'image_height' => $photos['size'][1],
                            'image_type' => $photos['size']['mime'],
                            'image_size_str' => $photos['size'][3],
                            'status' => 1,
                        );
                        $profilephoto = Photo::create($photoarray);
                        $photo_id = $profilephoto->id;
                    } else {
                        $photo_id = 1;
                    }
                } else {
                    $photo_id = 1;
                }
                $userProfile = array(
                    'user_id' => isset($user->id) ? $user->id : '',
                    'photo_id' => $photo_id,
                    'gender' => isset($socialUser['gender']) ? $socialUser['gender'] : '',
                    'aboutme' => isset($socialUser['description']) ? $socialUser['description'] : '',
                    'telephone' => isset($socialUser['telephone']) ? $socialUser['telephone'] : '',
                    'mobile' => isset($socialUser['mobile']) ? $socialUser['mobile'] : '',
                );
                $userprofile = Userprofile::create($userProfile);
                $dateofbirth = isset($socialUser['birthday']) ? databasedate($socialUser['birthday']) : '';
                if ($dateofbirth) {
                    $userprofile->dateofbirth = $dateofbirth;
                    $userprofile->save();
                }
                $conn->commit();
            } catch (Exception $e) {
                $conn->rollback();
                $this->write_logs($e);
                $this->_show_message('some_thing_wrong', 'error');
                redirect('login');
            }
        }
        $this->login($user);
    }

    /*
     * Used for social networks logins
     */

    public function login($user) {
        if (!$user)
            show_404();
        if ($user->banned) {
            $this->_show_message($this->lang->line('you_are_banned'), 'info');
            redirect('login');
        } else if (!$user->activated) {
            $this->_show_message($this->lang->line('your_profile_is_deactive'), 'info');
            redirect('login');
        } else {
//            if ($user->profile_complete) {
//                $this->session->set_userdata('user_data', array(
//                    'user_id' => $user->id,
//                    'name' => $user->firstname . ' ' . $user->lastname,
//                    'status' => $user->activated,
//                ));
//                redirect('home');
//            } else {
//                $this->session->set_userdata('userdata_onetime', $user->id);
//                $this->_show_message($this->lang->line('one_more_step_to_complete'), 'info');
//                redirect('myaccount');
//            }
            if ($user->firstname && $user->lastname) {
                $name = $user->firstname . ' ' . $user->lastname;
            } else {
                $name = $user->email;
            }
            $lasturl = $this->session->userdata('lasturl');
            $this->session->set_userdata(SITE_NAME . '_user_data', array(
                'user_id' => $user->id,
                'name' => $user->fullname,
                'status' => $user->activated,
                'firstname' => $user->firstname,
            ));
            if ($lasturl)
                redirect($lasturl);
            else if ($this->session->userdata(SITE_NAME . '_cart_redirect')) {
                $this->session->unset_userdata(SITE_NAME . '_cart_redirect');
                redirect('checkout');
            }
            redirect('myaccount');
        }
    }

    function save_profile_photo($link, $user) {
        if ($link) {
            $img = file_get_contents($link);
            $imagename = "/" . $user->firstname . '-' . date('y-m-d-H-i-s') . '.jpg';
            $file = base_url(FRONTEND_USER_PROFILE_IMAGE) . '/' . $imagename;
            file_put_contents($file, $img);

//            $imagename = "/" . $user->firstname . '-' . date('y-m-d-H-i-s') . '.jpg';
//            copy($link, './uploads/frontend/profileimages/' . $imagename);
            //$images = glob('./uploads/frontend/profileimages' . $imagename);
            $imgsize = getimagesize(FRONTEND_USER_PROFILE_IMAGE . '/' . $imagename);
            $photo = array('size' => $imgsize, 'path' => $images);
            if ($photo['size'] != '' && $photo['path'] != '')
                return $photo;
            else
                return false;
        }
    }

    function validate_password($user, $password) {
        $this->load->library('tank_auth');
        if ($this->tank_auth->match_password($password, $user->password)) {
            return true;
        } else {
            return false;
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('');
    }

    function forgotpassword() {
        if ($this->tank_auth->is_logged_in()) {         // logged in
            redirect('');
        } else {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

            $data['errors'] = array();
            if ($this->form_validation->run()) {        // validation ok
                if (!is_null($data = $this->tank_auth->forgot_password(
                                $this->form_validation->set_value('email')))) {

                    $data['site_name'] = $this->config->item('website_name', 'tank_auth');
                    // Send email with password activation link
                    $this->_send_email('forgot_password', $data['email'], $data);

                    $this->_show_message($this->lang->line('auth_message_new_password_sent'));
                } else {

                    $this->_show_message($this->lang->line("auth_incorrect_email_or_username") . '</br>', 'error');
                    redirect('/');
                }
            } else {
                $this->data['postdata'] = $this->input->post();
                $error = $this->form_validation->get_field_data();
                if ($error) {
                    $this->_show_message($error, "error");
                    redirect();
                }
            }
            $this->_show_message($this->lang->line("auth_message_new_email_sent") . '</br>', 'success');
            redirect('login');
        }
    }

    public function reset_password() {
        $this->load->library('tank_auth');
        $user_id = $this->uri->segment(4);
        $new_pass_key = $this->uri->segment(5);

        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

        $data['errors'] = array();

        if ($this->form_validation->run()) {        // validation ok
            $new_pass = User::find_by_id($user_id);
            $new_pass->password = $this->tank_auth->genrate_pass($this->form_validation->set_value('new_password'));
            $new_pass->new_password_key = "";
            $new_pass->save();
            $data['site_name'] = $this->config->item('website_name', 'tank_auth');
            $data['email'] = $new_pass->email;
            // Send email with new password
            $this->_send_email('reset_password', $data['email'], $data);

            $this->_show_message($this->lang->line('auth_message_new_password_activated') . '<br>', 'success');
            redirect('/');
        } else {
            // Try to activate user by password key (if not activated yet)
            if ($this->config->item('email_activation', 'tank_auth')) {
                $this->tank_auth->activate_user($user_id, $new_pass_key, FALSE);
            }

            if (!$this->tank_auth->can_reset_password($user_id, $new_pass_key)) {
                $this->_show_message($this->lang->line('auth_message_new_password_failed' . '<br>', 'error'));
            }
        }
        $view = "login/reset_password";
        $this->display_view("frontend", $view);
    }

//    function check_password($str) {
//        $id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
//        $pass = User::find_by_id($id);
//        $password = $pass->password;
//        if ($str == $password) {
//            return TRUE;
//        } else {
//            $this->form_validation->set_message("check_password", "Please Enter right password");
//            return FALSE;
//        }
//    }
}
