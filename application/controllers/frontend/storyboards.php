<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Storyboards extends My_front {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $conn = Storyboard::connection();
        $conn->transaction();
        try {
            if ($this->input->get('fabrics') && $this->input->get('url') && $this->input->get('style')) {
                $array = array(
                    'fabrics' => $this->input->get('fabrics'),
                    'style' => $this->input->get('style'),
                    'canvasUrl' => $this->input->get('url'),
                );
                /*
                 * Now save in storyboard session
                 */
                
                if (json_decode($this->session->userdata(SITE_NAME . '_story_board'))) {
                    $temp = json_decode($this->session->userdata(SITE_NAME . '_story_board'),true);
                    $temp2[] = $array;
                    $check = 0;
                    foreach ($temp as $key => $value) {
                    	if($value['canvasUrl'] == $array['canvasUrl']){
                    		$check = 1;
                    		break;
                    	}
                    }
                    if($check != 1){
                    	$temp = array_merge($temp, $temp2);
                    }else{
                    	print_r(json_encode(array('error' => 'Already in storyboard'))); die;
                    }
                    
                    $this->session->set_userdata(SITE_NAME . '_story_board', json_encode($temp));
                } else {
                    $temp[] = $array;
                    $this->session->set_userdata(SITE_NAME . '_story_board', json_encode($temp));
                }
                /*
                 * Check stroyboard is already save in database or not
                 */
                if ($this->session->userdata(SITE_NAME . '_save_user_storyboard')) {
                    $saveStoryBoard = $this->session->userdata(SITE_NAME . '_save_user_storyboard');
                    $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
                    $storyboard = new Storyboard();
                    $storyboard->user_id = $user_id;
                    $storyboard->name = $saveStoryBoard['name'];
                    $storyboard->stylename = $array['style'];
                    $storyboard->canvasurl = $array['canvasUrl'];
                    $storyboard->created = date("Y-m-d");
                    $storyboard->updated = date("Y-m-d");
                    $storyboard->save();

                    foreach ($array['fabrics'] as $key => $val) {
                        $storyboard_surface = new Storyboardsurface();
                        $storyboard_surface->story_id = $saveStoryBoard['id'];
                        $storyboard_surface->product_id = $val;
                        $storyboard_surface->surface = $key;
                        $storyboard_surface->created = date("Y-m-d");
                        $storyboard_surface->updated = date("Y-m-d");
                        $storyboard_surface->save();
                    }
                }
                $conn->commit();
                $data['view'] = $this->load->view('frontend/products/ajaxstoryboard', $array, true);
                $data['success'] = true;
                $this->output->set_output(json_encode($data));
            } else {
                print_r(json_encode(array('error' => 'unable to save data')));
            }
        } catch (Exception $e) {
            $this->write_logs($e);
        }
    }

    function delete() {
        $temp = json_decode($this->session->userdata(SITE_NAME . '_story_board'),TRUE);

        $temp = array_reverse($temp);
        /*
         * check is save in database or not 
         * and delete from database
         */
        if ($this->session->userdata(SITE_NAME . '_save_user_storyboard')) {
            $id = $temp[$this->input->get('id')]->database_id;
            $stroyboard = Storyboard::find($id);
            $stroyboard->delete();
        }
        unset($temp[$this->input->get('id')]);
        if ($temp) {
            $this->session->set_userdata(SITE_NAME . '_story_board', json_encode($temp));
        } else {
            $this->session->unset_userdata(SITE_NAME . '_story_board');
            $this->session->unset_userdata(SITE_NAME . '_save_user_storyboard');
        }
        echo 1;
    }

}
