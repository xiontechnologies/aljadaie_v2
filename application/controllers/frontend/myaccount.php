<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Myaccount extends My_front {

    function __construct() {
        parent::__construct();
//        $this->_api_require_user();
        $this->right_menu();
    }

    function index($user_id = "") {
        if ($this->session->userdata(SITE_NAME . '_user_data')) {
            $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
            array_push($this->scripts['js'], "js/datepicker_new.js");
            array_push($this->scripts['css'], "css/datePicker.css");
            $datejs = '$(document).ready(function(){
    $(function() {
        $( "#dob" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        });});';
            array_push($this->scripts['embed'], $datejs);

            $this->data['user'] = User::find_by_id($user_id, array('include', 'userprofile'));
            $this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('city', 'city', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('state', 'State', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('country', 'Country', 'trim|required|trim|xss_clean|strip_tags');
            $this->form_validation->set_rules('zip', 'Zip', 'trim|required|trim|xss_clean|strip_tags|integer');
            $this->form_validation->set_rules('fax', 'Fax', 'trim|trim|xss_clean|strip_tags|integer');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|trim|xss_clean|strip_tags|integer|min_length[10]');
            $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required|trim|xss_clean|strip_tags|integer');

            if ($this->form_validation->run()) {
                $conn = User::connection();
                $conn->transaction();
                try {
                    $user = User::find_by_id($user_id);
                    $user->firstname = $this->input->post('firstname');
                    $user->lastname = $this->input->post('lastname');
                    $user->modified = date("Y-m-d");
                    $user->save();
                    $userprofile = Userprofile::find_by_user_id($user_id);

                    if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['name'])) {
                        $config['upload_path'] = PROFILEIMAGE;
                        $config['max_size'] = '10000';
                        $config['max_height'] = '768';
                        $config['max_width'] = '1024';
                        $config['allowed_types'] = 'jpg|jpeg|png|bmp|gif';
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload()) {
                            $error = $this->upload->display_errors();
                            $this->_show_message($error, "error");
//                            $this->data['error'] = $error;
//                        $this->data['error'] = $error;
//                           echo '<pre>';print_r($this->upload->display_errors());exit;
                            redirect('myaccount');
                        } else {
                            $editdata = $this->upload->data();
                            unset($editdata['is_image']);
                            $photo_id = $userprofile->photo_id;
                            if (isset($photo_id) && $photo_id != '') {
                                $photo = Photo::find($photo_id);
                                if (isset($photo->file_name) && !empty($photo->file_name)) {
                                    $this->load->helper('file');
                                    $path = PROFILEIMAGE . $photo->file_name;
                                    @unlink($path);
                                }
                                $update_photo = Photo::find($photo_id);
                                $update_photo->update_attributes($editdata);
                                $update_photo_id = $update_photo->id;
                            } else {
                                $photo = new Photo($editdata);
                                $photo->save();
                                $update_photo_id = $photo->id;
                                $userprofile->photo_id = $update_photo_id;
                            }
                            $this->data['postdata'] = $this->input->post();
                        }
                    }
//                        $userprofile->user_id = $user->id;

                    $userprofile->dateofbirth = $this->input->post('dob');
                    $userprofile->gender = $this->input->post('gender');
                    $userprofile->telephone = $this->input->post('telephone');
                    if ($this->input->post('mobile'))
                        $userprofile->mobile = $this->input->post('mobile');
                    if ($this->input->post('aboutme'))
                        $userprofile->aboutme = $this->input->post('aboutme');
                    if ($this->input->post('address'))
                        $userprofile->address = $this->input->post('address');
                    if ($this->input->post('country'))
                        $userprofile->country = $this->input->post('country');
                    if ($this->input->post('state'))
                        $userprofile->state = $this->input->post('state');
                    if ($this->input->post('city'))
                        $userprofile->city = $this->input->post('city');
                    if ($this->input->post('zip'))
                        $userprofile->zip = $this->input->post('zip');
                    if ($this->input->post('fax'))
                        $userprofile->fax = $this->input->post('fax');
                    if ($this->input->post('company'))
                        $userprofile->company = $this->input->post('company');
                    $userprofile->modified = date("Y-m-d");
                    $userprofile->save();
                    $conn->commit();
                    $data['username'] = $user->fullname;
                    $email = $user->email;
                    $this->_send_email("profile_update", $email, $data);
                    $this->_show_message("Updated successfully...", 'success');
                } catch (Exception $e) {
                    //  print_r($e->getMessage()); exit;
                    $this->write_logs($e);
                    $conn->rollback();
                }
            } else {
                $formerror = '';
                $errors = $this->form_validation->get_field_data();
                //   print_r($this->form_validation->get_field_data()); 
                foreach ($errors as $k => $v) {
                    $formerror .= $v['error'];
                }
//                $this->_show_message($formerror, 'error');
                $this->data['error'] = $formerror;
                $this->data['postdata'] = $this->input->post();
            }

            $view = "myaccount/index";
            $this->display_view("frontend", $view, $this->data);
        } else {
            redirect('login');
        }
    }

    function cataloguelist() {
        $user_id = "";
        if ($this->session->userdata(SITE_NAME . '_user_data'))
            $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
        $sql = "SELECT *
                FROM `usercatalogues`
                LEFT JOIN catalogues ON usercatalogues.catalogue_id = catalogues.id
                WHERE user_id ='$user_id'
                order by usercatalogues.updated desc";
        $this->data['catalogue'] = Usercatalogue::find_by_sql($sql);
        $view = "myaccount/catloguelisting";
        $this->display_view('frontend', $view, $this->data);
    }

    function favoriteslist() {

        $user_id = "";
        if ($this->session->userdata(SITE_NAME . '_user_data'))
            $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
//        $sql = "SELECT *
//                FROM `usercatalogues`
//                LEFT JOIN catalogues ON usercatalogues.catalogue_id = catalogues.id
//                WHERE user_id ='$user_id'
//                order by usercatalogues.updated desc";
//        $this->data['catalogue'] = Usercatalogue::find_by_sql($sql);
        $this->data['fav'] = Favorite::find('all', array('conditions' => array('user_id= ?', $user_id)));
        $view = "myaccount/favoriteslisting";
        $this->display_view('frontend', $view, $this->data);
    }

    function viewlist($id = "") {
        $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
        $sql = "SELECT *
                FROM usercatalogues
                LEFT JOIN catalogues ON usercatalogues.catalogue_id = catalogues.id
                LEFT JOIN catalogueproducts ON catalogues.id = catalogueproducts.catalogue_id
                LEFT JOIN products ON catalogueproducts.product_id = products.id
                LEFT JOIN productphotos as pp ON products.id = pp.product_id
                LEFT JOIN photos ON pp.photo_id = photos.id 
                WHERE usercatalogues.user_id = '$user_id'
                and usercatalogues.catalogue_id = '$id'
                and products.status = '1'";
        $this->data['catalogue'] = Usercatalogue::find_by_sql($sql);
        $view = "myaccount/productlisting";
        $this->display_view('frontend', $view, $this->data);
    }

    function delete_catalogue($id = "") {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Catalogue::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("catalogue deleted", 'success');
        } else {
            $delete = Catalogue::find($id);
            $delete->delete();
            $this->data['success'] = "catalogue deleted successfully";
        }
        redirect('cataloguelist');
    }

    function delete_product_list($id = "") {
        $cat_id = $this->session->userdata[SITE_NAME . '_save_user_catalog']['id'];
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Catalogueproduct::find('all', array('conditions' => array('product_id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("catalogue deleted", 'success');
        } else {
            $delete = Catalogueproduct::find('all', array('conditions' => array('product_id = ?', $id)));
            foreach ($delete as $obj) {
                $obj->delete();
            }
            $this->data['success'] = "catalogue deleted successfully";
        }

        redirect('cataloguedetails/' . $cat_id);
    }

    function cataloguepdf($id) {
        $this->load->helper("file");
        if ($this->session->userdata(SITE_NAME . '_user_data') && $id) {
            $this->data['catalogue'] = Catalogue::find($id);
            $this->data['category'] = Category::find('all');
            $catalogue = $this->data['catalogue'];
            $category = $this->data['category'];
            foreach ($catalogue->catalogueproducts as $k => $v) {
            $spec[$k] = '';
            if ($v->product->productvariation) {
                foreach ($v->product->productvariation as $kvariation => $vvariation) {
                    $temp[$vvariation->subcategory->category->name][] = $vvariation->subcategory->name;
                }
                foreach ($category as $ck => $cv) {
                    $spec[$k] .= '<tr class = "odd gradeX"><th width="150">' . $cv->name . '</th><td class = "sorting_1">';
                    if (isset($temp[$cv->name]) && !empty($temp[$cv->name])) {
                        $str = array();
                        foreach ($temp[$cv->name] as $varik => $variv) {
                            $str[$variv] = $variv;
                        }
                        $spec[$k] .= implode(",", $str);
                    } else {
                        $spec[$k].= 'N-A';
                    }
                    $spec[$k].='</td></tr>';
                }
            }
        }
        $this->load->library('Pdf');
        $userdata = $this->session->userdata('Aljedaie_user_data');
        $name = 'by - ' . $userdata['name'];
        // create new PDF document
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(SITE_NAME);
        $pdf->SetTitle('Catelouge');
        $pdf->SetSubject('Catelouge Name');
        $pdf->SetKeywords('catelouge, demo, example');
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
// set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $catalogue->title, $name, array(0, 0, 0), array(255, 255, 255));

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'ltr';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 12);

// add a page
// set LTR direction for english translation http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0= &q=100&tw0=10&th0=10&w=250
        $pdf->setRTL(false);
        foreach ($catalogue->catalogueproducts as $k => $v) {
            $pdf->AddPage();
            $tbl.='<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1;">
                <tr>
                <td>
                    <div style="color:#666; margin:0; padding:0; font-size:21px; font-weight:bold">' . $v->product->title . '</div>
                    <span style="color:#6e6e70; margin:0; padding:0">SKU: ' . $v->product->sku . ' - ' . $v->product->colorcode . '</span>
                </td>
                </tr>
                <tr>
                <td align="center">
                    <img src = "' . $v->product->imagepath . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" />
                </td>
                </tr>
                <tr>
                <td style = "padding-left:15px" valign = "top" width="280">
                    <h3 style = "font-size:14px; border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">DESCRIPTION</h3>
                    <table cellpadding = "5" cellspacing = "0">
                        <tr><td>' . $v->product->description . '</td></tr>
                    </table>
                </td>
                </tr>
                <tr>
                <td style = "padding-left:15px" valign = "top" width="480">
                        <table cellpadding = "10" width = "100%" cellspacing = "0" >
                            <tr><td width = "350" valign = "top">
                                <h3 style = "font-size:14px;border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">TECHNICAL SPECIFICATIONS</h3><br>
                                <table cellpadding = "5" cellspacing = "0">' . $spec[$k] . '
                                 </table></td>
                            </tr>
                        </table>
                </td>
                </tr>
                <tr>
                <td width="100%" style = "padding-left:15px" valign = "top">
                    <h3 style="font-weight:500; position:relative; border-bottom:2px solid #25AAE1">AVAILABLE COLORS</h3>
                    <div style="color:#ffffff; margin:0; padding:0; font-size:10px; font-weight:bold">__</div>';
            if (($v->product->colors)) {
                $tbl.='<table cellpadding = "2" width = "100%" cellspacing = "0" style="padding-top:10px"><tr>';
                foreach ($v->product->colors as $avcolk => $avcolv) {
                    $tbl.='<td style="vertical-align: bottom; height:110px;"><img src="' . $avcolv->thumimagepath . '" alt="" title="" style="width:100px; height:100px"/></td>';
                }
                $tbl.='</tr></table>';
            }
            $tbl.='</td></tr></table>';
            $pdf->writeHTML($tbl, true, 0, true, 0);
            $tbl = '';
        }
//Close and output PDF document
$date = date("Y_m_d H_i_s");
            $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
            $pdf->Output($pdfName, 'D');
            @unlink($pdfName);


//============================================================+
// END OF FILE
//============================================================+

        }
    }

    function favdelete($id = "") {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Favorite::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("Favorite product deleted", 'success');
        } else {
            $delete = Favorite::find($id);
            $delete->delete();
            $this->data['success'] = "Favorite products deleted successfully";
        }
        redirect('favoriteslist');
    }

    function storyboardlist() {
        if ($this->session->userdata(SITE_NAME . '_user_data')) {
            $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
            $this->data['storyboard'] = Storyboard::find('all', array('conditions' => array('user_id = ?', $user_id), 'order' => 'updated desc'));
            $view = "myaccount/storyboardlisting";
            $this->display_view('frontend', $view, $this->data);
        }
    }

    function delete_storyboard($id = "") {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Storyboard::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("Storyboard deleted", 'success');
        } else {
            $delete = Storyboard::find('all', array('conditions' => array('id = ?', $id)));
            foreach ($delete as $obj) {
                $obj->delete();
            }
            $this->session->unset_userdata(SITE_NAME . '_story_board');
            $this->data['success'] = "storyboard deleted successfully";
        }

        redirect('storyboardlist');
    }

    function storyboardpdf($id = "") {
        $this->load->helper("file");
        if ($id) {
            $this->data['storyboard'] = Storyboard::find('all', array('conditions' => array('id = ? ', $id)));
            $storyboard = $this->data['storyboard'];
            $userdata = $this->session->userdata('Aljedaie_user_data');
            $name = 'by - ' . $userdata['name'];
            $this->load->library('Pdf');
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(SITE_NAME);
        $pdf->SetTitle('Story Board');
        $pdf->SetSubject('Story');
        $pdf->SetKeywords('storyboard, story, board');
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'StoryBoard', $name, array(0, 0, 0), array(255, 255, 255));

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'ltr';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 9);
            /*             * *************Changes by nlesh ******************** */
            $pdf->startPageGroup();
            
// set LTR direction for english translation
        $pdf->setRTL(false);
//            $this->load->view('frontend/products/download_story_pdf', $this->data);
            if ($storyboard) {
                foreach ($storyboard as $k => $v) {
                    $pdf->AddPage();
                    $tbl = '<table width="100%" cellpadding="0" cellspacing="2" border="0" style="border-top:10px solid #25AAE1">';
                    $tbl.='<tr><td class="header"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>
            <td>
            <h1 style="margin:0; padding:0"><br />Tital :: ' . $v->name . '</h1></td></tr></table></td></tr>';
                    $tbl.='<tr><td align="center"><img src = "' . $v->canvasurl . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" /></td></tr>';
                    $tbl.='<tr><td style="color:#ffffff;">kush</td></tr> <tr><td>';
                    $tbl.='<table cellpadding = "0" width = "100%" cellspacing = "0"><tr align="center">';
                    foreach ($v->storyboardsurface as $kp => $vp) {
                        if (($vp->product)) {
                            $tbl.='<td ><span style="border:2px solid #25AAE1;"><img src="' . $vp->product->thumimagepath . '" alt="" title="" style="width:100px; height:100px"/><p style="text-align: left !important;">Sku code </p>
                                                    <p> "' . $vp->product->sku . '"</p></span></td>';
                        }
                    }
                    $tbl.='</tr></table>';
                    $tbl.='</td></tr></table>';

                    $pdf->writeHTML($tbl, true, false, false, false, '');
                    $tbl='';
                }
            }
            $date = date("Y_m_d H_i_s");
            $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
            $pdf->Output($pdfName, 'D');
            @unlink($pdfName);
        }else {
            
        }
    }

    public function imagedetails($path) {
        $result = getimagesize($path);
        return json_encode(array($result[0], $result[1]));
    }

    function editstoryboard($id) {
        $this->data['edit_story_board'] = Storyboard::find($id);
        $storyboard = $this->data['edit_story_board'];
        $temp = array();
        foreach ($storyboard->storyboardsurface as $k => $v) {
            $data = $this->imagedetails($v->product->thumimagepath);
            $temp[$v->surface] = array('id' => $v->product_id, 'name' => $v->product->thumimagepath, 'size' => $data);
        }
        $temp = json_encode($temp);
        $editstoryboard = "
            $(document).ready(function(){
                editStoryborad('$storyboard->stylename','$storyboard->canvasurl',$temp);
                $('.style-icon').parents('li').attr('id', 'fabric-style');
                tabsoption();
            });
           ";
        array_push($this->scripts['embed'], $editstoryboard);
        $view = 'home/index';
        $this->display_view('frontend', $view, $this->data);
    }

    function couponlisting() {
        $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
        $data['coupon'] = Usercoupon::find('all', array('conditions' => array('user_id =?', $user_id)));
        $view = "myaccount/couponlisting";
        $this->display_view("frontend", $view, $data);
    }

    public function changepassword() {
        $view = "login/changepassword";
        $id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
        $this->form_validation->set_rules('old_password', 'Old Password', 'required');
        $this->form_validation->set_rules('new_password', 'New Password', 'required');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'required|matches[new_password]');
        if ($this->form_validation->run()) {
            $conn = User::connection();
            $conn->transaction();
            try {
                $change_password = User::find_by_id($id);
                $redata = $this->tank_auth->change_password($this->form_validation->set_value('old_password'), $this->form_validation->set_value('new_password'));
                if (!$redata) {
                    $this->_show_message("Your Enter wrong password", 'error');
                } else {

                    $data['username'] = $change_password->fullname;
                    $data['password'] = $this->form_validation->set_value('new_password');
                    $email = $change_password->email;
                    $this->_send_email("change_password", $email, $data);
                    $this->_show_message("Password changed succeessfully.", 'success');
                }
                $conn->commit();

                redirect("changepassword");
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
            $formerror = "";
            $error = $this->form_validation->get_field_data();
            if (isset($error)) {
                foreach ($error as $err) {
                    $formerror.= $err['error'];
                }
                $this->_show_message($formerror, 'error');
//                redirect('changepassword');
            }
        }
        $this->display_view("frontend", $view);
    }
}

?>
