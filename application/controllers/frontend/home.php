<?php

/*
 * @auther Shafiq
 */

class Home extends My_front {

    function __construct() {
        parent::__construct();
        array_push($this->scripts['css'], 'frontend/css/basic.css', 'frontend/css/template.css', 'frontend/css/style.css', 'frontend/css/jquery_ui.css');
        array_push($this->scripts['js'], "frontend/js/jquery-ui.js", "frontend/js/zoom.js", "frontend/js/jquery.slides.min.js", "frontend/js/jquery.tooltipster.js");
    }

    public function index() {
       
       // array_push($this->scripts['js'], "frontend/js/visualizer.js");
        $this->data['category'] = Category::find('all', array('conditions' => array('status = ? and showinfilter = ?', 1, 1), 'include' => 'filtersubcategory'));
        $this->data['product'] = Product::find('all', array('conditions' => array('status=?', 1), 'order' => 'updated desc', 'limit' => LIMIT));
        $this->data['offset'] = LIMIT;
        $this->data['total'] = Product::count(array('conditions' => array('status=?', 1)));
        $this->data['slider'] = Slider::find('all', array('limit' => 5));
        $this->data['page'] = LIMIT;
        if ($this->data['total'] < LIMIT)
            $this->data['page'] = $this->data['total'];

        /*      Add style category   * */
        $join = "left join style_categories on style_categories.id = style_images.category_id";
        $condition = "style_categories.status = '1' and style_images.status = '1'";
        $this->data['style_images'] = StyleImage::all(array('joins' => $join, 'conditions' => $condition, 'include' => 'photo'));

        $this->data['style_total'] = StyleImage::count(array('joins' => $join, 'conditions' => $condition));
        $this->data['style_page'] = LIMIT;
        if ($this->data['style_total'] < LIMIT)
            $this->data['style_page'] = $this->data['style_total'];
        $this->session->set_userdata(SITE_NAME . '_current_currency', 'SAR');
        $this->data['style_category'] = StyleCategory::find('all', array('conditions' => array('status=?', 1), 'order' => 'updated desc'));
        $view = "home/index";
		
        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    public function currency_convertor($country) {
        $this->session->set_userdata('change-currency-to', $country);
        echo 1;
    }

    public function advsearch() {
        $variation = $this->input->post('variation');
        if ($variation) {
            $this->data['product'] = Productvariation::find('all', array('select' => 'distinct product_id', 'conditions' => array('subcategory_id in (?)', $variation)));
            $this->data['post'] = TRUE;
        } else {
            $this->data['product'] = Product::find('all', array('conditions' => array('status = ?', 1), 'order' => 'sku desc,updated desc'));
        }

        //array_push($this->scripts['js'], "frontend/js/visualizer.js");
        $view = "visualizer/index";
        $this->display_view(FRONTENDFOLDERNAME, $view, '');
    }

    public function visualizer($id = '') {
        if (!$id)
            show_404();

        $product = Product::find('first', array('conditions' => array('id =? and status =?', $id, 1)));
        $path = $product->designpath;
        array_push($this->scripts['embed'], "$('document').ready(function(){
            $('#canvaspreloader').show();
            $('#canvas').hide();
            $('#canvas').attr('src','');
                $('.selectedfabric').attr('data','" . $path . "');
                //                var ctx = document.getElementById('canvas').getContext('2d');
                                var img = new Image();
                img.src = 'http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&on=0&fn=suit&tn0=$path&tw0=50&th0=50&q=100&h=500';
                //img.src = 'http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&on=0&fn=suit&tn0=http://97.107.132.91/demo/chhapega/uploads/abc_logo.jpg&tw0=50.454&th0=50.47&q=100&h=500&w=500';
                console.log(img.src);
                $('#canvas').attr('src',img.src).load(function() {  
                $('#canvaspreloader').hide();
                $('#canvas').show();
            });
                //img.onload = function () { ctx.drawImage(img,0,0);}
               });");
        $this->data['product'] = Product::find('all', array('conditions' => array('status=?', 1), 'order' => 'sku desc'));
        // array_push($this->scripts['js'], "frontend/js/visualizer.js");
        $view = "visualizer/index";
        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    public function downloadpdf() {
        $this->load->helper("file");
        if ($this->session->userdata(SITE_NAME . '_user_catalog') && $this->session->userdata(SITE_NAME . '_save_user_catalog')) {
            $this->data['catalogue'] = Catalogue::find('first', array('conditions' => array('id = ? ', $this->session->userdata[SITE_NAME . '_save_user_catalog']['id'])));
            $this->data['category'] = Category::find('all');
            $catalogue = $this->data['catalogue'];
            $category = $this->data['category'];
            $this->load->library('Pdf');
            foreach ($catalogue->catalogueproducts as $k => $v) {
            $spec[$k] = '';
            if ($v->product->productvariation) {
                foreach ($v->product->productvariation as $kvariation => $vvariation) {
                    $temp[$vvariation->subcategory->category->name][] = $vvariation->subcategory->name;
                }
                foreach ($category as $ck => $cv) {
                    $spec[$k] .= '<tr class = "odd gradeX"><th width="150">' . $cv->name . '</th><td class = "sorting_1">';
                    if (isset($temp[$cv->name]) && !empty($temp[$cv->name])) {
                        $str = array();
                        foreach ($temp[$cv->name] as $varik => $variv) {
                            $str[$variv] = $variv;
                        }
                        $spec[$k] .= implode(",", $str);
                    } else {
                        $spec[$k].= 'N-A';
                    }
                    $spec[$k].='</td></tr>';
                }
            }
        }
        $this->load->library('Pdf');
        $userdata = $this->session->userdata('Aljedaie_user_data');
        $name = 'by - ' . $userdata['name'];
        // create new PDF document
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(SITE_NAME);
        $pdf->SetTitle('Catelouge');
        $pdf->SetSubject('Catelouge Name');
        $pdf->SetKeywords('catelouge, demo, example');
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
// set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $catalogue->title, $name, array(0, 0, 0), array(255, 255, 255));

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'ltr';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 12);

// add a page

            // set LTR direction for english translation http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0= &q=100&tw0=10&th0=10&w=250
        $pdf->setRTL(false);
        foreach ($catalogue->catalogueproducts as $k => $v) {
            $pdf->AddPage();
            $tbl.='<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1;">
                <tr>
                <td>
                    <div style="color:#666; margin:0; padding:0; font-size:21px; font-weight:bold">' . $v->product->title . '</div>
                    <span style="color:#6e6e70; margin:0; padding:0">SKU: ' . $v->product->sku . ' - ' . $v->product->colorcode . '</span>
                </td>
                </tr>
                <tr>
                <td align="center">
                    <img src = "' . $v->product->imagepath . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" />
                </td>
                </tr>
                <tr>
                <td style = "padding-left:15px" valign = "top" width="280">
                    <h3 style = "font-size:14px; border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">DESCRIPTION</h3>
                    <table cellpadding = "5" cellspacing = "0">
                        <tr><td>' . $v->product->description . '</td></tr>
                    </table>
                </td>
                </tr>
                <tr>
                <td style = "padding-left:15px" valign = "top" width="480">
                        <table cellpadding = "10" width = "100%" cellspacing = "0" >
                            <tr><td width = "350" valign = "top">
                                <h3 style = "font-size:14px;border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">TECHNICAL SPECIFICATIONS</h3><br>
                                <table cellpadding = "5" cellspacing = "0">' . $spec[$k] . '
                                 </table></td>
                            </tr>
                        </table>
                </td>
                </tr>
                <tr>
                <td width="100%" style = "padding-left:15px" valign = "top">
                    <h3 style="font-weight:500; position:relative; border-bottom:2px solid #25AAE1">AVAILABLE COLORS</h3>
                    <div style="color:#ffffff; margin:0; padding:0; font-size:10px; font-weight:bold">__</div>';
            if (($v->product->colors)) {
                $tbl.='<table cellpadding = "2" width = "100%" cellspacing = "0" style="padding-top:10px"><tr>';
                foreach ($v->product->colors as $avcolk => $avcolv) {
                    $tbl.='<td style="vertical-align: bottom; height:110px;"><img src="' . $avcolv->thumimagepath . '" alt="" title="" style="width:100px; height:100px"/></td>';
                }
                $tbl.='</tr></table>';
            }
            $tbl.='</td></tr></table>';
            $pdf->writeHTML($tbl, true, 0, true, 0);
            $tbl = '';
        }
//Close and output PDF document

            $date = date("Y_m_d H_i_s");
            $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
            $pdf->Output($pdfName, 'D');
            @unlink($pdfName);
        }
    }

    function email($id = "") {
        if ($id) {
            $this->data['catalogue'] = Catalogue::find($id);
        }
        $this->data['title'] = $this->input->get('subject');
        $this->load->view('frontend/products/email', $this->data);
    }

    function emailpdf() {
        $this->load->helper("file");
        if ($this->input->get('email') && $this->input->get('subject')) {
            $email = $this->input->get('email');
            if ($this->session->userdata(SITE_NAME . '_user_catalog') && $this->session->userdata(SITE_NAME . '_save_user_catalog')) {
//                $this->data['catalogue'] = Catalogue::find('first', array('conditions' => array('id = ? ', $this->session->userdata['save_user_catalog']['id'])));
//                $this->data['category'] = Category::find('all');
//                $catalogue = $this->data['catalogue'];
//                $category = $this->data['category'];
//                $this->load->library('Pdf');
//                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
//                $pdf->SetAutoPageBreak(true);
//                $pdf->SetAuthor(SITE_NAME);
//                $pdf->SetFont('helvetica', '', 9);
//                $pdf->AddPage();
//
//                foreach ($catalogue->catalogueproducts as $k => $v) {
//                    $tbl = '<table width="520px" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1">';
//                    if ($k == 0) {
//                        $tbl.='<tr><td class="header"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr> 
//           <td><img src="' . base_url('assets/frontend/pdf-img/logo.png') . '" alt="" style="height:80px" title="" /></td><td>
//                  <h1 style="margin:0; padding:0"><br />' . $catalogue->title . '</h1></td></tr></table></td></tr>';
//                    }
//                    $tbl.='<tr><td>
//                <div style="color:#666; margin:0; padding:0; font-size:21px; font-weight:bold">' . $v->product->title . '</div>
//                <span style="color:#6e6e70; margin:0; padding:0">SKU: ' . $v->product->sku . ' - ' . $v->product->colorcode . '</span>
//            </td></tr><tr><td><img src = "' . $v->product->imagepath . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" /></td>
//</tr><tr><td><table cellpadding = "10" width = "100%" cellspacing = "0" ><tr><td width = "350" valign = "top">
//<h3 style = "font-size:14px;border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">TECHNICAL SPECIFICATIONS</h3><br><table cellpadding = "5" cellspacing = "0">';
//                    foreach ($v->product->productvariation as $kvariation => $vvariation) {
//                        $temp[$vvariation->subcategory->category->name][] = $vvariation->subcategory->name;
//                    }
//                    foreach ($category as $ck => $cv) {
//                        $tbl.='<tr class = "odd gradeX"><th width="70">' . $cv->name . '</th><td class = "sorting_1">';
//                        if (isset($temp[$cv->name]) && !empty($temp[$cv->name])) {
//                            $str = array();
//                            foreach ($temp[$cv->name] as $varik => $variv) {
//                                $str[$variv] = $variv;
//                            }
//                            $tbl.= implode(",", $str);
//                        } else {
//                            $tbl.='N-A';
//                        }
//                        $tbl.='</td></tr>';
//                    }
//                    $tbl.='</table></td>
//<td style = "padding-left:15px" valign = "top" width="180">
//<h3 style = "font-size:14px; border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">DESCRIPTION</h3>
//<table cellpadding = "5" cellspacing = "0"><tr><td>' . $v->product->description . '</td></tr></table></td></tr></table></td></tr><tr><td><h3 style="font-weight:500; position:relative; border-bottom:2px solid #25AAE1">AVAILABLE COLORS</h3></td></tr>
//         <tr><td>';
//                    if (($v->product->colors)) {
//                        $tbl.='<table cellpadding = "0" width = "100%" cellspacing = "0"><tr>';
//                        foreach ($v->product->colors as $avcolk => $avcolv) {
//                            $tbl.='<td><img src="' . $avcolv->thumimagepath . '" alt="" style="width:100px; height:100px" title="" /></td>';
//                        }
//                        $tbl.='</tr></table>';
//                    }
//                    $tbl.='</td></tr></table>';
//
//                    $pdf->writeHTML($tbl, true, false, false, false, '');
//                    if (count($catalogue->catalogueproducts) > ($k + 1))
//                        $pdf->AddPage();
//                }
//                $date = date("Y_m_d H_i_s");
//                $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
//                $pdf->Output($pdfName, 'F');
//                $data['attachment'] = $pdfName;
//                $data['emailAttachment'] = TRUE;
//                $data['subject'] = 'Catalogue PDf attachment';
                $data['subject'] = $this->input->get('subject');
                $data['message'] = $this->input->get('message');
                $this->_send_email('pdfmail', $email, $data);
//                if ($pdfName) {
//                    unlink($pdfName);
//                }
                echo 1;
            }
        } else {
            echo '';
        }
    }
    
    function emailpdfid($id) {
        $this->load->helper("file");
        if ($this->input->get('email') && $this->input->get('subject')) {
            $email = $this->input->get('email');
            if ($this->session->userdata(SITE_NAME . '_user_catalog') && $this->session->userdata(SITE_NAME . '_save_user_catalog')) {
//                $this->data['catalogue'] = Catalogue::find('first', array('conditions' => array('id = ? ', $this->session->userdata['save_user_catalog']['id'])));
//                $this->data['category'] = Category::find('all');
//                $catalogue = $this->data['catalogue'];
//                $category = $this->data['category'];
//                $this->load->library('Pdf');
//                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
//                $pdf->SetAutoPageBreak(true);
//                $pdf->SetAuthor(SITE_NAME);
//                $pdf->SetFont('helvetica', '', 9);
//                $pdf->AddPage();
//
//                foreach ($catalogue->catalogueproducts as $k => $v) {
//                    $tbl = '<table width="520px" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1">';
//                    if ($k == 0) {
//                        $tbl.='<tr><td class="header"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr> 
//           <td><img src="' . base_url('assets/frontend/pdf-img/logo.png') . '" alt="" style="height:80px" title="" /></td><td>
//                  <h1 style="margin:0; padding:0"><br />' . $catalogue->title . '</h1></td></tr></table></td></tr>';
//                    }
//                    $tbl.='<tr><td>
//                <div style="color:#666; margin:0; padding:0; font-size:21px; font-weight:bold">' . $v->product->title . '</div>
//                <span style="color:#6e6e70; margin:0; padding:0">SKU: ' . $v->product->sku . ' - ' . $v->product->colorcode . '</span>
//            </td></tr><tr><td><img src = "' . $v->product->imagepath . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" /></td>
//</tr><tr><td><table cellpadding = "10" width = "100%" cellspacing = "0" ><tr><td width = "350" valign = "top">
//<h3 style = "font-size:14px;border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">TECHNICAL SPECIFICATIONS</h3><br><table cellpadding = "5" cellspacing = "0">';
//                    foreach ($v->product->productvariation as $kvariation => $vvariation) {
//                        $temp[$vvariation->subcategory->category->name][] = $vvariation->subcategory->name;
//                    }
//                    foreach ($category as $ck => $cv) {
//                        $tbl.='<tr class = "odd gradeX"><th width="70">' . $cv->name . '</th><td class = "sorting_1">';
//                        if (isset($temp[$cv->name]) && !empty($temp[$cv->name])) {
//                            $str = array();
//                            foreach ($temp[$cv->name] as $varik => $variv) {
//                                $str[$variv] = $variv;
//                            }
//                            $tbl.= implode(",", $str);
//                        } else {
//                            $tbl.='N-A';
//                        }
//                        $tbl.='</td></tr>';
//                    }
//                    $tbl.='</table></td>
//<td style = "padding-left:15px" valign = "top" width="180">
//<h3 style = "font-size:14px; border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">DESCRIPTION</h3>
//<table cellpadding = "5" cellspacing = "0"><tr><td>' . $v->product->description . '</td></tr></table></td></tr></table></td></tr><tr><td><h3 style="font-weight:500; position:relative; border-bottom:2px solid #25AAE1">AVAILABLE COLORS</h3></td></tr>
//         <tr><td>';
//                    if (($v->product->colors)) {
//                        $tbl.='<table cellpadding = "0" width = "100%" cellspacing = "0"><tr>';
//                        foreach ($v->product->colors as $avcolk => $avcolv) {
//                            $tbl.='<td><img src="' . $avcolv->thumimagepath . '" alt="" style="width:100px; height:100px" title="" /></td>';
//                        }
//                        $tbl.='</tr></table>';
//                    }
//                    $tbl.='</td></tr></table>';
//
//                    $pdf->writeHTML($tbl, true, false, false, false, '');
//                    if (count($catalogue->catalogueproducts) > ($k + 1))
//                        $pdf->AddPage();
//                }
//                $date = date("Y_m_d H_i_s");
//                $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
//                $pdf->Output($pdfName, 'F');
//                $data['attachment'] = $pdfName;
//                $data['emailAttachment'] = TRUE;
//                $data['subject'] = 'Catalogue PDf attachment';
                $data['subject'] = $this->input->get('subject');
                $data['message'] = $this->input->get('message');
                $this->_send_email('pdfmail', $email, $data);
//                if ($pdfName) {
//                    unlink($pdfName);
//                }
                echo 1;
            }
        } else {
            echo '';
        }
    }

    function select_style_images($id) {
        $join = "left join style_categories on style_categories.id = style_images.category_id";
        $condition = "style_categories.status = '1' and style_images.status = '1'and style_images.category_id = '$id'";
        $this->data['style_images'] = StyleImage::all(array('joins' => $join, 'conditions' => $condition, 'include' => 'photo'));
//        echo '<pre>';print_r($sql);exit;
//        $data['style_images'] = StyleImage::find_by_sql($sql);
        $this->data['offset'] = LIMIT;
        $this->data['style_total'] = StyleImage::count(array('joins' => $join, 'conditions' => $condition));
        $this->data['style_page'] = LIMIT;
        if ($this->data['style_total'] < LIMIT)
            $this->data['style_page'] = $this->data['style_total'];
        $view = "home/index";
        $this->load->view('frontend/home/style_image', $this->data);
//        $this->load->view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    function select_style_images_list($id) {
        $join = "left join style_categories on style_categories.id = style_images.category_id";
        $condition = "style_categories.status = '1' and style_images.status = '1'and style_images.category_id = '$id'";
        $this->data['style_images'] = StyleImage::all(array('joins' => $join, 'conditions' => $condition, 'include' => 'photo'));
//        echo '<pre>';print_r($sql);exit;
//        $data['style_images'] = StyleImage::find_by_sql($sql);
        $this->data['offset'] = LIMIT;
        $this->data['style_total'] = StyleImage::count(array('joins' => $join, 'conditions' => $condition));
        $this->data['style_page'] = LIMIT;
        if ($this->data['style_total'] < LIMIT)
            $this->data['style_page'] = $this->data['style_total'];
        $view = "home/index";
//        $this->load->view('frontend/home/style_image', $this->data);
        $this->load->view('frontend/home/style_image_list', $this->data);
    }

    function downloadstoryboardpdf() {
        $this->load->helper("file");
        if ($this->session->userdata(SITE_NAME . '_story_board') && $this->session->userdata(SITE_NAME . '_save_user_storyboard')) {
//            echo '<pre>';print_r($this->session->userdata[SITE_NAME . '_story_board']);exit;
            $this->data['storyboard'] = Storyboard::find('all', array('conditions' => array('name = ? ', $this->session->userdata[SITE_NAME . '_save_user_storyboard']['name'])));
//            foreach($this->data['storyboard'] as $k=>$v){
//                foreach($v->storyboardsurface as $key=>$val){
//                     echo '<pre>';print_r($val->product);exit;
//                }
//                
//            }
            $storyboard = $this->data['storyboard'];
            $userdata = $this->session->userdata('Aljedaie_user_data');
            $name = 'by - ' . $userdata['name'];
            $this->load->library('Pdf');
            $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(SITE_NAME);
        $pdf->SetTitle('Story Board');
        $pdf->SetSubject('Story');
        $pdf->SetKeywords('storyboard, story, board');
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
           // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'StoryBoard', $name, array(0, 0, 0), array(255, 255, 255));

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'ltr';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 9);
            /*             * *************Changes by nlesh ******************** */
            $pdf->startPageGroup();
            
// set LTR direction for english translation
        $pdf->setRTL(false);
//            $this->load->view('frontend/products/download_story_pdf', $this->data);

           
            if ($storyboard) {
                foreach ($storyboard as $k => $v) {
                    $pdf->AddPage();
                     $tbl = '<table width="100%" cellpadding="0" cellspacing="2" border="0" style="border-top:10px solid #25AAE1">';
                     $tbl.='<tr><td class="header"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>
            <td>
            <h1 style="margin:0; padding:0"><br />Tital :: ' . $v->name . '</h1></td></tr></table></td></tr>';
                    $tbl.='<tr><td align="center"><img src = "' . $v->canvasurl . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" /></td></tr>';
                    $tbl.=' <tr><td style="color:#ffffff;">kush</td></tr> <tr><td>';
                    $tbl.='<table cellpadding = "0" width = "100%" cellspacing = "0"><tr align="center">';
                    foreach ($v->storyboardsurface as $kp => $vp) {
                        if (($vp->product)) {
                            $tbl.='<td><span style="border:2px solid #25AAE1;"><img src="' . $vp->product->thumimagepath . '" alt="" style="width:100px; height:100px" title="" /><p>Sku code </p>
                                                    <p> "' . $vp->product->sku . '"</p></span></td>';
                        }
                    }
                    $tbl.='</tr></table>';
                    $tbl.='</td></tr></table>';
                    
                    $pdf->writeHTML($tbl, true, false, false, false, '');
                    $tbl='';
                }
            }

            $date = date("Y_m_d H_i_s");
            $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
            $pdf->Output($pdfName, 'D');
            @unlink($pdfName);
        }
    }

}
