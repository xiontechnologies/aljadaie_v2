<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Newsletters extends My_front {

    function __construct() {
        parent::__construct();

        $this->right_menu();
    }

    function unsubscribenewsletter() {
//        echo '<pre>';print_r($_POST);exit;
        $this->right_menu();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|trim|xss_clean|strip_tags|valid_email|callback_check_email');
        if ($this->form_validation->run()) {
            try {
                $email = $this->input->post('email');
                $newslettser = Subscribnewsletters::find_by_email($email);
                $newslettser->status = 0;
                $newslettser->save();
                $this->_show_message("Newsletter Unsubscribed Successfully", 'success');
                redirect('frontend/newsletters/unsubscribenewsletter');
            } catch (Exception $e) {
                $this->write_logs($e);
            }
        } else {
            if ($this->input->post())
                $data['postdata'] = $this->input->post();
        }
        $view = "login/unsubscribe_newsletter";
        $this->display_view('frontend', $view);
    }

    function check_email($str) {
        $user = Subscribnewsletters::find('all', array('conditions' => array('email = ? and status = ?', $str, '1')));
        if ($user) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_email', 'email is not exist');
            return FALSE;
        }
    }

    function add_newsletter() {
        $view = "layout/newsletter";
        $this->display_view('frontend', $view);
    }

    public function newsletter() {
        array_push($this->scripts['css'], "frontend/css/validation.engine.css");
        array_push($this->scripts['js'], "frontend/js/jquery.validationEngine-en.js", "frontend/js/jquery.validationEngine.js");

        $validation = "$(document).ready(function(){
                        $('#newsletter').validationEngine();
                        })";
        array_push($this->scripts['embed'], $validation);
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_checkUser');
        if ($this->form_validation->run()) {
            $conn = Subscribnewsletters::connection();
            $conn->transaction();
            try {
                $newsletter = new Subscribnewsletters();
                $newsletter->email = $this->form_validation->set_value('email');
                $newsletter->status = 0;
                $newsletter->created = date('Y-m-d h:i:s');
                $newsletter->updated = date('Y-m-d h:i:s');
                $newsletter->save();
                $conn->commit();

                /*                 * ************************User mail*********************************** */
                $auto_id = $newsletter->id;
                $data['link'] = base_url("frontend/newsletters/subscribenewsletter/" . $auto_id);
                $email = $newsletter->email;

                $data['subject'] = "Newsletter subscription";
                $this->_send_email('newsletter', $email, $data);
                $this->_show_message('Thank you for Subscribed newsletter', 'success', '');
                redirect('newsletter');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
                redirect('');
            }
        } else {
            $error = $this->form_validation->get_field_data();
            $this->_show_message($error['email']['error'], 'error');
            redirect('newsletter');
            if ($this->input->post())
                $this->data['postdata'] = (object) $this->input->post();
        }
    }

    function checkUser($str) {
        $user = Subscribnewsletters::find('first', array('conditions' => array('email = ?', $str)));
        if ($user) {
            $this->form_validation->set_message('checkUser', 'Already subcribed');
            return FALSE;
        } else {
            return true;
        }
    }

    function subscribenewsletter($id = "") {
        $user = Subscribnewsletters::find($id);
        $user->status = 1;
        $user->save();
        $this->_show_message("Newsletter activated", 'success');
        redirect('newsletter');
    }

}

?>
