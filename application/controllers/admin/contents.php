<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Contents extends MY_admin {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $view = "contents/view";
        $this->data['alldata'] = Content::find('all', array('order' => 'updated desc'));
        $this->data['userRights'] = $this->check_rights();
        $this->display_view('admin', $view, $this->data);
    }

    public function add() {
        $view = "contents/add";
        $this->data['userRights'] = $this->check_rights();
        $configval = array(
            array('field' => 'title', 'label' => 'Title', 'rules' => 'required'),
            array('field' => 'short_desc', 'label' => 'Short Description', 'rules' => 'required'),
            array('field' => 'status', 'label' => 'Status', 'rules' => 'required'),
            array('field' => 'position', 'label' => 'Position', 'rules' => 'required'),
        );
        array_push($this->scripts['js'], 'admin/tinymc/tinymce.min.js', 'frontend/js/jquery-ui.js', 'admin/js/products/product.js');
        array_push($this->scripts['css'], 'frontend/css/jquery_ui.css');
        $desc = 'tinymce.init({ selector: "#long_desc"});tinymce.init({ selector: "#washcare"}); ';
        array_push($this->scripts['embed'], $desc);
        $this->form_validation->set_rules($configval);
        if ($this->form_validation->run()) {
            $conn = Content::connection();
            $conn->transaction();
            try {
                $adddata['title'] = $this->form_validation->set_value('title');
                $adddata['short_desc'] = $this->form_validation->set_value('short_desc');
                $adddata['long_desc'] = $this->input->post('long_desc');
                $adddata['status'] = $this->input->post('status');
                $adddata['position'] = $this->input->post('position');
                $adddata['created'] = date("Y-m-d H:i:s");
                $adddata['updated'] = date("Y-m-d H:i:s");

                $containts = new Content($adddata);
                $containts->save();
                $conn->commit();
                $this->_show_message("Contents add successfully", "success");
                redirect('admin/contents/list');
            } catch (Exception $e) {
                print_r($e->getMessage());
                $this->write_logs($e);
                $conn->rollback();
                $this->_show_message('Contents couldnot added please contact to developer', 'error');
                $data['postdata'] = (object) $this->input->post();
            }
        } else {
            if ($this->input->post()) {
                $data['postdata'] = (object) $this->input->post();
            }
        }
        $data['postdata'] = (object) $this->input->post();
        $this->display_view('admin', $view, $data);
    }

    public function edit($id) {
        if (!$id)
            show_404();
        $this->data['userRights'] = $this->check_rights();
        $view = "contents/edit";
        $data['editdata'] = Content::find_by_id($id);

        array_push($this->scripts['js'], 'admin/tinymc/tinymce.min.js', 'frontend/js/jquery-ui.js', 'admin/js/products/product.js');
        array_push($this->scripts['css'], 'frontend/css/jquery_ui.css');
        $desc = 'tinymce.init({ selector: "#long_desc"});tinymce.init({ selector: "#washcare"}); ';
        array_push($this->scripts['embed'], $desc);

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('short_desc', 'Short Description', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('position', 'Position', 'required');
        if ($this->form_validation->run()) {
            $conn = Content::connection();
            $conn->transaction();
            try {
                $editdata = Content::find_by_id($id);
                $editdata->title = $this->form_validation->set_value('title');
                $editdata->short_desc = $this->form_validation->set_value('short_desc');
                $editdata->long_desc = $this->input->post('long_desc');
                $editdata->status = $this->input->post('status');
                $editdata->position = $this->input->post('position');
                $editdata->updated = databasedate();

                $editdata->save();
                $conn->commit();
                $this->_show_message("Contents updated sucessfully", "success");
                redirect('admin/contents/list');
            } catch (Exception $e) {
                print_r($e->getMessage());
                $this->write_logs($e);
                $conn->rollback();
                $this->_show_message('Contents couldnot edit please contact to developer', 'error');
                $data['postdata'] = (object) $this->input->post();
            }
        } else {
//                echo '<pre>';print_r($this->form_validation->get_field_data());exit;
            if ($this->input->post()) {
                $data['postdata'] = (object) $this->input->post();
            }
        }

        $this->display_view('admin', $view, $data);
    }

    function delete($id = '') {

        if ($this->input->post('option')) {
            $ids = $this->input->post('option');
            $deleteObj = Content::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("Contents deleted", 'success');
        } else {
            if (!$id)
                show_404();
            $deleteObj = Content::find($id);
            $deleteObj->delete();
            $this->_show_message("Content deleted", 'success');
        }
        redirect('admin/contents/list');
    }

    function activate($id = "") {
        if (!$id)
            show_404();
        $conn = Content::connection();
        $conn->transaction();
        try {
            $activate = Content::find($id);
            $activate->status = 1;
            $activate->save();
            $conn->commit();
            $this->_show_message("Content activated successfully", 'success');
            redirect('admin/contents/list');
        } catch (Exception $e) {
            print_r($e->getMessage());
            $this->write_logs($e);
            $conn->rollback();
            $this->_show_message('Contents couldnot activated please contact to developer', 'error');
            $data['postdata'] = (object) $this->input->post();
        }
    }

    function deactivate($id = "") {
        if (!$id)
            show_404();
        $conn = Content::connection();
        $conn->transaction();
        try {
            $activate = Content::find($id);
            $activate->status = 0;
            $activate->save();
            $conn->commit();
            $this->_show_message("Content deactivated successfully", 'success');
            redirect('admin/contents/list');
        } catch (Exception $e) {
            print_r($e->getMessage());
            $this->write_logs($e);
            $conn->rollback();
            $this->_show_message('Contents couldnot deactivated please contact to developer', 'error');
            $data['postdata'] = (object) $this->input->post();
        }
    }

}

?>
