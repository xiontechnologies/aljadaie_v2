<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Dbbackups extends MY_admin {

    public function index() {
        $view = "dbbackup/view";
        $this->display_view("admin", $view);
    }

    public function export() {
        $this->load->dbutil();
        $dbs = $this->dbutil->list_databases();
        if ($this->dbutil->database_exists('tailermate')) {
            $prefs = array(
//                'tables' => array('table1', 'table2'), // Array of tables to backup.
                'ignore' => array(), // List of tables to omit from the backup
                'format' => 'txt', // gzip, zip, txt
                'filename' => 'mybackup.sql', // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
                'add_insert' => TRUE, // Whether to add INSERT data to backup file
                'newline' => "\n"               // Newline character used in backup file
            );
            $backup = $this->dbutil->backup($prefs);
            $db_name = 'backup-on-' . date("Y-m-d-H-i-s") . '.txt';
            $save = 'assets/dbbackup/' . $db_name;
            $this->load->helper('file');
            write_file($save, $backup);
            $this->load->helper('download');
            force_download($save, $backup);
        }
    }

    public function autobackup() {
        $this->load->dbutil();
        $dbs = $this->dbutil->list_databases();
        if ($this->dbutil->database_exists('tailermate')) {
            $prefs = array(
//                'tables' => array('table1', 'table2'), // Array of tables to backup.
                'ignore' => array(), // List of tables to omit from the backup
                'format' => 'txt', // gzip, zip, txt
                'filename' => 'mybackup.sql', // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
                'add_insert' => TRUE, // Whether to add INSERT data to backup file
                'newline' => "\n"               // Newline character used in backup file
            );
            $backup = $this->dbutil->backup($prefs);
            $db_name = 'backup-on-' . date("Y-m-d-H-i-s") . '.txt';
            $save = 'assets/dbbackup/' . $db_name;
            $this->load->helper('file');
            write_file($save, $backup);
            $this->_show_message("Database stored successfully", 'success');
            redirect("admin/dbbackups/list");
//            $this->load->helper('download');
//            force_download($save, $backup);
        }
    }

}

?>
