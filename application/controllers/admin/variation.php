<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Variation extends MY_admin {

    function index() {
        $view = 'variation/view';
        $this->metas['title'] = array('Admin | Attributes');
        $data['alldata'] = Subcategory ::find('all', array('order' => 'updated desc'));
        //$this->data['userRights'] = $this->check_rights();
        $this->display_view('admin', $view, $data);
    }

    function add() {
        $view = "variation/add";
        $this->metas['title'] = array('Admin | Variations | Add');
//        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
//        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
//        $validation = "
//                $('document').ready(function(){
//                    $('#addadminfrm').validationEngine();
//                });";
//        array_push($this->scripts['embed'], $validation);
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('category_id', 'Category Name', 'required|callback_select_category');
        if ($this->form_validation->run()) {
            $data = new Subcategory();
            $data->category_id = $this->form_validation->set_value('category_id');
            $data->name = $this->form_validation->set_value('name');
            $data->status = $this->form_validation->set_value('status');
            $data->created = date('Y-m-d H:i:s');
            $data->updated = date('Y-m-d H:i:s');
            $data->save();
            $this->_show_message('Variation added successfully', 'success');
            redirect('admin/variations');
        } else {

            if ($this->input->post()) {
                $this->_show_message('Error', 'error');
                $data['postdata'] = (object) $this->input->post();
            }
        }
        $data['category'] = (object) Category::find_assoc();
        $this->display_view('admin', $view, $data);
    }

    function select_category($str) {
        if ($str == '-1' || $str == '0' || $str == '') {
            $this->form_validation->set_message('select_category', "Please select Category");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function edit($id = '') {
        if (!$id)
            show_404();
        /*
         * @var view : view name
         * @title :title of page
         */
        $view = 'variation/add';
        $this->metas['title'] = array('Admin | Attributes | Edit');
        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
        $validation = "
                $('document').ready(function(){
                    $('#addadminfrm').validationEngine();
                });";
        array_push($this->scripts['embed'], $validation);
        $this->data['postdata'] = Subcategory::find(array('conditions' => array('id = ? ', $id)));
        $this->data['category'] = Category::find_assoc();
        $config = array(
            array('field' => 'name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'status', 'label' => 'Status', 'rules' => 'required'),
            array('field' => 'category_id', 'label' => 'Category Name', 'rules' => 'required')
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run()) {
            $editvariation = Subcategory::find($id);
            $editvariation->name = $this->form_validation->set_value('name');
            $editvariation->category_id = $this->form_validation->set_value('category_id');
            $editvariation->status = $this->form_validation->set_value('status');
            $editvariation->updated = date('Y-m-d H:i:s');
            $editvariation->save();
            $this->_show_message('Varition is edited successfully', 'success');
            redirect('admin/variations');
        } else {
            if ($this->input->post()) {
                $this->data['postdata'] = (object) $this->input->post();
                $this->_show_message('Error while Editing', 'error');
            }
        }
        $this->display_view('admin', $view, $this->data);
    }

    function delete($id = '') {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Subcategory::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("Varitions deleted", 'success');
            redirect('admin/variations');
        } else {
            if (!$id)
                show_404();
            $deleteObj = Subcategory :: find($id);
            $deleteObj->delete();
            $this->_show_message("Varition deleted", 'success');
            redirect('admin/variations');
        }
    }

    function activate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Subcategory::find_by_id($id);
        $editActivation->status = '1';
        $editActivation->save();
        $this->_show_message("Varition activated successfully", 'success');
        redirect('admin/variations');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Subcategory::find_by_id($id);
        $editActivation->status = '0';
        $editActivation->save();
        $this->_show_message("Varition deactivated successfully", 'success');
        redirect('admin/variations');
    }

}

?>
