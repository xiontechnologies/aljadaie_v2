<?php

/*
 * @auther Shafiq
 */

class Bulkupload extends MY_admin {

    function __construct() {
        parent::__construct();
    }

    function throwexception($message = '', $type = 'error') {
        if (isset($this->data['filename']) && file_exists($this->data['filename']))
            unlink($this->data['filename']);
        $this->_show_message($message, $type);
        redirect('admin/bulkupload');
    }

    function index() {
        $this->data['products'] = Product::find('all', array('order' => 'updated desc'));
        $this->display_view('admin', 'bulkupload/index', $this->data);
    }

    function check_image($productObj, $image, $row) {
        if ($image) {
            if (file_exists(PRODUCTIMAGE_PATH . '/' . $image)) {
                if ($productObj->photo) {
                    /*
                     * Check , is old image or new
                     * if new delete old one 
                     * and insert new
                     */
                    if ($productObj->photo->file_name != $image) {
                        $productObj->photo->delete();
                        $this->insert_into_image($image, $productObj->id);
                    }
                } else {
                    /*
                     * Insert into database for new design / new record
                     */
                    $this->insert_into_image($image, $productObj->id);
                }

                /*
                 * also check for thumb
                 */
                if (!file_exists(PRODUCTIMAGE_PATH . '/thumb/' . $image)) {
                    /*
                     * Create thumb
                     */
                    $this->create_thumb(PRODUCTIMAGE_PATH, $image);
                }
            } else {
                $this->throwexception('Image ' . $image . ' not found on location');
            }
        } else {
            $this->throwexception("Image is complusary for products check row " . $row);
        }
    }

    function check_design($productObj, $design, $row) {
        if ($design) {
            if (file_exists(PRODUCTDESIGN_PATH . '/' . $design)) {
                if ($productObj->design && $productObj->design[0]) {
                    /*
                     * Check , is old image or new
                     * if new delete old one 
                     * and insert new
                     */
                    if ($productObj->design[0]->file_name != $design) {
//                    $productdesigndeleteObj = Productdesign::find('first', array('conditions' => array('product_id = ? and design_id = ?', $productObj->id, $productObj->design[0]->id)));
//                    $productdesigndeleteObj->delete();
                        $productObj->design[0]->delete();
                        $this->insert_into_design($design, $productObj->id);
                    }
                } else {
                    /*
                     * Insert into database for new design / new record
                     */
                    $this->insert_into_design($design, $productObj->id);
                }

                /*
                 * also check for thumb
                 */
                if (!file_exists(PRODUCTDESIGN_PATH . '/thumb/' . $design)) {
                    /*
                     * Create thumb
                     */
                    $this->create_thumb(PRODUCTDESIGN_PATH, $design);
                }
            } else {
                $this->throwexception('Design image ' . $design . ' not found on location');
            }
        } else {
            $this->throwexception("Design is complusary for products check row " . $row);
        }
    }

    function create_thumb($path, $name) {
        $config_resize['image_library'] = 'gd2';
        $config_resize['source_image'] = $path . '/' . $name;
        $config_resize['new_image'] = $path . '/thumb/';
        $config_resize['maintain_ratio'] = TRUE;
        $config_resize['width'] = 200;
        $config_resize['height'] = 200;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config_resize);
        if (!$this->image_lib->resize()) {
            $message = $this->image_lib->display_errors();
            $this->write_logs('', $message);
        }
    }

    function insert_into_image($name, $product_id) {
        $photoObj = new Photo();
        $photoObj->file_title = $name;
        $photoObj->file_name = $name;
        $photoObj->orig_name = $name;
        $photoObj->client_name = $name;
        $photoObj->is_image = 1;
        $photoObj->status = 1;
        $photoObj->created = databasedate();
        $photoObj->updated = databasedate();
        $photoObj->save();
        $productdesignObj = new Productphoto();
        $productdesignObj->product_id = $product_id;
        $productdesignObj->photo_id = $photoObj->id;
        $productdesignObj->created = databasedate();
        $productdesignObj->updated = databasedate();
        $productdesignObj->save();
    }

    function insert_into_design($name, $product_id) {
        $designObj = new Design();
        $designObj->file_title = $name;
        $designObj->file_name = $name;
        $designObj->orig_name = $name;
        $designObj->client_name = $name;
        $designObj->is_image = 1;
        $designObj->status = 1;
        $designObj->created = databasedate();
        $designObj->updated = databasedate();
        $designObj->save();
        $productdesignObj = new Productdesign();
        $productdesignObj->product_id = $product_id;
        $productdesignObj->design_id = $designObj->id;
        $productdesignObj->created = databasedate();
        $productdesignObj->updated = databasedate();
        $productdesignObj->save();
    }

//    function delete_varaition($header, $productid) {
//        $join = "left join subcategories on `subcategory_id` =subcategories.id";
//        $cond = "category_id = $header";
//        $result = Productvariation::find('first', array('conditions' => $cond, 'joins' => $join));
//        if ($result) {
//            $result->delete();
//        }
//    }

    function varaition_check($header, $value, $productid) {
        $sub = Subcategory::find('first', array('conditions' => array('category_id = ? and name = ?', $header, trim($value, " "))));
        if ($sub) {
            /*
             * Now find into variation table 
             */
            $variation = Productvariation::find('first', array('conditions' => array('product_id = ? and subcategory_id = ?', $productid, $sub->id)));
            if (!$variation) {
                $this->insert_into_variation($productid, $sub->id);
            }
        } else {
            /*
             * Enter into database as its new entry
             */

            $sub = $this->insert_into_subcategory($value, $header);

            /*
             * Save in productvariation table
             */
            $this->insert_into_variation($productid, $sub->id);
        }
    }

    function insert_into_subcategory($name, $category) {
        $sub = new Subcategory();
        $sub->name = trim($name, ' ');
        $sub->category_id = $category;
        $sub->status = 1;
        $sub->showinfilter = 1;
        $sub->created = databasedate();
        $sub->updated = databasedate();
        $sub->save();
        return $sub;
    }

    function insert_into_variation($product, $subcategory) {
        $productvariation = new Productvariation();
        $productvariation->product_id = $product;
        $productvariation->subcategory_id = $subcategory;
        $productvariation->created = databasedate();
        $productvariation->updated = databasedate();
        $productvariation->save();
    }

    function delete_all_variation($product_id) {
        Productvariation::delete_all(array('conditions' => array('product_id = ?', $product_id)));
    }
/*
 * Data for export
 */
    function edit() {
        $this->data['products'] = Product::find('all');
        $this->data['category'] = Category::find('all');
        $this->load->view('admin/bulkupload/export', $this->data);
    }

    function add() {
        if (isset($_FILES['userfile']['error']) && $_FILES['userfile']['error'] == 0) {
            $this->load->library('Upload');
            $config['upload_path'] = BULKUPLOAD_PATH;
            $config['allowed_types'] = '*';
            $config['max_size'] = '0';
            $config['overwrite'] = FALSE;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) {
                $this->write_logs('', $this->upload->display_errors());
                $this->throwexception($this->upload->display_errors(), 'error');
            } else {
                $upload_file = $this->upload->data();
                $this->data['filename'] = BULKUPLOAD_PATH . '/' . $upload_file['file_name'];
                $conn = Product::connection();
                $conn->transaction();
                try {
                    // Load the spreadsheet reader library
                    $this->load->library('Spreadsheet_Excel_Reader');
                    $xls = new Spreadsheet_Excel_Reader(BULKUPLOAD_PATH . '/' . $upload_file['file_name']);
                    $arr = array();
                    $header = array();
                    $sheet = 0;
                    for ($row = 2; $row <= $xls->rowcount($sheet); $row++) {
                        for ($col = 1; $col <= $xls->colcount($sheet); $col++) {
                            $arr[$row][$col] = $xls->val($row, $col, $sheet);
                        }
                    }
                    for ($row = 1; $row <= 1; $row++) {
                        for ($col = 1; $col <= $xls->colcount($sheet); $col++) {
                            if ($col <= 15) {
                                $header[$col] = $xls->val($row, $col, $sheet);
                            } else {
                                $category = Category::find('first', array('conditions' => array('name = ? ', $xls->val($row, $col, $sheet))));
                                if ($category) {
                                    $header[$col] = $category->id;
                                } else {
                                    $this->throwexception('New column ' . $xls->val($row, $col, $sheet) . ' is added as attribue which not allowed in bulk upload');
                                }
                            }
                        }
                    }
                    $rows = 0;
                    foreach ($arr as $k => $v) {
                        $count = count($v);
                        /*
                         * As we know 13 first column goes to product table
                         */
                        $section = Section::find('first', array('conditions' => array('name = ?', $v[1])));
                        if ($section) {
                            $section_id = $section->id;
                            $productObj = Product::find('first', array('conditions' => array('sku = ? and colorcode = ?', $v[2], $v[3])));
                            if ($productObj) {
                                $productObj->section_id = $section_id;
                                $productObj->sku = $v[2];
                                $productObj->colorcode = $v[3];
                                $productObj->title = $v[4];
                                $productObj->description = $v[5];
                                $productObj->shortdescription = $v[6];
                                $productObj->price = $v[7];
                                $productObj->discountprice = $v[8];
                                $productObj->weight = $v[9];
                                $productObj->stock = $v[10];
                                $productObj->washcare = $v[11];
                                $productObj->tags = $v[12];
                                $productObj->metakeywords = $v[13];
                                $productObj->updated = databasedate();
                                $productObj->save();
                                /*
                                 * Search for images and design 
                                 */
                                $this->check_design($productObj, $v[14], $k);
                                $this->check_image($productObj, $v[15], $k);
                                /*
                                 * Find subcategory with thier category and insert data
                                 */
                                /*
                                 * Delete all variation of product and insert new one
                                 */
                                $this->delete_all_variation($productObj->id);
                                for ($i = 16; $i <= $count; $i++) {
                                    if ($v[$i]) {
                                        $temp = explode(',', $v[$i]);
                                        if (count($temp) >= 2) {
                                            foreach ($temp as $tk => $tv) {
                                                $this->varaition_check($header[$i], $tv, $productObj->id);
                                            }
                                        } else {
                                            $this->varaition_check($header[$i], $v[$i], $productObj->id);
                                        }
                                    }
                                }
                            } else {
                                /*
                                 * Create new product obj
                                 */
                                $productObj = new Product();
                                $productObj->section_id = $section_id;
                                $productObj->sku = $v[2];
                                $productObj->colorcode = $v[3];
                                $productObj->title = $v[4];
                                $productObj->description = $v[5];
                                $productObj->shortdescription = $v[6];
                                $productObj->price = $v[7];
                                $productObj->discountprice = $v[8];
                                $productObj->weight = $v[9];
                                $productObj->stock = $v[10];
                                $productObj->washcare = $v[11];
                                $productObj->tags = $v[12];
                                $productObj->metakeywords = $v[13];
                                $productObj->created = databasedate();
                                $productObj->updated = databasedate();

                                $productObj->save();

                                $this->check_design($productObj, $v[14], $k);
                                $this->check_image($productObj, $v[15], $k); /*
                                 * Find subcategory with thier category and insert data
                                 */
                                /*
                                 * Delete all variation of product and insert new one
                                 */
                                $this->delete_all_variation($productObj->id);
                                for ($i = 16; $i <= $count; $i++) {
                                    if ($v[$i]) {
                                        $temp = explode(',', $v[$i]);
                                        if (count($temp) >= 2) {
                                            foreach ($temp as $tk => $tv) {
                                                $this->varaition_check($header[$i], $tv, $productObj->id);
                                            }
                                        } else {
                                            $this->varaition_check($header[$i], $v[$i], $productObj->id);
                                        }
                                    }
                                }
                            }
                        } else {
                            $this->throwexception('Data inserted into database till ' . $k . ' rows', 'info');
                        }
                        $rows = $k;
                    }
                    $conn->commit();
                    $this->_show_message('Data inserted into database till ' . $rows . ' rows', 'success');
                    unlink($this->data['filename']);
                    redirect('admin/bulkupload');
                } catch (Exception $e) {
                    $this->write_logs($e);
                    $conn->rollback();
                    $this->_show_message('Some thing wrong with database contact developer', 'error');
                    redirect('admin/bulkupload');
                }
            }
        } else {
            $this->_show_message("Please upload file", 'error');
            redirect('admin/bulkupload');
        }
    }

}

