<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Attribute extends MY_admin {

    function index() {
        $view = 'attribute/view';
        $this->metas['title'] = array('Admin | Attributes');
        $data['alldata'] = Category ::find('all', array('order' => 'updated desc'));
        //$this->data['userRights'] = $this->check_rights();
        $this->display_view('admin', $view, $data);
    }

    function add() {
        $view = "attribute/add";
        $this->metas['title'] = array('Admin | Attributes | Add');
        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
        $validation = "
                $('document').ready(function(){
                    $('#addadminfrm').validationEngine();
                });";
        array_push($this->scripts['embed'], $validation);
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('showinfilter', 'Show in Filter', 'required');
        if ($this->form_validation->run()) {
            $data = new Category();
            $data->name = $this->form_validation->set_value('name');
            $data->status = $this->form_validation->set_value('status');
            $data->showinfilter = $this->form_validation->set_value('showinfilter');
            $data->created = date('Y-m-d H:i:s');
            $data->updated = date('Y-m-d H:i:s');
            $data->save();
            $this->_show_message('Attribute added successfully', 'success');
            redirect('admin/attributes');
        } else {

            if ($this->input->post()) {
                $this->_show_message('Error', 'error');
                $data['postdata'] = (object) $this->input->post();
            }
        }
        $data['postdata'] = $this->input->post();
        $this->display_view('admin', $view, $data);
    }

    function edit($id = '') {
        if (!$id)
            show_404();
        /*
         * @var view : view name
         * @title :title of page
         */
        $view = 'attribute/add';
        $this->metas['title'] = array('Admin | Attributes | Edit');
        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
        $validation = "
                $('document').ready(function(){
                    $('#addadminfrm').validationEngine();
                });";
        array_push($this->scripts['embed'], $validation);
        $this->data['postdata'] = Category::find(array('conditions' => array('id = ? ', $id)));
        $config = array(
            array('field' => 'name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'status', 'label' => 'Status', 'rules' => 'required'),
            array('field' => 'showinfilter', 'label' => 'Show In Filters', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run()) {
            $editcategory = Category::find($id);
            $editcategory->name = $this->form_validation->set_value('name');
            $editcategory->status = $this->form_validation->set_value('status');
            $editcategory->showinfilter = $this->form_validation->set_value('showinfilter');
            $editcategory->updated = date('Y-m-d H:i:s');
            $editcategory->save();
            $this->_show_message('Attribute is edited successfully', 'success');
            redirect('admin/attributes');
        } else {
            if ($this->input->post()) {
                $this->data['postdata'] = (object) $this->input->post();
                $this->_show_message('Error while Editing', 'error');
            }
        }
        $this->display_view('admin', $view, $this->data);
    }

    function delete($id = '') {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Category::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("Categories deleted", 'success');
            redirect('admin/attributes');
        } else {
            if (!$id)
                show_404();
            $deleteObj = Category :: find($id);
            $deleteObj->delete();
            $this->_show_message("Attribute deleted", 'success');
            redirect('admin/attributes');
        }
    }

    function activate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Category::find_by_id($id);
        $editActivation->status = '1';
        $editActivation->save();
        $this->_show_message("Attribute activated successfully", 'success');
        redirect('admin/attributes');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Category::find_by_id($id);
        $editActivation->status = '0';
        $editActivation->save();
        $this->_show_message("Attribute deactivated successfully", 'success');
        redirect('admin/attributes');
    }

}

?>
