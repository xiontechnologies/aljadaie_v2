<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Members extends MY_admin {

    public function index() {

        $view = "user/view";
        $data['userRights'] = $this->check_rights();

        $data['allusers'] = User::find('all', array('order' => 'modified desc'));
        //echo'<pre>';print_r($data['allusers'][0]->video);exit;
        $this->display_view('admin', $view, $data);
    }

    public function validation($info = "") {
        $config = array(
            array('field' => 'group_id', 'label' => 'Group', 'rules' => 'required|xss_clean|trim|callback_select_validate'),
            array('field' => 'firstname', 'label' => 'First Name', 'rules' => 'required'),
            array('field' => 'lastname', 'label' => 'Last Name', 'rules' => 'required'),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'required|valid_email|is_unique[users.email]'),
            array('field' => 'dob', 'label' => 'Date of Birth', 'rules' => 'required'),
            array('field' => 'status', 'label' => 'Status', 'rules' => 'required'),
            array('field' => 'gender', 'label' => 'Gender', 'rules' => 'required'),
            array('field' => 'telephone', 'label' => 'Telephone', 'rules' => 'required|integer'),
            array('field' => 'mobiles', 'label' => 'Mobile', 'rules' => 'integer'),
        );
        $this->form_validation->set_rules($config);
        if ($info == 'edit') {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->input->post('password') || $this->input->post('confirmpassword')) {
                $this->form_validation->set_rules('password', 'Password', 'required');
                $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required|matches[password]');
            }
        } else {
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required|matches[password]');
        }
    }

    function select_validate($str) {
        if ($str == -1 || $str == 0 || $str == '') {
            $this->form_validation->set_message('select_validate', 'Please select Category');
            return FALSE;
        } else {
            return true;
        }
    }

    public function add() {
//        echo '<pre>';print_r($_FILES);exit;
        $view = "user/add";
        array_push($this->scripts['js'], "js/datepicker_new.js");
        array_push($this->scripts['css'], "css/datePicker.css");
        $datejs = '$(document).ready(function(){
    $(function() {
        $( "#dob" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        });});';
        array_push($this->scripts['embed'], $datejs);
//        array_push($this->scripts ['css'], "js/datePicker.css");
        $this->validation('add');
        if ($this->form_validation->run()) {
            $conn = User::connection();
            $conn->transaction();
            try {

                /*                 * *********************Insert Record into Users table********************************** */
                $this->load->library('tank_auth_admin');
                $adddata['firstname'] = $this->form_validation->set_value('firstname');
                $adddata['lastname'] = $this->form_validation->set_value('lastname');
                $adddata['email'] = $this->form_validation->set_value('email');
                $adddata['password'] = $this->tank_auth_admin->create_password($this->form_validation->set_value('password'));
                $adddata['activated'] = 1;
                $adddata['created'] = date("Y-m-d h:i:s");
                $adddata['modified'] = date("Y-m-d h:i:s");

                $users = new User($adddata);
                $users->save();

                $usergroup = new User_Group();
                $usergroup->user_id = $users->id;
                $usergroup->group_id = $this->form_validation->set_value('group_id');
                $usergroup->created = date("Y-m-d h:i:s");
                $usergroup->updated = date("Y-m-d h:i:s");

                //echo'<pre>';print_r($usergroup);exit;
                $usergroup->save();
                /*                 * *********************Insert Record into Photo table********************************** */
                if (isset($_FILES['userfile']) && !empty($_FILES['userfile']) && $_FILES['userfile']['error'] != '4') {
                    $config['upload_path'] = ADMIN_UPLOAD_IMAGE_PATH;
                    $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
                    $config['max_size'] = '10000';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '768';
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('userfile')) {
                        $error = $this->upload->display_errors();
                        $this->_show_message($error, "error");
                    } else {
                        $data = $this->upload->data();
                        $photo = new Photo($data);
                        $photo->save();
                        $addprofile['photo_id'] = $photo->id;
                    }
                } else {
                    $addprofile['photo_id'] = '';
                }

                /*                 * *********************Insert Record into Userprofile table**************************** */
                $addprofile['user_id'] = $users->id;
                $addprofile['dateofbirth'] = $this->form_validation->set_value('dob');
                $addprofile['gender'] = $this->form_validation->set_value('gender');
                $addprofile['aboutme'] = $this->input->post('aboutme');
                $addprofile['telephone'] = $this->input->post('telephone');
                $addprofile['mobile'] = $this->input->post('mobiles');
                $addprofile['created'] = date('Y-m-d h:i:s');
                $addprofile['modified'] = date('Y-m-d h:i:s');

                $userprofile = new Userprofile($addprofile);
                $userprofile->save();

                $conn->commit();
                $email = $users->email;
                $data['email'] = $users->email;
                $data['password'] = $this->form_validation->set_value('password');
                $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;
                $this->_send_email("admin-activate", $email, $data);
                $this->_show_message("User added successfully", "success");
                redirect('admin/members/list');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
            if ($this->input->post())
                $this->data['postdata'] = (object) $this->input->post();
        }
        $data['postdata'] = (object) $this->input->post();

        $this->data['group'] = Membergroup::find_assoc(array('conditions' => array('status = ?', 1)));
        $this->display_view("admin", $view, $data);
    }

    public function edit($id = "") {
        $view = "user/edit";
        $data['editdata'] = User::find($id);

//        echo'<pre>';print_r($data['editdata']->user_group);exit;

        array_push($this->scripts['js'], "js/datepicker_new.js");
        array_push($this->scripts['css'], "css/datePicker.css");
        $datejs = '$(document).ready(function(){
    $(function() {
        $( "#dob" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        });});';
        array_push($this->scripts['embed'], $datejs);
        $this->validation('edit');

        if ($this->form_validation->run()) {
            $conn = User::connection();
            $conn->transaction();
            try {
                if (isset($id)) {
                    $Editdata['firstname'] = $this->form_validation->set_value('firstname');
                    $Editdata['lastname'] = $this->form_validation->set_value('lastname');
                    $Editdata['email'] = $this->form_validation->set_value('email');
                    if ($this->input->post('password')) {
                        $this->load->library('tank_auth_admin');
                        $Editdata['password'] = $this->tank_auth_admin->create_password($this->form_validation->set_value('password'));
                    }
                    $Editdata['activated'] = 1;
                    $Editdata['modified'] = date("Y-m-d h:i:s");
                    $Editusers = User::find($id);
                    $Editusers->update_attributes($Editdata);

                    $Editprof = Userprofile::find_by_user_id($id);
                    if (isset($_FILES['userfile']) && !empty($_FILES['userfile']) && $_FILES['userfile']['error'] != '4') {
                        $config['upload_path'] = ADMIN_UPLOAD_IMAGE_PATH;
                        $config['max_size'] = '10000';
                        $config['max_height'] = '768';
                        $config['max_width'] = '1024';
                        $config['allowed_types'] = 'jpg|jpeg|png|bmp|gif';

                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('userfile')) {
                            $error = $this->upload->display_errors();
                            $this->_show_message($error, "error");
                        } else {
                            $editdata = $this->upload->data();
                            unset($editdata['is_image']);
                            $userprofile = Userprofile::find_by_user_id($id);
                            $photo_id = $userprofile->photo_id;
                            if (isset($photo_id) && $photo_id != '') {
                                $photo = Photo::find($photo_id);
                                if (isset($photo->file_name) && !empty($photo->file_name)) {
                                    $this->load->helper('file');
                                    $path = ADMIN_UPLOAD_IMAGE_PATH . '/' . $photo->file_name;
//                                    echo '<pre>';print_r($path);exit;
                                    @unlink($path);
                                }
                                $update_photo = Photo::find($photo_id);
                                $update_photo->update_attributes($editdata);
                                $update_photo_id = $update_photo->id;
                                $Editprof->photo_id = $update_photo_id;
                            } else {
                                $photo = new Photo($editdata);
                                $photo->save();
                                $auto_id = $photo->id;
                                $Editprof->photo_id = $auto_id;
                            }
                        }
                    }
                    $Editprof->user_id = isset($Editusers->id) ? $Editusers->id : $Editprof->user_id;
//                    $Editprof->photo_id = isset($update_photo_id) ? $update_photo_id : $Editprof->photo_id;
                    $Editprof->dateofbirth = $this->input->post('dob');
                    $Editprof->gender = $this->input->post('gender');
                    $Editprof->aboutme = $this->input->post('aboutme');
                    $Editprof->telephone = $this->input->post('telephone');
                    $Editprof->mobile = $this->input->post('mobiles');
                    $Editprof->modified = date("Y-m-d h:i:s");
//                    $Editprof->update_attributes($Editprofile);
                    $Editprof->save();
                    $user = User_Group :: find_by_user_id($id);
                    if ($user) {
                        $user->group_id = $this->form_validation->set_value('group_id');
                        $user->updated = date("Y-m-d h:i:s");
                    } else {
                        $user = new User_Group();
                        $user->user_id = $id;
                        $user->group_id = $this->form_validation->set_value('group_id');
                        $user->updated = date("Y-m-d h:i:s");
                        $user->created = date("Y-m-d h:i:s");
                    }

                    $user->save();
                    $conn->commit();
                    $this->_show_message("User updated Successfully", "success");
                    redirect('admin/members/list');
                }
            } catch (Exception $e) {
                $this->write_logs($e);
            }
        } else {
//            echo '<pre>';
//            print_r($this->form_validation->get_field_data());
            if ($this->input->post()) {
                $data['postdata'] = (object) $this->input->post();
                //echo'<pre>';print_r($data['postdata']);exit;
            }
        }
        $data['postdata'] = (object) $this->input->post();
        $this->data['group'] = Membergroup::find_assoc(array('conditions' => array('status = ?', 1)));
        $this->display_view("admin", $view, $data);
    }

    function delete($id = '') {

        if ($this->input->post('option')) {
            $ids = $this->input->post('option');

            $deleteObj = User::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("User deleted", 'success');
        } else {
            if (!$id)
                show_404();
            $deleteObj = User::find($id);
            $userprofile = Userprofile::find_by_user_id($id);
            $photo_id = $userprofile->photo_id;
            if (isset($photo_id) && $photo_id != '') {
                $photo = Photo::find($photo_id);
                if (isset($photo->file_name) && !empty($photo->file_name)) {
                    $this->load->helper('file');
                    $path = ADMIN_UPLOAD_IMAGE_PATH . '/' . $photo->file_name;
//                                    echo '<pre>';print_r($path);exit;
                    @unlink($path);
                }
            }
            $deleteObj->delete();
            $this->_show_message("User deleted", 'success');
        }
        redirect('admin/members/list');
    }

    public function view_details($id = '') {
        // echo'<pre>';print_r($id);exit;
        $view = 'view/view_details';
    }

    function activate($id = '') {
        if (!$id)
            show_404();

        $editArticle = User::find_by_id($id);
        $editArticle->activated = '1';
        $editArticle->save();
        $this->_show_message("User activated successfully", 'success');
        redirect('admin/members/list');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editArticle = User::find_by_id($id);
        $editArticle->activated = '0';
        $editArticle->save();
        $this->_show_message("User deactivated successfully", 'success');
        redirect('admin/members/list');
    }

}

?>
