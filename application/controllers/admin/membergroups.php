<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Membergroups extends MY_admin {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $view = 'membergroup/view';
        $pagetitle = 'Section';
        $data['alldata'] = Membergroup::find('all', array('order' => 'updated desc'));
        //$this->data['userRights'] = $this->check_rights();
        $this->display_view('admin', $view, $data);
    }

    function add() {

//        echo'<pre>';print_r($_POST);exit;
        $view = 'membergroup/add';
        $this->metas['title'] = array('Admin | Member Group | Add');
        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
        $validation = "
                $('document').ready(function(){
                    $('#addadminfrm').validationEngine();
                });";
        array_push($this->scripts['embed'], $validation);
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        if ($this->form_validation->run()) {
            $conn = Membergroup::connection();
            $conn->transaction();
            try {
                $membergroup = new Membergroup();
                $membergroup->group = $this->form_validation->set_value('name');
                $membergroup->status = $this->form_validation->set_value('status');
                $membergroup->created = date('Y-m-d H:i:s');
                $membergroup->updated = date('Y-m-d H:i:s');
                $membergroup->save();
                $conn->commit();
                $this->_show_message('Member Group added successfully', 'success');
                redirect('admin/membergroups/list');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
                redirect('admin/membergroups/list');
            }
        } else {
//            $this->_show_message('Error', 'error');
            if ($this->input->post())
                $data['postdata'] = (object) $this->input->post();
        }
        $data['postdata'] = (object) $this->input->post();
        $this->display_view('admin', $view, $data);
    }

    function edit($id = '') {
        //s echo'<pre>';print_r($id);exit;
        if (!$id)
            show_404();
        /*
         * @var view : view name
         * @title :title of page
         */
        $view = 'membergroup/add';
        $this->metas['title'] = array('Admin | Member Group  | Edit');
        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
        $validation = "
                $('document').ready(function(){
                    $('#addadminfrm').validationEngine();
                });";
        array_push($this->scripts['embed'], $validation);
        $this->data['postdata'] = Membergroup::find(array('conditions' => array('id = ? ', $id)));
        $config = array(
            array('field' => 'name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'status', 'label' => 'Status', 'rules' => 'required'),
        );

        //echo'<pre>';print_r($_POST);exit;
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run()) {
            $conn = Membergroup::Connection();
            $conn->Transaction();
            try {

                $membergroup = Membergroup::find($id);
                $membergroup->group = $this->form_validation->set_value('name');
                $membergroup->status = $this->form_validation->set_value('status');
                $membergroup->updated = date('Y-m-d H:i:s');

                $membergroup->save();
                $conn->commit();
                $this->_show_message('Member Group edited successfully', 'success');
                redirect('admin/membergroups/list');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
                redirect('admin/membergroups/list');
            }
        } else {
            if ($this->input->post()) {
                $this->data['postdata'] = (object) $this->input->post();
                $this->_show_message('Error while Editing', 'error');
            }
        }
        $this->display_view('admin', $view, $this->data);
    }

    function delete($id = '') {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Membergroup::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
//            $this->_show_message("Member Group deleted", 'success');
//            redirect('admin/membergroups/list');
        } else {
            if (!$id)
                show_404();
            $deleteObj = Membergroup :: find($id);
            $deleteObj->delete();
        }
        $this->_show_message("Member Group deleted", 'success');
        redirect('admin/membergroups/list');
    }

    function activate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Membergroup::find_by_id($id);
        $editActivation->status = '1';
        $editActivation->save();
        $this->_show_message("Member Group activated successfully", 'success');
        redirect('admin/membergroups/list');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Membergroup::find_by_id($id);
        $editActivation->status = '0';
        $editActivation->save();
        $this->_show_message("Member Group deactivated successfully", 'success');
        redirect('admin/membergroups/list');
    }

}

?>
