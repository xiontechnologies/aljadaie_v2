<?php

/*
 * @auther Shafiq
 */

class Products extends MY_base {

    function __construct() {
        parent::__construct();
        $this->check_method();
        array_push($this->scripts['css'], 'frontend/css/basic.css', 'frontend/css/style.css', 'frontend/css/jquery_ui.css');
        array_push($this->scripts['js'], "frontend/js/jquery-ui.js", "frontend/js/zoom.js");
        $facebookJs = "(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = '//connect.facebook.net/en_US/all.js#xfbml=1&appId=435451156500782';
                    fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));";
        array_push($this->scripts['embed'], $facebookJs);
    }

    public function index($id = '') {
        $this->data['embed'] = '<script>$(document).ready(function(){
                   $("#zoom_01").elevateZoom({
                cursor: "crosshair",
                borderSize:4,
                scrollZoom : true,
                zoomWindowWidth: 457,
                zoomWindowHeight: 372,
                zoomWindowFadeIn: true,
        zoomWindowFadeOut: true,
        zoomWindowAlwaysShow: true,
        zoomTintFadeIn: true,
        zoomTintFadeOut: true,
            }); 
        $( "#accordion" ) . accordion();
        $(".stock").on("click",function(){
            $(".stcokpopup").toggle();
        });
        });
           $(".avail-color").on("click",function(){
        id = $(this).attr("data");
        $("#dialog").html("<img class=preloader src=' . base_url('/assets/images/loading.gif') . ' />");
        $.ajax({
            url:site_url+"product/ajaxdetails/"+id,
            success:function(data){
                $(".preloader").remove();
              $(".zoomContainer").remove();
                $("#dialog").html(data);
            }
        });
    });
        </script>';
        $this->data['category'] = Category::find('all', array('conditions' => array('status = ? and showinfilter = ? and id != ?', 1, 1, 7)));
        $this->data['product'] = Product::find('first', array('conditions' => array('id = ? and status = ?', $id, 1)));

        $view = "products/ajaxdetails";
        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data, TRUE);
    }

    public function removefromcatalog($id = '') {
        if (!$id)
            show_404();
        $sessionArray = $this->session->userdata(SITE_NAME . '_user_catalog');
        unset($sessionArray[$id]);
        $this->session->set_userdata(SITE_NAME . '_user_catalog', $sessionArray);
        if (count($this->session->userdata(SITE_NAME . '_user_catalog') == 0)) {
            $this->session->set_userdata(SITE_NAME . '_save_user_catalog', array('id' => FALSE, 'name' => FALSE));
        }
    }

    public function addtocatalog($id = '') {
        /*
         * Find id in session calatalog
         * if yes send already register
         * if not then add it and refresh catalog div
         */
        $conn = Product::connection();
        $conn->transaction();
        try {
            if (!$id)
                show_404();
            $view = '/products/ajaxcatalog';
            if ($this->session->userdata(SITE_NAME . '_user_catalog') && array_key_exists($id, $this->session->userdata(SITE_NAME . '_user_catalog'))) {
                $result = '';
            } else if ($this->session->userdata(SITE_NAME . '_user_catalog')) {
                /*
                 * Check wheter catalogue saved to databse or not
                 * if so add this product to database also
                 */
                $product = Product::find('first', array('conditions' => array('id=? and status = ?', $id, 1)));
                if ($this->session->userdata(SITE_NAME . '_save_user_catalog') && $this->session->userdata[SITE_NAME . '_save_user_catalog']['id']) {
                    $catlogproduct = new Catalogueproduct();
                    $catlogproduct->product_id = $product->id;
                    $catlogproduct->catalogue_id = $this->session->userdata[SITE_NAME . '_save_user_catalog']['id'];
                    $catlogproduct->updated = databasedate();
                    $catlogproduct->created = databasedate();
                    $catlogproduct->save();
                    $conn->commit();
                }
                $sessionArray = $this->session->userdata(SITE_NAME . '_user_catalog');
                $this->data['v'] = $sessionArray[$product->id] = array(
                    'id' => $product->id,
                    'sku' => $product->sku,
                    'colorcode' => $product->colorcode,
                    'availablecolor' => count($product->colors),
                    'imagethumbpath' => $product->thumimagepath,
                    'imagepath' => str_replace('/./', '/', base_url(PRODUCTIMAGE_PATH . $product->productphoto->photo->file_name)),
                    'designpath' => $product->designpath,
                    'photoname' => $product->productphoto->photo->file_name,
                );
                $this->session->set_userdata(SITE_NAME . '_user_catalog', $sessionArray);
                $result['view'] = $this->load->view(FRONTENDFOLDERNAME . $view, $this->data, true);
                $result['id'] = $product->id;
            } else {
                /*
                 * New catalog creating
                 */
                $product = Product::find('first', array('conditions' => array('id=? and status = ?', $id, 1)));
                $this->data['v'] = $productCatalog[$product->id] = array(
                    'id' => $product->id,
                    'sku' => $product->sku,
                    'colorcode' => $product->colorcode,
                    'availablecolor' => count($product->colors),
                    'imagethumbpath' => $product->thumimagepath,
                    'imagepath' => str_replace('/./', '/', base_url(PRODUCTIMAGE_PATH . $product->productphoto->photo->file_name)),
                    'designpath' => $product->designpath,
                    'photoname' => $product->productphoto->photo->file_name,
                );
                $this->session->set_userdata(SITE_NAME . '_user_catalog', $productCatalog);
                $result['view'] = $this->load->view(FRONTENDFOLDERNAME . $view, $this->data, true);
                $result['id'] = $product->id;
            }
        } catch (Exception $e) {
            $this->write_logs($e);
            $conn->rollback();
            $result = '';
        }
        print_r(json_encode($result));
    }

    public function catalogsave() {
        try {
            $value = $this->input->get('val');
            $conn = Catalogue::connection();
            $conn->transaction();
            if ($value && $this->session->userdata(SITE_NAME . '_user_catalog') && $this->session->userdata(SITE_NAME . '_user_data')) {
                $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];

                $catalog = new Catalogue();
                $catalog->title = $value;
                $catalog->updated = $catalog->created = databasedate();
                $catalog->save();

                $usercatalogue['catalogue_id'] = $catalog->id;
                $usercatalogue['user_id'] = $user_id;
                $usercatalogue['created'] = databasedate();
                $usercatalogue['updated'] = databasedate();
                $catalogue = new Usercatalogue($usercatalogue);
                $catalogue->save();

                foreach ($this->session->userdata(SITE_NAME . '_user_catalog') as $k => $v) {
                    $catlogproduct = new Catalogueproduct();
                    $catlogproduct->product_id = $v['id'];
                    $catlogproduct->catalogue_id = $catalog->id;
                    $catlogproduct->updated = $catlogproduct->created = databasedate();
                    $catlogproduct->save();
                }
                $conn->commit();
                $this->session->set_userdata(SITE_NAME . '_save_user_catalog', array('id' => $catalog->id, 'name' => $value));
                echo '1';
            } else {
                echo '';
            }
        } catch (Exception $e) {
            $this->write_logs($e);
            $conn->rollback();
            echo '';
        }
    }

    public function ajaxview() {
        try {
            $offset = $this->input->get('pagintation') ? $this->input->get('pagintation') : '0';
            $value = $this->input->get('val');
            $this->data['type'] = $this->input->get('type');
            $variation = $this->input->get('variation');

            if ($variation && $value) {
                $join = "left join productvariations on products.id = productvariations.product_id left join catalogueproducts on products.id = catalogueproducts.product_id left join catalogues on catalogues.id =  catalogueproducts.product_id left join subcategories on productvariations.subcategory_id = subcategories.id ";
                $cond = array("productvariations.subcategory_id in (?) and products.status = ? and products.title LIKE '%" . $value . "%' or products.description LIKE '%" . $value . "%' or products.shortdescription LIKE '%" . $value . "%' or products.sku LIKE '%" . $value . "%' or products.colorcode LIKE '%" . $value . "%' or products.price LIKE '%" . $value . "%' or products.weight LIKE '%" . $value . "%' or products.washcare LIKE '%" . $value . "%' or products.tags LIKE '%" . $value . "%' or subcategories.name LIKE '%" . $value . "%' or catalogues.title LIKE '%" . $value . "%'", 1, $variation);
                $data['total'] = count(Product::find('all', array('select' => 'distinct products.*', 'joins' => $join, 'conditions' => $cond)));
                $this->data['product'] = Product::find('all', array('select' => 'distinct products.*', 'joins' => $join, 'conditions' => $cond, 'order' => 'collection asc'));
            } else if ($variation) {
                $data['total'] = count(Productvariation::find('all', array('select' => 'distinct product_id', 'conditions' => array('subcategory_id in (?)', $variation))));
                $this->data['product'] = Productvariation::find('all', array('select' => 'distinct product_id', 'conditions' => array('subcategory_id in (?)', $variation), 'limit' => LIMIT, 'offset' => $offset));
                $this->data['post'] = TRUE;
            } else if (is_array($value) && !empty($value)) {
                $where = "products.status = '1'";
                $this->session->set_userdata('search', $value);
                foreach ($this->session->userdata('search') as $k => $v) {
                    if ($v['name'] != 'csrf_test_name' && $v['value'] != '') {
                        $name = $v['name'];
                        $value = $v['value'];
                        if ($name == 'colors') {
                            $where .= " and  concat(primary_color,secondary_color) = $value";
                        } else {
                            $where .= " and  $name = $value";
                        }
                    }
                }
//                echo '<pre>';print_r($where);exit;
//                $join = "left join productvariations on products.id = productvariations.product_id left join catalogueproducts on products.id = catalogueproducts.product_id left join catalogues on catalogues.id =  catalogueproducts.product_id left join subcategories on productvariations .subcategory_id = subcategories.id ";
                $data['total'] = count(Product::find('all', array('select' => 'distinct products.*', 'conditions' => $where)));
                $this->data['product'] = Product::find('all', array('select' => 'distinct products.*', 'conditions' => $where, 'limit' => LIMIT, 'offset' => $offset, 'order' => 'collection asc'));
            } else if ($value) {
                $join = "left join productvariations on products.id = productvariations.product_id left join catalogueproducts on products.id = catalogueproducts.product_id left join catalogues on catalogues.id =  catalogueproducts.product_id left join subcategories on productvariations .subcategory_id = subcategories.id ";
//                $cond = "products.title LIKE '%" . $value . "%' or products.description LIKE '%" . $value . "%' or products.shortdescription LIKE '%" . $value . "%' or products.sku LIKE '%" . $value . "%' or products.colorcode LIKE '%" . $value . "%' or products.price LIKE '%" . $value . "%' or products.weight LIKE '%" . $value . "%' or products.washcare LIKE '%" . $value . "%' or products.tags LIKE '%" . $value . "%'or subcategories.name LIKE '%" . $value . "%' or catalogues.title LIKE '%" . $value . "%'";
                $cond = array("products.status = ? and products.title LIKE '%" . $value . "%' or products.description LIKE '%" . $value . "%' or products.shortdescription LIKE '%" . $value . "%' or products.sku LIKE '%" . $value . "%' or products.colorcode LIKE '%" . $value . "%' or products.price LIKE '%" . $value . "%' or products.weight LIKE '%" . $value . "%' or products.washcare LIKE '%" . $value . "%' or products.tags LIKE '%" . $value . "%'or subcategories.name LIKE '%" . $value . "%' or catalogues.title LIKE '%" . $value . "%'", 1);
                $data['total'] = count(Product::find('all', array('select' => 'distinct products.*', 'joins' => $join, 'conditions' => $cond)));
                $this->data['product'] = Product::find('all', array('select' => 'distinct products.*', 'joins' => $join, 'conditions' => $cond, 'limit' => LIMIT, 'offset' => $offset, 'order' => 'collection asc'));
            } else if ($this->session->userdata('search')) {
                $where = "products.status = '1'";
                foreach ($this->session->userdata('search') as $k => $v) {
                    if ($v['name'] != 'csrf_test_name' && $v['value'] != '') {
                        $name = $v['name'];
                        $value = $v['value'];
                        if ($name == 'colors') {
                            $where .= " and  concat(primary_color,secondary_color) = $value";
                        } else {
                            $where .= " and  $name = $value";
                        }
                    }
                }
//                echo '<pre>';print_r($where);exit;
//                $join = "left join productvariations on products.id = productvariations.product_id left join catalogueproducts on products.id = catalogueproducts.product_id left join catalogues on catalogues.id =  catalogueproducts.product_id left join subcategories on productvariations .subcategory_id = subcategories.id ";
                $data['total'] = count(Product::find('all', array('select' => 'distinct products.*', 'conditions' => $where)));
                $this->data['product'] = Product::find('all', array('select' => 'distinct products.*', 'conditions' => $where, 'limit' => LIMIT, 'offset' => $offset, 'order' => 'collection asc'));
            } else {
                $this->data['product'] = Product::find('all', array('limit' => LIMIT, 'offset' => $offset, 'conditions' => array('status = ?', 1), 'order' => 'collection asc'));
                $data['total'] = Product::count(array('conditions' => array('status = ?', 1)));
            }
            $view = FRONTENDFOLDERNAME . "/products/ajaxpagination";
            $data['page'] = $data['offset'] = LIMIT + $offset;
            if ($data['offset'] >= $data['total']) {
                $data['page'] = $data['total'];
            }
            $data['view'] = $this->load->view($view, $this->data, true);
            $this->output->set_output(json_encode($data));
        } catch (Exception $e) {
            $this->write_logs($e);
        }
    }

    public function catalognew() {
        $this->session->unset_userdata(SITE_NAME . '_user_catalog');
        $this->session->unset_userdata(SITE_NAME . '_save_user_catalog');
    }

    public function imagedetails() {
        print_r(json_encode(getimagesize($_REQUEST['imge'])));
    }

    public function cart($id = '') {
        $this->session->unset_userdata(SITE_NAME . '_product_id');
        $this->session->set_userdata(SITE_NAME . '_product_id', $id);
        $data['product'] = Product::find_by_id($id);
        $view = "/products/cart";
        $this->load->view(FRONTENDFOLDERNAME . $view, $data);
    }

    public function favorities() {

        $userdata = $this->session->userdata(SITE_NAME . '_user_data');

        if ($userdata) {
            $conn = Favorite::connection();
            $conn->transaction();
            $product_id = $this->input->get('product_id');
            try {
                $exist = Favorite::find('first', array('conditions' => array('user_id = ? AND product_id = ?', $userdata['user_id'], $product_id)));
                if (!$exist) {
                    $fav = new Favorite();
                    $fav->product_id = $this->input->get('product_id');
                    $fav->user_id = $userdata['user_id'];
                    $fav->created = databasedate();
                    $fav->updated = databasedate();

                    $fav->save();
                    $conn->commit();
                    $result['id'] = $fav->id;
                } else {
                    $result = array('error' => '1', 'message' => 'already in favorite');
                }
            } catch (Exception $e) {
                $this->write_logs($e);
                $result = array('error' => '1', 'message' => 'Database error');
            }
        } else {
            $result = array('error' => '1', 'message' => 'Please login');
        }
        print_r(json_encode($result));
    }

    function storyboardsave() {
        try {
            $value = $this->input->get('val');
            $conn = Storyboard::connection();
            $conn->transaction();
            if ($value && $this->session->userdata(SITE_NAME . '_story_board') && $this->session->userdata(SITE_NAME . '_user_data')) {
                $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];

                /*
                 * Save story board into database
                 */
                $temp = json_decode($this->session->userdata(SITE_NAME . '_story_board'));
                foreach ($temp as $k => $v) {
                    $storyboard = new Storyboard();
                    $storyboard->user_id = $user_id;
                    $storyboard->name = $value;
                    $storyboard->stylename = $v->style;
                    $storyboard->canvasurl = $v->canvasUrl;
                    $storyboard->created = databasedate();
                    $storyboard->updated = databasedate();
                    $storyboard->save();
                    foreach ($v->fabrics as $key => $val) {
                        $storyboard_surface = new Storyboardsurface();
                        $storyboard_surface->story_id = $storyboard->id;
                        $storyboard_surface->product_id = $val;
                        $storyboard_surface->surface = $key;
                        $storyboard_surface->created = databasedate();
                        $storyboard_surface->updated = databasedate();
                        $storyboard_surface->save();
                    }
                    $temp[$k]->database_id = $storyboard->id;
                }
                $conn->commit();
                $this->session->unset_userdata(SITE_NAME . '_story_board');
                $this->session->set_userdata(SITE_NAME . '_story_board', json_encode($temp));
                $this->session->set_userdata(SITE_NAME . '_save_user_storyboard', array('id' => $storyboard->id, 'name' => $value));
                echo '1';
            } else {
                echo '';
            }
        } catch (Exception $e) {
            $this->write_logs($e);
            $conn->rollback();
            echo '';
        }
    }

    function searchBySku() {
        if ($id = $this->input->get('barcode')) {
            $result = Product::find('first', array('conditions' => array("status = ?  and `title` LIKE '%$id%' or `sku` LIKE '%$id%' ", 1)));
            if ($result) {
                $this->addcatsession($result);
            } else {
                print_r(json_encode(array('result' => '1')));
            }
        }
    }

    public function addcatsession($product) {
        $view = '/products/ajaxcatalog';
        if ($this->session->userdata(SITE_NAME . '_user_catalog') && array_key_exists($product->id, $this->session->userdata(SITE_NAME . '_user_catalog'))) {
            $result = '';
        } else if ($this->session->userdata(SITE_NAME . '_user_catalog')) {
            /*
             * Check wheter catalogue saved to databse or not
             * if so add this product to database also
             */
            if ($this->session->userdata(SITE_NAME . '_save_user_catalog') && $this->session->userdata[SITE_NAME . '_save_user_catalog']['id']) {
                $catlogproduct = new Catalogueproduct();
                $catlogproduct->product_id = $product->id;
                $catlogproduct->catalogue_id = $this->session->userdata[SITE_NAME . '_save_user_catalog']['id'];
                $catlogproduct->updated = databasedate();
                $catlogproduct->created = databasedate();
                $catlogproduct->save();
                $conn->commit();
            }
            $sessionArray = $this->session->userdata(SITE_NAME . '_user_catalog');
            $this->data['v'] = $sessionArray[$product->id] = array(
                'id' => $product->id,
                'sku' => $product->sku,
                'colorcode' => $product->colorcode,
                'price' => $product->price,
                'availablecolor' => count($product->colors),
                'imagethumbpath' => $product->thumimagepath,
                'imagepath' => str_replace('/./', '/', base_url(PRODUCTIMAGE_PATH . $product->productphoto->photo->file_name)),
                'designpath' => $product->designpath,
                'photoname' => $product->productphoto->photo->file_name,
                'test_report' => isset($product->product_test) ? $product->product_test->document->file_name : ''
            );
            $this->session->set_userdata(SITE_NAME . '_user_catalog', $sessionArray);
            $result['view'] = $this->load->view(FRONTENDFOLDERNAME . $view, $this->data, true);
            $result['id'] = $product->id;
        } else {
            /*
             * New catalog creating
             */
            $this->data['v'] = $productCatalog[$product->id] = array(
                'id' => $product->id,
                'sku' => $product->sku,
                'price' => $product->price,
                'colorcode' => $product->colorcode,
                'availablecolor' => count($product->colors),
                'imagethumbpath' => $product->thumimagepath,
                'imagepath' => str_replace('/./', '/', base_url(PRODUCTIMAGE_PATH . $product->productphoto->photo->file_name)),
                'designpath' => $product->designpath,
                'photoname' => $product->productphoto->photo->file_name,
                'test_report' => isset($product->product_test) ? $product->product_test->document->file_name : '',
            );
            $this->session->set_userdata(SITE_NAME . '_user_catalog', $productCatalog);
            $result['view'] = $this->load->view(FRONTENDFOLDERNAME . $view, $this->data, true);
            $result['id'] = $product->id;
        }
        print_r(json_encode($result));
    }

    function newstoryboard() {
        $this->session->unset_userdata(SITE_NAME . '_story_board');
        $this->session->unset_userdata(SITE_NAME . '_save_user_storyboard');
    }

}
