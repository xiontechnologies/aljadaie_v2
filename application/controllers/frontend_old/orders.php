<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Orders extends My_front {

    function __construct() {
        parent::__construct();
        $this->_api_require_user();
    }

    function index() {
        $user_id = $this->session->userdata[SITE_NAME . '_user_data']['user_id'];
        $this->data['allorders'] = Order :: find('all', array('conditions' => array('user_id = ?', $user_id), 'order' => 'updated_date desc'));
        $view = "order/orderlisting";
        $this->display_view('frontend', $view, $this->data);
    }

    function productlisting($order_id = '') {
        $view = "order/productlisting";
        $data['allproducts'] = Order :: find('all', array('conditions' => array('id=?', $order_id), 'order' => 'updated_date desc'));
        $this->display_view('frontend', $view, $data);
    }

}

?>
