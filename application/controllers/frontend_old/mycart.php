<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mycart extends My_front {

    function __construct() {
        parent::__construct();
        $this->_api_require_user(array('only' => array('checkout', 'addaddress', 'confirm')));
    }

    function index() {
        $view = "/cart/cartlisting";
        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    function update() {
        if ($this->input->post('update')) {
            $data = $this->input->post();
            unset($data['update']);
            $this->cart->update($data);
        } else if ($this->input->post('delete') && $this->input->post('option')) {
            foreach ($this->input->post('option') as $k => $v) {
                $data = array(
                    'rowid' => $v,
                    'qty' => 0
                );
                $this->cart->update($data);
                $array = $this->session->userdata(SITE_NAME . '_productIds');
                if (isset($array[$v])) {
                    unset($array[$v]);
                }
                $this->session->set_userdata(SITE_NAME . '_productIds', $array);
            }
        }
        redirect('cartlist');
    }

    function addtocart($id = '') {
        $qty = $this->input->get('qty') ? $this->input->get('qty') : '1';
        $product = Product::find($id);
        $data = array(
            'id' => $id,
            'qty' => $qty,
            'price' => $product->price,
            'name' => $product->title,
            'options' => array('file_name' => $product->photo->file_name, 'sku' => $product->sku, 'colorcode' => $product->colorcode)
        );
        $this->cart->product_name_rules = '\.\:\'\-_ a-z0-9';
        if ($rowid = $this->cart->insert($data)) {
            $array = $this->session->userdata(SITE_NAME . '_productIds');
            if ($array) {
                $array[$rowid] = $id;
            } else {
                $array = array($rowid => $id);
            }
            $this->session->set_userdata(SITE_NAME . '_productIds', $array);
            print_r(json_encode(array('result' => '1')));
        } else {
            print_r(json_encode(array('result' => '')));
        }
    }

    function checkout() {
        $userdata = $this->session->userdata(SITE_NAME . '_user_data');
        if ($userdata) {
            $this->data['user'] = User::find($userdata['user_id']);
            $view = 'cart/address';
            $this->display_view(FRONTENDFOLDERNAME, $view, $this->data);
        } else {
            $this->session->set_userdata(SITE_NAME . '_cart_redirect', '1');
            redirect('login');
        }
    }

    function addaddress() {
        $billingAddress = $this->input->post('billing');
        if ($this->input->post('sameaddress')) {
            $shippingAddress = $billingAddress;
        } else if ($this->input->post('shipping')) {
            $shippingAddress = $this->input->post('shipping');
        } else {
            $shippingAddress = $billingAddress;
        }
        $this->session->set_userdata('billingaddress', $billingAddress);
        $this->session->set_userdata('shippinggaddress', $shippingAddress);
        redirect('cart/confirm');
    }

    function confirm() {
        $view = "/cart/confirmcart";
        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    function delete($id = '') {
        $data = array(
            'rowid' => $id,
            'qty' => 0
        );
        $this->cart->update($data);
        $array = $this->session->userdata(SITE_NAME . '_productIds');
        unset($array[$id]);
        $this->session->set_userdata(SITE_NAME . '_productIds', $array);
//        $dataTmp = $this->cart->contents();
//        foreach ($dataTmp as $item) {
//            if ($item['id'] == $id) {
//                $qtyn = 0;
//                $data = array(
//                    'rowid' => $id,
//                    'qty' => $qtyn
//                );
//                $this->cart->update($data);
//                break;
//            }
//        }
        redirect('cartlist');
    }

    function applycoupon() {

        $userdata = $this->session->userdata(SITE_NAME . '_user_data');
        $user = User :: find('first', array('conditions' => array('id=?', $userdata['user_id'])));
        if ($this->input->get('coupon')) {
//            $conn = Coupon :: connection();
//            $conn->transaction();
            try {
                $coupon = Coupon :: find('first', array('conditions' => array('code =?', $this->input->get('coupon'))));

//                $result = array('success' => '1', 'discount' => $discount, 'cartamount' => $total, 'finalprice' => $totalamount);
                if ($coupon) {
                    if ($coupon->user_id == $user->id || ($coupon->user_group_id == $user->user_group[0]->group_id)) {
                        $current = date('Y-m-d H:i:s');
                        $total = $this->cart->total();
                        if ($coupon->no_of_time != 0 && (date("Y-m-d", strtotime($coupon->to_date)) >= $current) && ($current >= date("Y-m-d", strtotime($coupon->from_date))) && $coupon->to_amount <= $total && $coupon->from_amount >= $total) {
                            if ($coupon->discount_type == 1) {   //for discount type percent
                                $discount = $total * ($coupon->amount / 100);
                                $totalamount = $total - $discount;
                            } else if ($coupon->discount_type == 0) {   //for discount type amount
                                $discount = $coupon->amount;
                                $totalamount = $total - $discount;
                            }
                            $this->session->set_userdata(SITE_NAME . '_discount', $discount);
                            $result = array('success' => '', 'discount' => $discount, 'cartamount' => $total, 'finalprice' => $totalamount);
                        } else {
                            $result = array('error' => 'Invalid Coupon');
                        }
                    } else {
                        $result = array('error' => 'Invalid Coupon');
                    }
                } else {
                    $result = array('error' => 'Invalid Coupon');
                }
            } catch (Exception $e) {
                $this->write_logs($e);
                $result = array('error' => 'Sorry we cannot process with coupan right now');
            }
        } else {
            $result = array('error' => 'Enter coupan');
        }
        print_r(json_encode($result));
//        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    function confirmorder() {
        $userdata = $this->session->userdata(SITE_NAME . '_user_data');
        $billingaddress = $this->session->userdata('billingaddress');
        $dataTmp = $this->cart->contents();
        $shipping = implode(',', $this->session->userdata('shippinggaddress'));

        if ($dataTmp) {
            $conn = Order::connection();
            $conn->transaction();
            try {
                $discount = 0;
                if ($this->session->userdata('discount')) {
                    $discount = $this->session->userdata('discount');
                }
                $total_amount = ($this->cart->total() - $discount);
                $user = User :: find('first', array('conditions' => array('id = ?', $userdata['user_id'])));
                $order = new Order();
                $order->user_id = $userdata['user_id'];
                $order->first_name = $user->firstname;
                $order->last_name = $user->lastname;
                $order->email = $user->email;
                $order->company = $billingaddress['company'];
                $order->fax = $billingaddress['fax'];
                $order->address = $billingaddress['address'];
                $order->state = $billingaddress['state'];
                $order->country = $billingaddress['country'];
                $order->order_status = 'pending';
                $order->transaction_id = 1;
                $order->total_amt = $total_amount;
                $order->city = $billingaddress['city'];
                $order->postal_code = $billingaddress['postal'];
                $order->phone_number = $billingaddress['telephone'];
                $order->shipping_address = $shipping;
                $order->created_date = databasedate();
                $order->updated_date = databasedate();
                $order->save();
                foreach ($dataTmp as $item) {
                    $orderdetail = new Orderdetail();
                    $orderdetail->order_id = $order->id;
                    $orderdetail->product_id = $item['id'];
                    $orderdetail->quantity = $item['qty'];
                    $orderdetail->created_date = databasedate();
                    $orderdetail->updated_date = databasedate();
                    $orderdetail->save();
                }
                $conn->commit();
                $this->cart->destroy();
                $orderNumber = date("Ymd", strtotime($order->created_date)) . '000' . $order->id;

                /*
                 * Sending mail one to admin and other one to usr
                 */

                $this->session->unset_userdata('billingaddress');
                $this->session->unset_userdata('shippinggaddress');
                $this->session->unset_userdata('discount');
                $this->session->unset_userdata(SITE_NAME . '_productIds');

                $data['username'] = $user->fullname;
                $data['useremail'] = $user->email;
                $data['status'] = $order->order_status;
                $data['ordernumber'] = $orderNumber;
                $data['billingaddress'] = $order->address;
                $data['shippingaddress'] = $shipping;
                $data['created_date'] = $order->created_date;
                $data['product'] = Orderdetail::find('all', array('conditions' => array('order_id = ?', $order->id)));
                $data['order'] = Order::find(array('conditions' => array('id = ?', $order->id)));
                $this->_send_email('order_place', $data['useremail'], $data);
                $this->_send_email('admin_order_place', ADMIN_MAIL, $data);
                $this->_show_message("Your order is placed successfully your order number is $orderNumber", 'success');

//                $this->load->view('email/admin_order_place-html', $data);
            } catch (Exception $e) {
                $conn->rollback();
                $this->write_logs($e);
                $this->_show_message('Error while placing order', 'error');
            }
            redirect('home/orders');
        } else {
            redirect();
        }
    }

    function coupan_apply() {
        /*
         * Error
         * 1:coupon Number is not given
         * 2:Invalide coupon number
         * 3:Expire coupon Out of date
         * 4:Expire coupon already used (only one user)
         * 5:Expire coupon amount is not valid
         * 6:Not for this user
         * 7:Expire coupon used number of times
         * 8:If cart total is less then discount amount
         * 9:Trash coupon
         */
        $result = '';
        if ($this->input->get('coupon')) {
//            $coupon = Coupon::find('first', array('conditions' => array('code = ? and trash = ?', $id, 0)));
            $coupon = Coupon::find_by_code($this->input->get('coupon'));
            $result = $this->validate_coupon($coupon);
        } else {
            /**
             * Didn't get input
             */
            $result = json_encode(array('error' => 1, 'message' => 'Please enter the coupon value'));
        }
        if ($result) {
            $this->session->set_userdata('discount', 0);
            $this->session->set_userdata('discountCouponID', 0);
        } else {
            /*
             * If discount in percentage
             */
            if ($coupon->discount_type) {
                $array = $this->get_percentage_amount($coupon);
                $amount = $array['amount'];
                $total = $array['total'];
                if ($total < $amount) {
                    $result = json_encode(array('error' => 8, 'message' => 'Oops !Coupon discount is greater then cart amount'));
                } else {
                    $this->session->set_userdata('discount', $amount);
                    $this->session->set_userdata('discountCouponID', $coupon->id);
                    $result = json_encode(array('success' => '1', 'discount' => $amount, 'cartamount' => $total, 'finalprice' => ($total - $amount)));
                    //   $result = json_encode(array('success' => 1, 'message' => 'Coupon is applying to cart amount'));
                    $coupon->no_of_used = $coupon->no_of_used + 1;
                    $coupon->save();
                }
            } else {
                /**
                 * Cart total is less then discount amount
                 */
                $total = $this->cart->total();
                if ($total < $coupon->amount) {
                    $result = json_encode(array('error' => 8, 'message' => 'Oops !Coupon discount is greater then cart amount'));
                } else {
                    $this->session->set_userdata('discount', $coupon->amount);
                    $this->session->set_userdata('discountCouponID', $coupon->id);
                    $result = json_encode(array('success' => '1', 'discount' => $coupon->amount, 'cartamount' => $total, 'finalprice' => ($total - $coupon->amount)));
                    $coupon->no_of_used = $coupon->no_of_used + 1;
                    $coupon->save();
                }
            }
        }
        print_r($result);
    }

    function validate_coupon($coupon) {
        $result = '';
        if ($coupon) {
            /**
             * coupon found in database
             */
            if ($coupon->type) {

                /*
                 * Multiple user
                 */
                /*
                 * Check for date range
                 */
                if (!$this->date_range($coupon)) {

                    $result = json_encode(array('error' => 3, 'message' => 'Coupon date Expire'));
                }
                /*
                 * Check for amount range
                 */
                if (!$this->amount_range($coupon)) {
                    $result = json_encode(array('error' => 5, 'message' => 'Coupon is not valide for this ammount'));
                }
                /*
                 * Check for particular user
                 */
                if (!$this->check_user_coupon($coupon)) {
                    $result = json_encode(array('error' => 6, 'message' => 'Coupon is not valide for you'));
                }
                /*
                 * Already used no of time
                 */
                if (!$this->no_of_time_used($coupon)) {
                    $result = json_encode(array('error' => 7, 'message' => 'Coupon is already used no of times'));
                }
            } else {
                /**
                 * only one user can used one coupon
                 * value from database
                 * 0 for multiple 
                 * 1 for single
                 */
                if ($coupon->no_of_used) {
                    /**
                     * only one user can used one coupon
                     */
                    $result = json_encode(array('error' => 4, 'message' => 'Coupon already used'));
                } else {
                    /**
                     * Coupon can be used 
                     */
                    /*
                     * Check for date range
                     */
                    if (!$this->date_range($coupon)) {
                        $result = json_encode(array('error' => 3, 'message' => 'Coupon date Expire'));
                    }
                    /*
                     * Check for amount range
                     */

                    if (!$this->amount_range($coupon)) {

                        $result = json_encode(array('error' => 5, 'message' => 'Coupon is not valide for this ammount'));
                    }
                    /*
                     * Check for particular user
                     */
                    if (!$this->check_user_coupon($coupon)) {
                        $result = json_encode(array('error' => 6, 'message' => 'Coupon is not valide for you'));
                    }
                    /*
                     * Already used no of time
                     */
                    if (!$this->no_of_time_used($coupon)) {
                        $result = json_encode(array('error' => 7, 'message' => 'Coupon is already used no of times'));
                    }
                }
            }
        } else {
            /**
             * coupon not found in database
             */
            $result = json_encode(array('error' => 2, 'message' => 'Coupon number is invalide'));
        }
        return $result;
    }

    function date_range($coupon) {
        $flag = false;
        if ($coupon->to_date && $coupon->from_date) {
            if (date('Y-m-d', strtotime($coupon->to_date)) <= date('Y-m-d', strtotime(date('Y-m-d H:i:s'))) && date('Y-m-d', strtotime($coupon->from_date)) >= date('Y-m-d', strtotime(date('Y-m-d H:i:s')))) {
                $flag = true;
                return $flag;
            } else {
                $flag = false;
                return $flag;
            }
        } else {
            $flag = true;
        }
        if ($coupon->to_date) {
            if (date('Y-m-d', strtotime($coupon->to_date)) <= date('Y-m-d', strtotime(date('Y-m-d H:i:s')))) {
                $flag = true;
            } else {
                $flag = false;
            }
        } else {
            $flag = true;
        }
        if ($flag) {
            if ($coupon->from_date) {
                if (date('Y-m-d', strtotime($coupon->from_date)) >= date('Y-m-d', strtotime(date('Y-m-d H:i:s')))) {
                    $flag = true;
                } else {
                    $flag = false;
                }
            } else {
                $flag = true;
            }
        }

        return $flag;
    }

    function amount_range($coupon) {

        $total_price = $this->cart->total();

        if ($coupon->to_amount && $coupon->from_amount) {
            if (($total_price > $coupon->to_amount ) && ($total_price < $coupon->from_amount)) {
                return TRUE;
            }
        } else {
            $flag = FALSE;
        }
        $flag = false;
        if ($coupon->to_amount) {
            if ($total_price > $coupon->to_amount) {
                return TRUE;
            }
        }
        if ($coupon->from_amount) {
            if ($total_price < $coupon->from_amount) {
                return TRUE;
            }
        }

        return $flag;
    }

    function check_user_coupon($coupon) {
        /*
         * For particular user
         */
        $userdata = $this->session->userdata(SITE_NAME . '_user_data');
        $user = User :: find('first', array('conditions' => array('id=?', $userdata['user_id'])));
        if ($coupon->user_id) {

            if ($coupon->user_id == $user->id)
                return true;
            else
                return false;
        }else if ($coupon->user_group_id) {
            if ($coupon->user_id == $user->user_group[0]->group_id)
                return true;
            else
                return false;
        }else {
            return true;
        }
    }

    function no_of_time_used($coupon) {
        /*
         * 0 for unlimited use 
         * other wise numbers
         */
        if (!$coupon->no_of_time) {
            return true;
        }
        if ($coupon->no_of_time >= $coupon->no_of_used) {
            return true;
        }
        return false;
    }

    function get_percentage_amount($coupon) {
        $total_price = $this->cart->total();
        $amount = (($coupon->amount * $total_price) / 100);
        return array('amount' => $amount, 'total' => $total_price);
    }

}

?>
