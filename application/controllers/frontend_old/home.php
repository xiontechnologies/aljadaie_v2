<?php

/*
 * @auther Shafiq
 */

class Home extends MY_base {

    function __construct() {
        parent::__construct();
        array_push($this->scripts['css'], 'frontend/css/basic.css', 'frontend/css/template.css', 'frontend/css/style.css', 'frontend/css/jquery_ui.css');
        array_push($this->scripts['js'], "frontend/js/jquery-ui.js", "frontend/js/zoom.js", "frontend/js/jquery.slides.min.js", "frontend/js/jquery.tooltipster.js");
    }

    public function index() {

        array_push($this->scripts['js'], "frontend/js/visualizer.js");
        $this->data['category'] = Category::find('all', array('conditions' => array('status = ? and showinfilter = ?', 1, 1), 'include' => 'filtersubcategory'));
        $this->data['product'] = Product::find('all', array('limit' => LIMIT, 'conditions' => array('status=?', 1), 'order' => 'collection asc'));
        $this->data['offset'] = LIMIT;
        $this->data['total'] = Product::count(array('conditions' => array('status=?', 1)));
        $this->data['slider'] = Slider::find('all', array('limit' => 5));
        $this->data['page'] = LIMIT;
        if ($this->data['total'] < LIMIT)
            $this->data['page'] = $this->data['total'];

        /*      Add style category   * */
        $join = "left join style_categories on style_categories.id = style_images.category_id";
        $condition = "style_categories.status = '1' and style_images.status = '1'";
        $this->data['style_images'] = StyleImage::all(array('joins' => $join, 'conditions' => $condition, 'include' => 'photo'));

        $this->data['style_total'] = StyleImage::count(array('joins' => $join, 'conditions' => $condition));
        $this->data['style_page'] = LIMIT;
        if ($this->data['style_total'] < LIMIT)
            $this->data['style_page'] = $this->data['style_total'];
        $this->session->set_userdata(SITE_NAME . '_current_currency', 'SAR');
        $this->data['style_category'] = StyleCategory::find('all', array('conditions' => array('status=?', 1), 'order' => 'updated desc'));
        $view = "home/index";
        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    public function currency_convertor($country) {
        $this->session->set_userdata('change-currency-to', $country);
        echo 1;
    }

    public function advsearch() {
        $variation = $this->input->post('variation');
        if ($variation) {
            $this->data['product'] = Productvariation::find('all', array('select' => 'distinct product_id', 'conditions' => array('subcategory_id in (?)', $variation)));
            $this->data['post'] = TRUE;
        } else {
            $this->data['product'] = Product::find('all', array('conditions' => array('status = ?', 1)));
        }

        array_push($this->scripts['js'], "frontend/js/visualizer.js");
        $view = "visualizer/index";
        $this->display_view(FRONTENDFOLDERNAME, $view, '');
    }

    public function visualizer($id = '') {
        if (!$id)
            show_404();

        $product = Product::find('first', array('conditions' => array('id =? and status =?', $id, 1)));
        $path = $product->designpath;
        array_push($this->scripts['embed'], "$('document').ready(function(){
            $('#canvaspreloader').show();
            $('#canvas').hide();
            $('#canvas').attr('src','');
                $('.selectedfabric').attr('data','" . $path . "');
                //                var ctx = document.getElementById('canvas').getContext('2d');
                                var img = new Image();
                img.src = 'http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&on=0&fn=suit&tn0=$path&tw0=50&th0=50&q=100&h=500';
                //img.src = 'http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&on=0&fn=suit&tn0=http://97.107.132.91/demo/chhapega/uploads/abc_logo.jpg&tw0=50.454&th0=50.47&q=100&h=500&w=500';
                console.log(img.src);
                $('#canvas').attr('src',img.src).load(function() {  
                $('#canvaspreloader').hide();
                $('#canvas').show();
            });
                //img.onload = function () { ctx.drawImage(img,0,0);}
               });");
        $this->data['product'] = Product::find('all', array('conditions' => array('status=?', 1)));
        array_push($this->scripts['js'], "frontend/js/visualizer.js");
        $view = "visualizer/index";
        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    public function downloadpdf() {
        $this->load->helper("file");
        if ($this->session->userdata(SITE_NAME . '_user_catalog') && $this->session->userdata(SITE_NAME . '_save_user_catalog')) {
            $this->data['catalogue'] = Catalogue::find('first', array('conditions' => array('id = ? ', $this->session->userdata[SITE_NAME . '_save_user_catalog']['id'])));
            $this->data['category'] = Category::find('all');
            $catalogue = $this->data['catalogue'];
            $category = $this->data['category'];
            $this->load->library('Pdf');
            $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetAutoPageBreak(true);
            $pdf->setPrintHeader(true);
            $pdf->setPrintFooter(true);
            $pdf->SetAuthor(SITE_NAME);
            $pdf->SetFont('helvetica', '', 9);
            /*             * *************Changes by nlesh ******************** */
            $pdf->startPageGroup();
            $pdf->AddPage();

            foreach ($catalogue->catalogueproducts as $k => $v) {
                $tbl = '<table width="520px" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1">';
                if ($k == 0) {
                    $tbl.='<tr><td class="header"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr> 
           <td><img src="' . base_url('assets/frontend/images/logo.png') . '" alt="" style="height:80px" title="" /></td><td>
                  <h1 style="margin:0; padding:0"><br />' . $catalogue->title . '</h1></td></tr></table></td></tr>';
                }
                $tbl.='<tr><td>
                <div style="color:#666; margin:0; padding:0; font-size:21px; font-weight:bold">' . $v->product->title . '</div>
                <span style="color:#6e6e70; margin:0; padding:0">SKU: ' . $v->product->sku . ' - ' . $v->product->colorcode . '</span>
            </td></tr><tr><td><img src = "' . $v->product->imagepath . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" /></td>
</tr><tr><td><table cellpadding = "10" width = "100%" cellspacing = "0" ><tr><td width = "350" valign = "top">
<h3 style = "font-size:14px;border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">TECHNICAL SPECIFICATIONS</h3><br><table cellpadding = "5" cellspacing = "0">';
                foreach ($v->product->productvariation as $kvariation => $vvariation) {
                    $temp[$vvariation->subcategory->category->name][] = $vvariation->subcategory->name;
                }
                foreach ($category as $ck => $cv) {
                    $tbl.='<tr class = "odd gradeX"><th width="70">' . $cv->name . '</th><td class = "sorting_1">';
                    if (isset($temp[$cv->name]) && !empty($temp[$cv->name])) {
                        $str = array();
                        foreach ($temp[$cv->name] as $varik => $variv) {
                            $str[$variv] = $variv;
                        }
                        $tbl.= implode(",", $str);
                    } else {
                        $tbl.='N-A';
                    }
                    $tbl.='</td></tr>';
                }
                $tbl.='</table></td>
<td style = "padding-left:15px" valign = "top" width="180">
<h3 style = "font-size:14px; border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">DESCRIPTION</h3>
<table cellpadding = "5" cellspacing = "0"><tr><td>' . $v->product->description . '</td></tr></table></td></tr></table></td></tr><tr><td><h3 style="font-weight:500; position:relative; border-bottom:2px solid #25AAE1">AVAILABLE COLORS</h3></td></tr>
         <tr><td>';
                if (($v->product->colors)) {
                    $tbl.='<table cellpadding = "0" width = "100%" cellspacing = "0"><tr>';
                    foreach ($v->product->colors as $avcolk => $avcolv) {
                        $tbl.='<td><img src="' . $avcolv->thumimagepath . '" alt="" style="width:100px; height:100px" title="" /></td>';
                    }
                    $tbl.='</tr></table>';
                }
                $tbl.='</td></tr></table>';

                $pdf->writeHTML($tbl, true, false, false, false, '');
                if (count($catalogue->catalogueproducts) > ($k + 1))
                    $pdf->AddPage();
            }

            $date = date("Y_m_d H_i_s");
            $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
            $pdf->Output($pdfName, 'D');
            @unlink($pdfName);
        }
    }

    function email($id = "") {
        if ($id) {
            $this->data['catalogue'] = Catalogue::find($id);
        }
        $this->data['title'] = "";
        $this->load->view('frontend/products/email', $this->data);
    }

    function emailpdf() {
        $this->load->helper("file");
        if ($this->input->get('demail') && $this->input->get('sub')) {
            $email = $this->input->get('demail');
            if ($this->session->userdata(SITE_NAME . '_user_catalog') && $this->session->userdata(SITE_NAME . '_save_user_catalog')) {
//                $this->data['catalogue'] = Catalogue::find('first', array('conditions' => array('id = ? ', $this->session->userdata['save_user_catalog']['id'])));
//                $this->data['category'] = Category::find('all');
//                $catalogue = $this->data['catalogue'];
//                $category = $this->data['category'];
//                $this->load->library('Pdf');
//                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
//                $pdf->SetAutoPageBreak(true);
//                $pdf->SetAuthor(SITE_NAME);
//                $pdf->SetFont('helvetica', '', 9);
//                $pdf->AddPage();
//
//                foreach ($catalogue->catalogueproducts as $k => $v) {
//                    $tbl = '<table width="520px" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1">';
//                    if ($k == 0) {
//                        $tbl.='<tr><td class="header"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr> 
//           <td><img src="' . base_url('assets/frontend/pdf-img/logo.png') . '" alt="" style="height:80px" title="" /></td><td>
//                  <h1 style="margin:0; padding:0"><br />' . $catalogue->title . '</h1></td></tr></table></td></tr>';
//                    }
//                    $tbl.='<tr><td>
//                <div style="color:#666; margin:0; padding:0; font-size:21px; font-weight:bold">' . $v->product->title . '</div>
//                <span style="color:#6e6e70; margin:0; padding:0">SKU: ' . $v->product->sku . ' - ' . $v->product->colorcode . '</span>
//            </td></tr><tr><td><img src = "' . $v->product->imagepath . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" /></td>
//</tr><tr><td><table cellpadding = "10" width = "100%" cellspacing = "0" ><tr><td width = "350" valign = "top">
//<h3 style = "font-size:14px;border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">TECHNICAL SPECIFICATIONS</h3><br><table cellpadding = "5" cellspacing = "0">';
//                    foreach ($v->product->productvariation as $kvariation => $vvariation) {
//                        $temp[$vvariation->subcategory->category->name][] = $vvariation->subcategory->name;
//                    }
//                    foreach ($category as $ck => $cv) {
//                        $tbl.='<tr class = "odd gradeX"><th width="70">' . $cv->name . '</th><td class = "sorting_1">';
//                        if (isset($temp[$cv->name]) && !empty($temp[$cv->name])) {
//                            $str = array();
//                            foreach ($temp[$cv->name] as $varik => $variv) {
//                                $str[$variv] = $variv;
//                            }
//                            $tbl.= implode(",", $str);
//                        } else {
//                            $tbl.='N-A';
//                        }
//                        $tbl.='</td></tr>';
//                    }
//                    $tbl.='</table></td>
//<td style = "padding-left:15px" valign = "top" width="180">
//<h3 style = "font-size:14px; border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">DESCRIPTION</h3>
//<table cellpadding = "5" cellspacing = "0"><tr><td>' . $v->product->description . '</td></tr></table></td></tr></table></td></tr><tr><td><h3 style="font-weight:500; position:relative; border-bottom:2px solid #25AAE1">AVAILABLE COLORS</h3></td></tr>
//         <tr><td>';
//                    if (($v->product->colors)) {
//                        $tbl.='<table cellpadding = "0" width = "100%" cellspacing = "0"><tr>';
//                        foreach ($v->product->colors as $avcolk => $avcolv) {
//                            $tbl.='<td><img src="' . $avcolv->thumimagepath . '" alt="" style="width:100px; height:100px" title="" /></td>';
//                        }
//                        $tbl.='</tr></table>';
//                    }
//                    $tbl.='</td></tr></table>';
//
//                    $pdf->writeHTML($tbl, true, false, false, false, '');
//                    if (count($catalogue->catalogueproducts) > ($k + 1))
//                        $pdf->AddPage();
//                }
//                $date = date("Y_m_d H_i_s");
//                $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
//                $pdf->Output($pdfName, 'F');
//                $data['attachment'] = $pdfName;
//                $data['emailAttachment'] = TRUE;
//                $data['subject'] = 'Catalogue PDf attachment';
                $data['subject'] = $this->input->get('sub');
                $data['message'] = $this->input->get('msg');
                $this->_send_email('pdfmail', $email, $data);
//                if ($pdfName) {
//                    unlink($pdfName);
//                }
                echo 1;
            }
        } else {
            echo '';
        }
    }

    function select_style_images($id) {
        $join = "left join style_categories on style_categories.id = style_images.category_id";
        $condition = "style_categories.status = '1' and style_images.status = '1'and style_images.category_id = '$id'";
        $this->data['style_images'] = StyleImage::all(array('joins' => $join, 'conditions' => $condition, 'include' => 'photo'));
//        echo '<pre>';print_r($sql);exit;
//        $data['style_images'] = StyleImage::find_by_sql($sql);
        $this->data['offset'] = LIMIT;
        $this->data['style_total'] = StyleImage::count(array('joins' => $join, 'conditions' => $condition));
        $this->data['style_page'] = LIMIT;
        if ($this->data['style_total'] < LIMIT)
            $this->data['style_page'] = $this->data['style_total'];
        $view = "home/index";
        $this->load->view('frontend/home/style_image', $this->data);
//        $this->load->view(FRONTENDFOLDERNAME, $view, $this->data);
    }

    function select_style_images_list($id) {
        $join = "left join style_categories on style_categories.id = style_images.category_id";
        $condition = "style_categories.status = '1' and style_images.status = '1'and style_images.category_id = '$id'";
        $this->data['style_images'] = StyleImage::all(array('joins' => $join, 'conditions' => $condition, 'include' => 'photo'));
//        echo '<pre>';print_r($sql);exit;
//        $data['style_images'] = StyleImage::find_by_sql($sql);
        $this->data['offset'] = LIMIT;
        $this->data['style_total'] = StyleImage::count(array('joins' => $join, 'conditions' => $condition));
        $this->data['style_page'] = LIMIT;
        if ($this->data['style_total'] < LIMIT)
            $this->data['style_page'] = $this->data['style_total'];
        $view = "home/index";
//        $this->load->view('frontend/home/style_image', $this->data);
        $this->load->view('frontend/home/style_image_list', $this->data);
    }

    function downloadstoryboardpdf() {
        $this->load->helper("file");
        if ($this->session->userdata(SITE_NAME . '_story_board') && $this->session->userdata(SITE_NAME . '_save_user_storyboard')) {
//            echo '<pre>';print_r($this->session->userdata[SITE_NAME . '_story_board']);exit;
            $this->data['storyboard'] = Storyboard::find('all', array('conditions' => array('name = ? ', $this->session->userdata[SITE_NAME . '_save_user_storyboard']['name'])));
//            foreach($this->data['storyboard'] as $k=>$v){
//                foreach($v->storyboardsurface as $key=>$val){
//                     echo '<pre>';print_r($val->product);exit;
//                }
//                
//            }
            $storyboard = $this->data['storyboard'];
            $this->load->library('Pdf');
            $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetAutoPageBreak(true);
            $pdf->setPrintHeader(true);
            $pdf->setPrintFooter(true);
            $pdf->SetAuthor(SITE_NAME);
            $pdf->SetFont('helvetica', '', 9);
            /*             * *************Changes by nlesh ******************** */
            $pdf->startPageGroup();
            $pdf->AddPage();

//            $this->load->view('frontend/products/download_story_pdf', $this->data);

            $tbl = '<table width="520px" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1">';
            $tbl.='<tr><td class="header"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>
            <td><img src="' . base_url('assets/frontend/images/logo.png') . '" alt="" style="height:80px" title="" /></td><td>
            <h1 style="margin:0; padding:0"><br />' . $storyboard[0]->name . '</h1></td></tr></table></td></tr>';
            if ($storyboard) {
                foreach ($storyboard as $k => $v) {

                    $tbl.='<tr><td><img src = "' . $v->canvasurl . '" alt = "" style = "width:520px; height:250px; border:2px solid #25AAE1;" title = "" /></td></tr>';
                    $tbl.=' <tr><td>';
                    $tbl.='<table cellpadding = "0" width = "100%" cellspacing = "0"><tr>';
                    foreach ($v->storyboardsurface as $kp => $vp) {
                        if (($vp->product)) {
                            $tbl.='<td><img src="' . $vp->product->thumimagepath . '" alt="" style="width:100px; height:100px" title="" /><p>Sku code </p>
                                                    <p> "' . $vp->product->sku . '"</p></td>';
                        }
                    }
                    $tbl.='</tr></table>';
                }
            }

            $tbl.='</td></tr></table>';
            echo $tbl;
            exit;
            $pdf->writeHTML($tbl, true, false, false, false, '');
            if (count($v->storyboardsurface) > ($k + 1))
                $pdf->AddPage();

            $date = date("Y_m_d H_i_s");
            $pdfName = 'pdf/' . str_replace(' ', '', $date) . '.pdf';
            $pdf->Output($pdfName, 'I');
            @unlink($pdfName);
        }
    }

}
