<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function logs() {
        /*
          highly advised that you use authentification
          before running this controller to keep the world out of your logs!!!
          you can use whatever method you like does not have to be logs
         */
        $this->load->spark('fire_log/0.8.2');
        // thats it, ill take if from there
    }

    public function index() {

        // ($type, $message, $write_to_file);
        $this->console->log('this will show an error in firebug', 'error', FALSE);
        console_log('Shafiq and nilesh expriment', 'info', TRUE);
// or use the alias console_log()
        console_log('This is my log message', 'log', TRUE);
        console_log('This is my error message, also written to a log file', 'error', TRUE);
// set the log path or file name
//        $this->console->log_path = APPPATH . 'logs/';
//        $this->console->log_file = 'console-' . date('y-m-d h-i-s.php');
        //$this->load->spark('fire_log/0.8.2');
        //$this->load->view('welcome_message');
    }

    function database() {
        $result = Productphoto::find_by_sql('SELECT *,count(product_id) as count FROM `productphotos` group by product_id having count >1');
//        echo '<pre>';print_r($result);exit;
        foreach ($result as $k => $v) {
            $products = Productphoto::find('all', array('conditions' => array('product_id  =? and photo_id = ?', $v->product_id, 1)));
            if (count($products) > 1)
                $products[0]->delete();
        }
//        013713004802202
//
//        echo '<pre>';
//        print_r($ids);
//        exit;
//        $products = Productphoto::find('all', array('conditions' => array('product_id  in (?) and photo_id = ?', $ids, 1)));
//        echo '<pre>';
//        print_r($products);
        echo 'completed';
        exit;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */