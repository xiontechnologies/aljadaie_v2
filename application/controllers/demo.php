<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Demo extends CI_Controller {

    public function index() {
        $str = 'shafiq@themedios.com admin123$$';
        $this->load->helper("file");

        $this->data['catalogue'] = Catalogue::find(13);
        $this->data['category'] = Category::find('all');
        $catalogue = $this->data['catalogue'];
        $category = $this->data['category'];

        foreach ($catalogue->catalogueproducts as $k => $v) {
            $spec[$k] = '';
            if ($v->product->productvariation) {
                foreach ($v->product->productvariation as $kvariation => $vvariation) {
                    $temp[$vvariation->subcategory->category->name][] = $vvariation->subcategory->name;
                }
                foreach ($category as $ck => $cv) {
                    $spec[$k] .= '<tr class = "odd gradeX"><th width="150">' . $cv->name . '</th><td class = "sorting_1">';
                    if (isset($temp[$cv->name]) && !empty($temp[$cv->name])) {
                        $str = array();
                        foreach ($temp[$cv->name] as $varik => $variv) {
                            $str[$variv] = $variv;
                        }
                        $spec[$k] .= implode(",", $str);
                    } else {
                        $spec[$k].= 'N-A';
                    }
                    $spec[$k].='</td></tr>';
                }
            }
        }
        $this->load->library('Pdf');
        $userdata = $this->session->userdata('Aljedaie_user_data');
        $name = 'by - ' . $userdata['name'];
        // create new PDF document
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor(SITE_NAME);
        $pdf->SetTitle('Catelouge');
        $pdf->SetSubject('Catelouge Name');
        $pdf->SetKeywords('catelouge, demo, example');
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(false);
// set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $catalogue->title, $name, array(0, 0, 0), array(255, 255, 255));

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'ltr';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
        $pdf->setLanguageArray($lg);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('dejavusans', '', 12);

// add a page
       
// set LTR direction for english translation
        $pdf->setRTL(false);
        foreach ($catalogue->catalogueproducts as $k => $v) {
             $pdf->AddPage();
            $tbl.='<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top:10px solid #25AAE1;">
                <tr>
                <td>
                    <div style="color:#666; margin:0; padding:0; font-size:21px; font-weight:bold">' . $v->product->title . '</div>
                    <span style="color:#6e6e70; margin:0; padding:0">SKU: ' . $v->product->sku . ' - ' . $v->product->colorcode . '</span>
                </td>
                </tr>
                <tr>
                <td align="center">
                    <img src = "http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0=' . $v->product->imagepath . '&q=100&tw0=10&th0=10&w=250" alt = "" style = "border:2px solid #25AAE1;" title = "" />
                </td>
                </tr>
                <tr>
                <td style = "padding-left:15px" valign = "top" width="280">
                    <h3 style = "font-size:14px; border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">DESCRIPTION</h3>
                    <table cellpadding = "5" cellspacing = "0">
                        <tr><td>' . $v->product->description . '</td></tr>
                    </table>
                </td>
                </tr>
                <tr>
                <td style = "padding-left:15px" valign = "top" width="480">
                        <table cellpadding = "10" width = "100%" cellspacing = "0" >
                            <tr><td width = "350" valign = "top">
                                <h3 style = "font-size:14px;border-bottom:2px solid #25AAE1; font-weight:bold; position:relative;">TECHNICAL SPECIFICATIONS</h3><br>
                                <table cellpadding = "5" cellspacing = "0">' . $spec[$k] . '
                                 </table></td>
                            </tr>
                        </table>
                </td>
                </tr>
                <tr>
                <td width="100%" style = "padding-left:15px" valign = "top">
                    <h3 style="font-weight:500; position:relative; border-bottom:2px solid #25AAE1">AVAILABLE COLORS</h3>
                    <div style="color:#ffffff; margin:0; padding:0; font-size:10px; font-weight:bold">__</div>';
            if (($v->product->colors)) {
                $tbl.='<table cellpadding = "2" width = "100%" cellspacing = "0" style="padding-top:10px"><tr>';
                foreach ($v->product->colors as $avcolk => $avcolv) {
                    $tbl.='<td style="vertical-align: bottom; height:110px;"><img src="http://vivek2.picarisplatform.com/picaris/getimage.ashx?ft=1&fn=1024&tn0=' . $avcolv->thumimagepath . '&q=100&tw0=10&th0=10&w=100" alt="" title="" /></td>';
                }
                $tbl.='</tr></table>';
            }
            $tbl.='</td></tr></table>';
            $pdf->writeHTML($tbl, true, 0, true, 0);
            $tbl = '';
        }
        
// set LTR direction for english translation
        $pdf->setRTL(false);
// ---------------------------------------------------------
//Close and output PDF document
        $pdf->Output('example_018.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
    }

}

?>
