<?php

/*
 * @auther Shafiq
 */

class Products extends MY_base {

    function __construct() {
        parent::__construct();
        $this->check_method();
        array_push($this->scripts['css'], 'frontend/css/basic.css', 'frontend/css/style.css', 'frontend/css/jquery_ui.css');
        array_push($this->scripts['js'], "frontend/js/jquery-ui.js", "frontend/js/zoom.js");
        $facebookJs = "(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = '//connect.facebook.net/en_US/all.js#xfbml=1&appId=435451156500782';
                    fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));";
        array_push($this->scripts['embed'], $facebookJs);
    }

    public function index($id = '') {
        $this->data['embed'] = '<script>$(document).ready(function(){
                   $("#zoom_01").elevateZoom({
                cursor: "crosshair",
                borderSize:4,
                scrollZoom : true,
                zoomWindowWidth: 457,
                zoomWindowHeight: 372,
                zoomWindowFadeIn: true,
        zoomWindowFadeOut: true,
        zoomWindowAlwaysShow: true,
        zoomTintFadeIn: true,
        zoomTintFadeOut: true,
            }); 
        $( "#accordion" ) . accordion();
        $(".stock").on("click",function(){
            $(".stcokpopup").toggle();
        });
        });
           $(".avail-color").on("click",function(){
        id = $(this).attr("data");
        $("#dialog").html("<img class=preloader src=' . base_url('/assets/images/loading.gif') . ' />");
        $.ajax({
            url:site_url+"product/ajaxdetails/"+id,
            success:function(data){
                $(".preloader").remove();
                 $(".zoomContainer").remvoe();
                $("#dialog").html(data);
            }
        });
    });
        </script>';
        $this->data['category'] = Category::find('all', array('conditions' => array('status = ? and showinfilter = ? and id != ?', 1, 1, 7)));
        $this->data['product'] = Product::find('first', array('conditions' => array('id = ? and status = ?', $id, 1)));
        $view = "products/ajaxdetails";
        $this->display_view(FRONTENDFOLDERNAME, $view, $this->data, TRUE);
    }

    public function removefromcatalog($id = '') {
        if (!$id)
            show_404();
        $sessionArray = $this->session->userdata('user_catalog');
        unset($sessionArray[$id]);
        $this->session->set_userdata('user_catalog', $sessionArray);
        if (count($this->session->userdata('user_catalog') == 0)) {
            $this->session->set_userdata('save_user_catalog', array('id' => FALSE, 'name' => FALSE));
        }
    }

    public function addtocatalog($id = '') {
        /*
         * Find id in session calatalog
         * if yes send already register
         * if not then add it and refresh catalog div
         */
        $conn = Product::connection();
        $conn->transaction();
        try {
            if (!$id)
                show_404();
            $view = '/products/ajaxcatalog';
            if ($this->session->userdata('user_catalog') && array_key_exists($id, $this->session->userdata('user_catalog'))) {
                echo '';
            } else if ($this->session->userdata('user_catalog')) {
                /*
                 * Check wheter catalogue saved to databse or not
                 * if so add this product to database also
                 */
                $product = Product::find('first', array('conditions' => array('id=? and status = ?', $id, 1)));
                if ($this->session->userdata('save_user_catalog') && $this->session->userdata['save_user_catalog']['id']) {
                    $catlogproduct = new Catalogueproduct();
                    $catlogproduct->product_id = $product->id;
                    $catlogproduct->catalogue_id = $this->session->userdata['save_user_catalog']['id'];
                    $catlogproduct->updated = databasedate();
                    $catlogproduct->created = databasedate();
                    $catlogproduct->save();
                    $conn->commit();
                }
                $sessionArray = $this->session->userdata('user_catalog');
                $this->data['v'] = $sessionArray[$product->id] = array(
                    'id' => $product->id,
                    'sku' => $product->sku,
                    'colorcode' => $product->colorcode,
                    'availablecolor' => count($product->colors),
                    'imagethumbpath' => $product->thumimagepath,
                    'imagepath' => str_replace('/./', '/', base_url(PRODUCTIMAGE_PATH . $product->productphoto->photo->file_name)),
                    'designpath' => $product->designpath,
                    'photoname' => $product->productphoto->photo->file_name,
                );
                $this->session->set_userdata('user_catalog', $sessionArray);
                $this->load->view(FRONTENDFOLDERNAME . $view, $this->data);
            } else {
                /*
                 * New catalog creating
                 */
                $product = Product::find('first', array('conditions' => array('id=? and status = ?', $id, 1)));
                $this->data['v'] = $productCatalog[$product->id] = array(
                    'id' => $product->id,
                    'sku' => $product->sku,
                    'colorcode' => $product->colorcode,
                    'availablecolor' => count($product->colors),
                    'imagethumbpath' => $product->thumimagepath,
                    'imagepath' => str_replace('/./', '/', base_url(PRODUCTIMAGE_PATH . $product->productphoto->photo->file_name)),
                    'designpath' => $product->designpath,
                    'photoname' => $product->productphoto->photo->file_name,
                );
                $this->session->set_userdata('user_catalog', $productCatalog);
                $this->load->view(FRONTENDFOLDERNAME . $view, $this->data);
            }
        } catch (Exception $e) {
            $this->write_logs($e);
            $conn->rollback();
        }
    }

    public function catalogsave() {
        try {
            $value = $this->input->get('val');
            $conn = Catalogue::connection();
            $conn->transaction();
            if ($value && $this->session->userdata('user_catalog')) {
                $catalog = new Catalogue();
                $catalog->title = $value;
                $catalog->updated = $catalog->created = databasedate();
                $catalog->save();
                foreach ($this->session->userdata('user_catalog') as $k => $v) {
                    $catlogproduct = new Catalogueproduct();
                    $catlogproduct->product_id = $v['id'];
                    $catlogproduct->catalogue_id = $catalog->id;
                    $catlogproduct->updated = $catlogproduct->created = databasedate();
                    $catlogproduct->save();
                }
                $conn->commit();
                $this->session->set_userdata('save_user_catalog', array('id' => $catalog->id, 'name' => $value));
                echo '1';
            } else {
                echo '';
            }
        } catch (Exception $e) {
            $this->write_logs($e);
            $conn->rollback();
            echo '';
        }
    }

    public function ajaxview() {
        try {
            $offset = $this->input->get('pagintation') ? $this->input->get('pagintation') : '0';
            $value = $this->input->get('val');
            $this->data['type'] = $this->input->get('type');
            $variation = $this->input->get('variation');
            if ($variation && $value) {
                $join = "left join productvariations on products.id = productvariations.product_id left join catalogueproducts on products.id = catalogueproducts.product_id left join catalogues on catalogues.id =  catalogueproducts.product_id left join subcategories on productvariations.subcategory_id = subcategories.id ";
                $cond = array("productvariations.subcategory_id in (?) and products.status = ? and products.title LIKE '%" . $value . "%' or products.description LIKE '%" . $value . "%' or products.shortdescription LIKE '%" . $value . "%' or products.sku LIKE '%" . $value . "%' or products.colorcode LIKE '%" . $value . "%' or products.price LIKE '%" . $value . "%' or products.weight LIKE '%" . $value . "%' or products.washcare LIKE '%" . $value . "%' or products.tags LIKE '%" . $value . "%' or subcategories.name LIKE '%" . $value . "%' or catalogues.title LIKE '%" . $value . "%'", 1, $variation);
                $data['total'] = count(Product::find('all', array('select' => 'distinct products.*', 'joins' => $join, 'conditions' => $cond)));
                $this->data['product'] = Product::find('all', array('select' => 'distinct products.*', 'joins' => $join, 'conditions' => $cond));
            } else if ($variation) {
                $data['total'] = count(Productvariation::find('all', array('select' => 'distinct product_id', 'conditions' => array('subcategory_id in (?)', $variation))));
                $this->data['product'] = Productvariation::find('all', array('select' => 'distinct product_id', 'conditions' => array('subcategory_id in (?) and status = ?', $variation, 1), 'limit' => LIMIT, 'offset' => $offset));
                $this->data['post'] = TRUE;
            } else if ($value) {
                $join = "left join productvariations on products.id = productvariations.product_id left join catalogueproducts on products.id = catalogueproducts.product_id left join catalogues on catalogues.id =  catalogueproducts.product_id left join subcategories on productvariations .subcategory_id = subcategories.id ";
//                $cond = "products.title LIKE '%" . $value . "%' or products.description LIKE '%" . $value . "%' or products.shortdescription LIKE '%" . $value . "%' or products.sku LIKE '%" . $value . "%' or products.colorcode LIKE '%" . $value . "%' or products.price LIKE '%" . $value . "%' or products.weight LIKE '%" . $value . "%' or products.washcare LIKE '%" . $value . "%' or products.tags LIKE '%" . $value . "%'or subcategories.name LIKE '%" . $value . "%' or catalogues.title LIKE '%" . $value . "%'";
                $cond = array("products.status = ? and products.title LIKE '%" . $value . "%' or products.description LIKE '%" . $value . "%' or products.shortdescription LIKE '%" . $value . "%' or products.sku LIKE '%" . $value . "%' or products.colorcode LIKE '%" . $value . "%' or products.price LIKE '%" . $value . "%' or products.weight LIKE '%" . $value . "%' or products.washcare LIKE '%" . $value . "%' or products.tags LIKE '%" . $value . "%'or subcategories.name LIKE '%" . $value . "%' or catalogues.title LIKE '%" . $value . "%'", 1);
                $data['total'] = count(Product::find('all', array('select' => 'distinct products.*', 'joins' => $join, 'conditions' => $cond)));
                $this->data['product'] = Product::find('all', array('select' => 'distinct products.*', 'joins' => $join, 'conditions' => $cond));
            } else {
                $this->data['product'] = Product::find('all', array('limit' => LIMIT, 'offset' => $offset, 'conditions' => array('status = ?', 1)));
                $data['total'] = Product::count();
            }
            $view = FRONTENDFOLDERNAME . "/products/ajaxpagination";
            $data['page'] = $data['offset'] = LIMIT + $offset;
            if ($data['offset'] >= $data['total']) {
                $data['page'] = $data['total'];
            }
            $data['view'] = $this->load->view($view, $this->data, true);
            $this->output->set_output(json_encode($data));
        } catch (Exception $e) {
            $this->write_logs($e);
        }
    }

    public function catalognew() {
        $this->session->unset_userdata('user_catalog');
        $this->session->unset_userdata('save_user_catalog');
    }

    public function imagedetails() {
        print_r(json_encode(getimagesize($_REQUEST['imge'])));
    }

}