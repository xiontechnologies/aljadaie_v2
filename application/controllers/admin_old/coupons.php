<?php

class Coupons extends MY_admin {

    function index() {
        $view = 'coupon/view';
        $data['pagetitle'] = 'Coupan';
        $pagetitle = 'Coupan';
        $data['userRights'] = $this->check_rights();
        $data['allcoupans'] = Coupon::find('all', array('order' => 'updated desc'));
        $this->display_view('admin', $view, $data);
    }

    function &_coupan($params = '') {
        $data['fab'] = Coupon::find_assoc($params);
        return $data['fab'];
    }

    function search_coupan() {
        $coupan = &$this->_coupan();
        echo json_encode($coupan);
    }

    function check_coupon($str) {
        $coupon = Coupon::find_by_code($str);
        if ($coupon) {
            $this->form_validation->set_message('check_coupon', "Coupon already present");
            return false;
        }
        else
            return true;
    }

    function checkoption() {
        if ($this->input->post('asignto')) {
            if (!$this->input->post('option')) {
                $this->form_validation->set_message('checkoption', "Please select atleast one user");
                return false;
            }else
                return true;
        }
        else
            return true;
    }

    function checkgroupoption() {
        if ($this->input->post('asigntogroup')) {
            if (!$this->input->post('group_option')) {
                $this->form_validation->set_message('checkgroupoption', "Please select atleast one user group");
                return false;
            }else
                return true;
        }
        else
            return true;
    }

    function add() {
        $view = 'coupon/add';
        $this->data['allusers'] = User::find_assoc();
        array_push($this->scripts['js'], "js/datepicker_new.js");
        array_push($this->scripts['css'], "css/datePicker.css");
        $datejs = '$(document).ready(function(){
    $(function() {
        $( "#to_date" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        $( "#from_date" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        });});';
        array_push($this->scripts['embed'], $datejs);

        $config = array(
            array('field' => 'code', 'label' => 'Code', 'rules' => 'required|callback_check_coupon|max_length[4]'),
            array('field' => 'type', 'label' => 'User Type', 'rules' => 'required'),
            array('field' => 'discount_type', 'label' => 'Discount Type', 'rules' => 'required'),
            array('field' => 'amount', 'label' => 'Amount', 'rules' => 'required|integer'),
            array('field' => 'no_of_time', 'label' => 'Number Of Time', 'rules' => 'integer'),
            array('field' => 'option[]', 'label' => 'Option', 'rules' => 'callback_checkoption'),
            array('field' => 'group_option[]', 'label' => 'Option', 'rules' => 'callback_checkgroupoption'),
        );
        $this->form_validation->set_rules($config);
        if ($this->input->post('type')) {
            $this->form_validation->set_rules('no_of_time', 'no of time', 'integer');
        }
        if ($this->form_validation->run()) {
            $coupan = new Coupon();
            $coupan->code = $this->input->post('code');
            $coupan->type = $this->input->post('type');
            $coupan->amount = $this->input->post('amount');
            $coupan->discount_type = $this->input->post('discount_type');
            if ($this->input->post('to_date'))
                $coupan->to_date = $this->input->post('to_date');
            if ($this->input->post('from_date'))
                $coupan->from_date = $this->input->post('from_date');
            if ($this->input->post('to_amount'))
                $coupan->to_amount = $this->input->post('to_amount');
            if ($this->input->post('from_amount'))
                $coupan->from_amount = $this->input->post('from_amount');
            if ($this->input->post('asignto')) {
                $coupan->user_id = implode(',', $this->input->post('option'));
            } else {
                $coupan->user_id = 0;
            }
            if ($this->input->post('asigntogroup')) {
                $coupan->user_group_id = implode(',', $this->input->post('group_option'));
            } else {
                $coupan->user_group_id = 0;
            }
            if ($this->input->post('no_of_time'))
                $coupan->no_of_time = $this->input->post('no_of_time');
            else
                $coupan->no_of_time = 1;
            $coupan->created = date('d-m-Y H:i:s');
            $coupan->updated = date('d-m-Y H:i:s');
            $coupan->save();

            if ($this->input->post('asignto')) {
                if ($this->input->post('option')) {
                    foreach ($this->input->post('option') as $value) {
                        $user_coupon = new Usercoupon();
                        $user_coupon->user_id = $value;
                        $user_coupon->coupon_id = $coupan->id;
                        $user_coupon->created = date("Y-m-d h:i:s");
                        $user_coupon->updated = date("Y-m-d h:i:s");
                        $user_coupon->save();
                    }
                }
            }
            if ($this->input->post('asigntogroup')) {
                $ids = $this->input->post('group_option');
                $user_group = User_Group::find('all', array('conditions' => array('group_id in(?)', $ids)));
                if (isset($user_group) && !empty($user_group)) {
                    foreach ($user_group as $ugroup) {
                        $user_id = $ugroup->user_id;
                        $user = User::find(array('conditions' => array('id = ?', $user_id)));
                        $user_coupon = Usercoupon::find(array('conditions' => array('user_id = ? and coupon_id =?', $user->id, $coupan->id)));
                        if (!$user_coupon) {
                            $user_coupon = new Usercoupon();
                            $user_coupon->user_id = $user->id;
                            $user_coupon->coupon_id = $coupan->id;
                            $user_coupon->created = date("Y-m-d h:i:s");
                            $user_coupon->updated = date("Y-m-d h:i:s");
                            $user_coupon->save();
                        }
                    }
                }
            }

            /*
             * Send mail 
             */
            $email = "";
            $data['discount'] = $coupan->amount;
            $data['type'] = $coupan->discount_type;
            $data['lastdate'] = $coupan->from_date;
            $data['fromdate'] = $coupan->to_date;
            $data['code'] = $coupan->code;
            if ($this->input->post('asignto')) {
                $ids = $this->input->post('option');
                $userObj = User::find('all', array('conditions' => array('id in (?)', $ids)));
                foreach ($userObj as $obj) {
                    $email = $obj->email;
                    $this->_send_email('send-coupon', $email, $data);
                }
            }
            if ($this->input->post('asigntogroup')) {
                $ids = $this->input->post('group_option');
                $usergroup = User_Group::find('all', array('conditions' => array('group_id in(?)', $ids)));
                if (isset($usergroup) && !empty($usergroup)) {
                    foreach ($usergroup as $group) {
                        $user_id = $group->user_id;
                        $user = User::find(array('conditions' => array('id = ?', $user_id)));
                        $user_coupon = Usercoupon::find(array('conditions' => array('user_id = ? and coupon_id =?', $user->id, $coupan->id)));
                        if (!$user_coupon) {
                            $user_id = $group->user_id;
                            $user = User::find(array('conditions' => array('id = ?', $user_id)));
                            $email = $user->email;
                            $this->_send_email('send-coupon', $email, $data);
                        }
                    }
                }
            }
            $this->session->set_flashdata('message', 'Coupan added succefully..');
            redirect('admin/coupons/list');
        } else {
//            echo '<pre>';print_r($this->form_validation->get_field_data());exit;
            if ($this->input->post('asignto')) {
                $this->data['user'] = $this->getalluser();
            }
            if ($this->input->post('asigntogroup')) {
                $this->data['usergroup'] = $this->getallusergroup();
            }
            $this->data['postdata'] = $this->input->post();
        }
        $this->display_view('admin', $view, $this->data);
    }

    /*     * **************Update /edit in the database************ */

    function edit($id = '') {
        if (!$id)
            show_404();
        $view = 'coupon/edit';
        $pagetitle = 'Edit';
        $this->data['EditCoupan'] = Coupon::find(array('conditions' => array('id = ? ', $id)));
        if ($this->data['EditCoupan']->user_id) {
            $this->data['user'] = $this->getalluser();
        }
        if ($this->data['EditCoupan']->user_group_id) {
            $this->data['usergroup'] = $this->getallusergroup();
        }

        $config = array(
            array('field' => 'type', 'label' => 'User Type', 'rules' => 'required'),
            array('field' => 'discount_type', 'label' => 'Discount Type', 'rules' => 'required'),
            array('field' => 'amount', 'label' => 'Amount', 'rules' => 'required|integer'),
            array('field' => 'no_of_time', 'label' => 'Number Of Time', 'rules' => 'integer'),
            array('field' => 'option[]', 'label' => 'Option', 'rules' => 'callback_checkoption'),
            array('field' => 'group_option[]', 'label' => 'Option', 'rules' => 'callback_checkgroupoption'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run()) {
            $coupan = Coupon::find($id);
            $coupan->type = $this->input->post('type');
            $coupan->amount = $this->input->post('amount');
            $coupan->discount_type = $this->input->post('discount_type');
            if ($this->input->post('to_date'))
                $coupan->to_date = $this->input->post('to_date');
            if ($this->input->post('from_date'))
                $coupan->from_date = $this->input->post('from_date');
            if ($this->input->post('to_amount'))
                $coupan->to_amount = $this->input->post('to_amount');
            if ($this->input->post('from_amount'))
                $coupan->from_amount = $this->input->post('from_amount');
            if ($this->input->post('asignto'))
                $coupan->user_id = implode(',', $this->input->post('option'));
            else
                $coupan->user_id = 0;
            if ($this->input->post('asigntogroup')) {
                $coupan->user_group_id = implode(',', $this->input->post('group_option'));
            } else {
                $coupan->user_group_id = 0;
            }
            if ($this->input->post('no_of_time'))
                $coupan->no_of_time = $this->input->post('no_of_time');
            else
                $coupan->no_of_time = 1;
            $coupan->updated = date('d-m-Y H:i:s');
            $coupan->save();

            if ($this->input->post('asignto')) {
                $delSub = Usercoupon::delete_all(array('conditions' => array('coupon_id=?', $id)));
                $att_coupan = $this->input->post('option');
                if (isset($att_coupan) && !empty($att_coupan)) {
                    foreach (array_filter($att_coupan) as $key => $value) {
                        $couponArr = array(
                            'user_id' => $value,
                            'coupon_id' => $coupan->id,
                            'created' => databasedate(),
                            'updated' => databasedate()
                        );
                        $sub = new Attendedcoursesubject($couponArr);
                        $sub->save();
                    }
                }
            }

            if ($this->input->post('asigntogroup')) {
                $delSub = Usercoupon::delete_all(array('conditions' => array('coupon_id=?', $id)));
                $ids = $this->input->post('group_option');
                $usergroup = User_Group::find('all', array('conditions' => array('group_id in(?)', $ids)));
                if (isset($usergroup) && !empty($usergroup)) {
                    foreach ($usergroup as $group) {
                        $user_id = $group->user_id;
                        $user = User::find(array('conditions' => array('id = ?', $user_id)));
                        $user_coupon = Usercoupon::find(array('conditions' => array('user_id = ? and coupon_id =?', $user->id, $coupan->id)));
                        if (!$user_coupon) {
                            $user_id = $group->user_id;
                            $user = User::find(array('conditions' => array('id = ?', $user_id)));
                            $email = $user->email;
                            $this->_send_email('send-coupon', $email, $data);
                        }
                    }
                }
            }

            $this->session->set_flashdata('message', 'Coupan Edited succefully..');
            redirect('admin/coupons/list');
        } else {
            if ($this->input->post('asignto')) {
                $this->data['user'] = $this->getalluser();
            }
            $this->data['postdata'] = $this->input->post();
            $this->data['postdata']['id'] = $id;
            $coupon = Coupon::find(array('conditions' => array('id = ? ', $id)));
            $this->data['postdata']['code'] = $coupon->code;
        }
        $this->display_view('admin', $view, $this->data);
    }

    function getalluser() {
        $user = User::find('all', array('conditions' => array('activated  = ?', 1)));
        return $user;
    }

    function getusers() {
        $data['user'] = $this->getalluser();
        $this->load->view('admin/coupon/userlist', $data);
    }

    function getallusergroup() {
        $usergroup = Membergroup::find('all', array('conditions' => array('status = ?', 1)));
        return $usergroup;
    }

    function getusergroups() {
        $data['usergroup'] = $this->getallusergroup();
        $this->load->view('admin/coupon/grouplist', $data);
    }

    function delete($id = '') {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Coupon::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("Coupon deleted", 'success');
            redirect('admin/coupons/list');
        } else {
            if (!$id)
                show_404();
            $deleteObj = Coupon :: find($id);
            $deleteObj->delete();
            $this->_show_message("Coupon deleted", 'success');
            redirect('admin/coupons/list');
        }
    }

//    function activate($id = '') {
//        if (!$id)
//            show_404();
//        $editActivation = Coupon::find_by_id($id);
//        $editActivation->status = '1';
//        $editActivation->save();
//        $this->_show_message("Category activated successfully", 'success');
//        redirect('admin/coupons/list');
//    }
//
//    function deactivate($id = '') {
//        if (!$id)
//            show_404();
//        $editActivation = Coupon::find_by_id($id);
//        $editActivation->status = '0';
//        $editActivation->save();
//        $this->_show_message("Category deactivated successfully", 'success');
//        redirect('admin/coupons/list');
//    }
}

?>
