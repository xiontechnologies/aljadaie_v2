<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Styleimages extends MY_admin {

    function index() {
        $view = "styleimage/view";
        $data['userRights'] = $this->check_rights();
        $data['alldata'] = StyleImage::find('all', array('order' => 'updated desc', 'include' => 'photo'));
//        echo '<pre>';print_r($data['alldata']);exit;
        $data['category'] = StyleCategory::find_assoc(array('conditions' => array('status=?', 1)));
        $this->display_view("admin", $view, $data);
    }

    function add() {
        $view = "styleimage/add";
        $this->form_validation->set_rules('category_id', 'Style Category', 'required|xss_clean|trim|callback_select_validate');
        $this->form_validation->set_rules('design_name', 'Name', 'required|xss_clean');
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']) && $_FILES['userfile']['error'] != '4') {
            $config['upload_path'] = ADMIN_STYLE_IMAGE_PATH;
//            $config['max_size'] = '10000';
//            $config['max_height'] = '768';
//            $config['max_width'] = '1024';
            $config['allowed_types'] = 'jpg|jpeg|png|bmp|gif';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $error = $this->upload->display_errors();
                $this->_show_message($error, "error");
                redirect("admin/styleimages/add");
                $flag = False;
            } else {
                $data = $this->upload->data();
                $photo = new Photo($data);
                $photo->save();
                $addimage['photo_id'] = $photo->id;
                $flag = TRUE;
            }
        } else {
            if ($this->input->post())
                $this->_show_message("Image is compulsary", 'error');
            $flag = False;
        }
        if ($this->form_validation->run() && $flag) {
            $conn = StyleImage::connection();
            $conn->transaction();
            try {
                $addimage['category_id'] = $this->input->post('category_id');
                $addimage['design_name'] = $this->form_validation->set_value('design_name');
                $addimage['status'] = $this->input->post('status');
                $addimage['created'] = date('Y-m-d h:i:s');
                $addimage['updated'] = date('Y-m-d h:i:s');
                $newStyleImage = new StyleImage($addimage);
                $newStyleImage->save();
                $conn->commit();
                $this->_show_message("Style Image added successfully", 'success');
                redirect('admin/styleimages');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
            if ($this->input->post()) {
                $data['postdata'] = $this->input->post();
            }
        }
        $data['category'] = StyleCategory::find_assoc(array('conditions' => array('status= ?', 1)));
        $data['postdata'] = (object) $this->input->post();
        $this->display_view("admin", $view, $data);
    }

    function select_validate($str) {
        if ($str == -1 || $str == 0 || $str == '') {
            $this->form_validation->set_message('select_validate', 'Please select Category');
            return FALSE;
        } else {
            return true;
        }
    }

    function edit($id = "") {
//        echo '<pre>';print_r($id);exit;
        $view = "styleimage/edit";
        $data['editdata'] = StyleImage::find($id);
        $this->form_validation->set_rules('category_id', 'Style Category', 'required|callback_select_category');
        $this->form_validation->set_rules('design_name', 'Name', 'required|xss_clean');
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']) && $_FILES['userfile']['error'] != '4') {
            $config['upload_path'] = ADMIN_STYLE_IMAGE_PATH;
//            $config['max_size'] = '10000';
//            $config['max_height'] = '768';
//            $config['max_width'] = '1024';
            $config['allowed_types'] = 'jpg|jpeg|png|bmp|gif';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $error = $this->upload->display_errors();
                $this->_show_message($error, "error");
                redirect("admin/styleimages/edit/" . $id);
            } else {
                $editdata = $this->upload->data();
                unset($editdata['is_image']);
                $image = StyleImage::find_by_id($id);
                $photo_id = $image->photo_id;
                if (isset($photo_id) && $photo_id != '') {
                    $photo = Photo::find($photo_id);
                    if (isset($photo->file_name) && !empty($photo->file_name)) {
                        $this->load->helper('file');
                        $path = ADMIN_STYLE_IMAGE_PATH . $photo->file_name;
                        @unlink($path);
                    }
                    $update_photo = Photo::find($photo_id);
                    $update_photo->update_attributes($editdata);
                    $update_photo_id = $update_photo->id;
                } else {
                    $photo = new Photo($editdata);
                    $photo->save();
                    $auto_id = $photo->id;
                }
            }
        }
        if ($this->form_validation->run()) {
            $conn = StyleImage::connection();
            $conn->transaction();
            try {

                $newStyleImage = StyleImage::find($id);
                $newStyleImage->category_id = $this->form_validation->set_value('category_id');
                $newStyleImage->photo_id = isset($update_photo_id) ? $update_photo_id : $newStyleImage->photo_id;
                $newStyleImage->design_name = $this->form_validation->set_value('design_name');
                $newStyleImage->status = $this->input->post('status');
                $newStyleImage->updated = date('Y-m-d h:i:s');
                $newStyleImage->save();
                $conn->commit();
                $this->_show_message("Style Image updated successfully", 'success');
                redirect('admin/styleimages');
            } catch (Exception $e) {
//                echo '<pre>';print_r($e);exit;
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
//            echo '<pre>';print_r($this->form_validation->get_field_data());exit;
            if ($this->input->post()) {
                $data['postdata'] = $this->input->post();
            }
        }
        $data['category'] = StyleCategory::find_assoc(array('conditions' => array('status= ?', 1)));
        $this->display_view("admin", $view, $data);
    }

    function delete($id = '') {

        if ($this->input->post('option')) {
            $ids = $this->input->post('option');

            $deleteObj = StyleImage::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $photo_id = $obj->photo_id;
                $photo = Photo::find_by_id($photo_id);
                if (isset($delObj->file_name) && !empty($delObj->file_name)) {
                    $this->load->helper('file');
                    $path = ADMIN_STYLE_IMAGE_PATH . "/" . $delObj->file_name;
                    @unlink($path);
                }
                $obj->delete();
            }
            $this->_show_message("style images deleted", 'success');
        } else {
            if (!$id)
                show_404();
            $deleteObj = StyleImage::find($id);
//            echo '<pre>';print_r($deleteObj->photo_id);exit;
            if ($deleteObj->photo_id && !empty($deleteObj->photo_id)) {
                $photo_id = $deleteObj->photo_id;
                $photo = Photo::find_by_id($photo_id);
//                $this->load->helper('file');
                $path = ADMIN_STYLE_IMAGE_PATH . '/' . $photo->file_name;
                unlink($path);
            }
            $deleteObj->delete();
            $this->_show_message("style image deleted", 'success');
        }
        redirect('admin/styleimages');
    }

    function activate($id = '') {
        if (!$id)
            show_404();

        $editArticle = StyleImage::find_by_id($id);
        $editArticle->status = '1';
        $editArticle->save();
        $this->_show_message("style image activated successfully", 'success');
        redirect('admin/styleimages');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editArticle = StyleImage::find_by_id($id);
        $editArticle->status = '0';
        $editArticle->save();
        $this->_show_message("style image deactivated successfully", 'success');
        redirect('admin/styleimages');
    }

}

?>
