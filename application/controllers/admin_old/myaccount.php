<?php

/*
 * -----------------------------
 * ----Author shafiq
 * ----Class Admin
 * ----For admin user
 * ----Important Function index,add,edit,delete,active,deactive
 * -----------------------------
 */

class Myaccount extends MY_admin {
    /*
     * ------Contructor------
     * load library for tank auth admin 
     * used for the authanticating the 
     * validate user
     */

    function __construct() {
        parent::__construct();
        $this->load->library('tank_auth_admin');
    }

    function index() {
        $id = $this->session->userdata[SITE_NAME . '_admin_user_data']['user_id'];
        $view = 'myaccount/edit';
        $pagetitle = 'Edit';

        if ($this->input->post()) {

            $config = array(
                array('field' => 'username', 'label' => 'Name', 'rules' => 'required|xss_clean|trim'),
                array('field' => 'activated', 'label' => 'Status', 'rules' => 'required|xss_clean|trim|callback_status_validate'),
                array('field' => 'contact_number', 'label' => 'Contact Number', 'rules' => 'required|xss_clean|trim|integer'),
                array('field' => 'email', 'label' => 'email', 'rules' => 'required|valid_email|xss_clean|trim'),
            );
            if ($this->input->post('password') || $this->input->post('Cpassword')) {
                $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
                $this->form_validation->set_rules('Cpassword', 'Confirm Password', 'required|matches[password]');
            }

            $this->form_validation->set_rules($config);
            /*
             * This flag is use for the upload error
             * if flag is false then upload has some error 
             * need to show them on page
             * 
             */
            $flag = TRUE;
            if (isset($_FILES['photo']['name']) && !empty($_FILES['photo']['name'])) {
                $config['upload_path'] = ADMIN_UPLOAD_IMAGE_PATH;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = ADMIN_MAX_UPLOAD_SIZE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('photo')) {
                    $error = array('errorUpload' => $this->upload->display_errors());
                    $flag = FALSE;
                } else {
                    $upload_data = $this->upload->data();
                    $adminuser = Adminuser::find($id);
                    if ($adminuser->image_name) {
                        $this->load->helper('file');
                        $path = ADMIN_UPLOAD_IMAGE_PATH . '/' . $adminuser->image_name;
                        @unlink($path);
                    }
                    $config['image_library'] = 'gd2';
                    $config ['new_image'] = ADMIN_UPLOAD_IMAGE_PATH . '/thumb';
                    $config['source_image'] = ADMIN_UPLOAD_IMAGE_PATH . '/' . $upload_data['raw_name'] . $upload_data['file_ext'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['thumb_marker'] = '';
                    $config['width'] = 27;
                    $config['height'] = 27;
                    $this->load->library('image_lib', $config);
                    if (!$this->image_lib->resize()) {
                        $error = array('errorResize' => $this->image_lib->display_errors());
                        $flag = FALSE;
                    }
                }
            }
            if ($this->form_validation->run() && $flag) {
                $conn = Adminuser::connection();
                $conn->transaction();
                try {
                    $editUser = adminuser::find($id);
                    $editUser->username = $this->form_validation->set_value('username');
                    $editUser->contact_number = $this->form_validation->set_value('contact_number');
                    $editUser->activated = $this->form_validation->set_value('activated');
                    $editUser->banned = 0;
                    $editUser->email = $this->form_validation->set_value('email');
                    $editUser->image_path = ADMIN_UPLOAD_IMAGE_PATH;
                    $editUser->updated = date('Y-m-d:h:i:s');
                    if ($this->input->post('password') && $this->input->post('Cpassword')) {
                        $editUser->password = $this->tank_auth_admin->create_password($this->form_validation->set_value('password'));
                    }

                    if (isset($upload_data['raw_name']) && !empty($upload_data['raw_name']))
                        $editUser->image_name = $upload_data['raw_name'] . $upload_data['file_ext'];
                    $editUser->save();
                    $this->landingpage($id);
                    $conn->commit();
                    $this->_show_message('Profile Updated successfully', 'success');
                    redirect('admin/myaccount');
                } catch (Exception $e) {
                    $conn->rollback();
                    $this->write_logs($e);
                }
            } else {
                if (isset($error) && !empty($error)) {
                    $this->data['uploadError'] = '';
                    foreach ($error as $k => $v) {
                        $this->data['uploadError'] .= $v . ' ';
                    }
                }
            }
        }
        $data['adminusers'] = Adminuser::find('first', array('conditions' => array('id = ?  ', $id)));

        if ($data['adminusers']->landingpage) {
            $data['landingPage'] = $data['adminusers']->landingpage->module_id;
        } else {
            $data['landingPage'] = 1;
        }
//        $paramsModule = array('conditions' => array('status = ?', 1));
//        $data['allmodules'] = Module::find_assoc($paramsModule, FALSE);
        if ($this->session->userdata[SITE_NAME . '_admin_user_data']['admingroup'] == 1 && $this->session->userdata[SITE_NAME . '_admin_user_data']['admingroup'] == $id) {
            $params = array('conditions' => array('status = ? and id = ?', 1, 1));
        } else if ($this->session->userdata[SITE_NAME . '_admin_user_data']['admingroup'] == 1 && $this->session->userdata[SITE_NAME . '_admin_user_data']['admingroup'] != $id) {
            $params = array('conditions' => array('status = ? ', 1));
        } else if ($this->session->userdata[SITE_NAME . '_admin_user_data']['admingroup'] == 2) {
            $params = array('conditions' => array('status = ? and id != ?', 1, 1));
        } else {
            $params = array('conditions' => array('status = ? and id != ?', 1, 1));
        }
        $data['admingp'] = Admingroup::find_assoc($params);
        $this->display_view('admin', $view, $data);
    }

    function landingpage($user_id) {
        if ($this->input->post('landing')) {
            /*
             * First check for landing page already in databse
             */
            $landing = Landingpage::find('first', array('conditions' => array('adminuser_id  = ?', $user_id)));
            if ($landing) {
                $landing->module_id = $this->input->post('landing');
                $landing->modified = databasedate();
                $landing->save();
            } else {
                $landing = new Landingpage();
                $landing->adminuser_id = $user_id;
                $landing->module_id = $this->input->post('landing');
                $landing->created = databasedate();
                $landing->modified = databasedate();
                $landing->save();
            }
        } else {
            $landing = new Landingpage();
            $landing->adminuser_id = $user_id;
            $landing->module_id = 1;
            $landing->created = databasedate();
            $landing->modified = databasedate();
            $landing->save();
        }
    }

    function status_validate($str) {
        if ($str == 1 || $str == 0) {
            return true;
        } else {
            $this->form_validation->set_message('status_validate', 'Please choose status');
            return FALSE;
        }
    }

    function select_validate($str) {
        if ($str == -1 || $str == 0 || $str == '') {
            $this->form_validation->set_message('select_validate', 'Please select group');
            return FALSE;
        } else {
            return true;
        }
    }

    function select_email($str) {
        $result = Adminuser::find_by_email($str);
        if ($result) {
            $this->form_validation->set_message('select_email', 'email address is already in use');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}

?>