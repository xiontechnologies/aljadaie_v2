<?php

/*
 * -----------------------------
 * ----Author shafiq
 * ----Class Product
 * ----For Products
 * ----Important Function index,add,edit,delete,active,deactive
 * -----------------------------
 */

class Products extends MY_admin {

    public function __construct() {
        parent::__construct();
    }

    /**
     * function index
     * @descp Used to list all products
     * */
    public function index($id = '') {
        /*
         * @var view : view name
         * @title :title of page
         */
        $view = 'products/view';
        $this->metas['title'] = array('Admin | Product List');
        array_push($this->scripts['js'], 'frontend/js/jquery-ui.js');
        array_push($this->scripts['css'], 'frontend/css/jquery_ui.css');
        /*
         * if user is super admin list all 
         * if user is admin list only its lower level admin
         * currently showing all user to except super admin
         * 
         */

        $data['products'] = Product::find('all', array('order' => 'updated desc'));
        /*
         * checking user all rights and sent to view
         * for its permission view
         */
        //$this->data['userRights'] = $this->check_rights();
        $this->display_view('admin', $view, $data);
    }

    public function add() {
        array_push($this->scripts['js'], 'admin/tinymc/tinymce.min.js', 'frontend/js/jquery-ui.js', 'admin/js/products/product.js');
        array_push($this->scripts['css'], 'frontend/css/jquery_ui.css');
        $desc = 'tinymce.init({ selector: "#description"});tinymce.init({ selector: "#washcare"}); ';
        array_push($this->scripts['embed'], $desc);
        /*
         * @var view : view name
         * @title :title of page
         */
        $view = 'products/add';
        $this->metas['title'] = array('Admin | Product | Add');
        $where = array('conditions' => array('status = ?', 1));
        $data['section'] = Section::find_assoc($where);
        $data['category'] = Category::find('all', array('conditions' => array('status = ? and showinfilter = ? and id != ?', 1, 1, 7)));
        $config = array(
            array('field' => 'title', 'label' => 'Title', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'shortdescription', 'label' => 'Short Description', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'description', 'label' => 'Description', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'sku', 'label' => 'SKU', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'colorcode', 'label' => 'Color Code', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'category', 'label' => 'Category', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'price', 'label' => 'Price', 'rules' => 'required|xss_clean|trim|integer'),
            array('field' => 'stock', 'label' => 'Stock', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'washcare', 'label' => 'Washcare', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'metakeywords', 'label' => 'Meta keywords', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'productdesign', 'label' => 'Design', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'productimage', 'label' => 'Image', 'rules' => 'required|xss_clean|trim'),
//            array('field' => 'collection', 'label' => 'Collection', 'rules' => 'required|xss_clean|trim')
        );
        $this->form_validation->set_rules($config);
//        foreach ($data['category'] as $catk => $catv) {
//            $field = "variation[" . $catv->name . "][]";
//            $this->form_validation->set_rules($field, $catv->name, 'required|xss_clean|trim');
//        }
        if ($this->form_validation->run()) {
            $postdata = $this->input->post();
            $variation = $postdata['variation'];
            $productimage = explode('|', $postdata['productimage']);
            $productdesign = explode('|', $postdata['productdesign']);
            $postdata['section_id'] = $postdata['category'];
            $postdata['created'] = $postdata['updated'] = date("Y:m:d H:i:s");
            //            $sku = str_split("",$postdata['sku'],1);
            $sku = $postdata['sku'];
            $str_split = str_split($sku, 1);
            $postdata['item_kind'] = $str_split[0] . $str_split[1];
            $postdata['item_definition'] = $str_split[2] . $str_split[3];
            $postdata['collection'] = $str_split[4] . $str_split[5] . $str_split[6] . $str_split[7] . $str_split[8] . $str_split[9];
            $postdata['product_design'] = $str_split[10] . $str_split[11];
            $postdata['primary_color'] = $str_split[12] . $str_split[13];
            $postdata['secondary_color'] = $str_split[14];

            unset($postdata['category']);
            unset($postdata['variation']);
            unset($postdata['productimage']);
            unset($postdata['productdesign']);
            unset($postdata['submit']);
            $conn = Product::connection();
            $conn->transaction();
            try {
                $product = new Product($postdata);
                $product->save();
                foreach ($variation as $vk => $vv) {
//                    foreach ($vv as $k => $v) {
                    $productvariation = new Productvariation();
                    $productvariation->product_id = $product->id;
                    $productvariation->subcategory_id = $vv;
                    $productvariation->created = date("Y:m:d H:i:s");
                    $productvariation->updated = date("Y:m:d H:i:s");
                    $productvariation->save();
//                    }
                }
                $productphoto = new Productphoto();
                $productphoto->photo_id = $productimage[0];
                $productphoto->product_id = $product->id;
                $productphoto->created = date("Y:m:d H:i:s");
                $productphoto->updated = date("Y:m:d H:i:s");
                $productphoto->save();

                $productdesignObj = new Productdesign();
                $productdesignObj->design_id = $productdesign[0];
                $productdesignObj->product_id = $product->id;
                $productdesignObj->created = date("Y:m:d H:i:s");
                $productdesignObj->updated = date("Y:m:d H:i:s");
                $productdesignObj->save();
                $conn->commit();
                $this->_show_message('Product is added successfully', 'success');
                redirect('admin/products');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
                $this->_show_message('Product couldnot added please contact to developer', 'error');
                $this->data['product'] = (object) $this->input->post();
            }
        } else {
            if ($this->input->post()) {
                $this->data['product'] = (object) $this->input->post();
            }
        }
        $this->display_view('admin', $view, $data);
    }

    function edit($id = '') {
        if (!$id)
            show_404();
        array_push($this->scripts['js'], 'admin/tinymc/tinymce.min.js', 'frontend/js/jquery-ui.js', 'admin/js/products/product.js');
        array_push($this->scripts['css'], 'frontend/css/jquery_ui.css');
        $desc = 'tinymce.init({ selector: "#description"});tinymce.init({ selector: "#washcare"}); ';
        array_push($this->scripts['embed'], $desc);

        /*
         * @var view : view name
         * @title :title of page
         */
        $view = 'products/edit';
        $this->metas['title'] = array('Admin | Products | Edit');
        $this->data['product'] = Product::find('first', array('conditions' => array('id = ? ', $id)));
        $where = array('conditions' => array('status = ?', 1));
        $this->data['section'] = Section::find_assoc($where);
        $this->data['category'] = Category::find('all', array('conditions' => array('status = ? and showinfilter = ? and id != ?', 1, 1, 7)));
        $config = array(
            array('field' => 'title', 'label' => 'Title', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'shortdescription', 'label' => 'Short Description', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'description', 'label' => 'Description', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'sku', 'label' => 'SKU', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'colorcode', 'label' => 'Color Code', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'category', 'label' => 'Category', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'price', 'label' => 'Price', 'rules' => 'required|xss_clean|trim|integer'),
            array('field' => 'stock', 'label' => 'Stock', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'washcare', 'label' => 'Washcare', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'metakeywords', 'label' => 'Meta keywords', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'productdesign', 'label' => 'Design', 'rules' => 'required|xss_clean|trim'),
            array('field' => 'productimage', 'label' => 'Image', 'rules' => 'required|xss_clean|trim'),
//            array('field' => 'collection', 'label' => 'Collection', 'rules' => 'required|xss_clean|trim'),
        );
        $this->form_validation->set_rules($config);
//        foreach ($this->data['category'] as $catk => $catv) {
//            $field = "variation[" . $catv->name . "][]";
//            $this->form_validation->set_rules($field, $catv->name, 'required|xss_clean|trim');
//        }
        if ($this->form_validation->run()) {
            $postdata = $this->input->post();
            $variation = $postdata['variation'];
            $productimage = explode('|', $postdata['productimage']);
            $productdesign = explode('|', $postdata['productdesign']);
            $postdata['section_id'] = $postdata['category'];
            $sku = $postdata['sku'];
            $str_split = str_split($sku, 1);
            $postdata['item_kind'] = $str_split[0] . $str_split[1];
            $postdata['item_definition'] = $str_split[2] . $str_split[3];
            $postdata['collection'] = $str_split[4] . $str_split[5] . $str_split[6] . $str_split[7] . $str_split[8] . $str_split[9];
            $postdata['product_design'] = $str_split[10] . $str_split[11];
            $postdata['primary_color'] = $str_split[12] . $str_split[13];
            $postdata['secondary_color'] = $str_split[14];
            $postdata['updated'] = date("Y:m:d H:i:s");
            unset($postdata['category']);
            unset($postdata['variation']);
            unset($postdata['productimage']);
            unset($postdata['productdesign']);
            unset($postdata['submit']);
            $conn = Product::connection();
            $conn->transaction();
            try {
                $this->data['product']->update_attributes($postdata);
//                foreach ($this->data['product']->productphoto as $k => $v) {
//                    $v->update_attributes(array('photo_id' => $productimage[0], 'updated' => date("Y:m:d H:i:s")));
//                }
//                foreach ($this->data['product']->productdesign as $k => $v) {
//                    $v->update_attributes(array('design_id' => $productdesign[0], 'updated' => date("Y:m:d H:i:s")));
//                }
                $this->data['product']->productphoto->update_attributes(array('photo_id' => $productimage[0], 'updated' => date("Y:m:d H:i:s")));
                $this->data['product']->productdesign->update_attributes(array('design_id' => $productdesign[0], 'updated' => date("Y:m:d H:i:s")));
//                $temp = array();
//                foreach ($variation as $k => $v) {
//                    foreach ($v as $fv) {
//                        $temp[] = $fv;
//                    }
//                }
                foreach ($this->data['product']->productvariation as $k => $v) {
                    if (!in_array($v->subcategory_id, $variation)) {
                        /*
                         * Delete rows its unchecked
                         */
                        $v->delete();
                    } else {
                        /*
                         * Unset if available
                         */
                        $key = array_search($v->subcategory_id, $variation);
                        unset($variation[$key]);
                    }
                }
                /*
                 * Check remainging new variation and adding
                 */
                if ($variation) {
                    foreach ($variation as $k => $v) {
                        if (!empty($v)) {
//                        foreach ($v as $nk => $nv) {
                            $productvariation = new Productvariation();
                            $productvariation->product_id = $this->data['product']->id;
                            $productvariation->subcategory_id = $v;
                            $productvariation->updated = date("Y:m:d H:i:s");
                            $productvariation->save();
//                        }
                        }
                    }
                }
                $conn->commit();
                $this->_show_message('Product Edited successfully', 'success');
                redirect('admin/products');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
                $this->_show_message('Product couldnot Edited please contact to developer', 'error');
                $this->data['product'] = (object) $this->input->post();
                $this->data['postdata']['productimage'] = $this->input->post('productimage');
                $this->data['postdata']['productdesign'] = $this->input->post('productdesign');
                $this->data['postdata']['variation'] = $this->input->post('variation');
                if (empty($this->data['postdata']['variation']))
                    $this->data['product']->variation = array();
            }
        } else {
            if ($this->input->post()) {
                $this->data['product'] = (object) $this->input->post();
                $this->data['postdata']['productimage'] = $this->input->post('productimage');
                $this->data['postdata']['productdesign'] = $this->input->post('productdesign');
                $this->data['postdata']['variation'] = $this->input->post('variation');
                if (empty($this->data['postdata']['variation']))
                    $this->data['product']->variation = array();
                $this->_show_message('Error while Editing', 'error');
            }
        }
        $this->display_view('admin', $view, $this->data);
    }

    function delete($id = '') {
        if ($this->input->post('option')) {
            $ids = $this->input->post('option');
            $deleteObj = Product::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("Products deleted", 'success');
        } else {
            if (!$id)
                show_404();
            $deleteObj = Product::find($id);
            $deleteObj->delete();
            $this->_show_message("Product deleted", 'success');
        }
        redirect('admin/products');
    }

    function activate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Product::find_by_id($id);
        $editActivation->status = '1';
        $editActivation->save();
        $this->_show_message("Product activated successfully", 'success');
        redirect('admin/products');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Product::find_by_id($id);
        $editActivation->status = '0';
        $editActivation->save();
        $this->_show_message("Product deactivated successfully", 'success');
        redirect('admin/products');
    }

    function copyproduct($id = "") {
        $product = Product::find('first', array('conditions' => array('id=?', $id)));
        try {
            $conn = Product::connection();
            $conn->transaction();

            $copypro['description'] = $product->description;
            $copypro['shortdescription'] = $product->shortdescription;
            $copypro['sku'] = $product->sku;
            $copypro['colorcode'] = $product->colorcode;
            $copypro['section_id'] = $product->section_id;
            $copypro['price'] = $product->price;
            $copypro['discountprice'] = $product->discountprice;
            $copypro['weight'] = $product->weight;
            $copypro['stock'] = $product->stock;
            $copypro['washcare'] = $product->washcare;
            $copypro['tags'] = $product->tags;
            $copypro['metakeywords'] = $product->metakeywords;
            $copypro['metadescription'] = $product->metadescription;
            $copypro['status'] = $product->status;
            $copypro['title'] = $product->title;
            $copypro['created'] = date("Y:m:d H:i:s");
            $copypro['updated'] = date("Y:m:d H:i:s");
            $copypro['title'] = 'copy_' . $product->title;

            $copyproduct = new Product($copypro);
            $copyproduct->save();

            foreach ($product->variation as $vk => $vv) {
                $productvariation = new Productvariation();
                $productvariation->product_id = $copyproduct->id;
                $productvariation->subcategory_id = $vv;
                $productvariation->created = date("Y:m:d H:i:s");
                $productvariation->updated = date("Y:m:d H:i:s");
                $productvariation->save();
            }
            /*
             * Add photo and design in 
             * photo table as well as desing table
             * and replace $productimage[0] with photo table id
             * and replace $productdesign[0] with design table id
             */

            $product_photo = Productphoto::find(array('conditions' => array('product_id =? ', $id)));
            if ($product_photo->photo_id) {
                $photo_id = $product_photo->photo_id;
                $photo = Photo::find(array('conditions' => array('id =?', $photo_id)));
                $copyphoto = new Photo();
                $copyphoto->file_title = $photo->file_title;
                $copyphoto->file_name = $photo->file_name;
                $copyphoto->full_path = $photo->full_path;
                $copyphoto->file_path = $photo->file_path;
                $copyphoto->file_type = $photo->file_type;
                $copyphoto->raw_name = $photo->raw_name;
                $copyphoto->orig_name = $photo->orig_name;
                $copyphoto->client_name = $photo->client_name;
                $copyphoto->file_ext = $photo->file_ext;
                $copyphoto->file_size = $photo->file_size;
                $copyphoto->image_width = $photo->image_width;
                $copyphoto->image_height = $photo->image_height;
                $copyphoto->image_type = $photo->image_type;
                $copyphoto->status = $photo->status;
                $copyphoto->created = date("Y:m:d H:i:s");
                $copyphoto->updated = date("Y:m:d H:i:s");
                $copyphoto->save();
            }

            $productphoto = new Productphoto();
            $productphoto->photo_id = $copyphoto->id;
            $productphoto->product_id = $copyproduct->id;
            $productphoto->created = date("Y:m:d H:i:s");
            $productphoto->updated = date("Y:m:d H:i:s");
            $productphoto->save();

            $product_design = Productdesign::find(array('conditions' => array('product_id =? ', $id)));
            if ($product_design->design_id) {
                $photo_id = $product_design->design_id;
                $photo = Design::find(array('conditions' => array('id =?', $photo_id)));
                $copydesign = new Design();
                $copydesign->file_title = $photo->file_title;
                $copydesign->file_name = $photo->file_name;
                $copydesign->full_path = $photo->full_path;
                $copydesign->file_path = $photo->file_path;
                $copydesign->file_type = $photo->file_type;
                $copydesign->raw_name = $photo->raw_name;
                $copydesign->orig_name = $photo->orig_name;
                $copydesign->client_name = $photo->client_name;
                $copydesign->file_ext = $photo->file_ext;
                $copydesign->file_size = $photo->file_size;
                $copydesign->image_width = $photo->image_width;
                $copydesign->image_height = $photo->image_height;
                $copydesign->image_type = $photo->image_type;
                $copydesign->status = $photo->status;
                $copydesign->created = date("Y:m:d H:i:s");
                $copydesign->updated = date("Y:m:d H:i:s");
                $copydesign->save();
            }

            $productdesignObj = new Productdesign();
            $productdesignObj->design_id = $copydesign->id;
            $productdesignObj->product_id = $copyproduct->id;
            $productdesignObj->created = date("Y:m:d H:i:s");
            $productdesignObj->updated = date("Y:m:d H:i:s");
            $productdesignObj->save();
            $conn->commit();
            $this->_show_message('Product is copy successfully', 'success');
            redirect('admin/products');
        } catch (Exception $e) {
            echo '<pre>';
            print_r($e->getMessage());
            exit;
        }
    }

    function viewimages() {
        $offset = $this->input->get('of') ? $this->input->get('of') : 0;
        $limit = 50;
        $folder = 'uploads/productimage/';
        $filetype = '*.*';
        $files = glob($folder . $filetype);
        $count = count($files);
        $data['product'] = array();
        foreach ($files as $image) {
            $data['product'][] = $image;
        }
        $data['view'] = $this->load->view('admin/products/viewimages', $data, true);
        $data['offset'] = $offset + $limit;
        print_r(json_encode($data));
    }

    function viewdesigns() {
        $offset = $this->input->get('of') ? $this->input->get('of') : 0;
        $limit = 50;
        $folder = 'uploads/productdesign/';
        $filetype = '*.*';
        $files = glob($folder . $filetype);
        $count = count($files);
        $data['product'] = array();
        foreach ($files as $image) {
            $data['product'][] = $image;
        }
        $data['view'] = $this->load->view('admin/products/viewdesigns', $data, true);
        $data['offset'] = $offset + $limit;
        print_r(json_encode($data));
    }

    function adddesign() {
        $title = $this->input->get('name');
        $size = getimagesize($this->input->get('source'));
        if ($data = $this->insert_into_design($title, $size)) {
            $message['message'] = $data;
            echo json_encode($message);
            exit;
        } else {
            $message['error'] = $this->lang->line('some_thing_wrong');
            echo json_encode($message);
            exit;
        }
    }

    function addimage() {
        $title = $this->input->get('name');
        $size = getimagesize($this->input->get('source'));
        if ($data = $this->insert_into_image($title, $size)) {
            $message['message'] = $data;
            echo json_encode($message);
            exit;
        } else {
            $message['error'] = $this->lang->line('some_thing_wrong');
            echo json_encode($message);
            exit;
        }
    }

    function insert_into_design($name, $size) {
        $photoObj = new Design();
        $photoObj->file_title = $name;
        $photoObj->file_name = $name;
        $photoObj->orig_name = $name;
        $photoObj->client_name = $name;
        $photoObj->is_image = 1;
        $photoObj->status = 1;
        $photoObj->created = databasedate();
        $photoObj->updated = databasedate();
        $photoObj->save();
        $savephoto[] = array('id' => $photoObj->id . '|' . $photoObj->file_name, 'path' => base_url(PRODUCTDESIGN_PATH) . "/" . $photoObj->file_name);
        return $savephoto;
    }

    function insert_into_image($name, $size) {
        $photoObj = new Photo();
        $photoObj->file_title = $name;
        $photoObj->file_name = $name;
        $photoObj->orig_name = $name;
        $photoObj->client_name = $name;
        $photoObj->is_image = 1;
        $photoObj->status = 1;
        $photoObj->created = databasedate();
        $photoObj->updated = databasedate();
        $photoObj->save();
        $savephoto[] = array('id' => $photoObj->id . '|' . $photoObj->file_name, 'path' => base_url(PRODUCTIMAGE_PATH) . "/" . $photoObj->file_name);
        return $savephoto;
    }

}
