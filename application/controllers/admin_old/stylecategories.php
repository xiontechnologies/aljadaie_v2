<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Stylecategories extends MY_admin {

    function index() {
        $view = "stylecategory/view";
        $data['userRights'] = $this->check_rights();
        $data['alldata'] = StyleCategory::find('all', array('order' => 'updated desc'));
        $this->display_view("admin", $view, $data);
    }

    function add() {
        $view = "stylecategory/add";
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean');

        if ($this->form_validation->run()) {
            $conn = StyleCategory::connection();
            $conn->transaction();
            try {
                $addcategory['name'] = $this->form_validation->set_value('name');
                $addcategory['status'] = $this->input->post('status');
                $addcategory['created'] = date("Y-m-d h:i:s");
                $addcategory['updated'] = date("Y-m-d h:i:s");

                $styleCategory = new StyleCategory($addcategory);
                $styleCategory->save();
                $conn->commit();
                $this->_show_message("Style Category added successfully", "success");
                redirect('admin/stylecategory');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
            if ($this->input->post()) {
                $data['postdata'] = $this->input->post();
            }
        }
        $data['postdata'] = $this->input->post();
        $this->display_view("admin", $view, $data);
    }

    function edit($id) {
        $view = "stylecategory/add";
        $data['postdata'] = StyleCategory::find($id);
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean');

        if ($this->form_validation->run()) {
            $conn = StyleCategory::connection();
            $conn->transaction();
            try {
                $styleCategory = StyleCategory::find($id);
                $styleCategory->name = $this->form_validation->set_value('name');
                $styleCategory->status = $this->input->post('status');
                $styleCategory->updated = date("Y-m-d h:i:s");
                $styleCategory->save();
                $conn->commit();
                $this->_show_message("Style Category updated successfully", "success");
                redirect('admin/stylecategory');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
            if ($this->input->post()) {
                $data['postdata'] = $this->input->post();
            }
        }
        $this->display_view("admin", $view, $data);
    }

    function delete($id) {
        if ($this->input->post('option')) {
            $ids = $this->input->post('option');

            $deleteObj = StyleCategory::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("style categories deleted", 'success');
        } else {
            if (!$id)
                show_404();
            $deleteObj = StyleCategory::find($id);
            $deleteObj->delete();
            $this->_show_message("Style category deleted", 'success');
        }
        redirect('admin/stylecategory');
    }

    function activate($id = '') {
        if (!$id)
            show_404();

        $editArticle = StyleCategory::find_by_id($id);
        $editArticle->status = '1';
        $editArticle->save();
        $this->_show_message("Style category activated successfully", 'success');
        redirect('admin/stylecategory');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editArticle = StyleCategory::find_by_id($id);
        $editArticle->status = '0';
        $editArticle->save();
        $this->_show_message("Style category deactivated successfully", 'success');
        redirect('admin/stylecategory');
    }

}

?>
