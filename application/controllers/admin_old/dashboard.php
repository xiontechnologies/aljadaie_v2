<?php

/**
 * @author Nilesh
 * */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends MY_admin {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $view = 'dashboard';
        $this->metas['title'] = array('Dashboard');
        $this->display_view('admin', $view, $data = '');
    }

}

?>
