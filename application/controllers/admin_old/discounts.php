<?php

Class Discounts extends MY_admin {

    function index() {
        $view = "discount/view";
        $data['alldata'] = Discount::find('all', array('order' => 'updated desc'));
        $this->display_view('admin', $view, $data);
    }

    function select_usergroup($str) {
        if ($str == '-1' || $str == '0' || $str == '') {
            $this->form_validation->set_message('select_usergroup', "Please Select atleast one usergroup");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function add() {
        $view = "discount/add";
        array_push($this->scripts['js'], "js/datepicker_new.js");
        array_push($this->scripts['css'], "css/datePicker.css");
        $datejs = '$(document).ready(function(){
    $(function() {
        $( "#start_date" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        $( "#end_date" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        });});';
        array_push($this->scripts['embed'], $datejs);
        $config = array(
            array(
                'field' => 'usergroup',
                'label' => 'Usergroup',
                'rules' => 'required|callback_select_usergroup'
            ),
            array(
                'field' => 'amount',
                'label' => 'Amount',
                'rules' => 'required|integer'
            ),
            array(
                'field' => 'start_date',
                'label' => 'Start Date',
                'rules' => 'required'
            ),
            array(
                'field' => 'end_date',
                'label' => 'End Date',
                'rules' => 'required'
            )
        );
//        echo '<pre>';print_r($_POST);exit;
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run()) {
            $conn = Discount::connection();
            $conn->transaction();
            try {
                if ($this->input->post('usergroup'))
                    $adddiscount['user_group_id'] = implode(',', $this->input->post('usergroup'));
                $adddiscount['type'] = $this->input->post('discount');
                $adddiscount['amount'] = $this->form_validation->set_value('amount');
                $adddiscount['start_date'] = date("Y-m-d", strtotime($this->form_validation->set_value('start_date')));
                $adddiscount['end_date'] = date("Y-m-d", strtotime($this->form_validation->set_value('end_date')));
                $adddiscount['created'] = date("Y-m-d");
                $adddiscount['updated'] = date("Y-m-d");
                $discount = new Discount($adddiscount);
                $discount->save();
                $conn->commit();
                $this->_show_message("Discount added successfully", 'success');
                redirect('admin/discounts/list');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
//            $formerror = "";
//            $error = $this->form_validation->get_field_data();
//            foreach ($error as $err) {
//                $formerror.= $err['error'];
//            }
//            $this->_show_message($formerror, 'error');
//            redirect('admin/discounts/add');
            if ($this->input->post()) {
                $data['postdata'] = (object) $this->input->post();
            }
        }
        $data['usergroup'] = Membergroup::find_assoc(array('conditions' => array('status = ?', 1)));
        $this->display_view('admin', $view, $data);
    }

    function edit($id = "") {
        $view = "discount/edit";
        array_push($this->scripts['js'], "js/datepicker_new.js");
        array_push($this->scripts['css'], "css/datePicker.css");
        $datejs = '$(document).ready(function(){
    $(function() {
        $( "#start_date" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        $( "#end_date" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        });});';
        array_push($this->scripts['embed'], $datejs);
        $config = array(
            array(
                'field' => 'usergroup',
                'label' => 'Usergroup',
                'rules' => 'required|callback_select_usergroup'
            ),
            array(
                'field' => 'amount',
                'label' => 'Amount',
                'rules' => 'required'
            ),
            array(
                'field' => 'start_date',
                'label' => 'Start Date',
                'rules' => 'required'
            ),
            array(
                'field' => 'end_date',
                'label' => 'End Date',
                'rules' => 'required'
            )
        );
//        echo '<pre>';print_r($_POST);exit;
        $this->form_validation->set_rules($config);
        $data['editdata'] = Discount::find($id);
        if ($this->form_validation->run()) {
            $conn = Discount::connection();
            $conn->transaction();
            try {
                $editdata = Discount::find($id);
                if ($this->input->post('usergroup'))
                    $editdata->user_group_id = implode(",", $this->input->post('usergroup'));
                $editdata->type = $this->input->post('discount');
                $editdata->amount = $this->form_validation->set_value('amount');
                $editdata->start_date = date("Y-m-d", strtotime($this->input->post('start_date')));
                $editdata->end_date = date("Y-m-d", strtotime($this->input->post('end_date')));
                $editdata->updated = date("Y-m-d");
                $editdata->save();
                $conn->commit();
                $this->_show_message("Discount updated successfully", 'success');
                redirect('admin/discounts/list');
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
            if ($this->input->post())
                $data['postdata'] = $this->input->post();
        }
        $data['usergroup'] = Membergroup::find_assoc(array('conditions' => array('status=?', 1)));
        $this->display_view('admin', $view, $data);
    }

    function delete($id = "") {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Discount::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
        } else {
            $deleteobj = Discount::find($id);
            $deleteobj->delete();
            echo '1';
        }
        $this->_show_message("deleted successfully", 'success');
        redirect("admin/discounts/list");
    }

}

?>