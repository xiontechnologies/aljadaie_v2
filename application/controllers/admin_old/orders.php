<?php

Class Orders extends MY_admin {

    function index() {
        array_push($this->scripts['js'], 'frontend/js/jquery-ui.js');
        array_push($this->scripts['css'], 'frontend/css/jquery_ui.css');
        $view = "order/view";
        $data['allorders'] = Order::find('all', array('order' => 'updated_date desc'));
        $this->display_view('admin', $view, $data);
    }

    function productlisting($order_id = '') {
        $view = "order/productlisting";
        $data['allproducts'] = Order :: find('all', array('conditions' => array('id=?', $order_id), 'order' => 'updated_date desc'));
//        echo'<pre>';
//        print_r($this->data['allproducts'][0]->orderdetail[0]->product);
//        exit;
        $this->display_view('admin', $view, $data);
    }

    function edit($id = '') {
        if (!$id)
            show_404();

        $view = 'order/edit';
        $this->metas['title'] = array('Admin | order | Edit');
        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
        $validation = "
                $('document').ready(function(){
                    $('#addadminfrm').validationEngine();
                });";
        array_push($this->scripts['embed'], $validation);
        $this->data['order'] = Order::find(array('conditions' => array('id = ? ', $id)));
        $config = array(
            array('field' => 'order_Status', 'label' => 'Order Status', 'rules' => 'required'),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run()) {
            $editorder = Order::find($id);
            $editorder->order_status = $this->form_validation->set_value('order_Status');
            $editorder->save();

            $orderNumber = date("Ymd", strtotime($editorder->created_date)) . '000' . $order->id;
            $data['order'] = Order::find(array('conditions' => array('id = ?', $id)));
            $data['username'] = $data['order']->first_name . " " . $data['order']->last_name;
            $data['useremail'] = $data['order']->email;
            $data['status'] = $editorder->order_status;
            $data['ordernumber'] = $orderNumber;
            $data['billingaddress'] = $editorder->address;
            $data['shippingaddress'] = $editorder->shipping_address;
            $data['created_date'] = $editorder->created_date;
            $data['product'] = Orderdetail::find('all', array('conditions' => array('order_id = ?', $id)));
            $this->_send_email('order_place', $data['useremail'], $data);
            $this->_show_message('Order status updated successfully', 'success');
            redirect('admin/orders/index');
        } else {
            if ($this->input->post()) {
                $this->data['postdata'] = (object) $this->input->post();
                $this->_show_message('Error while Editing', 'error');
            }
        }
        $this->display_view('admin', $view, $this->data);
    }

    function changestatus($order_id = '') {
        if (!$order_id)
            show_404();
        $conn = Order :: connection();
        $conn->transaction();
        try {
            $order = Order :: find($order_id);
            $order->order_status = $this->input->post('status');
            $order->save();
            $conn->commit();
            $orderNumber = date("Ymd", strtotime($order->created_date)) . '000' . $order->id;
            $data['order'] = Order::find(array('conditions' => array('id = ?', $order_id)));
            $data['username'] = $data['order']->first_name . " " . $data['order']->last_name;
            $data['useremail'] = $data['order']->email;
            $data['status'] = $order->order_status;
            $data['ordernumber'] = $orderNumber;
            $data['billingaddress'] = $order->address;
            $data['shippingaddress'] = $order->shipping_address;
            $data['created_date'] = $order->created_date;
            $data['product'] = Orderdetail::find('all', array('conditions' => array('order_id = ?', $order_id)));
            $this->_send_email('order_place', $data['useremail'], $data);
        } catch (exception $e) {
            $this->write_logs($e);
        }
        redirect('admin/orders/list');
    }

    function viewproduct($id = "") {
        $data['category'] = Category::find('all', array('conditions' => array('status = ? and showinfilter = ? and id != ?', 1, 1, 7)));
        $data['order'] = Order::find('first', array('conditions' => array('id =? ', $id)));
        $this->load->view('admin/order/viewproduct', $data);
    }

}

?>