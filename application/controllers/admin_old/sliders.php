<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Sliders extends MY_admin {

    function index() {
        $data['alldata'] = Slider::find('all', array('order' => 'updated desc'));
        $view = "slider/view";
        $this->display_view("admin", $view, $data);
    }

    function add() {
        array_push($this->scripts['js'], 'admin/js/products/slider.js');
        $view = "slider/add";

        $this->form_validation->set_rules('name', 'Design Name', 'required');
        $this->form_validation->set_rules('sliderimage', 'Slider Image', 'required');
        if ($this->form_validation->run()) {
            $postdata = $this->input->post();
            unset($postdata['submit']);
            unset($postdata['sliderimage']);
            $sliderimage = $this->input->post('sliderimage');
            $photo_id = explode("|", $sliderimage);
            $postdata['photo_id'] = $photo_id[0];
            $postdata['created'] = $postdata['updated'] = date("Y:m:d H:i:s");
            $add_slider = new Slider($postdata);
            $add_slider->save();
            $this->_show_message("Slider added successfully", 'success');
            redirect("admin/sliders/list");
        } else {
//            echo '<pre>';print_r($this->form_validation->get_field_data());exit;
            if ($this->input->post())
                $data['postdata'] = (object) $this->input->post();
        }
//        echo '<pre>';print_r( $data['postdata']);exit;
        $data['postdata'] = (object) $this->input->post();
        $this->display_view('admin', $view, $data);
    }

    function edit($id = "") {
        array_push($this->scripts['js'], 'admin/js/products/slider.js');
        $data['editdata'] = Slider::find_by_id($id);
        $this->form_validation->set_rules('name', 'Design Name', 'required');
        $this->form_validation->set_rules('sliderimage', 'Slider Image', 'required');
        if ($this->form_validation->run()) {
            $conn = Slider::connection();
            $conn->transaction();
            try {
                $sliderimage = $this->input->post('sliderimage');
                $photo_id = explode("|", $sliderimage);
                $editdata = Slider::find_by_id($id);
                $editdata->name = $this->input->post('name');
                $editdata->status = $this->input->post('status');
                $editdata->photo_id = $photo_id[0];
                $editdata->updated = date("Y:m:d H:i:s");
                $editdata->save();
                $conn->commit();

//            $editdata->nam
                $this->_show_message("Slider updated successfully", 'success');
                redirect("admin/sliders/list");
            } catch (Exception $e) {
                $this->write_logs($e);
                $conn->rollback();
            }
        } else {
//            echo '<pre>';print_r($this->form_validation->get_field_data());exit;
            if ($this->input->post())
                $data['postdata'] = (object) $this->input->post();
        }
        $view = "slider/edit";
        $this->display_view("admin", $view, $data);
    }

    function delete($id = '') {

        if ($this->input->post('option')) {
            $ids = $this->input->post('option');

            $deleteObj = Slider::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $photo_id = $obj->photo_id;
                $photo = Photo::find_by_id($photo_id);
                if (isset($photo->file_name) && !empty($photo->file_name)) {
                    $this->load->helper('file');
                    $path = SLIDER_PATH . "/" . $photo->file_name;
                    @unlink($path);
                }
                $obj->delete();
            }
            $this->_show_message("Sliders deleted successfully", 'success');
        } else {
            if (!$id)
                show_404();
            $deleteObj = Slider::find($id);
//            echo '<pre>';print_r($deleteObj->photo_id);exit;
            if ($deleteObj->photo_id && !empty($deleteObj->photo_id)) {
                $photo_id = $deleteObj->photo_id;
                $photo = Photo::find_by_id($photo_id);
//                $this->load->helper('file');
                $path = SLIDER_PATH . '/' . $photo->file_name;
                unlink($path);
            }
            $deleteObj->delete();
            $this->_show_message("Slider deleted successfully", 'success');
        }
        redirect('admin/sliders');
    }

    function activate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Slider::find_by_id($id);
        $editActivation->status = '1';
        $editActivation->save();
        $this->_show_message("Slider activated successfully", 'success');
        redirect('admin/sliders');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Slider::find_by_id($id);
        $editActivation->status = '0';
        $editActivation->save();
        $this->_show_message("Slider deactivated successfully", 'success');
        redirect('admin/sliders');
    }

}

?>
