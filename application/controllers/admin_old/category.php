<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Category extends MY_admin {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $view = 'category/view';
        $pagetitle = 'Section';
        $data['alldata'] = Section::find('all', array('order' => 'updated desc'));
        //$this->data['userRights'] = $this->check_rights();
        $this->display_view('admin', $view, $data);
    }

    function add() {
        $view = 'category/add';
        $this->metas['title'] = array('Admin | Categories | Add');
        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
        $validation = "
                $('document').ready(function(){
                    $('#addadminfrm').validationEngine();
                });";
        array_push($this->scripts['embed'], $validation);
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        if ($this->form_validation->run()) {
            $data = new Section();
            $data->name = $this->form_validation->set_value('name');
            $data->status = $this->form_validation->set_value('status');
            $data->created = date('Y-m-d H:i:s');
            $data->updated = date('Y-m-d H:i:s');
            $data->save();
            $this->_show_message('Category is added successfully', 'success');
            redirect('admin/categories');
        } else {

            if ($this->input->post()) {
                $this->_show_message('Error', 'error');
                $data['postdata'] = (object) $this->input->post();
            }
        }
        $data['postdata'] = (object) $this->input->post();
        $this->display_view('admin', $view, $data);
    }

    function edit($id = '') {
        if (!$id)
            show_404();
        /*
         * @var view : view name
         * @title :title of page
         */
        $view = 'category/add';
        $this->metas['title'] = array('Admin | Categories | Edit');
        array_push($this->scripts['css'], 'css/validation.engine.css', 'css/validationEngine.jquery.css');
        array_push($this->scripts['js'], 'js/jquery.validationEngine-en.js', 'js/jquery.validationEngine.js');
        $validation = "
                $('document').ready(function(){
                    $('#addadminfrm').validationEngine();
                });";
        array_push($this->scripts['embed'], $validation);
        $this->data['postdata'] = Section::find(array('conditions' => array('id = ? ', $id)));
        $config = array(
            array('field' => 'name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'status', 'label' => 'Status', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run()) {
            $editcategory = Section::find($id);
            $editcategory->name = $this->form_validation->set_value('name');
            $editcategory->status = $this->form_validation->set_value('status');
            $editcategory->updated = date('Y-m-d H:i:s');
            $editcategory->save();
            $this->_show_message('Categoryis edited successfully', 'success');
            redirect('admin/categories');
        } else {
            if ($this->input->post()) {
                $this->data['postdata'] = (object) $this->input->post();
                $this->_show_message('Error while Editing', 'error');
            }
        }
        $this->display_view('admin', $view, $this->data);
    }

    function delete($id = '') {
        if ($this->input->post()) {
            $ids = $this->input->post('option');
            $deleteObj = Section::find('all', array('conditions' => array('id in (?)', $ids)));
            foreach ($deleteObj as $obj) {
                $obj->delete();
            }
            $this->_show_message("Categories deleted", 'success');
            redirect('admin/categories');
        } else {
            if (!$id)
                show_404();
            $deleteObj = Section :: find($id);
            $deleteObj->delete();
            $this->_show_message("Category deleted", 'success');
            redirect('admin/categories');
        }
    }

    function activate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Section::find_by_id($id);
        $editActivation->status = '1';
        $editActivation->save();
        $this->_show_message("Category activated successfully", 'success');
        redirect('admin/categories');
    }

    function deactivate($id = '') {
        if (!$id)
            show_404();
        $editActivation = Section::find_by_id($id);
        $editActivation->status = '0';
        $editActivation->save();
        $this->_show_message("Category deactivated successfully", 'success');
        redirect('admin/categories');
    }

}

?>
