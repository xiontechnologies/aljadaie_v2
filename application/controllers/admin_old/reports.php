<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Class Reports extends MY_admin {

    public function index() {

//        echo '<pre>';print_r($_POST);exit;
        $view = "report/view";
        array_push($this->scripts['js'], "js/datepicker_new.js");
        array_push($this->scripts['css'], "css/datePicker.css");
        $datejs = '$(document).ready(function(){
    $(function() {
        $( "#from_date" ).datepicker({
        yearRange: "1950:" + new Date().getFullYear(),
        changeMonth: true,
        changeYear: true
        });
        });
        
    $(function() {
            $( "#to_date" ).datepicker({
            yearRange: "1950:" + new Date().getFullYear(),
            changeMonth: true,
            changeYear: true
            });
            });
});';
        array_push($this->scripts['embed'], $datejs);
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $username = $this->input->post('username');
        if (($from_date != "") && ($to_date == "") && ($username == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('created_date = ?', date("Y-m-d", strtotime($from_date)))));
        } else if (($to_date != "") && ($from_date == "") && ($username == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('updated_date = ?', date("Y-m-d", strtotime($to_date)))));
        } else if (($from_date != "") && ($to_date != "") && ($username == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('created_date >= ? and created_date <= ?', date("Y-m-d", strtotime($from_date)), date("Y-m-d", strtotime($to_date)))));
        } else if (($from_date != "") && ($username != "") && ($to_date == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('first_name = ? and created_date = ?', $username, date("Y-m-d", strtotime($from_date)))));
        } else if (($to_date != "") && ($username != "") && ($from_date == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('first_name = ? and updated_date = ?', $username, date("Y-m-d", strtotime($to_date)))));
        } else if ($from_date && $to_date && $username) {
            $data['allorders'] = Order::find('all', array('conditions' => array('date(created_date) >=? AND date(created_date) <= ? AND first_name=?', $from_date, $to_date, $username)));
        } else if (($username != "") && ($from_date == "") && ($to_date == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('first_name=?', $username)));
        } else {
            $data['allorders'] = Order::find('all', array('order' => 'updated_date desc'));
        }
        $data['postdata'] = (object) $this->input->post();
        $this->display_view('admin', $view, $data);
    }

    public function export() {
        $from_date = $this->input->get('from_date');
        $to_date = $this->input->get('to_date');
        $username = $this->input->get('username');
        if (($from_date != "") && ($to_date == "") && ($username == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('created_date = ?', date("Y-m-d", strtotime($from_date)))));
        } else if (($to_date != "") && ($from_date == "") && ($username == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('created_date = ?', date("Y-m-d", strtotime($to_date)))));
        } else if (($from_date != "") && ($to_date != "") && ($username == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('date(created_date) >=? AND date(created_date) <= ?', date("Y-m-d", strtotime($from_date)),date("Y-m-d", strtotime($to_date)))));
        } else if (($from_date != "") && ($username != "") && ($to_date == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('first_name = ? and created_date = ?', $username, date("Y-m-d", strtotime($from_date)))));
        } else if (($to_date != "") && ($username != "") && ($from_date == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('date(created_date) >=? AND date(created_date) <= ? AND first_name=?', date("Y-m-d", strtotime($from_date)), date("Y-m-d", strtotime($to_date)), $username)));
        } else if ($from_date && $to_date && $username) {
            $data['allorders'] = Order::find('all');
        } else if (($username != "") && ($from_date == "") && ($to_date == "")) {
            $data['allorders'] = Order::find('all', array('conditions' => array('first_name=?', $username)));
        } else {
            $data['allorders'] = Order::find('all', array('order' => 'updated_date desc'));
        }
        $this->output->set_header("Access-Control-Allow-Origin: *");
        $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
        $this->output->set_status_header(200);
        $this->output->set_content_type('application/json');

        $this->load->view('admin/report/downloadreport', $data);
    }

    public function userdownloadproducts($id = "") {
        $this->data['order'] = Order::find(array('conditions' => array('id=?', $id)));
        $this->load->view('admin/report/userreport', $this->data);
    }

}

?>
