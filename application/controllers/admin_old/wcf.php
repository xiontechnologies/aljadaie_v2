<?php

/* ALTER TABLE `products` CHANGE `price` `price` FLOAT NULL;
 * ALTER TABLE `products` CHANGE `discountprice` `discountprice` FLOAT NULL;
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Wcf extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('common');
    }

    function delete() {
        $result = $this->db->query("SELECT count(*),id,sku FROM `products` group by sku having count(*) > 1");
        $sku = $result->result();
        foreach ($sku as $k => $v) {
            $this->db->query("DELETE FROM `aljedaie_v2`.`products` WHERE `products`.`id` = $v->id");
        }
    }

    function imageProducts() {
        $result = $this->db->query("SELECT sku FROM `products` left join productphotos on productphotos.product_id = products.id where productphotos.photo_id !=1");
        $sku = $result->result();
        echo '<pre>';
        print_r($sku);
        exit;
    }

//UPDATE `aljedaie_v2`.`products` SET `status` = '0' WHERE `collection` LIKE '999%'
    function demo() {
        $wcfClient = new SoapClient('http://212.12.163.145:81/aljedaie_service/AljedaieService.svc?wsdl');
        $value = $this->input->get('value') ? $this->input->get('value') : '919904001499999';
        //        echo '<pre>';
//        print_r($wcfClient);
//        print_r($wcfClient->__getFunctions());
//        print_r($wcfClient->__getTypes());
        $result = $wcfClient->GetJSON_TableData_Where(array("username" => "consistenza", "password" => "dev@123", "table_name" => "J_PRODUCTS", "column_name" => 'ITEM_NO', "column_value" => '919904001499999'));
        echo '<pre>';
        print_r($result);
        exit;
//            echo '<meta charset="utf-8">';
        if ($result) {
            $product = json_decode($result->GetJSON_TableData_WhereResponse);
            echo '<pre>';
            print_r($product);
            exit;
        } else {
            echo '<pre>';
            print_r('$product');
            exit;
        }
    }

    function viewimages() {
        try {
//        $offset = $this->input->get('of') ? $this->input->get('of') : 0;
//        $limit = 50;
            $folder = 'uploads/productimage/';
            $filetype = '*.*';
            $files = glob($folder . $filetype);
            $count = count($files);
            $data['product'] = array();
            foreach ($files as $image) {
                $newImage = explode('.', (str_replace($folder, " ", $image)));
                $sku = trim($newImage[0]);
                $extension = trim($newImage[1]);
                $product = Product::find(array('conditions' => array("sku = '$sku'")));
                if ($product) {
//                if (file_exists(base_url(PRODUCTIMAGE_PATH) . '/' . $sku . 'jpg')) {
                    /*
                     * Update no image in product design and product image table
                     */
                    /*
                     * Get image data and insert into photo and design tables then to Productphoto and Productdesign
                     */


                    $productdesignObj = Productphoto::find('first', array('conditions' => array('product_id = ? ', $product->id)));
                    if ($productdesignObj) {
//                            $OldPhoto->delete();
                        $photoObj = new Photo();
                        $photoObj->file_title = $sku;
                        $photoObj->file_name = $sku . '.' . $extension;
                        $photoObj->orig_name = $sku;
                        $photoObj->client_name = $sku . '.' . $extension;
                        $photoObj->is_image = 1;
                        $photoObj->status = 1;
                        $photoObj->created = databasedate();
                        $photoObj->updated = databasedate();
                        $photoObj->save();
                        $productdesignObj->photo_id = $photoObj->id;
                    } else {
                        $productdesignObj = new Productphoto();
                        $productdesignObj->photo_id = 1;
                    }
                    $productdesignObj->product_id = $product->id;
                    $productdesignObj->created = databasedate();
                    $productdesignObj->updated = databasedate();
                    $productdesignObj->save();

                    /*
                     * also check for thumb
                     */
                    if (!file_exists(PRODUCTIMAGE_PATH . '/thumb/' . $sku . '.' . $extension)) {
                        /*
                         * Create thumb
                         */
                        $config_resize['image_library'] = 'gd2';
                        $config_resize['source_image'] = PRODUCTIMAGE_PATH . '/' . $sku . '.' . $extension;
                        $config_resize['new_image'] = PRODUCTIMAGE_PATH . '/thumb/';
                        $config_resize['maintain_ratio'] = TRUE;
                        $config_resize['width'] = 200;
                        $config_resize['height'] = 200;
                        $this->load->library('image_lib');

                        $this->image_lib->initialize($config_resize);
                        if (!$this->image_lib->resize()) {
                            echo '<pre>';
                            print_r($config_resize);
                            print_r($newImage);
                            exit;
                            $message = $this->image_lib->display_errors();
                            echo '<pre>';
                            print_r($message);
                            exit;
//                            $this->write_logs('', $message);
                        }
                    }


                    $productdesignObj = Productdesign::find('first', array('conditions' => array('product_id = ? ', $product->id)));
                    if ($productdesignObj) {
//                            $Olddesign->delete();

                        $designObj = new Design();
                        $designObj->file_title = $sku;
                        $designObj->file_name = $sku . '.' . $extension;
                        $designObj->orig_name = $sku;
                        $designObj->client_name = $sku . '.' . $extension;
                        $designObj->is_image = 1;
                        $designObj->status = 1;
                        $designObj->created = databasedate();
                        $designObj->updated = databasedate();
                        $designObj->save();
                        $productdesignObj->design_id = $designObj->id;
                    } else {
                        $productdesignObj = new Productdesign();
                        $productdesignObj->design_id = 1;
                    }
                    $productdesignObj->product_id = $product->id;
                    $productdesignObj->created = databasedate();
                    $productdesignObj->updated = databasedate();
                    $productdesignObj->save();
                    /*
                     * also check for thumb
                     */
                    if (!file_exists(PRODUCTDESIGN_PATH . '/thumb/' . $sku . '.' . $extension)) {
                        /*
                         * Create thumb
                         */
                        if (!file_exists(PRODUCTDESIGN_PATH . '/' . $sku . '.' . $extension)) {
                            copy(PRODUCTIMAGE_PATH . '/' . $sku . '.' . $extension, PRODUCTDESIGN_PATH . '/' . $sku . '.' . $extension);
                        }

                        $config_resize['image_library'] = 'gd2';
                        $config_resize['source_image'] = PRODUCTDESIGN_PATH . '/' . $sku . '.' . $extension;
                        $config_resize['new_image'] = PRODUCTDESIGN_PATH . '/thumb/';
                        $config_resize['maintain_ratio'] = TRUE;
                        $config_resize['width'] = 200;
                        $config_resize['height'] = 200;
                        $this->load->library('image_lib');
                        $this->image_lib->initialize($config_resize);
                        if (!$this->image_lib->resize()) {
                            $message = $this->image_lib->display_errors();
                            echo '<pre>';
                            print_r($message);
                            print_r($config_resize);
                            print_r('$message');
                            exit;
                        }
                    }
//                } else {
//                    $productdesignObj = new Productphoto();
//                    $productdesignObj->product_id = $product->id;
//                    $productdesignObj->photo_id = 1;
//                    $productdesignObj->created = databasedate();
//                    $productdesignObj->updated = databasedate();
//                    $productdesignObj->save();
//
//                    $productdesignObj = new Productdesign();
//                    $productdesignObj->product_id = $product->id;
//                    $productdesignObj->design_id = 1;
//                    $productdesignObj->created = databasedate();
//                    $productdesignObj->updated = databasedate();
//                    $productdesignObj->save();
//                }
                } else {
//                    echo '<pre>';
//                    print_r('else');
//                    exit;
                    $data['product'][] = $image;
                }
                //            $data['product'][] = $image;
            }
//        $data['view'] = $this->load->view('admin/products/viewimages', $data, true);
//        $data['offset'] = $offset + $limit;
            print_r(json_encode($data));
        } catch (Exception $e) {
            echo '<pre>';
            print_r($e->getMessage());
            exit;
        }
    }

    public function subcategoriesTables($subcat, $subcatOrcale, $pv, $categoryId, $tablename) {
        if (!$subcat) {
            $subcat = new Subcategory();
            $subcat->created = date("Y-m-d H:i:s");
        }
        $subcat->category_id = $categoryId;
        if (!empty($subcatOrcale)) {
            switch ($tablename) {
                case 'color':
                    $subcat->oracle_id = $subcatOrcale[0]->COLORCODE;
                    $subcat->name = $subcatOrcale[0]->COLORNAME_ENG;
                    break;
                case 'color2':
                    $subcat->oracle_id = $subcatOrcale[0]->COLORCODE2;
                    $subcat->name = $subcatOrcale[0]->COLORNAME_ENG;
                    break;
                case 'design':
                    $subcat->oracle_id = $subcatOrcale[0]->ITEMDESIGNID;
                    $subcat->name = $subcatOrcale[0]->ITEMDESIGNNAME_ENG;
                    break;
                case 'rows':
                    $subcat->oracle_id = $subcatOrcale[0]->ITEMROWID;
                    $subcat->name = $subcatOrcale[0]->ITEMROWNAME_ENG;
                    break;

                default:
                    break;
            }
            $subcat->status = 1;
            $subcat->showinfilter = 1;
        } else {
            switch ($tablename) {
                case 'color':
                    $subcat->oracle_id = $pv->COLORCODE;
                    $subcat->name = 'Orcale Color Code ' . $pv->COLORCODE;
                    break;
                case 'color2':
                    $subcat->oracle_id = $pv->COLORCODE2;
                    $subcat->name = 'Orcale Color2 Code ' . $pv->COLORCODE2;
                    break;
                case 'design':
                    $subcat->oracle_id = $pv->PRODUCTDESIGN;
                    $subcat->name = 'Orcale Design Code ' . $pv->PRODUCTDESIGN;
                    break;
                case 'rows':
                    $subcat->oracle_id = $pv->PRODUCTROW;
                    $subcat->name = 'Orcale itemrows Code ' . $pv->PRODUCTROW;
                    break;
                default:
                    break;
            }

            $subcat->status = 0;
            $subcat->showinfilter = 0;
        }
        $subcat->updated = date("Y-m-d H:i:s");

        $subcat->save();
        /*
         * Insert into product variation table
         */

        return $subcat->id;
    }

    public function productVariation($productVariation, $productId) {
        $variation = Productvariation::find('first', array('conditions' => array('subcategory_id = ? and product_id = ?', $productVariation, $productId)));
        if (!$variation) {
            $variation = new Productvariation();
            $variation->created = date("Y-m-d H:i:s");
        }
        $variation->subcategory_id = $productVariation;
        $variation->product_id = $productId;
        $variation->updated = date("Y-m-d H:i:s");
        $variation->save();
    }

    public function add() {
        $this->load->view('admin/wcf');
    }

// string GetJSON_TableData(string username, string password, string table_name);
//
//
//        string GetJSON_TableData_Limited(string username, string password, string table_name, int limit, int offset);
//
//
//        string GetJSON_TableData_Where(string username, string password, string table_name, string column_name, string column_value);
//
//
//        string GetJSON_ProductsData(string username, string password, string item_ids);
    public function index() {
        /*
         * CHANGE_FLAG == 0
         * No change in row
         * CHANGE_FLAG == 1
         * New Products
         * CHANGE_FLAG == 2
         * Product is Modified
         */
//        $data = databasedate();
//        mail('shafiq@consistenza.com', 'runn', "$data");
//        exit;
        //$cronDate = databasedate();
        //mail('shafiq@consistenza.com', 'runn', "Wcf runn start at $cronDate");
        set_time_limit(30000);
        //max_execution_time(30000);
        ini_set('max_execution_time', '3000000');
        ini_set('upload_max_filesize', '8000M');
        ini_set('default_socket_timeout', 6000000);
        $conn = Product::connection();
        $query = $this->db->query('select * from temp where id =1');
        $myLimit = $query->first_row();
        $limit = $myLimit->count + 21;
        $offset = $myLimit->count;
        //        $limit = $this->input->get('limit') ? $this->input->get('limit') : '10';
//        $offset = $this->input->get('offset') ? $this->input->get('offset') : '1';
//        if ($limit == '' && $offset == '') {
//            $productCount = Product::count();
//            $offset = $productCount + 47;
//            $limit = $offset + 22;
//        }
//        $offset = 1;
//        $limit = 50400;
//        $products = Product::find('all');
//        foreach ($products as $ok => $ov) {
//            $item[] = $ov->oracle_id;
//        }
//        $itemcode = implode(',', $item);

        $conn->transaction();
        try {
            $wcfClient = new SoapClient('http://212.12.163.145:81/aljedaie_service/AljedaieService.svc?wsdl');
//            echo '<pre>';
//            print_r($wcfClient);
//            print_r($wcfClient->__getFunctions());
//            print_r($wcfClient->__getTypes());

            $result = $wcfClient->GetJSON_TableData_Limited(array("username" => "consistenza", "password" => "dev@123", "table_name" => "J_PRODUCTS", "limit" => $limit, "offset" => $offset));
//            echo '<meta charset="utf-8">';
            $product = json_decode($result->GetJSON_TableData_LimitedResult);
            if ($product) {
                $cronDate = databasedate();
                //mail('shafiq@consistenza.com', 'runn', "Fetched all products at $cronDate");
                $resultOutput = '';
                foreach ($product as $pk => $pv) {

                    /*
                     *      a.Insert into product table
                     *      b.Insert into subcategories (variation) table if not present
                     *          1.Insert into productvairation table
                     *      c.Insert into images and design
                     *          1.Insert into productimages and productdesign
                     */
                    /*
                     * Find if product available or not
                     */
                    $str_split = str_split($pv->ITEM_NO, 1);
                    $product = Product::find('first', array('conditions' => array('oracle_id = ? ', $pv->ITEMID)));
                    $productArray = array(
                        'title' => $pv->PRODUCTNAME_ENG,
                        'title_arabic' => $pv->PRODUCTNAME,
                        'title_eng' => $pv->PRODUCTNAME_ENG,
                        'description' => $pv->PRODUCTNAME_ENG,
                        'sku' => $pv->ITEM_NO,
                        'colorcode' => '',
                        'collection' => $str_split[4] . $str_split[5] . $str_split[6] . $str_split[7] . $str_split[8] . $str_split[9],
                        'item_kind' => $str_split[0] . $str_split[1],
                        'item_definition' => $str_split[2] . $str_split[3],
                        'product_design' => $str_split[10] . $str_split[11],
                        'primary_color' => $str_split[12] . $str_split[13],
                        'secondary_color' => $str_split[14],
                        'section_id' => 1,
                        'price' => $pv->PRICE ? $pv->PRICE : '',
                        'discountprice' => $pv->DISCOUNT,
                        'weight' => '',
                        'stock' => $pv->QUANTITY,
                        'washcare' => $pv->PRODUCTNAME_ENG,
                        'tags' => $pv->PRODUCTNAME_ENG,
                        'metakeywords' => $pv->PRODUCTNAME_ENG,
                        'metadescription' => $pv->PRODUCTNAME_ENG,
                        'oracle_id' => $pv->ITEMID,
                        'oracleitemnumber' => $pv->ITEM_NO,
                        'set_id' => $pv->SETID,
//                        'set_id2' => $pv->SETID,
                        'status' => ($pv->ACTIVE == 'Y') ? '1' : '0',
                        'created' => $pv->CREATIONDATE,
                    );
                    if ($product) {
                        $productArray['updated'] = databasedate();
//                    $product->updated = databasedate();
                        $product->update_attributes($productArray);

                        $resultOutput = 'update';
                    } else {
                        /*
                         * product not found
                         */
                        $productArray['updated'] = $pv->CREATIONDATE;
                        $product = new Product($productArray);
                        $product->save();
                        $resultOutput = 'new';
                    }
//                $product->title = $pv->PRODUCTNAME_ENG;
//                $product->title_arabic = $pv->PRODUCTNAME;
//                $product->title_eng = $pv->PRODUCTNAME_ENG;
//                $product->description = $pv->PRODUCTNAME_ENG;
//                /*
//                 * Need to break sku code 
//                 *  Sku code : - 01-03-130052-02-447
//                 *  Item kind :- 01
//                 *  Item definition: - 03
//                 *  Collection code : 130052
//                 *  Design :02
//                 *  Primary color: 44
//                 *  Secondary color:7
//                 */
//                $str_split = str_split($pv->ITEM_NO, 1);
//                $product->sku = $pv->ITEM_NO;
//                $product->colorcode = '';
//                $product->collection = $str_split[4] . $str_split[5] . $str_split[6] . $str_split[7] . $str_split[8] . $str_split[9];
//                $product->item_kind = $str_split[0] . $str_split[1];
//                $product->item_definition = $str_split[2] . $str_split[3];
//                $product->product_design = $str_split[10] . $str_split[11];
//                $product->primary_color = $str_split[12] . $str_split[13];
//                $product->secondary_color = $str_split[14];
//                $product->section_id = 1;
//                $product->price = $pv->PRICE ? $pv->PRICE : '-';
//                $product->discountprice = $pv->DISCOUNT;
//                $product->weight = '';
//                $product->stock = $pv->QUANTITY;
//                $product->washcare = $pv->PRODUCTNAME_ENG;
//                $product->tags = $pv->PRODUCTNAME_ENG;
//                $product->metakeywords = $pv->PRODUCTNAME_ENG;
//                $product->metadescription = $pv->PRODUCTNAME_ENG;
//                $product->oracle_id = $pv->ITEMID;
//                $product->oracleitemnumber = $pv->ITEM_NO;
//                $product->status = ($pv->ACTIVE == 'Y') ? '1' : '0';
//                $product->created = $pv->CREATIONDATE;
//                echo '<pre>';
//                print_r($product);
//                exit;
//                $product->save();
                    /*
                     * ALTER TABLE `products` ADD `secondary_color` VARCHAR( 50 ) NOT NULL AFTER `collection` ,
                      ADD `primary_color` VARCHAR( 50 ) NOT NULL AFTER `secondary_color` ,
                      ADD `product_design` VARCHAR( 50 ) NOT NULL AFTER `primary_color` ,
                      ADD `item_definition` VARCHAR( 50 ) NOT NULL AFTER `product_design` ,
                      ADD `item_kind` VARCHAR( 50 ) NOT NULL AFTER `item_definition`
                     */
                    /*
                     * Primary color
                     * Category Id 1
                     */
                    $resultJ_Colors = $wcfClient->GetJSON_TableData_Where(array("username" => 'consistenza', "password" => 'dev@123', "table_name" => "J_Colors", "column_name" => "COLORCODE", "column_value" => $pv->COLORCODE));
                    if ($resultJ_Colors) {
                        $subcatOrcale = json_decode($resultJ_Colors->GetJSON_TableData_WhereResult);
                        $subcat = Subcategory::find('first', array('conditions' => array('oracle_id = ? and category_id = ?', $pv->COLORCODE, 1)));
                        $subId = $this->subcategoriesTables($subcat, $subcatOrcale, $pv, '1', 'color');
                        $this->productVariation($subId, $product->id);
                    } else {
//                    echo '<pre>';
//                    print_r($resultJ_Colors);
//                    exit;
                    }
                    /*
                     * Secondary color
                     * Category Id 2
                     */
                    $resultJ_Colors2 = $wcfClient->GetJSON_TableData_Where(array("username" => 'consistenza', "password" => 'dev@123', "table_name" => "J_Colors2", "column_name" => "COLORCODE2", "column_value" => $pv->COLORCODE2));
                    if ($resultJ_Colors) {
                        $subcatOrcale = json_decode($resultJ_Colors2->GetJSON_TableData_WhereResult);
                        $subcat = Subcategory::find('first', array('conditions' => array('oracle_id = ? and category_id = ?', $pv->COLORCODE2, 2)));
                        $subId = $this->subcategoriesTables($subcat, $subcatOrcale, $pv, '2', 'color2');
                        $this->productVariation($subId, $product->id);
                    }
                    /*
                     * Product design
                     * Category id 3
                     */
                    $resultJ_ITEMDESIGN = $wcfClient->GetJSON_TableData_Where(array("username" => 'consistenza', "password" => 'dev@123', "table_name" => "J_ITEMDESIGN", "column_name" => "ITEMDESIGNID", "column_value" => $pv->PRODUCTDESIGN));
                    if ($resultJ_ITEMDESIGN) {
                        $subcatOrcale = json_decode($resultJ_ITEMDESIGN->GetJSON_TableData_WhereResult);
                        $subcat = Subcategory::find('first', array('conditions' => array('oracle_id = ? and category_id = ?', $pv->PRODUCTDESIGN, 3)));
                        $subId = $this->subcategoriesTables($subcat, $subcatOrcale, $pv, '3', 'design');
                        $this->productVariation($subId, $product->id);
                    }
                    /*
                     * Product row
                     * Category id 4
                     */

                    $resultJ_ITEMROW = $wcfClient->GetJSON_TableData_Where(array("username" => 'consistenza', "password" => 'dev@123', "table_name" => "J_ITEMROW", "column_name" => "ITEMROWID", "column_value" => $pv->PRODUCTROW));
                    if ($resultJ_ITEMROW) {
                        $subcatOrcale = json_decode($resultJ_ITEMROW->GetJSON_TableData_WhereResult);
                        $subcat = Subcategory::find('first', array('conditions' => array('oracle_id = ? and category_id = ?', $pv->PRODUCTROW, 4)));
                        $subId = $this->subcategoriesTables($subcat, $subcatOrcale, $pv, '4', 'rows');
                        $this->productVariation($subId, $product->id);
                    }
//
//                    /*
//                     * Item kind row
//                     * Category id 5
//                     */
//                    $result = $wcfClient->GetJSONData_Column(array("username" => 'consistenza', "password" => 'dev@123', "table_name" => "J_Product_row", "column_name" => "ITEMROWID", "column_value" => $pv->ITEMROWID));
//                    $subcatOrcale = json_decode($result->GetJSONData_ColumnResult);
//                    $subcat = Subcategory::find('first', array('conditions' => array('oracle_id = ? and category_id = ?', $pv->ITEMROWID, 4)));
//                    $subId = $this->subcategoriesTables($subcat, $subcatOrcale, $pv, '4', 'rows');
//                    $this->productVariation($subId, $product->id);
//          
                    /*
                     * Same design and image for now
                     */


                    $result = explode('.', $pv->BIGIMAGEURL);
                    $fileName = $pv->BIGIMAGEURL;
                    if ($fileName && file_exists($fileName)) {
                        $img = file_get_contents($fileName);
                        $file = base_url(PRODUCTIMAGE_PATH) . '/' . $pv->ITEM_NO . $result[1];
                        file_put_contents($file, $img);
                        echo '<pre>';
                        print_r("Finally got first image");
                        exit;
//                    $imagename = "/" . $user->firstname . '-' . date('y-m-d-H-i-s') . '.jpg';
//                    $file = base_url(FRONTEND_USER_PROFILE_IMAGE) . '/' . $imagename;
//                    file_put_contents($file, $img);
//
////            $imagename = "/" . $user->firstname . '-' . date('y-m-d-H-i-s') . '.jpg';
////            copy($link, './uploads/frontend/profileimages/' . $imagename);
//                    //$images = glob('./uploads/frontend/profileimages' . $imagename);
//                    $imgsize = getimagesize(FRONTEND_USER_PROFILE_IMAGE . '/' . $imagename);
//                    $photo = array('size' => $imgsize, 'path' => $images);
//                    if ($photo['size'] != '' && $photo['path'] != '')
//                        return $photo;
//                    else
//                        return false;
                    } else if (file_exists(base_url(PRODUCTIMAGE_PATH) . '/' . $pv->ITEM_NO . '.jpg')) {
//                        echo '<pre>';
//                        print_r("found images $pv->ITEM_NO");
//                        exit;
                        /*
                         * Update no image in product design and product image table
                         */
                        /*
                         * Get image data and insert into photo and design tables then to Productphoto and Productdesign
                         */

                        $photoObj = new Photo();
                        $photoObj->file_title = $pv->ITEM_NO;
                        $photoObj->file_name = $pv->ITEM_NO . '.jpg';
                        $photoObj->orig_name = $pv->ITEM_NO;
                        $photoObj->client_name = $pv->ITEM_NO . '.jpg';
                        $photoObj->is_image = 1;
                        $photoObj->status = 1;
                        $photoObj->created = databasedate();
                        $photoObj->updated = databasedate();
                        $photoObj->save();
                        $productdesignObj = Productphoto::find('first', array('conditions' => array('product_id = ? ', $product->id)));
                        if ($productdesignObj) {
//                            $OldPhoto->delete();
                            $productdesignObj->photo_id = $photoObj->id;
                        } else {
                            $productdesignObj = new Productphoto();
                            $productdesignObj->photo_id = $photoObj->id;
                        }
                        $productdesignObj->product_id = $product->id;
                        $productdesignObj->created = databasedate();
                        $productdesignObj->updated = databasedate();
                        $productdesignObj->save();

                        /*
                         * also check for thumb
                         */
                        if (!file_exists(PRODUCTIMAGE_PATH . '/thumb/' . $pv->ITEM_NO . '.jpg')) {
                            /*
                             * Create thumb
                             */
                            $config_resize['image_library'] = 'gd2';
                            $config_resize['source_image'] = PRODUCTIMAGE_PATH . '/' . $pv->ITEM_NO . '.jpg';
                            $config_resize['new_image'] = PRODUCTIMAGE_PATH . '/thumb/';
                            $config_resize['maintain_ratio'] = TRUE;
                            $config_resize['width'] = 200;
                            $config_resize['height'] = 200;
                            $this->load->library('image_lib');
                            $this->image_lib->initialize($config_resize);
                            if (!$this->image_lib->resize()) {
                                $message = $this->image_lib->display_errors();
                                $this->write_logs('', $message);
                            }
                        }


                        $designObj = new Design();
                        $designObj->file_title = $pv->ITEM_NO;
                        $designObj->file_name = $pv->ITEM_NO . '.jpg';
                        $designObj->orig_name = $pv->ITEM_NO;
                        $designObj->client_name = $pv->ITEM_NO . '.jpg';
                        $designObj->is_image = 1;
                        $designObj->status = 1;
                        $designObj->created = databasedate();
                        $designObj->updated = databasedate();
                        $designObj->save();
                        $productdesignObj = Productdesign::find('first', array('conditions' => array('product_id = ? ', $product->id)));
                        if ($productdesignObj) {
//                            $Olddesign->delete();
                            $productdesignObj->design_id = $designObj->id;
                        } else {
                            $productdesignObj = new Productdesign();
                            $productdesignObj->design_id = $designObj->id;
                        }
                        $productdesignObj->product_id = $product->id;
                        $productdesignObj->created = databasedate();
                        $productdesignObj->updated = databasedate();
                        $productdesignObj->save();
                        /*
                         * also check for thumb
                         */
                        if (!file_exists(PRODUCTDESIGN_PATH . '/thumb/' . $pv->ITEM_NO . '.jpg')) {
                            /*
                             * Create thumb
                             */
                            $config_resize['image_library'] = 'gd2';
                            $config_resize['source_image'] = PRODUCTDESIGN_PATH . '/' . $pv->ITEM_NO . '.jpg';
                            $config_resize['new_image'] = PRODUCTDESIGN_PATH . '/thumb/';
                            $config_resize['maintain_ratio'] = TRUE;
                            $config_resize['width'] = 200;
                            $config_resize['height'] = 200;
                            $this->load->library('image_lib');
                            $this->image_lib->initialize($config_resize);
                            if (!$this->image_lib->resize()) {
                                $message = $this->image_lib->display_errors();
                                $this->write_logs('', $message);
                            }
                        }
                    } else {
                        $productdesignObj = new Productphoto();
                        $productdesignObj->product_id = $product->id;
                        $productdesignObj->photo_id = 1;
                        $productdesignObj->created = databasedate();
                        $productdesignObj->updated = databasedate();
                        $productdesignObj->save();

                        $productdesignObj = new Productdesign();
                        $productdesignObj->product_id = $product->id;
                        $productdesignObj->design_id = 1;
                        $productdesignObj->created = databasedate();
                        $productdesignObj->updated = databasedate();
                        $productdesignObj->save();
                    }
                }
                $conn->commit();
                $sql = "UPDATE `aljedaie_v2`.`temp` SET `count` = $limit WHERE `temp`.`id` =1";
                $sql2 = "UPDATE `aljedaie_v2`.`products` SET `status` = '0' WHERE `collection` LIKE '999%'";
                $this->db->query($sql);
                $this->db->query($sql2);
                //            $this->output->set_header("Access-Control-Allow-Origin: *");
//            $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
//            $this->output->set_status_header(200);
//            $this->output->set_content_type('application/json');
//            $this->output->_display();
                //print_r($this->input->get('callback') . "(" . json_encode(array('count' => $limit)) . ")");
                if (!$resultOutput)
                    $resultOutput = Product::count();
//                 $cronDate = databasedate();
//                mail('shafiq@consistenza.com', 'runn', "Wcf runn succfully at  $cronDate");
                print_r(json_encode(array('count' => $resultOutput)));
            } else {
//                mail('shafiq@consistenza.com', 'runn', 'No data found');
                print_r(json_encode(array('error' => 'no data found')));
            }
        } catch (Exception $e) {
            $conn->rollback();
            mail('shafiq@consistenza.com', 'runn', "rollback $e->getMessage()");
//            $this->output->set_header("Access-Control-Allow-Origin: *");
//            $this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
//            $this->output->set_status_header(200);
//            $this->output->set_content_type('application/json');
//            $this->output->_display();
            //print_r($this->input->get('callback') . "(" . json_encode(array('count' => $e->getMessage())) . ")");
            print_r(json_encode(array('count' => $e->getMessage())));

            exit;
        }
//        redirect('');
    }

}

//            
//  id
//  title = PRODUCTNAME_ENG
//  description = 
//  shortdescription
//  sku 
//  colorcode
//  collection
//  section_id
//  price = PRICE
//  discountprice = DISCOUNT
//  weight 
//  stock = QUANTITY
//  washcare
//  tags
//  metakeywords
//  metadescription = 
//  oracleId = ITEMID
//  oracleItemNumber = ITEM_NO
//  status = ACTIVE
//  created =CREATIONDATE 
//  updated
//            
//            "ITEMID": 62152,
//      "PRODUCTNAME": "Ø¬Ø§ÙƒØ§Ø±Ø¯ Ø´Ø§Ù†ÙŠÙ„ 140Ø³Ù… DIAMOND",
//      "ITEM_NO": "010299989025019",
//      "COLLECTIONID": null,
//      "SETID": null,
//      "PRICE": 82,
//      "DISCOUNT": 53,
//      "COLORCODE": 1,
//      "COLORCODE2": 9,
//      "ACTIVE": "Y",
//      "CREATIONDATE": "2005-02-12T15:24:23",
//      "BIGIMAGEURL": "http:\\\\jedaie-fur\\images\\big\\010299989025019.jpg",
//      "SMALLIMAGEURL": "http:\\\\jedaie-fur\\images\\medium\\010299989025019.jpg",
//      "SMALLIMAGEURL2": "http:\\\\jedaie-fur\\images\\small\\010299989025019.jpg",
//      "QUANTITY": 10,
//      "PRODUCTROW": 2,
//      "PRODUCTDESIGN": 25,
//      "PRODUCTNAME_ENG": "Ø¬Ø§ÙƒØ§Ø±Ø¯ Ø´Ø§Ù†ÙŠÙ„ 140Ø³Ù… DIAMOND",
//      "CHANGE_FLAG": 0,
//      "HAS_IMG": "Y"
//            
