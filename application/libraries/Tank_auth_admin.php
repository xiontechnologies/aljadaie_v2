<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @author Nilesh
 * */
require_once('phpass-0.1/PasswordHash.php');

/**
 * Tank_auth
 *
 * Authentication library for Code Igniter.
 *
 * @package		Tank_auth
 * @author		Ilya Konyukhov (http://konyukhov.com/soft/)
 * @version		1.0.9
 * @based on	DX Auth by Dexcell (http://dexcell.shinsengumiteam.com/dx_auth)
 * @license		MIT License Copyright (c) 2008 Erick Hartanto
 */
class Tank_auth_admin {

    private $error = array();
    public $ci = "";

    function __construct() {
        $this->ci = & get_instance();

        $this->ci->load->config('tank_auth', TRUE);

        $this->ci->load->library('session');
        $this->ci->load->database();
        $this->ci->load->model('tank_auth/adminusers');

        // Try to autologin
        // $this->autologin();
    }

    function login($login, $password) {
        if ((strlen($login) > 0) AND (strlen($password) > 0)) {
            if (!is_null($user = $this->ci->adminusers->get_user_by_email($login))) { // login ok
                // Does password match hash in database?
                $hasher = new PasswordHash(
                                $this->ci->config->item('phpass_hash_strength', 'tank_auth'),
                                $this->ci->config->item('phpass_hash_portable', 'tank_auth'));
                if ($hasher->CheckPassword($password, $user->password)) {  // password ok
                    //added by shafiq 
                    if (!$this->ci->adminusers->group_status($user->id) || $user->banned == 1) {         // fail - banned
                        $banned = $this->ci->lang->line('auth_message_banned');
                        $this->error = array('banned' => $banned);
                    } else {
                        $this->ci->session->set_userdata(SITE_NAME.'_admin_user_data', array(
                            'user_id' => $user->id,
                            'username' => $user->username,
                            'image' => $user->image_path,
                            'image_name' => $user->image_name,
                            'admingroup' => $user->admingroup_id,
                            'status' => ($user->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
                        ));
                        if ($user->activated == 0) {       // fail - not activated
                            $notactive = $this->ci->lang->line('auth_message_not_activated');
                            $this->error = array('not_activated' => $notactive);
                        } else {            // success
                            $this->clear_login_attempts($login);
                            $this->ci->adminusers->update_login_info($user->id, $this->ci->config->item('login_record_ip', 'tank_auth'), $this->ci->config->item('login_record_time', 'tank_auth'));
                            return TRUE;
                        }
                    }
                } else {              // fail - wrong password
                    $this->increase_login_attempt($login);
                    $this->error = array('password' => 'auth_incorrect_password');
                }
            } else {               // fail - wrong login
                $this->increase_login_attempt($login);
                $this->error = array('login' => 'auth_incorrect_login');
            }
        }
        return FALSE;
    }

    /**
     * Logout user from the site
     *
     * @return	void
     */
    function logout() {
        // See http://codeigniter.com/forums/viewreply/662369/ as the reason for the next line
        $this->ci->session->set_userdata(array(SITE_NAME.'_user_id' => '', SITE_NAME.'_username' => '', SITE_NAME.'_status' => ''));

        $this->ci->session->sess_destroy();
    }

    /**
     * Check if user logged in. Also test if user is activated or not.
     *
     * @param	bool
     * @return	bool
     */
    function is_logged_in($activated = TRUE) {
        return $this->ci->session->userdata(SITE_NAME.'_status') === ($activated ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED);
    }

    private function increase_login_attempt($login) {
        if ($this->ci->config->item('login_count_attempts', 'tank_auth')) {
            if (!$this->is_max_login_attempts_exceeded($login)) {
                $this->ci->load->model('tank_auth/login_attempts');
                $this->ci->login_attempts->increase_attempt($this->ci->input->ip_address(), $login);
            }
        }
    }

    function is_max_login_attempts_exceeded($login) {
        if ($this->ci->config->item('login_count_attempts', 'tank_auth')) {
            $this->ci->load->model('tank_auth/login_attempts');
            return $this->ci->login_attempts->get_attempts_num($this->ci->input->ip_address(), $login)
                    >= $this->ci->config->item('login_max_attempts', 'tank_auth');
        }
        return FALSE;
    }

    function get_error_message() {
        return $this->error;
    }

    /**
     * Clear all attempt records for given IP-address and login
     * (if attempts to login is being counted)
     *
     * @param	string
     * @return	void
     */
    private function clear_login_attempts($login) {
        if ($this->ci->config->item('login_count_attempts', 'tank_auth')) {
            $this->ci->load->model('tank_auth/login_attempts');
            $this->ci->login_attempts->clear_attempts(
                    $this->ci->input->ip_address(), $login, $this->ci->config->item('login_attempt_expire', 'tank_auth'));
        }
    }

    function create_user($data) {
        if (!$this->ci->adminusers->is_email_available($data['email'])) {
            $this->error = array('email' => 'auth_email_in_use');
        } else {
            // Hash password using phpass
            $password = $data['password'];
            $hasher = new PasswordHash(
                            $this->ci->config->item('phpass_hash_strength', 'tank_auth'),
                            $this->ci->config->item('phpass_hash_portable', 'tank_auth'));
            $hashed_password = $hasher->HashPassword($data['password']);
            unset($data['password']);

            $createdata = array(
                'password' => $hashed_password,
                'last_ip' => $this->ci->input->ip_address(),
            );
            $createdata = array_merge($createdata, $data);
            if (!is_null($res = $this->ci->adminusers->create_user($createdata))) {
                $data['user_id'] = $res['user_id'];
                $data['password'] = $password;
                unset($data['last_ip']);
                return $data;
            }
        }
        return NULL;
    }

    function create_password($new_pass) {
        $hasher = new PasswordHash(
                        $this->ci->config->item('phpass_hash_strength', 'tank_auth'),
                        $this->ci->config->item('phpass_hash_portable', 'tank_auth'));
        $hashed_password = $hasher->HashPassword($new_pass);
        return $hashed_password;
    }

}

?>
