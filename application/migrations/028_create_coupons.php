<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Coupons extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `type` enum('0','1') NOT NULL COMMENT '0 for single user 1 for multiple user',
  `amount` float NOT NULL,
  `to_date` datetime NOT NULL,
  `from_date` datetime NOT NULL,
  `to_amount` float NOT NULL,
  `from_amount` float NOT NULL,
  `no_of_used` int(10) NOT NULL,
  `no_of_time` int(11) NOT NULL,
  `discount_type` enum('0','1') NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `user_group_id` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
");
    }

    public function down() {
        $this->dbforge->drop_table('coupons');
    }

}

