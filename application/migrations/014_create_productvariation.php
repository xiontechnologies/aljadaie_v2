<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Productvariation extends CI_Migration {

    public function up() {
        $sql = "
CREATE TABLE `productvariations` (
`id` INT( 11 ) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`subcategory_id` INT( 11 ) unsigned NOT NULL ,
`product_id` INT( 11 ) unsigned NOT NULL ,
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL ,
INDEX ( `subcategory_id` )
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
";
        $this->dbforge->db->query($sql);
        $this->dbforge->db->query("ALTER TABLE `productvariations` ADD FOREIGN KEY ( `subcategory_id` ) REFERENCES `subcategories` (`id`) ON DELETE CASCADE ;");
        $this->dbforge->db->query("ALTER TABLE `productvariations` ADD FOREIGN KEY ( `product_id` ) REFERENCES `products` (`id`) ON DELETE CASCADE ;");
    }

    public function down() {
        $this->dbforge->drop_table('productvariations');
    }

}

