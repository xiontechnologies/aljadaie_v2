<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Order extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `company` varchar(250) NOT NULL,
  `fax` varchar(250) NOT NULL,
  `address` varchar(255) NOT NULL,
  `state` varchar(250) NOT NULL,
  `country` varchar(250) NOT NULL,
  `order_status` enum('pending','process','delivered') NOT NULL DEFAULT 'pending',
  `transaction_id` varchar(250) NOT NULL,
  `total_amt` decimal(15,2) NOT NULL,
  `city` varchar(250) NOT NULL,
  `postal_code` varchar(15) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `last_ip` varchar(100) NOT NULL,
  `shipping_address` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
    }
        

    public function down() {
        $this->dbforge->drop_table('orders');
    }

}

