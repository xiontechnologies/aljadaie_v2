<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Subcategory extends CI_Migration {

    public function up() {
        $sql = "CREATE TABLE `subcategories` (
`id` INT( 11 ) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`category_id` INT( 11 ) unsigned NOT NULL ,
`oracle_id` INT( 11 ) unsigned NOT NULL ,
`name` VARCHAR( 255 ) NOT NULL ,
`status` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT '0 for deactive 1 for active',
`showinfilter` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT '0 for no 1 for ye',
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL ,
INDEX ( `category_id` )
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
";
        $this->dbforge->db->query($sql);
        $this->dbforge->db->query("ALTER TABLE `subcategories` ADD FOREIGN KEY ( `category_id` ) REFERENCES `categories` (`id`) ON DELETE CASCADE ;");

    }

    public function down() {
        $this->dbforge->drop_table('subcategories');
    }

}

