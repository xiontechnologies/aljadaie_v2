<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Contents extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `short_desc` varchar(250) NOT NULL,
  `long_desc` text NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 for deactive, 1 for active',
  `position` enum('0','1') NOT NULL COMMENT '0 for top menu and 1 for footer menu',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

        $this->dbforge->db->query("INSERT INTO `contents` (`id`, `title`, `short_desc`, `long_desc`, `status`, `position`, `created`, `updated`) VALUES
(1, 'Home', 'Home', '', '1', '0', '2014-02-17 09:10:43', '2014-02-17 09:10:43'),
(2, 'About Us', 'About Us', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.</p>', '1', '0', '2014-02-17 09:11:04', '2014-02-17 09:11:04'),
(3, 'Privacy Policy', 'Privacy Policy', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '1', '1', '2014-02-17 09:12:30', '2014-02-17 09:12:30'),
(4, 'Terms of Use', 'Terms of Use', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '1', '1', '2014-02-17 09:12:45', '2014-02-17 09:12:45');
");
    }

    public function down() {
        $this->dbforge->drop_table('contents');
    }

}
