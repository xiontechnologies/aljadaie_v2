<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Productphoto extends CI_Migration {

    public function up() {
        $sql = "CREATE TABLE IF NOT EXISTS `productphotos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `photo_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `photo_id` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
        $this->dbforge->db->query($sql);
        $this->dbforge->db->query("ALTER TABLE `productphotos` ADD FOREIGN KEY ( `product_id` ) REFERENCES `products` (`id`) ON DELETE CASCADE ;");
        $this->dbforge->db->query("ALTER TABLE `productphotos` ADD FOREIGN KEY ( `photo_id` ) REFERENCES `photos` (`id`) ON DELETE CASCADE ;");
    }

    public function down() {
        $this->dbforge->drop_table('productphotos');
    }

}

