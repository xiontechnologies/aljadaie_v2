<?php

/*
 * @auther Shafiq
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Adminrights extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'module_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
            ),
            'adminuser_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
            ),
            'right' => array(
                'type' => 'INT',
                'constraint' => '20',
            ),
            'created' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
            'modified' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('adminuser_id');
        $this->dbforge->add_key('module_id');
        $this->dbforge->create_table('adminrights');
        $this->dbforge->db->query('ALTER TABLE `adminrights` ADD FOREIGN KEY ( `adminuser_id` ) REFERENCES `adminusers` (`id`) ON DELETE CASCADE ');
        $this->dbforge->db->query('ALTER TABLE `adminrights` ADD FOREIGN KEY ( `module_id` ) REFERENCES `modules` (`id`) ON DELETE CASCADE ');
        $sql = "INSERT INTO `adminrights` (`id`, `module_id`, `adminuser_id`, `right`, `created`, `modified`) VALUES
(1, 1, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(2, 2, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(3, 3, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(4, 4, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(5, 5, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(6, 6, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(7, 7, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(8, 8, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(9, 9, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(10,10, 1, 127, '2013-12-23 10:44:41', '2013-12-23 10:44:41'),
(11, 11, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(12, 12, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(13, 13, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(14, 14, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(15, 15, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(16, 16, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(17, 17, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(18, 18, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(19, 19, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(20, 20, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41'),
(21, 21, 1, 127, '2014-03-05 08:33:41', '2014-03-05 08:33:41');";
        $this->dbforge->db->query($sql);
    }

    public function down() {
        $this->dbforge->drop_table('adminrights');
    }

}
