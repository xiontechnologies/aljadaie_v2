<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Subscribnewsletters extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `subscribnewsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
    }

    public function down() {
        $this->dbforge->drop_table('subscribnewsletters');
    }

}

