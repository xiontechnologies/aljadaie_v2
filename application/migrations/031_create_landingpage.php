<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Landingpage extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `landingpages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) unsigned NOT NULL,
  `adminuser_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL DEFAULT '2014-01-21 06:40:34',
  `modified` datetime NOT NULL DEFAULT '2014-01-21 06:40:34',
  PRIMARY KEY (`id`),
  KEY `adminuser_id` (`adminuser_id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
        $this->dbforge->db->query("INSERT INTO `landingpages` (`id`, `module_id`, `adminuser_id`, `created`, `modified`) VALUES
(1, 1, 1, '2014-03-11 09:35:42', '2014-03-11 09:51:36');");
    }

    public function down() {
        $this->dbforge->drop_table('landingpages');
    }

}
