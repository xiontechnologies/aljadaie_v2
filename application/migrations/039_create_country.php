<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Country extends CI_Migration {

    public function up() {
        $sql = "
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CountryCode` char(5) NOT NULL,
  `CountryName` varchar(50) NOT NULL,
  `symbol` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;
";
        $this->dbforge->db->query($sql);

        $this->dbforge->db->query("
INSERT INTO `countries` (`id`, `CountryCode`, `CountryName`, `symbol`, `status`) VALUES
(1, 'EUR', 'Euro', 'â‚¬', '1'),
(2, 'USD', 'United States Dollars', '$', '1'),
(3, 'GBP', 'United Kingdom Pounds', 'Â£', '1'),
(4, 'CAD', 'Canada Dollars', '$', '1'),
(5, 'AUD', 'Australia Dollars', '$', '1'),
(6, 'JPY', 'Japan Yen', '', '0'),
(7, 'INR', 'India Rupees', '', '0'),
(8, 'NZD', 'New Zealand Dollars', '', '0'),
(9, 'CHF', 'Switzerland Francs', '', '0'),
(10, 'ZAR', 'South Africa Rand', '', '0'),
(11, 'DZD', 'Algeria Dinars', '', '0'),
(12, 'USD', 'America (United States) Dollars', '', '0'),
(13, 'ARS', 'Argentina Pesos', '', '0'),
(14, 'AUD', 'Australia Dollars', '', '0'),
(15, 'BHD', 'Bahrain Dinars', '', '0'),
(16, 'BRL', 'Brazil Reais', '', '0'),
(17, 'BGN', 'Bulgaria Leva', '', '0'),
(18, 'CAD', 'Canada Dollars', '', '0'),
(19, 'CLP', 'Chile Pesos', '', '0'),
(20, 'CNY', 'China Yuan Renminbi', '', '0'),
(21, 'CNY', 'RMB (China Yuan Renminbi)', '', '0'),
(22, 'COP', 'Colombia Pesos', '', '0'),
(23, 'CRC', 'Costa Rica Colones', '', '0'),
(24, 'HRK', 'Croatia Kuna', '', '0'),
(25, 'CZK', 'Czech Republic Koruny', '', '0'),
(26, 'DKK', 'Denmark Kroner', '', '0'),
(27, 'DOP', 'Dominican Republic Pesos', '', '0'),
(28, 'EGP', 'Egypt Pounds', '', '0'),
(29, 'EEK', 'Estonia Krooni', '', '0'),
(30, 'EUR', 'Euro', '', '0'),
(31, 'FJD', 'Fiji Dollars', '', '0'),
(32, 'HKD', 'Hong Kong Dollars', '', '0'),
(33, 'HUF', 'Hungary Forint', '', '0'),
(34, 'ISK', 'Iceland Kronur', '', '0'),
(35, 'INR', 'India Rupees', '?', '1'),
(36, 'IDR', 'Indonesia Rupiahs', '', '0'),
(37, 'ILS', 'Israel New Shekels', '', '0'),
(38, 'JMD', 'Jamaica Dollars', '', '0'),
(39, 'JPY', 'Japan Yen', '', '0'),
(40, 'JOD', 'Jordan Dinars', '', '0'),
(41, 'KES', 'Kenya Shillings', '', '0'),
(42, 'KRW', 'Korea (South) Won', '', '0'),
(43, 'KWD', 'Kuwait Dinars', '', '0'),
(44, 'LBP', 'Lebanon Pounds', '', '0'),
(45, 'MYR', 'Malaysia Ringgits', '', '0'),
(46, 'MUR', 'Mauritius Rupees', '', '0'),
(47, 'MXN', 'Mexico Pesos', '', '0'),
(48, 'MAD', 'Morocco Dirhams', '', '0'),
(49, 'NZD', 'New Zealand Dollars', '', '0'),
(50, 'NOK', 'Norway Kroner', '', '0'),
(51, 'OMR', 'Oman Rials', '', '0'),
(52, 'PKR', 'Pakistan Rupees', '', '0'),
(53, 'PEN', 'Peru Nuevos Soles', '', '0'),
(54, 'PHP', 'Philippines Pesos', '', '0'),
(55, 'PLN', 'Poland Zlotych', '', '0'),
(56, 'QAR', 'Qatar Riyals', '', '0'),
(57, 'RON', 'Romania New Lei', '', '0'),
(58, 'RUB', 'Russia Rubles', '', '0'),
(59, 'SAR', 'Saudi Arabia Riyals', 'SAR', '1'),
(60, 'SGD', 'Singapore Dollars', '', '0'),
(61, 'SKK', 'Slovakia Koruny', '', '0'),
(62, 'ZAR', 'South Africa Rand', '', '0'),
(63, 'KRW', 'South Korea Won', '', '0'),
(64, 'LKR', 'Sri Lanka Rupees', '', '0'),
(65, 'SEK', 'Sweden Kronor', '', '0'),
(66, 'CHF', 'Switzerland Francs', '', '0'),
(67, 'TWD', 'Taiwan New Dollars', '', '0'),
(68, 'THB', 'Thailand Baht', '', '0'),
(69, 'TTD', 'Trinidad and Tobago Dollars', '', '0'),
(70, 'TND', 'Tunisia Dinars', '', '0'),
(71, 'TRY', 'Turkey Lira', '', '0'),
(72, 'AED', 'United Arab Emirates Dirhams', '', '0'),
(73, 'GBP', 'United Kingdom Pounds', '', '0'),
(74, 'USD', 'United States Dollars', '', '0'),
(75, 'VEB', 'Venezuela Bolivares', '', '0'),
(76, 'VND', 'Vietnam Dong', '', '0'),
(77, 'ZMK', 'Zambia Kwacha', '', '0');
");
    }

    public function down() {
        $this->dbforge->drop_table('countries');
    }

}
