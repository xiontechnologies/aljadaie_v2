<?php

/*
 * @auther Shafiq
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Modules extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'status' => array(
                'type' => 'TINYINT',
                'constraint' => '2',
            ),
            'created_date' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
            'updated_date' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('modules');
        $sql = "
INSERT INTO `modules` (`id`, `name`, `description`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Myaccount', 'Manage Myaccount', 1, '2013-12-23 10:44:40', '2013-12-23 10:44:40'),
(2, 'Admin', 'Manage Admin', 1, '2013-12-23 10:44:40', '2013-12-23 10:44:40'),
(3, 'Dashboard', 'Manage Dahsbord', 0, '2013-12-23 10:44:40', '2013-12-23 10:44:40'),
(4, 'Admingroups', 'Admin Group Manage', 1, '2013-12-23 10:44:40', '2013-12-23 10:44:40'),
(5, 'Category', 'Manage Category', 1, '2013-12-23 10:44:40', '2013-12-23 10:44:40'),
(6, 'Attribute', 'Manage Attribute', 1, '2013-12-23 10:44:40', '2013-12-23 10:44:40'),
(7, 'Products', 'Manage Products', 1, '2013-12-23 10:44:40', '2013-12-23 10:44:40'),
(8, 'Variation', 'Manage Variation', 1, '2013-12-23 13:27:21', '2013-12-23 13:27:21'),
(9, 'Membergroups', 'Manage Member groups', 1, '2013-12-23 13:27:21', '2013-12-23 13:27:21'),
(10, 'Members', 'Manage Members', 1, '2013-12-23 13:27:21', '2013-12-23 13:27:21'),
(11, 'Contents', 'Manage contents', 1, '2014-03-05 08:33:40', '2014-03-05 08:33:40'),
(13, 'Stylecategories', 'Manage Style Category', '1', '2014-03-10 09:31:40.000000', '2014-03-10 09:31:40.000000'),
(12, 'Styleimages', 'Manage Style Category', '1', '2014-03-10 09:31:40.000000', '2014-03-10 09:31:40.000000'),
(14, 'Bulkupload', 'Manage Bulk Upload', '1', '2014-03-10 09:31:40.000000', '2014-03-10 09:31:40.000000'),
(15, 'Coupons', 'Manage Coupons', 1, '2014-03-05 13:18:35', '2014-03-05 13:18:35'),
(16, 'Orders', 'Manage Orders', 1, '2014-03-05 13:18:35', '2014-03-05 13:18:35'),
(17 , 'Discounts', 'Manage Discounts', 1, '2014-03-05 13:18:35', '2014-03-05 13:18:35'),
(18 , 'Newsletters', 'Manage Newsletters', 1, '2014-03-05 13:18:35', '2014-03-05 13:18:35'),
(19, 'Reports', 'Manage Reports', 1, '2014-03-12 11:23:54', '2014-03-12 11:23:54'),
(20, 'Sliders', 'Manage Sliders', 1, '2014-03-12 11:23:54', '2014-03-12 11:23:54'),
(21, 'Dbbackups', 'Manage Dbbackups', 1, '2014-03-12 11:23:54', '2014-03-12 11:23:54');";
        $this->dbforge->db->query($sql);
    }

    public function down() {
        $this->dbforge->drop_table('modules');
    }

}
