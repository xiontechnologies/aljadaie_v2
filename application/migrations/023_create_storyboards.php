<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Storyboards extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `storyboards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `stylename` varchar(100) NOT NULL,
  `canvasurl` varchar(250) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

        $this->dbforge->db->query("ALTER TABLE `storyboards`
  ADD CONSTRAINT `storyboards_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;");
        $this->dbforge->db->query("ALTER TABLE `storyboards` CHANGE `canvasurl` `canvasurl` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");
    }

    public function down() {
        $this->dbforge->drop_table('storyboards');
    }

}
