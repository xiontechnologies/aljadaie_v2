<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Photo extends CI_Migration {

    public function up() {
        $sql = "CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_title` varchar(300) NOT NULL,
  `file_name` varchar(300) NOT NULL,
  `full_path` varchar(300) NOT NULL,
  `file_path` varchar(300) NOT NULL,
  `file_type` varchar(200) NOT NULL,
  `raw_name` varchar(200) NOT NULL,
  `orig_name` varchar(200) NOT NULL,
  `client_name` varchar(200) NOT NULL,
  `file_ext` varchar(20) NOT NULL,
  `file_size` int(10) NOT NULL,
  `is_image` tinyint(2) NOT NULL,
  `image_width` int(10) NOT NULL,
  `image_height` int(10) NOT NULL,
  `image_type` varchar(20) NOT NULL,
  `image_size_str` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
        $this->dbforge->db->query($sql);
        $sql = "INSERT INTO `photos` ( `id` , `file_title` , `file_name` , `full_path` , `file_path` , `file_type` , `raw_name` , `orig_name` , `client_name` , `file_ext` , `file_size` , `is_image` , `image_width` , `image_height` , `image_type` , `image_size_str` )
VALUES ( 1, 'noimage', 'noimage.png', '/uploads/admin/noimage.png', '/uploads/admin/', 'image/png', 'noimage', 'noimage.png', 'noimage.png', '.png', 51, 1, '114', '114', 'png', 'width = 114 height = 114' ) ";
        $this->dbforge->db->query($sql);
    }

    public function down() {
        $this->dbforge->drop_table('photos');
    }

}
