<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Section extends CI_Migration {

    public function up() {
        $sql = "CREATE TABLE `sections` (
`id` INT( 11 ) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `name` VARCHAR( 250 ) NOT NULL,
 `created` DATETIME NOT NULL,
 `updated` DATETIME NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
        $this->dbforge->db->query($sql);
        $this->dbforge->db->query("ALTER TABLE `sections` ADD `status` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `name` ");
        $this->dbforge->db->query("
INSERT INTO `sections` (`id`, `name`, `created`, `updated`) VALUES
(1, 'SAMPLE', '2013-12-23 14:49:53', '2013-12-23 14:49:57');
");
//        (2, 'STOCK', '2013-12-23 14:50:05', '2013-12-23 14:50:08');
    }

    public function down() {
        $this->dbforge->drop_table('sections');
    }

}

