<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Userprofiles extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `userprofiles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `photo_id` int(11) unsigned NOT NULL,
  `dateofbirth` datetime NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `aboutme` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `country` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `telephone` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '2014-03-03 08:57:22',
  `modified` datetime NOT NULL DEFAULT '2014-03-03 08:57:22',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `photo_id` (`photo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

        $this->dbforge->db->query("ALTER TABLE `userprofiles` ADD CONSTRAINT `userprofiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;");
    }

    public function down() {
        $this->dbforge->drop_table('userprofiles');
    }

}

