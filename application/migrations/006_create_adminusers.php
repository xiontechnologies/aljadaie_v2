<?php

/*
 * @auther Shafiq
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Adminusers extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'admingroup_id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'contact_number' => array(
                'type' => 'varchar',
                'constraint' => '11',
                'default' => '0'
            ),
            'image_path' => array(
                'type' => 'varchar',
                'constraint' => '255',
                'default' => '0'
            ),
            'image_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'activated' => array(
                'type' => 'TINYINT',
                'constraint' => '2',
                'default' => '0'
            ),
            'banned' => array(
                'type' => 'TINYINT',
                'constraint' => '2',
                'default' => '0'
            ),
            'ban_reason' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'NULL' => TRUE
            ),
            'last_ip' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'last_login' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
            'no_of_login' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'created' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('admingroup_id');
        $this->dbforge->create_table('adminusers');
        $this->dbforge->db->query('ALTER TABLE adminusers ADD UNIQUE (email)');
        $this->dbforge->db->query('ALTER TABLE `adminusers` ADD FOREIGN KEY ( `admingroup_id` ) REFERENCES `admingroups` (`id`) ON DELETE CASCADE ');
        $password = '$P$BTW8sQC7H0UOsE47o7VxaIUzEUjxFu/';
        $sql = "INSERT INTO `adminusers` (`id`, `email`, `username`, `password`, `contact_number`, `image_path`, `image_name`, `admingroup_id`, `activated`, `banned`, `ban_reason`, `last_ip`, `no_of_login`) VALUES(1, 'shafiq@themedios.com', 'shafiq', '$password', '1234567890', '', '', 1, 1, 0, NULL, '127.0.0.1',  0);";
        $this->dbforge->db->query($sql);
    }

    public function down() {
        $this->dbforge->drop_table('adminusers');
    }

}
