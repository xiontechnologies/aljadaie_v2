<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Usercatalogue extends CI_Migration {

    public function up() {
//        $this->dbforge->drop_table('usercatalogues');
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `usercatalogues` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catalogue_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catalogue_id` (`catalogue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

        $this->dbforge->db->query("ALTER TABLE `usercatalogues`
  ADD CONSTRAINT `usercatalogues_ibfk_1` FOREIGN KEY (`catalogue_id`) REFERENCES `catalogues` (`id`) ON DELETE CASCADE;");
    }

    public function down() {
        $this->dbforge->drop_table('usercatalogues');
    }

}
