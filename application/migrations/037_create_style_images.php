<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Style_Images extends CI_Migration {

    public function up() {
        $sql = "CREATE TABLE IF NOT EXISTS `style_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `photo_id` int(11) unsigned NOT NULL,
  `design_name` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `photo_id` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        $this->dbforge->db->query($sql);
        
        $this->dbforge->db->query("ALTER TABLE `style_images`
  ADD CONSTRAINT `style_images_ibfk_2` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `style_images_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `style_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
    }

    public function down() {
        $this->dbforge->drop_table('style_images');
    }

}

