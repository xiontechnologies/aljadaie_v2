<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Socialnetworks extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `socialnetworks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `provider` varchar(25) NOT NULL,
  `uid` varchar(25) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `expires` int(15) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `handle` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '2014-03-03 08:57:22',
  `modified` datetime NOT NULL DEFAULT '2014-03-03 08:57:22',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`provider`),
  UNIQUE KEY `provider` (`provider`,`uid`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1  ;
");
        $this->dbforge->db->query("ALTER TABLE `socialnetworks`
  ADD CONSTRAINT `socialnetworks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;");
    }

    public function down() {
        $this->dbforge->drop_table('orderdetails');
    }

}

