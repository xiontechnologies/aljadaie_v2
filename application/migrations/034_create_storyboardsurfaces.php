<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Storyboardsurfaces extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `storyboardsurfaces` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `story_id` int(50) NOT NULL,
  `product_id` int(50) unsigned NOT NULL,
  `surface` int(50) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `story_id` (`story_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

        $this->dbforge->db->query("ALTER TABLE `storyboardsurfaces`
  ADD CONSTRAINT `storyboardsurfaces_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `storyboardsurfaces_ibfk_1` FOREIGN KEY (`story_id`) REFERENCES `storyboards` (`id`) ON DELETE CASCADE;");
    }

    public function down() {
        $this->dbforge->drop_table('storyboardsurfaces');
    }

}

