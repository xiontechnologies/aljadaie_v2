<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Member extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `activated` tinyint(2) NOT NULL DEFAULT '0',
  `banned` tinyint(2) NOT NULL DEFAULT '0',
  `ban_reason` varchar(100) NOT NULL,
  `new_password_key` varchar(100) NOT NULL,
  `new_password_requested` varchar(100) NOT NULL,
  `new_email` varchar(100) NOT NULL,
  `new_email_key` varchar(100) NOT NULL,
  `last_ip` varchar(100) NOT NULL,
  `profile_complete` tinyint(2) NOT NULL,
  `created` datetime NOT NULL DEFAULT '2014-02-15 06:04:25',
  `modified` datetime NOT NULL DEFAULT '2014-02-15 06:04:25',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;");

        $this->dbforge->db->query("ALTER TABLE `users` ADD `status` ENUM( '0', '1' ) NOT NULL COMMENT '0 = for no button, 1= for yes botton' AFTER `profile_complete` ");
    }

    public function down() {
        $this->dbforge->drop_table('users');
    }

}

