<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Style_Categories extends CI_Migration {

    public function up() {
        $sql = "CREATE TABLE IF NOT EXISTS `style_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
        $this->dbforge->db->query($sql);
    }

    public function down() {
        $this->dbforge->drop_table('style_categories');
    }

}

