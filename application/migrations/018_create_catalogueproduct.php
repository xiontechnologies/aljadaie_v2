<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Catalogueproduct extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("CREATE TABLE IF NOT EXISTS `catalogueproducts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `catalogue_id` int(11) unsigned NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`,`catalogue_id`),
  KEY `catalogue_id` (`catalogue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8");
        $this->dbforge->db->query("ALTER TABLE `catalogueproducts`
  ADD CONSTRAINT `catalogueproducts_ibfk_2` FOREIGN KEY (`catalogue_id`) REFERENCES `catalogues` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalogueproducts_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;");
    }

    public function down() {
        $this->dbforge->drop_table('catalogueproducts');
    }

}

