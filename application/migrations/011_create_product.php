<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Product extends CI_Migration {

    public function up() {
        $sql = "
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `title_eng` varchar(50) NOT NULL,
  `title_arabic` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `shortdescription` varchar(150) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `colorcode` varchar(50) NOT NULL,
  `collection` varchar(50) NOT NULL,
  `secondary_color` varchar(50) NOT NULL,
  `primary_color` varchar(50) NOT NULL,
  `product_design` varchar(50) NOT NULL,
  `item_definition` varchar(50) NOT NULL,
  `item_kind` varchar(50) NOT NULL,
  `section_id` int(11) unsigned NOT NULL,
  `price` float DEFAULT NULL,
  `discountprice` float DEFAULT NULL,
  `weight` varchar(20) NOT NULL,
  `stock` int(11) NOT NULL,
  `washcare` text NOT NULL,
  `tags` varchar(255) NOT NULL,
  `metakeywords` text NOT NULL,
  `metadescription` text NOT NULL,
  `oracle_id` int(11) NOT NULL,
  `oracleItemNumber` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 for deactive and 1 for active',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;";
        $this->dbforge->db->query($sql);
        $this->dbforge->db->query("ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE;");
    }

    public function down() {
        $this->dbforge->drop_table('products');
    }

}
