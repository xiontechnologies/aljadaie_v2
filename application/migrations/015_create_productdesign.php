<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Productdesign extends CI_Migration {

    public function up() {
        $sql = "CREATE TABLE IF NOT EXISTS `productdesigns` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `design_id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `design_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
        $this->dbforge->db->query($sql);
        $this->dbforge->db->query("ALTER TABLE `productdesigns` ADD FOREIGN KEY ( `product_id` ) REFERENCES `products` (`id`) ON DELETE CASCADE ;");
        $this->dbforge->db->query("ALTER TABLE `productdesigns` ADD FOREIGN KEY ( `design_id` ) REFERENCES `designs` (`id`) ON DELETE CASCADE ;");
    }

    public function down() {
        $this->dbforge->drop_table('productdesigns');
    }

}

