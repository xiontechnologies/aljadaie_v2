<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Category extends CI_Migration {

    public function up() {
        $sql = "CREATE TABLE `categories` (
`id` INT( 11 ) unsigned NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 255 ) NOT NULL ,
`status` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT '0 for deactive 1 for active',
`showinfilter` ENUM( '0', '1' ) NOT NULL DEFAULT '1' COMMENT '0 for no 1 for yes',
`created` DATETIME NOT NULL ,
`updated` DATETIME NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
";
        $this->dbforge->db->query($sql);

        $this->dbforge->db->query("
INSERT INTO `categories` (`id`, `name`, `status`, `showinfilter`, `created`, `updated`) VALUES
(1, 'Primary color', '1', '1', '2013-12-23 15:21:46', '2013-12-23 15:21:48'),
(2, 'Secondary color', '1', '1', '2013-12-23 15:22:14', '2013-12-23 15:22:16'),
(3, 'Design', '1', '1', '2013-12-23 15:22:14', '2013-12-23 15:22:16'),
(4, 'Item Definition', '1', '1', '2013-12-23 15:22:26', '2013-12-23 15:22:29'),
(5, 'Item Kind', '1', '1', '2013-12-23 15:22:55', '2013-12-23 15:22:57')");
    }

    public function down() {
        $this->dbforge->drop_table('categories');
    }

}
