<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Membergroup extends CI_Migration {

    public function up() {
        $this->dbforge->db->query("
CREATE TABLE IF NOT EXISTS `membergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(25) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

        $this->dbforge->db->query("ALTER TABLE `membergroups` ADD `status` ENUM( '0', '1' ) NOT NULL COMMENT '0 for deactive and 1 for active' AFTER `group` ");
    }

    public function down() {
        $this->dbforge->drop_table('membergroups');
    }

}

