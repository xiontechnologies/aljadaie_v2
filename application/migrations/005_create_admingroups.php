<?php

/*
 * @auther Shafiq
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Admingroups extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'status' => array(
                'type' => 'TINYINT',
                'constraint' => '2',
            ),
            'created' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'),
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('admingroups');
        $sql = "INSERT INTO `admingroups` (`id`, `name`, `status`) VALUES (1, 'Supar Admin', 1),(3, 'Admin', 1);";
        $this->dbforge->db->query($sql);
    }

    public function down() {
        $this->dbforge->drop_table('admingroups');
    }

}
