<?php

//@author name Shafiq
/*
 * Class My_front
 * Use for the front end extend
 * and checking for the valide login 
 * in contrustor
 * 
 */
class My_front extends MY_base {

    function __construct() {
        parent::__construct();
        $this->front_js_css();
        $this->_api_require_user();
        $this->data['folder_type'] = FRONTENDFOLDERNAME;
    }

    function front_js_css() {
        array_push($this->scripts['css'], 'frontend/css/basic.css', 'frontend/css/template.css', 'frontend/css/style.css', 'frontend/css/jquery_ui.css');
        array_push($this->scripts['js'], "frontend/js/e-smart-zoom-jquery.min.js", "frontend/js/jquery-ui.js", "frontend/js/zoom.js", "frontend/js/jquery.tooltipster.js");
        $this->session->unset_userdata(SITE_NAME . '_cart_product');
        array_push($this->scripts['js'], "frontend/js/visualizer.js");
    }

    function _api_require_user($params = array()) {
        if (empty($params['except']))
            $params['except'] = array();
        if (empty($params['only']))
            $params['only'] = array();
        if (count($params['except']) > 0 AND in_array($this->data['action'], $params['except']))
            return TRUE;
        if (count($params['only']) > 0 AND !in_array($this->data['action'], $params['only']))
            return TRUE;
        if ($this->session->userdata(SITE_NAME . '_user_data') && $this->session->userdata[SITE_NAME . '_user_data']['user_id'] > 0) {
            return TRUE; //user is logged in    
        }
        if ($this->data['action'] == 'checkout') {
            $this->session->set_userdata(SITE_NAME . '_cart_redirect', '1');
        }
        redirect('login');
    }

}
