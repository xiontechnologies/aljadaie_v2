<?php

/**
 * @author Nilesh
 * */
/* *
 * This function is basically convert a date format in user date format 
 * for example :- by default date fomat is Y-m-d (2013-07-21) user want 
 * d/m/Y (21/07/2013) so just pass the date and format to this fucntion 
 * it will return a date 
 * */
//function currencyCovert($amt, $from, $to) {
//    $url = 'http://www.webservicex.net/CurrencyConvertor.asmx/ConversionRate?FromCurrency=' . $from . '&ToCurrency=' . $to;
//    $xml = simpleXML_load_file($url, "SimpleXMLElement", LIBXML_NOCDATA);
//    if ($xml === FALSE) {
//        //deal with error
//    } else {
//        $rate = $xml;
//    }
//    print_r($xml[0]);
//    //    return $amt;
////    echo '<pre>';
////    print_r($amt);
////    exit;
//}

function convertCurrency($amount, $from, $to) {
    $url = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
    $data = file_get_contents($url);
    preg_match("/<span class=bld>(.*)<\/span>/", $data, $converted);
//    echo '<pre>';
//    print_r($converted);
//    exit;
    if ($converted) {
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
    } else {
        $converted = 0;
    }
    return round($converted, 3);
}

# Call function  
//echo convertCurrency(1, "USD", "INR");

function userdateformat($date, $format = "Y-m-d H:i:s") {
    $returndate = date($format, strtotime($date));
    return $returndate;
}

function dynamicinput($type, $data, $extra) {
    print_r($data);
    $type = 'input';
    $var = "form_" . $type($data);

    return $var;
}

/* *
 * This function is basically convert a date format in dataabase date form 
 * you can just pass a date it will convet in to "Y-m-d" 
 * ex. 2013-07-21
 * */

function databasedate($date = '') {
    if ($date)
        $returndate = date("Y-m-d H:i:s", strtotime($date));
    else
        $returndate = date("Y-m-d H:i:s");
    return $returndate;
}

/* *
 * This function is return a created and modified date array 
 * 
 * */

function cmdates() {
    $cmarray = array('created' => date("Y-m-d H:i:s"), 'modified' => date("Y-m-d H:i:s"));
    return $cmarray;
}

/* * 
 * This function use for name of title
 * */

function nameTitle() {
    return array('Mr.' => 'Mr.', 'Miss.' => 'Miss.', 'Mrs.' => 'Mrs.', 'Ms.' => 'Ms.', 'Dr.' => 'Dr.');
}

/* *
 * Status it return status active or deactive 
 * */

function status($value = '0') {
    if ($value == '1') {
        $status = "Active";
    } else {
        $status = "deactivate";
    }
    return $status;
}

/* *
 * common function for gender and intersted in 
 * */

function gender() {
    return $valueArr = array("male" => "Male", "female" => "Female", "other" => "Other");
    return $valueArr;
}

function interestedin() {
    $valueArr = array("men" => "Men", "women" => "Women", "Both" => "both");
    return $valueArr;
}

function typeofschool() {
    $valueArr = array("" => 'Select', "high" => "High School", "col" => "Collage", "grad" => "Graduate");
    return $valueArr;
}

/* *
 * This function is use  for creating excle file 
 * @ $column pass the header column name array to this variable 
 * example $column = array("id","prodcutName");
 * @ $colval is user for the data value of the column like bellow it's array withen array 
 * example : $colval=array(array("1", "pune"), array("3", "jamner")); 
 * @ $file name for this variable you can pass the file name whatever you want like 
 * eg .orderlist.csv
 * */

if (!function_exists('array_to_xls')) {

    function array_to_xls($column, $colval, $filename) {
        header('Content-type: text/csv');
        header('Content-disposition: attachment;filename=' . $filename);
        $headerCol = implode(",", $column);

        $data = '';
        echo $headerCol . PHP_EOL;
        foreach ($colval as $k => $v) {
            $data .=implode(", ", $v) . PHP_EOL;
        }
        echo $data;
        return true;
    }

}

function isInCart($array, $key, $value) {
    # check if an item is in the cart
    foreach ($array as $k => $v) {
        if ($v[$key] == $value) {
            return TRUE;
        }
    }
}

?>
