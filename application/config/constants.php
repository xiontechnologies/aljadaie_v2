<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */

define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

define('STATUS_ACTIVATED', '1');
define('STATUS_NOT_ACTIVATED', '0');
/*
  |
  |--------------------------------------------------------------------------
  | Bit bashing user modules right
  |--------------------------------------------------------------------------
  |
 */

define('READ_MODULE', 1 << 0);
define('ADD_MODULE', 1 << 1);
define('EDIT_MODULE', 1 << 2);
define('DELETE_MODULE', 1 << 3);
define('SEND_EMAIL_MODULE', 1 << 4);
define('COPY_PRODUCT_MODULE', 1 << 5);
define('DOWNLOAD_MODULE', 1 << 6);

/*
  |--------------------------------------------------------------------------
  | site_name
  |--------------------------------------------------------------------------
  |
 */
define('SITE_NAME', 'Aljedaie');

define("BULKUPLOAD", "bulkupload");

define('ADMIN_UPLOAD_IMAGE_PATH', "./uploads/admin/profileimages");
define('GROUP_UPLOAD_IMAGE_PATH', "./uploads/admin/groupimages");
define('ADMIN_MAX_UPLOAD_SIZE', "2048");
define("BULKUPLOAD_PATH", "./uploads/bulkupload");
define('ADMIN_STYLE_IMAGE_PATH', "./uploads/admin/styleimages");
define("FRONTEND_USER_PROFILE_IMAGE", "./uploads/frontend/profileimages");
define("FRONTEND_USER_PROFILE_IMAGE_ICON", "./uploads/frontend/profileimages/icon");
define("PRODUCTIMAGE_PATH", "./uploads/productimage");
define("PRODUCTDESIGN_PATH", "./uploads/productdesign");
define("SLIDER_PATH", "./uploads/slider");
define("PROFILEIMAGE", "./uploads/profileimage");
define("Max_size_uploadify_in_KB_image_controller", "100000");
/*
  |--------------------------------------------------------------------------
  | Default city state and country
  |--------------------------------------------------------------------------
  |
 */
define('DEFULAT_CITY', 'canada');
/*
  |--------------------------------------------------------------------------
  | Folder names
  |--------------------------------------------------------------------------
  |
 */
define('FRONTENDFOLDERNAME', 'frontend');
/*
  |--------------------------------------------------------------------------
  | Pagination
  |--------------------------------------------------------------------------
  |
 */
define('OFFSET', 0);
define('LIMIT', 15);

/* End of file constants.php */
/* Location: ./application/config/constants.php */
define('MYACCOUNT', 1);
define('MODULES_NOT_SHOW', 'Orders');
define('ADMIN_MAIL', 'varsha@consistenza.com');
/*
 * Defualt currency
 */
define('DEFUELT_CURRENCY', 'SAR');