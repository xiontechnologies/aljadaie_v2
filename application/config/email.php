<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Email
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|
*/
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['protocol'] = 'smtp';
$config['_smtp_auth'] = TRUE;
$config['smtp_host'] = 'ssl://smtp.gmail.com';
$config['smtp_port'] = '465';
$config['smtp_user'] = '';
$config['smtp_pass'] = '';
$config['send_multipart'] = TRUE;
//$config['validate'] = 'false';
$config['wordwrap'] = TRUE;
$config['crlf'] = "\r\n";
//$config['smtp_timeout'] = '100';
$config['newline'] = "\r\n";
/* End of file email.php */
/* Location: ./application/config/email.php */