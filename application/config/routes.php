<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

//$route['default_controller'] = "frontend/home";
$route['default_controller'] = "frontend/auth/signup";
$route['404_override'] = '';
/* * **************admin side adminuser Routing ********************** */

$route['admin'] = 'admin/auth/login';
$route['dashboard'] = 'admin/dashboard';
$route['admin/admin/list'] = 'admin/admin/index';
$route['admin/add'] = 'admin/admin/add';
$route['admin/edit/(:num)'] = 'admin/admin/edit/$1';
$route['admin/active/(:num)'] = 'admin/admin/activate/$1';
$route['admin/deactive/(:num)'] = 'admin/admin/deactivate/$1';
/**
 * Admin Group routing
 * */
$route['admin/admingroups/list'] = 'admin/admingroups';
$route['admin/groups/list'] = 'admin/admingroups';
$route['admin/groups'] = 'admin/admingroups';
$route['admin/groups/add'] = 'admin/admingroups/add';
$route['admin/groups/edit/(:num)'] = 'admin/admingroups/edit/$1';
$route['admin/groups/active/(:num)'] = 'admin/admingroups/activate/$1';
$route['admin/groups/deactive/(:num)'] = 'admin/admingroups/deactivate/$1';
$route['admin/groups/delete/(:num)'] = 'admin/admingroups/delete/$1';

/**
 * Admin Category routing
 * */
$route['admin/category/list'] = 'admin/category';
$route['admin/categories'] = 'admin/category';
$route['admin/categories/add'] = 'admin/category/add';
$route['admin/categories/edit/(:num)'] = 'admin/category/edit/$1';
$route['admin/categories/active/(:num)'] = 'admin/category/activate/$1';
$route['admin/categories/deactive/(:num)'] = 'admin/category/deactivate/$1';
$route['admin/categories/delete/(:num)'] = 'admin/category/delete/$1';

/**
 * Admin Attribute routing
 * */
$route['admin/attribute/list'] = 'admin/attribute';
$route['admin/attributes'] = 'admin/attribute';
$route['admin/attributes/add'] = 'admin/attribute/add';
$route['admin/attributes/edit/(:num)'] = 'admin/attribute/edit/$1';
$route['admin/attributes/active/(:num)'] = 'admin/attribute/activate/$1';
$route['admin/attributes/deactive/(:num)'] = 'admin/attribute/deactivate/$1';
$route['admin/attributes/delete/(:num)'] = 'admin/attribute/delete/$1';

/**
 * Admin Varitaion routing
 * */
$route['admin/variation/list'] = 'admin/variation';
$route['admin/variations'] = 'admin/variation';
$route['admin/variations/add'] = 'admin/variation/add';
$route['admin/variations/edit/(:num)'] = 'admin/variation/edit/$1';
$route['admin/variations/active/(:num)'] = 'admin/variation/activate/$1';
$route['admin/variations/deactive/(:num)'] = 'admin/variation/deactivate/$1';
$route['admin/variations/delete/(:num)'] = 'admin/variation/delete/$1';

/*
 * Admin Prducts routing
 */
$route['admin/product'] = 'admin/products';
$route['admin/product/add'] = 'admin/products/add';
$route['admin/product/edit/(:num)'] = 'admin/products/edit/$1';
$route['admin/product/active/(:num)'] = 'admin/products/activate/$1';
$route['admin/product/deactive/(:num)'] = 'admin/products/deactivate/$1';
$route['admin/product/delete/(:num)'] = 'admin/products/delete/$1';

$route['admin/product/variation'] = 'admin/ajax/variation/';
$route['admin/product/image'] = 'admin/ajax/uploadimage/';
$route['admin/product/design'] = 'admin/ajax/uploaddesign/';
$route['admin/product/details/(:num)'] = 'admin/ajax/productdetails/$1';

$route['admin/bulkupload/list'] = 'admin/bulkupload/index';
$route['admin/bulkupload/export'] = 'admin/bulkupload/edit';
$route['admin/bulkupload/import'] = 'admin/bulkupload/add';


/**
 * Admin Category routing
 * */
$route['admin/newsletters/list'] = 'admin/newsletters';
$route['admin/newsletters/add'] = 'admin/newsletters/add';
$route['admin/newsletters/edit/(:num)'] = 'admin/newsletters/edit/$1';
$route['admin/newsletters/delete/(:num)'] = 'admin/newsletters/delete/$1';
$route['admin/newsletters/send_mail/(:num)'] = 'admin/newsletters/send_mail/$1';

$route['admin/reports/list'] = 'admin/reports/index';
$route['admin/dbbackups/list'] = 'admin/dbbackups/index';




/*
 * Frontend routing
 */
$route['auth/login'] = "frontend/auth";
$route['home'] = 'frontend/home';
$route['home/advsearch'] = "frontend/home/advsearch";
$route['home/visualizer/(:num)'] = "frontend/home/visualizer/$1";
$route['catalogue/createpdf'] = "frontend/home/downloadpdf";
$route['storyboard/createpdf'] = "frontend/home/downloadstoryboardpdf";
$route['catalogue/emailpdf'] = "frontend/home/emailpdf";
$route['catalogue/emailpdf/(:num)'] = "frontend/home/emailpdfid/$1";
$route['product/ajaxdetails/(:num)'] = "frontend/products/index/$1";
$route['product/ajaxcatalog/(:num)'] = "frontend/products/addtocatalog/$1";
$route['product/ajaxcatalogdelete/(:num)'] = "frontend/products/removefromcatalog/$1";
$route['product/ajaxcatalogsave'] = "frontend/products/catalogsave";
$route['product/ajaxnewstoryboard'] = "frontend/products/newstoryboard";
$route['product/storyboardsave'] = "frontend/products/storyboardsave";
$route['product/ajaxcatalognew'] = "frontend/products/catalognew";
$route['product/ajaxview'] = "frontend/products/ajaxview";
$route['product/ajaxfavorites'] = "frontend/products/ajaxfavorites";
$route['products/getdetails'] = "frontend/products/imagedetails";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
$route['admin/membergroups/list'] = "admin/membergroups/index";
$route['admin/members/list'] = 'admin/members/index';

/*
 * Style Image Routing
 */
$route['admin/styleimages/list'] = 'admin/styleimages';
$route['admin/styleimage/'] = 'admin/styleimages';
$route['admin/styleimage/add'] = 'admin/styleimages/add';
$route['admin/styleimage/edit/(:num)'] = 'admin/styleimages/edit/$1';
$route['admin/styleimage/activate/(:num)'] = 'admin/styleimages/activate/$1';
$route['admin/styleimage/deactivate/(:num)'] = 'admin/styleimages/deactivate/$1';
$route['admin/styleimage/delete/(:num)'] = 'admin/styleimages/delete/$1';

$route['admin/contents/list'] = 'admin/contents/index';
/*
 * Style Category Routing
 */
$route['admin/stylecategories/list'] = 'admin/stylecategories';
$route['admin/stylecategory'] = 'admin/stylecategories';
$route['admin/stylecategory/add'] = 'admin/stylecategories/add';
$route['admin/stylecategory/edit/(:num)'] = 'admin/stylecategories/edit/$1';
$route['admin/stylecategory/activate/(:num)'] = 'admin/stylecategories/activate/$1';
$route['admin/stylecategory/deactivate/(:num)'] = 'admin/stylecategories/deactivate/$1';
$route['admin/stylecategory/delete/(:num)'] = 'admin/stylecategories/delete/$1';

$route['admin/products/list'] = 'admin/products';

$route['admin/coupons/list'] = 'admin/coupons/index';
$route['admin/orders/list'] = 'admin/orders/index';
$route['admin/discounts/list'] = 'admin/discounts';
$route['admin/sliders/list'] = 'admin/sliders/index';
$route['admin/dbbackups/list'] = 'admin/dbbackups/index';


/*
 * frontend login
 */

$route['login'] = 'frontend/auth/signup';
$route['signup'] = 'cataloguelist';
$route['register'] = 'frontend/auth/register';
$route['forgotpassword'] = 'frontend/auth/forgotpassword';
$route['changepassword'] = 'frontend/myaccount/changepassword';
$route['login/(:any)'] = "frontend/auth/oauth2/$1";
$route['activate/(:num)/(:any)'] = "frontend/auth/activate/$2";
$route['myaccount'] = "frontend/myaccount/index";
$route['cataloguelist'] = "frontend/myaccount/cataloguelist";
$route['favoriteslist'] = "frontend/myaccount/favoriteslist";
$route['cataloguedelete/(:num)'] = "frontend/myaccount/delete_catalogue/$1";
$route['storyboarddelete/(:num)'] = "frontend/myaccount/delete_storyboard/$1";
$route['favdelete/(:num)'] = "frontend/myaccount/favdelete/$1";
$route['cataloguedetails/(:num)'] = "frontend/myaccount/viewlist/$1";
$route['storyboarddetails/(:num)'] = "frontend/myaccount/editstoryboard/$1";
$route['cataloguedeleteproduct/(:num)'] = "frontend/myaccount/delete_product_list/$1";
$route['cataloguelist'] = "frontend/myaccount/cataloguelist";
$route['storyboardlist'] = "frontend/myaccount/storyboardlist";
$route['cataloguepdf/(:num)'] = "frontend/myaccount/cataloguepdf/$1";
$route['storyboardpdf/(:num)'] = "frontend/myaccount/storyboardpdf/$1";
$route['catalogueemailpdf/(:num)'] = "frontend/myaccount/catalogueemailpdf/$1";

$route['addtocart/(:num)'] = 'frontend/mycart/addtocart/$1';
$route['cartlist'] = 'frontend/mycart/index';
$route['cart/delete/(:any)'] = 'frontend/mycart/delete/$1';
$route['cart/update'] = 'frontend/mycart/update';
$route['checkout'] = 'frontend/mycart/checkout';
$route['cart/address'] = 'frontend/mycart/addaddress';
$route['cart/confirm'] = 'frontend/mycart/confirm';
$route['cart/order'] = 'frontend/mycart/confirmorder';

$route['coupon'] = 'frontend/mycart/coupan_apply';
$route['home/select_style_images/(:num)'] = "frontend/home/select_style_images/$1";
$route['home/select_style_images_list/(:num)'] = "frontend/home/select_style_images_list/$1";
$route['home/orders'] = 'frontend/orders/index';
$route['home/thank'] = 'frontend/orders/thank';
$route['home/coupon'] = 'frontend/myaccount/couponlisting';
$route['productlisting/(:num)'] = 'frontend/orders/productlisting/$1';
$route['favorities'] = 'frontend/products/favorities';
$route['newsletter'] = 'frontend/newsletters/add_newsletter';

$route['storyboard'] = "frontend/storyboards/index";
$route['storyboard/delete'] = "frontend/storyboards/delete";
$route['product/searchBySku'] = "frontend/products/searchBySku";
$route['product/storeval'] = "auth/storeval";
